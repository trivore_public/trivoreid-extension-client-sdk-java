## TrivoreID Extended Client SDK

The extended version of the Client SDK that wraps around the TrivoreID APIs.

Documentation to get started and use Services can be found [here](https://trivore.atlassian.net/wiki/spaces/TISpubdoc/pages/20515293/Client+SDK+for+Java).

### Maven Dependency

History of all releases can be found [here](https://mvnrepository.com/artifact/com.trivore/trivoreid-sdk).

```
<dependency>
    <groupId>com.trivore</groupId>
    <artifactId>trivoreid-sdk-extended</artifactId>
    <version>1.0.0</version>
</dependency>
```