package com.trivore.id.sdk.exceptions;

/**
 * A Trivore ID forbidden exception.
 * <p>
 * This exception is typically thrown on errors that occur when the request was
 * forbidden.
 * </p>
 */
@SuppressWarnings("serial")
public class ForbiddenException extends TrivoreIDException {

	/**
	 * Construct a new exception with the given messages, error and status code.
	 *
	 * @param message    The message to include.
	 * @param statusCode HTTP response code
	 */
	public ForbiddenException(String message, int statusCode) {
		super(message, statusCode);
	}
}
