package com.trivore.id.sdk.exceptions;

/**
 * A Trivore ID Mobile Quota Reached exception.
 * <p>
 * This exception is typically thrown on errors that occur when the quota for
 * the movile verification has reached.
 * </p>
 */
@SuppressWarnings("serial")
public class MobileQuotaReachedException extends TrivoreIDException {

	/**
	 * Construct a new exception with the given messages, error and status code.
	 *
	 * @param message    The message to include.
	 * @param statusCode HTTP response code
	 */
	public MobileQuotaReachedException(String message, int statusCode) {
		super(message, statusCode);
	}

}
