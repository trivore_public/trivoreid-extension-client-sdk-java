package com.trivore.id.sdk.exceptions;

/**
 * A Trivore ID error exception.
 * <p>
 * This exception is typically thrown on client errors. See errorCode and
 * errorMessage for details.
 * </p>
 */
@SuppressWarnings("serial")
public class TrivoreIDErrorException extends TrivoreIDException {

	/**
	 * Construct a new exception with the given messages, error and status code.
	 *
	 * @param message      The message to include.
	 * @param statusCode   HTTP response code
	 */
	public TrivoreIDErrorException(String message, int statusCode) {
		super(message, statusCode);
	}
}
