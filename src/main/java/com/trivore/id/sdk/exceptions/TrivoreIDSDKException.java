package com.trivore.id.sdk.exceptions;

/**
 * A Trivore ID specific exception.
 * <p>
 * These exceptions are typically thrown on errors that occur when user wrongly
 * uses Trivore ID SDK.
 * </p>
 */
@SuppressWarnings("serial")
public class TrivoreIDSDKException extends Exception {

	/**
	 * Construct a new exception without message or cause.
	 */
	public TrivoreIDSDKException() {
		// ...
	}

	/**
	 * Construct a new exception with the given message.
	 *
	 * @param message The message to include.
	 */
	public TrivoreIDSDKException(String message) {
		super(message);
	}

	/**
	 * Construct a new exception with the given cause.
	 *
	 * @param cause The source cause of the exception.
	 */
	public TrivoreIDSDKException(Throwable cause) {
		super(cause);
	}

}
