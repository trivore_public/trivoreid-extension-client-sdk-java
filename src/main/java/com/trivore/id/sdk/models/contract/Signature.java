package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents signature in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Signature implements Serializable {

	private String timestamp;

	/**
	 * Construct a new signature object.
	 */
	public Signature() {
		// ...
	}

	/**
	 * Get timestamp.
	 *
	 * @return timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Set timestamp.
	 *
	 * @param timestamp timestamp
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		if (getTimestamp() == null) {
			return super.hashCode();
		}
		return Objects.hash(timestamp);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Signature)) {
			return false;
		}
		Signature o = (Signature) obj;
		if (o.getTimestamp() == null && getTimestamp() == null) {
			return super.equals(obj);
		}
		return Objects.equals(timestamp, o.timestamp);
	}

	@Override
	public String toString() {
		return "Signature [timestamp=" + timestamp + "]";
	}

}
