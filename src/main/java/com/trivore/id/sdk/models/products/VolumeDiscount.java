package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single pricing in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID pricing definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeDiscount implements Serializable {

	private String title;
	private Float discountPercentage;
	private Float discountAmount;
	private Float discountAmountPerItem;
	private List<String> limitedToApiClients;
	private List<String> limitedToUsers;
	private List<String> limitedToUserGroups;
	private Integer rangeStart;
	private Integer rangeEnd;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the discountPercentage
	 */
	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	/**
	 * @param discountPercentage the discountPercentage to set
	 */
	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	/**
	 * @return the discountAmount
	 */
	public Float getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(Float discountAmount) {
		this.discountAmount = discountAmount;
	}

	/**
	 * @return the discountAmountPerItem
	 */
	public Float getDiscountAmountPerItem() {
		return discountAmountPerItem;
	}

	/**
	 * @param discountAmountPerItem the discountAmountPerItem to set
	 */
	public void setDiscountAmountPerItem(Float discountAmountPerItem) {
		this.discountAmountPerItem = discountAmountPerItem;
	}

	/**
	 * @return the limitedToApiClients
	 */
	public List<String> getLimitedToApiClients() {
		if (limitedToApiClients == null) {
			limitedToApiClients = new ArrayList<>();
		}
		return limitedToApiClients;
	}

	/**
	 * @param limitedToApiClients the limitedToApiClients to set
	 */
	public void setLimitedToApiClients(List<String> limitedToApiClients) {
		this.limitedToApiClients = limitedToApiClients;
	}

	/**
	 * @return the limitedToUsers
	 */
	public List<String> getLimitedToUsers() {
		if (limitedToUsers == null) {
			limitedToUsers = new ArrayList<>();
		}
		return limitedToUsers;
	}

	/**
	 * @param limitedToUsers the limitedToUsers to set
	 */
	public void setLimitedToUsers(List<String> limitedToUsers) {
		this.limitedToUsers = limitedToUsers;
	}

	/**
	 * @return the limitedToUserGroups
	 */
	public List<String> getLimitedToUserGroups() {
		if (limitedToUserGroups == null) {
			limitedToUserGroups = new ArrayList<>();
		}
		return limitedToUserGroups;
	}

	/**
	 * @param limitedToUserGroups the limitedToUserGroups to set
	 */
	public void setLimitedToUserGroups(List<String> limitedToUserGroups) {
		this.limitedToUserGroups = limitedToUserGroups;
	}

	/**
	 * @return the rangeStart
	 */
	public Integer getRangeStart() {
		return rangeStart;
	}

	/**
	 * @param rangeStart the rangeStart to set
	 */
	public void setRangeStart(Integer rangeStart) {
		this.rangeStart = rangeStart;
	}

	/**
	 * @return the rangeEnd
	 */
	public Integer getRangeEnd() {
		return rangeEnd;
	}

	/**
	 * @param rangeEnd the rangeEnd to set
	 */
	public void setRangeEnd(Integer rangeEnd) {
		this.rangeEnd = rangeEnd;
	}

	@Override
	public int hashCode() {
		return Objects.hash(rangeStart, rangeEnd, limitedToUserGroups, limitedToUsers, limitedToApiClients,
				discountAmountPerItem, discountAmount, discountPercentage, title);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof VolumeDiscount)) {
			return false;
		}
		VolumeDiscount o = (VolumeDiscount) obj;
		return Objects.equals(title, o.title) //
				&& Objects.equals(discountPercentage, o.discountPercentage) //
				&& Objects.equals(discountAmount, o.discountAmount) //
				&& Objects.equals(discountAmountPerItem, o.discountAmountPerItem) //
				&& Objects.equals(limitedToApiClients, o.limitedToApiClients) //
				&& Objects.equals(limitedToUsers, o.limitedToUsers)//
				&& Objects.equals(limitedToUserGroups, o.limitedToUserGroups)//
				&& Objects.equals(rangeEnd, o.rangeEnd) //
				&& Objects.equals(rangeStart, o.rangeStart);
	}

	@Override
	public String toString() {
		return "VolumeDiscount [title=" + title + ", discountPercentage=" + discountPercentage + ", discountAmount="
				+ discountAmount + ", discountAmountPerItem=" + discountAmountPerItem + ", limitedToApiClients="
				+ limitedToApiClients + ", limitedToUsers=" + limitedToUsers + ", limitedToUserGroups="
				+ limitedToUserGroups + ", rangeStart=" + rangeStart + ", rangeEnd=" + rangeEnd + "]";
	}

}
