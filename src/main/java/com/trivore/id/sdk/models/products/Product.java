package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single product in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID product definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {

	private String id;
	private String ownerId;
	private List<String> accessControlIds;
	private String sku;
	private List<LocalisedDescription> translations;
	private Map<String, Object> customFields;
	private List<Validity> sellable;
	private List<Validity> usable;
	private List<String> pricingPlans;
	private String provider;
	private int validityLength;
	private ValidityStartFrom validityStartFrom;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @return the accessControlIds
	 */
	public List<String> getAccessControlIds() {
		if (accessControlIds == null) {
			accessControlIds = new ArrayList<>();
		}
		return accessControlIds;
	}

	/**
	 * @param accessControlIds the accessControlIds to set
	 */
	public void setAccessControlIds(List<String> accessControlIds) {
		this.accessControlIds = accessControlIds;
	}

	/**
	 * @return the sku
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * @param sku the sku to set
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	/**
	 * @return the product names and descriptions
	 */
	public List<LocalisedDescription> getTranslations() {
		if (translations == null) {
			translations = new ArrayList<>();
		}
		return translations;
	}

	/**
	 * @param translations product names and descriptions
	 */
	public void setTranslations(List<LocalisedDescription> translations) {
		this.translations = translations;
	}

	/**
	 * @return the customFields
	 */
	public Map<String, Object> getCustomFields() {
		if (customFields == null) {
			customFields = new HashMap<>();
		}
		return customFields;
	}

	/**
	 * @param customFields the customFields to set
	 */
	public void setCustomFields(Map<String, Object> customFields) {
		this.customFields = customFields;
	}

	/**
	 * @return the sellable
	 */
	public List<Validity> getSellable() {
		if (sellable == null) {
			sellable = new ArrayList<>();
		}
		return sellable;
	}

	/**
	 * @param sellable the sellable to set
	 */
	public void setSellable(List<Validity> sellable) {
		this.sellable = sellable;
	}

	/**
	 * @return the usable
	 */
	public List<Validity> getUsable() {
		if (usable == null) {
			usable = new ArrayList<>();
		}
		return usable;
	}

	/**
	 * @param usable the usable to set
	 */
	public void setUsable(List<Validity> usable) {
		this.usable = usable;
	}

	/**
	 * @return the pricingPlans
	 */
	public List<String> getPricingPlans() {
		if (pricingPlans == null) {
			pricingPlans = new ArrayList<>();
		}
		return pricingPlans;
	}

	/**
	 * @param pricingPlans the pricingPlans to set
	 */
	public void setPricingPlans(List<String> pricingPlans) {
		this.pricingPlans = pricingPlans;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * @return the validityLength
	 */
	public int getValidityLength() {
		return validityLength;
	}

	/**
	 * @param validityLength the validityLength to set
	 */
	public void setValidityLength(int validityLength) {
		this.validityLength = validityLength;
	}

	/**
	 * @return the validityStartFrom
	 */
	public ValidityStartFrom getValidityStartFrom() {
		return validityStartFrom;
	}

	/**
	 * @param validityStartFrom the validityStartFrom to set
	 */
	public void setValidityStartFrom(ValidityStartFrom validityStartFrom) {
		this.validityStartFrom = validityStartFrom;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Product)) {
			return false;
		}
		Product o = (Product) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", ownerId=" + ownerId + ", accessControlIds=" + accessControlIds + ", sku=" + sku
				+ ", translations=" + translations + ", customFields=" + customFields + ", sellable=" + sellable
				+ ", usable=" + usable + ", pricingPlans=" + pricingPlans + ", provider=" + provider
				+ ", validityLength=" + validityLength + ", validityStartFrom=" + validityStartFrom + "]";
	}

}
