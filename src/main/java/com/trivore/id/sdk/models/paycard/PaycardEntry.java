package com.trivore.id.sdk.models.paycard;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a paycard entry.
 * <p>
 * Used as a paycard data and capabilities entry in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaycardEntry implements Serializable {

	private String key;
	private String value;

	/**
	 * Construct paycard entry.
	 */
	public PaycardEntry() {
		// ...
	}

	/**
	 * Get the paycard entry key.
	 *
	 * @return paycard entry key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Set the paycard entry key.
	 *
	 * @param key paycard entry key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Get the paycard entry value.
	 *
	 * @return paycard entry value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the paycard entry value.
	 *
	 * @param value paycard entry value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(key, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PaycardEntry)) {
			return false;
		}
		PaycardEntry o = (PaycardEntry) obj;
		return Objects.equals(key, o.key) && Objects.equals(value, o.value);
	}

	@Override
	public String toString() {
		return "PaycardEntry [key=" + key + ", value=" + value + "]";
	}

}
