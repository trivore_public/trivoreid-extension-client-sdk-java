package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single authorisation subject in Trivore ID.
 * <p>
 * Used to specify the authorisation subject Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorisationSubject implements Serializable {

	private AuthSubjectType type;
	private String value;

	/**
	 * Construct a new authorisation subject.
	 */
	public AuthorisationSubject() {
		// ...
	}

	/**
	 * Get the type of the authorisation subject.
	 *
	 * @return the type of the authorisation subject
	 */
	public AuthSubjectType getType() {
		return type;
	}

	/**
	 * Set the type of the authorisation subject.
	 *
	 * @param type the type of the authorisation subject
	 */
	public void setType(AuthSubjectType type) {
		this.type = type;
	}

	/**
	 * Get the subject value.
	 * <p>
	 * Subject value. Must be either User or Group ID or if type is 'String', then
	 * may be any value. The entity, whose ID is specifed, must belong to the same
	 * namespace as the authorisation.
	 * </p>
	 *
	 * @return the subject value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the subject value.
	 * <p>
	 * Subject value. Must be either User or Group ID or if type is 'String', then
	 * may be any value. The entity, whose ID is specifed, must belong to the same
	 * namespace as the authorisation.
	 * </p>
	 *
	 * @param value the subject value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthorisationSubject)) {
			return false;
		}
		AuthorisationSubject o = (AuthorisationSubject) obj;
		return Objects.equals(type, o.type) && Objects.equals(value, o.value);
	}

	@Override
	public String toString() {
		return "AuthorisationSubject [type=" + type + ", value=" + value + "]";
	}

}
