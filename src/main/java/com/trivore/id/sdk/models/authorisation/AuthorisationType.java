package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.Meta;

/**
 * An object which presents a single authorisation type in Trivore ID.
 * <p>
 * Used to specify the authorisation type in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorisationType implements Serializable {

	private String id;
	private String nsCode;
	private String code;
	private List<LocalisedName> names;
	private String description;
	private Meta metadata;

	/**
	 * Construct a new grant right object.
	 */
	public AuthorisationType() {
		// ...
	}

	/**
	 * Get the unique identifier.
	 *
	 * @return the unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the unique identifier.
	 *
	 * @param id the unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the namespace code.
	 *
	 * @return the namespace code
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set the namespace code.
	 *
	 * @param nsCode the namespace code
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * Get authorisation type code.
	 *
	 * @return authorisation type code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Set authorisation type code.
	 *
	 * @param code authorisation type code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get authorization type locale specific names.
	 *
	 * @return authorization type locale specific names
	 */
	public List<LocalisedName> getNames() {
		if (names == null) {
			names = new ArrayList<>();
		}
		return names;
	}

	/**
	 * Set authorization type locale specific names.
	 *
	 * @param names idauthorization type locale specific names
	 */
	public void setNames(List<LocalisedName> names) {
		this.names = names;
	}

	/**
	 * Get the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return metadata;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.metadata = meta;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthorisationType)) {
			return false;
		}
		AuthorisationType o = (AuthorisationType) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "AuthorisationType [id=" + id + ", nsCode=" + nsCode + ", code=" + code + ", names=" + names
				+ ", description=" + description + "]";
	}

}
