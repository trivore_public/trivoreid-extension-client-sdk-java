package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single catalog item in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID catalog item definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CatalogItem implements Serializable {

	private String productId;
	private String productSku;
	private boolean productValuesInherited;
	private Map<String, Map<String, Object>> descriptions;
	private Map<String, Object> customFields;
	private List<Validity> sellable;
	private List<Validity> usable;
	private List<String> pricingPlans;
	private int validityLength;
	private ValidityStartFrom validityStartFrom;

	/**
	 * @return product ID
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId product ID
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return Product SKU
	 */
	public String getProductSku() {
		return productSku;
	}

	/**
	 * @param productSku Product SKU.
	 */
	public void setProductSku(String productSku) {
		this.productSku = productSku;
	}

	/**
	 * True: Empty or null fields values are replaced with values from original
	 * product.
	 *
	 * @return Empty or null fields values are replaced with values from original
	 *         product.
	 */
	public boolean isProductValuesInherited() {
		return productValuesInherited;
	}

	/**
	 * @param productValuesInherited Empty or null fields values are replaced with
	 *                               values from original product.
	 */
	public void setProductValuesInherited(boolean productValuesInherited) {
		this.productValuesInherited = productValuesInherited;
	}

	/**
	 * @return localised product names and descriptions
	 */
	public Map<String, Map<String, Object>> getDescriptions() {
		if (descriptions == null) {
			descriptions = new HashMap<>();
		}
		return descriptions;
	}

	/**
	 * @param descriptions localised product names and descriptions
	 */
	public void setDescriptions(Map<String, Map<String, Object>> descriptions) {
		this.descriptions = descriptions;
	}

	/**
	 * @return custom fields
	 */
	public Map<String, Object> getCustomFields() {
		if (customFields == null) {
			customFields = new HashMap<>();
		}
		return customFields;
	}

	/**
	 * @param customFields custom fields
	 */
	public void setCustomFields(Map<String, Object> customFields) {
		this.customFields = customFields;
	}

	/**
	 * @return times when sellable. If null or empty, base product sellable value is
	 *         used.
	 */
	public List<Validity> getSellable() {
		if (sellable == null) {
			sellable = new ArrayList<>();
		}
		return sellable;
	}

	/**
	 * @param sellable times when sellable. If null or empty, base product sellable
	 *                 value is used.
	 */
	public void setSellable(List<Validity> sellable) {
		this.sellable = sellable;
	}

	/**
	 * @return times when usable. If null or empty, base product usable value is
	 *         used.
	 */
	public List<Validity> getUsable() {
		if (usable == null) {
			usable = new ArrayList<>();
		}
		return usable;
	}

	/**
	 * @param usable times when usable. If null or empty, base product usable value
	 *               is used.
	 */
	public void setUsable(List<Validity> usable) {
		this.usable = usable;
	}

	/**
	 * @return pricing plan ID values. If null or empty, base product value is used.
	 */
	public List<String> getPricingPlans() {
		if (pricingPlans == null) {
			pricingPlans = new ArrayList<>();
		}
		return pricingPlans;
	}

	/**
	 * @param pricingPlans pricing plan ID values. If null or empty, base product
	 *                     value is used.
	 */
	public void setPricingPlans(List<String> pricingPlans) {
		this.pricingPlans = pricingPlans;
	}

	/**
	 * @return product validity time length, in seconds. Time start depends on
	 *         validityStartFrom.
	 */
	public int getValidityLength() {
		return validityLength;
	}

	/**
	 * @param validityLength product validity time length, in seconds. Time start
	 *                       depends on validityStartFrom.
	 */
	public void setValidityLength(int validityLength) {
		this.validityLength = validityLength;
	}

	/**
	 * @return product validity start from option.
	 */
	public ValidityStartFrom getValidityStartFrom() {
		return validityStartFrom;
	}

	/**
	 * @param validityStartFrom product validity start from option.
	 */
	public void setValidityStartFrom(ValidityStartFrom validityStartFrom) {
		this.validityStartFrom = validityStartFrom;
	}

	@Override
	public int hashCode() {
		return Objects.hash(validityStartFrom, validityLength, pricingPlans, usable, sellable, customFields,
				descriptions, productValuesInherited, productSku, productId);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof CatalogItem)) {
			return false;
		}
		CatalogItem o = (CatalogItem) obj;
		return Objects.equals(productId, o.productId) //
				&& Objects.equals(productSku, o.productSku) //
				&& Objects.equals(productValuesInherited, o.productValuesInherited) //
				&& Objects.equals(descriptions, o.descriptions) //
				&& Objects.equals(customFields, o.customFields) //
				&& Objects.equals(sellable, o.sellable) //
				&& Objects.equals(usable, o.usable) //
				&& Objects.equals(pricingPlans, o.pricingPlans) //
				&& Objects.equals(validityLength, o.validityLength) //
				&& Objects.equals(validityStartFrom, o.validityStartFrom);
	}

	@Override
	public String toString() {
		return "CatalogItem [productId=" + productId + ", productSku=" + productSku + ", productValuesInherited="
				+ productValuesInherited + ", descriptions=" + descriptions + ", customFields=" + customFields
				+ ", sellable=" + sellable + ", usable=" + usable + ", pricingPlans=" + pricingPlans
				+ ", validityLength=" + validityLength + ", validityStartFrom=" + validityStartFrom + "]";
	}

}
