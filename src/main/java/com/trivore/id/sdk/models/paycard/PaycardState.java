package com.trivore.id.sdk.models.paycard;

/**
 * An enumeration of the paycard state.
 * <p>
 * These values are used with {@link Paycard} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum PaycardState {
	ACTIVE, INACTIVE
}
