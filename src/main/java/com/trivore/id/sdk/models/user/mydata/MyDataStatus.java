package com.trivore.id.sdk.models.user.mydata;

/**
 * An enumeration representing a status of MyData.
 */
@SuppressWarnings("javadoc")
public enum MyDataStatus {
	COLLECTING, PENDING, READY, UNKNOWN, PARTIAL, FAILED;
}
