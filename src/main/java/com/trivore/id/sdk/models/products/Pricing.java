package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single pricing in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID pricing definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pricing implements Serializable {

	private Boolean enabled;
	private String title;
	private String description;
	private Float price;
	private String currency;
	private Boolean vatIncluded;
	private Float vatPercent;
	private List<CodeDiscount> codeDiscounts;
	private List<VolumeDiscount> volumeDiscounts;
	private List<CustomerSegmentDiscount> customerSegmentDiscounts;
	private List<PaymentMethodDiscount> paymentMethodDiscounts;
	private List<VariableDiscount> variableDiscounts;
	private Boolean codeDiscountStacking;
	private Boolean volumeDiscountStacking;
	private Boolean customerSegmentDiscountStacking;
	private Boolean variableDiscountStacking;

	/**
	 * @return the enabled
	 */
	public Boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/**
	 * @return the currency. ISO 4217 currency code.
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set. ISO 4217 currency code.
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the vatIncluded
	 */
	public Boolean isVatIncluded() {
		return vatIncluded;
	}

	/**
	 * @param vatIncluded the vatIncluded to set
	 */
	public void setVatIncluded(Boolean vatIncluded) {
		this.vatIncluded = vatIncluded;
	}

	/**
	 * @return the vatPercent
	 */
	public Float getVatPercent() {
		return vatPercent;
	}

	/**
	 * @param vatPercent the vatPercent to set
	 */
	public void setVatPercent(Float vatPercent) {
		this.vatPercent = vatPercent;
	}

	/**
	 * @return the codeDiscounts
	 */
	public List<CodeDiscount> getCodeDiscounts() {
		if (codeDiscounts == null) {
			codeDiscounts = new ArrayList<>();
		}
		return codeDiscounts;
	}

	/**
	 * @param codeDiscounts the codeDiscounts to set
	 */
	public void setCodeDiscounts(List<CodeDiscount> codeDiscounts) {
		this.codeDiscounts = codeDiscounts;
	}

	/**
	 * @return the volumeDiscounts
	 */
	public List<VolumeDiscount> getVolumeDiscounts() {
		if (volumeDiscounts == null) {
			volumeDiscounts = new ArrayList<>();
		}
		return volumeDiscounts;
	}

	/**
	 * @param volumeDiscounts the volumeDiscounts to set
	 */
	public void setVolumeDiscounts(List<VolumeDiscount> volumeDiscounts) {
		this.volumeDiscounts = volumeDiscounts;
	}

	/**
	 * @return the customerSegmentDiscounts
	 */
	public List<CustomerSegmentDiscount> getCustomerSegmentDiscounts() {
		if (customerSegmentDiscounts == null) {
			customerSegmentDiscounts = new ArrayList<>();
		}
		return customerSegmentDiscounts;
	}

	/**
	 * @param customerSegmentDiscounts the customerSegmentDiscounts to set
	 */
	public void setCustomerSegmentDiscounts(List<CustomerSegmentDiscount> customerSegmentDiscounts) {
		this.customerSegmentDiscounts = customerSegmentDiscounts;
	}

	/**
	 * @return the paymentMethodDiscounts
	 */
	public List<PaymentMethodDiscount> getPaymentMethodDiscounts() {
		if (paymentMethodDiscounts == null) {
			paymentMethodDiscounts = new ArrayList<>();
		}
		return paymentMethodDiscounts;
	}

	/**
	 * @param paymentMethodDiscounts the paymentMethodDiscounts to set
	 */
	public void setPaymentMethodDiscounts(List<PaymentMethodDiscount> paymentMethodDiscounts) {
		this.paymentMethodDiscounts = paymentMethodDiscounts;
	}

	/**
	 * @return the variableDiscounts
	 */
	public List<VariableDiscount> getVariableDiscounts() {
		if (variableDiscounts == null) {
			variableDiscounts = new ArrayList<>();
		}
		return variableDiscounts;
	}

	/**
	 * @param variableDiscounts the variableDiscounts to set
	 */
	public void setVariableDiscounts(List<VariableDiscount> variableDiscounts) {
		this.variableDiscounts = variableDiscounts;
	}

	/**
	 * @return the codeDiscountStacking
	 */
	public Boolean isCodeDiscountStacking() {
		return codeDiscountStacking;
	}

	/**
	 * @param codeDiscountStacking the codeDiscountStacking to set
	 */
	public void setCodeDiscountStacking(Boolean codeDiscountStacking) {
		this.codeDiscountStacking = codeDiscountStacking;
	}

	/**
	 * @return the volumeDiscountStacking
	 */
	public Boolean isVolumeDiscountStacking() {
		return volumeDiscountStacking;
	}

	/**
	 * @param volumeDiscountStacking the volumeDiscountStacking to set
	 */
	public void setVolumeDiscountStacking(Boolean volumeDiscountStacking) {
		this.volumeDiscountStacking = volumeDiscountStacking;
	}

	/**
	 * @return the customerSegmentDiscountStacking
	 */
	public Boolean isCustomerSegmentDiscountStacking() {
		return customerSegmentDiscountStacking;
	}

	/**
	 * @param customerSegmentDiscountStacking the customerSegmentDiscountStacking to
	 *                                        set
	 */
	public void setCustomerSegmentDiscountStacking(Boolean customerSegmentDiscountStacking) {
		this.customerSegmentDiscountStacking = customerSegmentDiscountStacking;
	}

	/**
	 * @return the variableDiscountStacking
	 */
	public Boolean isVariableDiscountStacking() {
		return variableDiscountStacking;
	}

	/**
	 * @param variableDiscountStacking the variableDiscountStacking to set
	 */
	public void setVariableDiscountStacking(Boolean variableDiscountStacking) {
		this.variableDiscountStacking = variableDiscountStacking;
	}

	@Override
	public int hashCode() {
		return Objects.hash(enabled, title, description, price, currency, vatIncluded, vatPercent, codeDiscounts,
				volumeDiscounts, customerSegmentDiscounts, paymentMethodDiscounts, variableDiscounts,
				codeDiscountStacking, volumeDiscountStacking, customerSegmentDiscountStacking,
				variableDiscountStacking);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Pricing)) {
			return false;
		}
		Pricing o = (Pricing) obj;
		return Objects.equals(variableDiscountStacking, o.variableDiscountStacking) //
				&& Objects.equals(customerSegmentDiscountStacking, o.customerSegmentDiscountStacking) //
				&& Objects.equals(volumeDiscountStacking, o.volumeDiscountStacking) //
				&& Objects.equals(codeDiscountStacking, o.codeDiscountStacking) //
				&& Objects.equals(variableDiscounts, o.variableDiscounts) //
				&& Objects.equals(paymentMethodDiscounts, o.paymentMethodDiscounts) //
				&& Objects.equals(customerSegmentDiscounts, o.customerSegmentDiscounts) //
				&& Objects.equals(volumeDiscounts, o.volumeDiscounts) //
				&& Objects.equals(codeDiscounts, o.codeDiscounts) //
				&& Objects.equals(vatPercent, o.vatPercent) //
				&& Objects.equals(vatIncluded, o.vatIncluded) //
				&& Objects.equals(currency, o.currency) //
				&& Objects.equals(price, o.price) //
				&& Objects.equals(description, o.description) //
				&& Objects.equals(title, o.title) //
				&& Objects.equals(enabled, o.enabled);
	}

	@Override
	public String toString() {
		return "Pricing [enabled=" + enabled + ", title=" + title + ", description=" + description + ", price=" + price
				+ ", currency=" + currency + ", vatIncluded=" + vatIncluded + ", vatPercent=" + vatPercent
				+ ", codeDiscounts=" + codeDiscounts + ", volumeDiscounts=" + volumeDiscounts
				+ ", customerSegmentDiscounts=" + customerSegmentDiscounts + ", paymentMethodDiscounts="
				+ paymentMethodDiscounts + ", variableDiscounts=" + variableDiscounts + ", codeDiscountStacking="
				+ codeDiscountStacking + ", volumeDiscountStacking=" + volumeDiscountStacking
				+ ", customerSegmentDiscountStacking=" + customerSegmentDiscountStacking + ", variableDiscountStacking="
				+ variableDiscountStacking + "]";
	}

}
