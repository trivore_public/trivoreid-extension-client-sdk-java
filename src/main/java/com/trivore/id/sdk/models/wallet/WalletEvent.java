package com.trivore.id.sdk.models.wallet;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that represents a wallet event.
 * <p>
 * Used to read wallet events in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletEvent implements Serializable {

	private String id;
	private String time;
	private Float balanceChange;
	private String message;
	private String transferTo;
	private String transferFrom;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the balanceChange
	 */
	public Float getBalanceChange() {
		return balanceChange;
	}

	/**
	 * @param balanceChange the balanceChange
	 */
	public void setBalanceChange(Float balanceChange) {
		this.balanceChange = balanceChange;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the transfer to
	 */
	public String getTransferTo() {
		return transferTo;
	}

	/**
	 * @param transferTo the transfer to
	 */
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	/**
	 * @return the transfer from
	 */
	public String getTransferFrom() {
		return transferFrom;
	}

	/**
	 * @param transferFrom the transfer from
	 */
	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, time, balanceChange, message, transferTo, transferFrom);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof WalletEvent)) {
			return false;
		}
		WalletEvent o = (WalletEvent) obj;
		return Objects.equals(id, o.id) //
				&& Objects.equals(time, o.time) //
				&& Objects.equals(balanceChange, o.balanceChange) //
				&& Objects.equals(message, o.message) //
				&& Objects.equals(transferTo, o.transferTo) //
				&& Objects.equals(transferFrom, o.transferFrom);

	}

	@Override
	public String toString() {
		return "WalletEvent [id=" + id + ", time=" + time + ", balanceChange=" + balanceChange + ", message=" + message
				+ ", transferTo=" + transferTo + ", transferFrom=" + transferFrom + "]";
	}

}
