package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that presents a period's period in Trivore ID.
 * <p>
 * Used to manage period's periods in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Period implements Serializable {

	private String id;
	private String created;
	private String externalId;
	private String from;
	private String until;
	private Map<String, Object> customFields;

	/**
	 * Build a new period.
	 */
	public Period() {
		// ...
	}

	/**
	 * Get period's unique identifier.
	 *
	 * @return period's unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set period's unique identifier.
	 *
	 * @param id period's unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get external ID for period.
	 *
	 * @return external ID for period
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * Set external ID for period.
	 *
	 * @param externalId external ID for period
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * Get period custom fields.
	 *
	 * @return period custom fields
	 */
	public Map<String, Object> getCustomFields() {
		if (customFields == null) {
			customFields = new HashMap<>();
		}
		return customFields;
	}

	/**
	 * Set period custom fields.
	 *
	 * @param customFields period custom fields
	 */
	public void setCustomFields(Map<String, Object> customFields) {
		this.customFields = customFields;
	}

	/**
	 * Get time when period info was added.
	 *
	 * @return time when period info was added
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * Set time when period info was added.
	 *
	 * @param created time when period info was added
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * Get period start time.
	 *
	 * @return period start time
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Set period start time.
	 *
	 * @param from period start time
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Get period end time.
	 *
	 * @return period end time
	 */
	public String getUntil() {
		return until;
	}

	/**
	 * Set period end time.
	 *
	 * @param until period end time
	 */
	public void setUntil(String until) {
		this.until = until;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof Period)) {
			return false;
		}
		Period o = (Period) object;
		if (o.getId() == null && getId() == null) {
			return super.equals(object);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Period [id=" + id + ", created=" + created + ", externalId=" + externalId + ", from=" + from
				+ ", until=" + until + ", customFields=" + customFields + "]";
	}

}
