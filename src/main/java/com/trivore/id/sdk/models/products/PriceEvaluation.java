package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents price evaluation in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID price evaluated lowest price
 * from the indicated pricing plan.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceEvaluation implements Serializable {

	private Float basePriceWithVat;
	private Float basePriceWithoutVat;
	private Float baseVatAmount;
	private Float totalPriceWithVat;
	private Float totalPriceWithoutVat;
	private Float totalVatAmount;
	private Float pricePerUnitWithVat;
	private Float pricePerUnitWithoutVat;
	private Float pricePerUnitVatAmount;
	private Float vatPercent;
	private String currency;
	private List<DiscountInfo> discounts;

	/**
	 * @return the base price per unit without volume or discounts, with VAT
	 */
	public Float getBasePriceWithVat() {
		return basePriceWithVat;
	}

	/**
	 * @param basePriceWithVat the base price per unit without volume or discounts,
	 *                         with VAT
	 */
	public void setBasePriceWithVat(Float basePriceWithVat) {
		this.basePriceWithVat = basePriceWithVat;
	}

	/**
	 * @return the base price per unit without volume or discounts, without VAT
	 */
	public Float getBasePriceWithoutVat() {
		return basePriceWithoutVat;
	}

	/**
	 * @param basePriceWithoutVat the base price per unit without volume or
	 *                            discounts, without VAT
	 */
	public void setBasePriceWithoutVat(Float basePriceWithoutVat) {
		this.basePriceWithoutVat = basePriceWithoutVat;
	}

	/**
	 * @return the VAT amount for base price per unit without volume or discounts
	 */
	public Float getBaseVatAmount() {
		return baseVatAmount;
	}

	/**
	 * @param baseVatAmount the VAT amount for base price per unit without volume or
	 *                      discounts
	 */
	public void setBaseVatAmount(Float baseVatAmount) {
		this.baseVatAmount = baseVatAmount;
	}

	/**
	 * @return the final total price for given parameters with volume and discounts,
	 *         with VAT
	 */
	public Float getTotalPriceWithVat() {
		return totalPriceWithVat;
	}

	/**
	 * @param totalPriceWithVat the final total price for given parameters with
	 *                          volume and discounts, with VAT
	 */
	public void setTotalPriceWithVat(Float totalPriceWithVat) {
		this.totalPriceWithVat = totalPriceWithVat;
	}

	/**
	 * @return the final total price for given parameters with volume and discounts,
	 *         without VAT
	 */
	public Float getTotalPriceWithoutVat() {
		return totalPriceWithoutVat;
	}

	/**
	 * @param totalPriceWithoutVat the final total price for given parameters with
	 *                             volume and discounts, without VAT
	 */
	public void setTotalPriceWithoutVat(Float totalPriceWithoutVat) {
		this.totalPriceWithoutVat = totalPriceWithoutVat;
	}

	/**
	 * @return the VAT amount for final total price per unit with volume and
	 *         discounts
	 */
	public Float getTotalVatAmount() {
		return totalVatAmount;
	}

	/**
	 * @param totalVatAmount the VAT amount for final total price per unit with
	 *                       volume and discounts
	 */
	public void setTotalVatAmount(Float totalVatAmount) {
		this.totalVatAmount = totalVatAmount;
	}

	/**
	 * @return the final price per unit for given parameters with discounts, with
	 *         VAT
	 */
	public Float getPricePerUnitWithVat() {
		return pricePerUnitWithVat;
	}

	/**
	 * @param pricePerUnitWithVat the final price per unit for given parameters with
	 *                            discounts, with VAT
	 */
	public void setPricePerUnitWithVat(Float pricePerUnitWithVat) {
		this.pricePerUnitWithVat = pricePerUnitWithVat;
	}

	/**
	 * @return the final price per unit for given parameters with discounts, without
	 *         VAT
	 */
	public Float getPricePerUnitWithoutVat() {
		return pricePerUnitWithoutVat;
	}

	/**
	 * @param pricePerUnitWithoutVat the final price per unit for given parameters
	 *                               with discounts, without VAT
	 */
	public void setPricePerUnitWithoutVat(Float pricePerUnitWithoutVat) {
		this.pricePerUnitWithoutVat = pricePerUnitWithoutVat;
	}

	/**
	 * @return the VAT amount for final price per unit with discounts, with VAT
	 */
	public Float getPricePerUnitVatAmount() {
		return pricePerUnitVatAmount;
	}

	/**
	 * @param pricePerUnitVatAmount the VAT amount for final price per unit with
	 *                              discounts, with VAT
	 */
	public void setPricePerUnitVatAmount(Float pricePerUnitVatAmount) {
		this.pricePerUnitVatAmount = pricePerUnitVatAmount;
	}

	/**
	 * @return the value Added Tax percentage included in prices
	 */
	public Float getVatPercent() {
		return vatPercent;
	}

	/**
	 * @param vatPercent the value Added Tax percentage included in prices
	 */
	public void setVatPercent(Float vatPercent) {
		this.vatPercent = vatPercent;
	}

	/**
	 * @return the currency prices are in
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency prices are in
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the discounts applied to the final total price
	 */
	public List<DiscountInfo> getDiscounts() {
		return discounts;
	}

	/**
	 * @param discounts the discounts applied to the final total price
	 */
	public void setDiscounts(List<DiscountInfo> discounts) {
		this.discounts = discounts;
	}

	@Override
	public int hashCode() {
		return Objects.hash(basePriceWithVat, basePriceWithoutVat, baseVatAmount, totalPriceWithVat,
				totalPriceWithoutVat, totalVatAmount, pricePerUnitWithVat, pricePerUnitWithoutVat,
				pricePerUnitVatAmount, vatPercent, currency, discounts);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PriceEvaluation)) {
			return false;
		}
		PriceEvaluation o = (PriceEvaluation) obj;
		return Objects.equals(basePriceWithVat, o.basePriceWithVat) //
				&& Objects.equals(basePriceWithoutVat, o.basePriceWithoutVat) //
				&& Objects.equals(baseVatAmount, o.baseVatAmount) //
				&& Objects.equals(totalPriceWithVat, o.totalPriceWithVat) //
				&& Objects.equals(totalPriceWithoutVat, o.totalPriceWithoutVat) //
				&& Objects.equals(totalVatAmount, o.totalVatAmount) //
				&& Objects.equals(pricePerUnitWithVat, o.pricePerUnitWithVat) //
				&& Objects.equals(pricePerUnitWithoutVat, o.pricePerUnitWithoutVat) //
				&& Objects.equals(pricePerUnitVatAmount, o.pricePerUnitVatAmount) //
				&& Objects.equals(vatPercent, o.vatPercent) //
				&& Objects.equals(currency, o.currency) //
				&& Objects.equals(discounts, o.discounts);
	}

	@Override
	public String toString() {
		return "PriceEvaluation [basePriceWithVat=" + basePriceWithVat + ", basePriceWithoutVat=" + basePriceWithoutVat
				+ ", baseVatAmount=" + baseVatAmount + ", totalPriceWithVat=" + totalPriceWithVat
				+ ", totalPriceWithoutVat=" + totalPriceWithoutVat + ", totalVatAmount=" + totalVatAmount
				+ ", pricePerUnitWithVat=" + pricePerUnitWithVat + ", pricePerUnitWithoutVat=" + pricePerUnitWithoutVat
				+ ", pricePerUnitVatAmount=" + pricePerUnitVatAmount + ", vatPercent=" + vatPercent + ", currency="
				+ currency + ", discounts=" + discounts + "]";
	}

}
