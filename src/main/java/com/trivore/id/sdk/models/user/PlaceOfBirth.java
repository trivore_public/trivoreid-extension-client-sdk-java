package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User's place of birth.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceOfBirth implements Serializable {

	private String country;
	private String locality;
	private String region;

	/**
	 * @return country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return locality
	 */
	public String getLocality() {
		return locality;
	}

	/**
	 * @param locality locality
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}

	/**
	 * @return region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region region
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public int hashCode() {
		return Objects.hash(country, locality, region);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PlaceOfBirth)) {
			return false;
		}
		PlaceOfBirth o = (PlaceOfBirth) obj;
		return Objects.equals(country, o.country) //
				&& Objects.equals(locality, o.locality) //
				&& Objects.equals(region, o.region);
	}

	@Override
	public String toString() {
		return "PlaceOfBirth [country=" + country + ", locality=" + locality + ", region=" + region + "]";
	}

}
