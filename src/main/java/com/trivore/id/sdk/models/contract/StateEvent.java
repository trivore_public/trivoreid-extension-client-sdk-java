package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a state event in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StateEvent implements Serializable {

	private CurrentState state;
	private String enactor;
	private String timestamp;

	/**
	 * Construct a new state event object.
	 */
	public StateEvent() {
		// ...
	}

	/**
	 * Get state.
	 *
	 * @return state
	 */
	public CurrentState getState() {
		return state;
	}

	/**
	 * Set state.
	 *
	 * @param state state
	 */
	public void setState(CurrentState state) {
		this.state = state;
	}

	/**
	 * Get enactor.
	 *
	 * @return enactor
	 */
	public String getEnactor() {
		return enactor;
	}

	/**
	 * Set enactor.
	 *
	 * @param enactor enactor
	 */
	public void setEnactor(String enactor) {
		this.enactor = enactor;
	}

	/**
	 * Get timestamp.
	 *
	 * @return timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Set timestamp.
	 *
	 * @param timestamp timestamp
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(state, enactor, timestamp);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof StateEvent)) {
			return false;
		}
		StateEvent o = (StateEvent) obj;
		return Objects.equals(state, o.state) && Objects.equals(enactor, o.enactor)
				&& Objects.equals(timestamp, o.timestamp);
	}

	@Override
	public String toString() {
		return "StateEvent [state=" + state + ", enactor=" + enactor + ", timestamp=" + timestamp + "]";
	}

}
