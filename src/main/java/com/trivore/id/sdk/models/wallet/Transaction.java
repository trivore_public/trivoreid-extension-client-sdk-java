package com.trivore.id.sdk.models.wallet;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that represents transaction info.
 * <p>
 * Used to read transaction info in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction implements Serializable {

	private String amount;
	private String currency;
	private String message;
	private String transferTo;

	/**
	 * @return the transaction amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the transaction amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * Transaction currency as ISO 4217 code. Target wallets must have matching
	 * currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Transaction currency as ISO 4217 code. Target wallets must have matching
	 * currency.
	 *
	 * @param currency the currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the message stored in transaction events.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message stored in transaction events.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the ID of another wallet where funds are transferred.
	 */
	public String getTransferTo() {
		return transferTo;
	}

	/**
	 * @param transferTo the ID of another wallet where funds are transferred.
	 */
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, currency, message, transferTo);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Transaction)) {
			return false;
		}
		Transaction o = (Transaction) obj;
		return Objects.equals(amount, o.amount) //
				&& Objects.equals(currency, o.currency) //
				&& Objects.equals(message, o.message) //
				&& Objects.equals(transferTo, o.transferTo);

	}

	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", currency=" + currency + ", message=" + message + ", transferTo="
				+ transferTo + "]";
	}

}
