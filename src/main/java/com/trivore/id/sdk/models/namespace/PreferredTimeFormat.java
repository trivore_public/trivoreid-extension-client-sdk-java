package com.trivore.id.sdk.models.namespace;

/**
 * An enumeration of the preferred time format.
 * <p>
 * These values are used with {@link GroupPolicy} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum PreferredTimeFormat {
	INHERIT, BROWSER, H_MM_SS_COLON_SEPARATOR
}
