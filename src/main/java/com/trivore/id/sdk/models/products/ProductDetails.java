package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents product details in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID product details definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDetails implements Serializable {

	private String name;
	private String shortName;
	private String description;
	private Map<Object, Object> customFields;
	private List<Validity> usable;
	private Integer validityLength;
	private ValidityStartFrom validityStartFrom;
	private String productId;
	private String productSku;
	private String catalogId;
	private String pricingPlanId;
	private PriceEvaluation price;
	private String priceToken;
	private String priceTokenExpires;
	private List<LocalisedDescription> translations;

	/**
	 * Product name in selected language.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Product name in selected language.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Short product name in selected language.
	 *
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Short product name in selected language.
	 *
	 * @param shortName the shortName
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the product description in selected language
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the product description in selected language
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the custom fields
	 */
	public Map<Object, Object> getCustomFields() {
		return customFields;
	}

	/**
	 * @param customFields the custom fields
	 */
	public void setCustomFields(Map<Object, Object> customFields) {
		this.customFields = customFields;
	}

	/**
	 * @return times when product is expected to be usable
	 */
	public List<Validity> getUsable() {
		if (usable == null) {
			usable = new ArrayList<>();
		}
		return usable;
	}

	/**
	 * @param usable times when product is expected to be usable
	 */
	public void setUsable(List<Validity> usable) {
		this.usable = usable;
	}

	/**
	 * @return the product validity length in seconds, from validityStartFrom event.
	 */
	public Integer getValidityLength() {
		return validityLength;
	}

	/**
	 * @param validityLength the product validity length in seconds, from
	 *                       validityStartFrom event.
	 */
	public void setValidityLength(Integer validityLength) {
		this.validityLength = validityLength;
	}

	/**
	 * @return the event from which validityLength counting starts.
	 */
	public ValidityStartFrom getValidityStartFrom() {
		return validityStartFrom;
	}

	/**
	 * @param validityStartFrom the event from which validityLength counting starts.
	 */
	public void setValidityStartFrom(ValidityStartFrom validityStartFrom) {
		this.validityStartFrom = validityStartFrom;
	}

	/**
	 * @return the internal ID for product this item is based on
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the internal ID for product this item is based on
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the SKU of the product this item is based on
	 */
	public String getProductSku() {
		return productSku;
	}

	/**
	 * @param productSku the SKU of the product this item is based on
	 */
	public void setProductSku(String productSku) {
		this.productSku = productSku;
	}

	/**
	 * @return the ID of product catalog where this item is
	 */
	public String getCatalogId() {
		return catalogId;
	}

	/**
	 * @param catalogId the ID of product catalog where this item is
	 */
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	/**
	 * @return the ID of pricing plan whose price is selected
	 */
	public String getPricingPlanId() {
		return pricingPlanId;
	}

	/**
	 * @param pricingPlanId the ID of pricing plan whose price is selected
	 */
	public void setPricingPlanId(String pricingPlanId) {
		this.pricingPlanId = pricingPlanId;
	}

	/**
	 * @return the evaluated lowest price from the indicated pricing plan
	 */
	public PriceEvaluation getPrice() {
		return price;
	}

	/**
	 * @param price the evaluated lowest price from the indicated pricing plan
	 */
	public void setPrice(PriceEvaluation price) {
		this.price = price;
	}

	/**
	 * Token passed to Sale API if price is accepted. Contains enough information to
	 * make a sale, including item identity, discounted price with VAT. Has an
	 * expiration time, token must be used before this time or sale will be blocked.
	 *
	 * @return the price token
	 */
	public String getPriceToken() {
		return priceToken;
	}

	/**
	 * Token passed to Sale API if price is accepted. Contains enough information to
	 * make a sale, including item identity, discounted price with VAT. Has an
	 * expiration time, token must be used before this time or sale will be blocked.
	 *
	 * @param priceToken the price token
	 */
	public void setPriceToken(String priceToken) {
		this.priceToken = priceToken;
	}

	/**
	 * @return the time when priceToken will expire and becomes unusable
	 */
	public String getPriceTokenExpires() {
		return priceTokenExpires;
	}

	/**
	 * @param priceTokenExpires the time when priceToken will expire and becomes
	 *                          unusable
	 */
	public void setPriceTokenExpires(String priceTokenExpires) {
		this.priceTokenExpires = priceTokenExpires;
	}

	/**
	 * @return the translations
	 */
	public List<LocalisedDescription> getTranslations() {
		if (translations == null) {
			translations = new ArrayList<>();
		}
		return translations;
	}

	/**
	 * @param translations the translations
	 */
	public void setTranslations(List<LocalisedDescription> translations) {
		this.translations = translations;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, shortName, description, customFields, usable, validityLength, validityStartFrom,
				productId, productSku, catalogId, pricingPlanId, price, priceToken, priceTokenExpires, translations);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ProductDetails)) {
			return false;
		}
		ProductDetails o = (ProductDetails) obj;
		return Objects.equals(name, o.name) //
				&& Objects.equals(shortName, o.shortName) //
				&& Objects.equals(description, o.description) //
				&& Objects.equals(customFields, o.customFields) //
				&& Objects.equals(usable, o.usable) //
				&& Objects.equals(validityLength, o.validityLength) //
				&& Objects.equals(validityStartFrom, o.validityStartFrom) //
				&& Objects.equals(productId, o.productId) //
				&& Objects.equals(productSku, o.productSku) //
				&& Objects.equals(catalogId, o.catalogId) //
				&& Objects.equals(pricingPlanId, o.pricingPlanId) //
				&& Objects.equals(price, o.price) //
				&& Objects.equals(priceToken, o.priceToken) //
				&& Objects.equals(priceTokenExpires, o.priceTokenExpires) //
				&& Objects.equals(translations, o.translations) //
		;
	}

	@Override
	public String toString() {
		return "ProductDetails [name=" + name + ", shortName=" + shortName + ", description=" + description
				+ ", customFields=" + customFields + ", usable=" + usable + ", validityLength=" + validityLength
				+ ", validityStartFrom=" + validityStartFrom + ", productId=" + productId + ", productSku=" + productSku
				+ ", catalogId=" + catalogId + ", pricingPlanId=" + pricingPlanId + ", price=" + price + ", priceToken="
				+ priceToken + ", priceTokenExpires=" + priceTokenExpires + ", translations=" + translations + "]";
	}

}
