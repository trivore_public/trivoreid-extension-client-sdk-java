package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single grant right of the authorisation in
 * Trivore ID.
 * <p>
 * Used to specify the grant right of the authorisation in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorisationGrantRight implements Serializable {

	private String id;
	private List<String> authorisationTypes;
	private AuthorisationGrant authorisationGrant;
	private String validFrom;
	private String validTo;
	private AuthGrantRightPrincipal principal;
	private AuthGrantRightObject object;

	/**
	 * Construct a new grant right object.
	 */
	public AuthorisationGrantRight() {
		// ...
	}

	/**
	 * Get right grant unique identifier.
	 *
	 * @return right grant unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set right grant unique identifier.
	 *
	 * @param id right grant unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get list of authorisation types.
	 * <p>
	 * Make an HTTP GET request to the authorisation_type endpoint for a list of
	 * available types.
	 * </p>
	 *
	 * @return list of authorisation types
	 */
	public List<String> getAuthorisationTypes() {
		if (authorisationTypes == null) {
			authorisationTypes = new ArrayList<>();
		}
		return authorisationTypes;
	}

	/**
	 * Set list of authorisation types.
	 * <p>
	 * Make an HTTP GET request to the authorisation_type endpoint for a list of
	 * available types.
	 * </p>
	 *
	 * @param authorisationTypes list of authorisation types
	 */
	public void setAuthorisationTypes(List<String> authorisationTypes) {
		this.authorisationTypes = authorisationTypes;
	}

	/**
	 * Get the authorisation grant.
	 * <p>
	 * Additional information about the authorisation grant right. It contains
	 * read-only information about possible revocation as well as the OAuth 2.0
	 * client which has managed the authorisation grant right.
	 * </p>
	 *
	 * @return the authorisation grant
	 */
	public AuthorisationGrant getAuthorisationGrant() {
		if (authorisationGrant == null) {
			authorisationGrant = new AuthorisationGrant();
		}
		return authorisationGrant;
	}

	/**
	 * Set the authorisation grant.
	 * <p>
	 * Additional information about the authorisation grant right. It contains
	 * read-only information about possible revocation as well as the OAuth 2.0
	 * client which has managed the authorisation grant right.
	 * </p>
	 *
	 * @param authorisationGrant the authorisation grant
	 */
	public void setAuthorisationGrant(AuthorisationGrant authorisationGrant) {
		this.authorisationGrant = authorisationGrant;
	}

	/**
	 * Get the valid from.
	 * <p>
	 * Authorisation grant right valid from in ISO format. Defaults to grant right
	 * creation time.
	 * </p>
	 *
	 * @return the valid from
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * Set the valid from.
	 * <p>
	 * Authorisation grant right valid from in ISO format. Defaults to grant right
	 * creation time.
	 * </p>
	 *
	 * @param validFrom the valid from
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * Get the valid to.
	 * <p>
	 * Authorisation grant right valid to in ISO format. Defaults to one year after
	 * authorisation grant right creation.
	 * </p>
	 *
	 * @return the valid to
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Set the valid to.
	 * <p>
	 * Authorisation grant right valid to in ISO format. Defaults to one year after
	 * authorisation grant right creation.
	 * </p>
	 *
	 * @param validTo the valid to
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Get authorisation principal information.
	 *
	 * @return authorisation principal information
	 */
	public AuthGrantRightPrincipal getPrincipal() {
		if (principal == null) {
			principal = new AuthGrantRightPrincipal();
		}
		return principal;
	}

	/**
	 * Set authorisation principal information.
	 *
	 * @param principal authorisation principal information
	 */
	public void setPrincipal(AuthGrantRightPrincipal principal) {
		this.principal = principal;
	}

	/**
	 * Get authorisation object information. Will be determined automatically.
	 *
	 * @return authorisation object information
	 */
	public AuthGrantRightObject getObject() {
		if (object == null) {
			object = new AuthGrantRightObject();
		}
		return object;
	}

	/**
	 * Set authorisation object information. Will be determined automatically.
	 *
	 * @param object authorisation object information
	 */
	public void setObject(AuthGrantRightObject object) {
		this.object = object;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthorisationGrantRight)) {
			return false;
		}
		AuthorisationGrantRight o = (AuthorisationGrantRight) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "AuthorisationGrantRight [id=" + id + ", authorisationTypes=" + authorisationTypes
				+ ", authorisationGrant=" + authorisationGrant + ", validFrom=" + validFrom + ", validTo=" + validTo
				+ ", principal=" + principal + ", object=" + object + "]";
	}

}
