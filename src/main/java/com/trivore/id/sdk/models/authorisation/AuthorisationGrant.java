package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single grant in Trivore ID.
 * <p>
 * Used to specify the grant of the authorisation in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorisationGrant implements Serializable {

	private String createdAt;
	private String createdByOauthClientId;
	private String revokedByOauthClientId;
	private boolean revoked;
	private String revokedAt;

	/**
	 * Construct a new grant right object.
	 */
	public AuthorisationGrant() {
		// ...
	}

	/**
	 * Get created at.
	 * <p>
	 * Time of authorisation grant creation in ISO format.
	 * </p>
	 *
	 * @return created at
	 */
	public String getCreatedAt() {
		return createdAt;
	}

	/**
	 * Set created at.
	 * <p>
	 * Time of authorisation grant creation in ISO format.
	 * </p>
	 *
	 * @param createdAt created at
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Get created by Oauth Client ID.
	 * <p>
	 * The id of the OAuth 2.0 client, which created the entry.
	 * </p>
	 *
	 * @return created by Oauth Client ID
	 */
	public String getCreatedByOauthClientId() {
		return createdByOauthClientId;
	}

	/**
	 * Set created by Oauth Client ID.
	 * <p>
	 * The id of the OAuth 2.0 client, which created the entry.
	 * </p>
	 *
	 * @param createdByOauthClientId created by Oauth Client ID
	 */
	public void setCreatedByOauthClientId(String createdByOauthClientId) {
		this.createdByOauthClientId = createdByOauthClientId;
	}

	/**
	 * Get revoked by Oauth Client ID.
	 * <p>
	 * If the authorisation is revoked, this field shows the id of the OAuth 2.0
	 * client, which did the revoking on user's behalf.
	 * </p>
	 *
	 * @return revoked by Oauth Client ID
	 */
	public String getRevokedByOauthClientId() {
		return revokedByOauthClientId;
	}

	/**
	 * Set revoked by Oauth Client ID.
	 * <p>
	 * If the authorisation is revoked, this field shows the id of the OAuth 2.0
	 * client, which did the revoking on user's behalf.
	 * </p>
	 *
	 * @param revokedByOauthClientId revoked by Oauth Client ID
	 */
	public void setRevokedByOauthClientId(String revokedByOauthClientId) {
		this.revokedByOauthClientId = revokedByOauthClientId;
	}

	/**
	 * Get is revoked.
	 * <p>
	 * True if the end-user has revoked this authorisation grant right. The
	 * authorisation grant right can be revoked at the revocation endpoint.
	 * </p>
	 *
	 * @return is revoked
	 */
	public boolean isRevoked() {
		return revoked;
	}

	/**
	 * Set is revoked.
	 * <p>
	 * True if the end-user has revoked this authorisation grant right. The
	 * authorisation grant right can be revoked at the revocation endpoint.
	 * </p>
	 *
	 * @param revoked is revoked
	 */
	public void setRevoked(boolean revoked) {
		this.revoked = revoked;
	}

	/**
	 * Get revoked at.
	 * <p>
	 * Time of authorisation revocation in ISO format.
	 * </p>
	 *
	 * @return revoked at
	 */
	public String getRevokedAt() {
		return revokedAt;
	}

	/**
	 * Set revoked at.
	 * <p>
	 * Time of authorisation revocation in ISO format.
	 * </p>
	 *
	 * @param revokedAt revoked at
	 */
	public void setRevokedAt(String revokedAt) {
		this.revokedAt = revokedAt;
	}

	@Override
	public int hashCode() {
		return Objects.hash(createdAt, createdByOauthClientId, revokedByOauthClientId, revoked, revokedAt);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthorisationGrant)) {
			return false;
		}
		AuthorisationGrant o = (AuthorisationGrant) obj;
		return Objects.equals(createdAt, o.createdAt)
				&& Objects.equals(createdByOauthClientId, o.createdByOauthClientId)
				&& Objects.equals(revokedByOauthClientId, o.revokedByOauthClientId)
				&& Objects.equals(revoked, o.revoked) && Objects.equals(revokedAt, o.revokedAt);
	}

	@Override
	public String toString() {
		return "AuthorisationGrant [createdAt=" + createdAt + ", createdByOauthClientId=" + createdByOauthClientId
				+ ", revokedByOauthClientId=" + revokedByOauthClientId + ", revoked=" + revoked + ", revokedAt="
				+ revokedAt + "]";
	}

}
