package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Access Control object contains lists of users and clients who have read or
 * write access to other entities.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessControl implements Serializable {

	private String id;
	private String title;
	private String description;
	private Set<String> usage;
	private List<String> userIdRead;
	private List<String> userIdWrite;
	private List<String> groupIdRead;
	private List<String> groupIdWrite;
	private List<String> apiClientIdRead;
	private List<String> apiClientIdWrite;

	/**
	 * @return access control unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id access control unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return Short title for object
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title Short title for object
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Description for object
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description Description for object
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Usages for this access control. Does not prevent other uses, is used
	 *         as a hint in UIs if this applies to a specific purpose.
	 */
	public Set<String> getUsage() {
		if (usage == null) {
			usage = new HashSet<>();
		}
		return usage;
	}

	/**
	 * @param usage Usages for this access control. Does not prevent other uses, is
	 *              used as a hint in UIs if this applies to a specific purpose.
	 */
	public void setUsage(Set<String> usage) {
		this.usage = usage;
	}

	/**
	 * @return IDs of users who have read-only access
	 */
	public List<String> getUserIdRead() {
		if (userIdRead == null) {
			userIdRead = new ArrayList<>();
		}
		return userIdRead;
	}

	/**
	 * @param userIdRead IDs of users who have read-only access
	 */
	public void setUserIdRead(List<String> userIdRead) {
		this.userIdRead = userIdRead;
	}

	/**
	 * @return IDs of users who have read+write access
	 */
	public List<String> getUserIdWrite() {
		if (userIdWrite == null) {
			userIdWrite = new ArrayList<>();
		}
		return userIdWrite;
	}

	/**
	 * @param userIdWrite IDs of users who have read+write access
	 */
	public void setUserIdWrite(List<String> userIdWrite) {
		this.userIdWrite = userIdWrite;
	}

	/**
	 * @return IDs of user groups whose members have read-only access
	 */
	public List<String> getGroupIdRead() {
		if (groupIdRead == null) {
			groupIdRead = new ArrayList<>();
		}
		return groupIdRead;
	}

	/**
	 * @param groupIdRead IDs of user groups whose members have read-only access
	 */
	public void setGroupIdRead(List<String> groupIdRead) {
		this.groupIdRead = groupIdRead;
	}

	/**
	 * @return IDs of user groups whose members have read+write access
	 */
	public List<String> getGroupIdWrite() {
		if (groupIdWrite == null) {
			groupIdWrite = new ArrayList<>();
		}
		return groupIdWrite;
	}

	/**
	 * @param groupIdWrite IDs of user groups whose members have read+write access
	 */
	public void setGroupIdWrite(List<String> groupIdWrite) {
		this.groupIdWrite = groupIdWrite;
	}

	/**
	 * @return Client IDs of Management API Clients who have read-only access
	 */
	public List<String> getApiClientIdRead() {
		if (apiClientIdRead == null) {
			apiClientIdRead = new ArrayList<>();
		}
		return apiClientIdRead;
	}

	/**
	 * @param apiClientIdRead Client IDs of Management API Clients who have
	 *                        read-only access
	 */
	public void setApiClientIdRead(List<String> apiClientIdRead) {
		this.apiClientIdRead = apiClientIdRead;
	}

	/**
	 * @return Client IDs of Management API Clients who have read+write access
	 */
	public List<String> getApiClientIdWrite() {
		if (apiClientIdWrite == null) {
			apiClientIdWrite = new ArrayList<>();
		}
		return apiClientIdWrite;
	}

	/**
	 * @param apiClientIdWrite Client IDs of Management API Clients who have
	 *                         read+write access
	 */
	public void setApiClientIdWrite(List<String> apiClientIdWrite) {
		this.apiClientIdWrite = apiClientIdWrite;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof AccessControl)) {
			return false;
		}
		AccessControl o = (AccessControl) other;
		if (o.getId() == null && getId() == null) {
			return super.equals(other);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "AccessControl [id=" + id + ", title=" + title + ", description=" + description + ", usage=" + usage
				+ ", userIdRead=" + userIdRead + ", userIdWrite=" + userIdWrite + ", groupIdRead=" + groupIdRead
				+ ", groupIdWrite=" + groupIdWrite + ", apiClientIdRead=" + apiClientIdRead + ", apiClientIdWrite="
				+ apiClientIdWrite + "]";
	}

}
