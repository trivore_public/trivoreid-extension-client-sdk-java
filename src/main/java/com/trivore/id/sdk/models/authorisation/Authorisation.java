package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.Meta;

/**
 * An object which presents a single authorisation in Trivore ID.
 * <p>
 * Used to specify the authorisation Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Authorisation implements Serializable {

	private String id;
	private String author;
	private String nsCode;
	private String authType;
	private String authSource;
	private AuthorisationObject object;
	private AuthorisationSubject subject;
	private String validFrom;
	private String validTo;
	private AuthorisationCreator creator;
	private String createdAt;
	private boolean revoked;
	private String revokedAt;
	private Meta meta;

	/**
	 * Construct a new authorisation object.
	 */
	public Authorisation() {
		// ...
	}

	/**
	 * Get the authorisation unique identifier.
	 *
	 * @return unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the authorisation unique identifier.
	 *
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the author.
	 * <p>
	 * Author of the authorization, a free-form string that the API caller is
	 * responsible for maintaining. Not used by server itself.
	 * </p>
	 *
	 * @return author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Set the author.
	 * <p>
	 * Author of the authorization, a free-form string that the API caller is
	 * responsible for maintaining. Not used by the server itself.
	 * </p>
	 *
	 * @param author author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Get the namespace code.
	 * <p>
	 * The namespace, to which the authorization belongs to. Optional - primary
	 * namespace is used if value is not specified. Must be one one of the
	 * accessible namespaces.
	 * </p>
	 *
	 * @return namespace code
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set the namespace code.
	 * <p>
	 * The namespace, to which the authorization belongs to. Optional - primary
	 * namespace is used if value is not specified. Must be one one of the
	 * accessible namespaces.
	 * </p>
	 *
	 * @param nsCode namespace code
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * Get the authorisation type.
	 * <p>
	 * The type of the authorisation. Must be an existing type in the same namespace
	 * as the authorisation. See the authorisation_type endpoint for available
	 * types.
	 * </p>
	 *
	 * @return authorisation type
	 */
	public String getAuthType() {
		return authType;
	}

	/**
	 * Set the authorisation type.
	 * <p>
	 * The type of the authorisation. Must be an existing type in the same namespace
	 * as the authorisation. See the authorisation_type endpoint for available
	 * types.
	 * </p>
	 *
	 * @param authType authorisation type
	 */
	public void setAuthType(String authType) {
		this.authType = authType;
	}

	/**
	 * Get the authorisation source.
	 * <p>
	 * Optional external source of the authorisation. Must be one of the
	 * authorisation sources in the namespace. See the authorisation_source endpoint
	 * for a list of allowed sources.
	 * </p>
	 *
	 * @return authorisation source
	 */
	public String getAuthSource() {
		return authSource;
	}

	/**
	 * Set the authorisation source.
	 * <p>
	 * Optional external source of the authorisation. Must be one of the
	 * authorisation sources in the namespace. See the authorisation_source endpoint
	 * for a list of allowed sources.
	 * </p>
	 *
	 * @param authSource authorisation source
	 */
	public void setAuthSource(String authSource) {
		this.authSource = authSource;
	}

	/**
	 * Get the object of authorisation.
	 * <p>
	 * The object of the authorisation. Usually tells the entity on whose behalf the
	 * subject entity is allowed to do something.
	 * </p>
	 *
	 * @return object of authorisation
	 */
	public AuthorisationObject getObject() {
		if (object == null) {
			object = new AuthorisationObject();
		}
		return object;
	}

	/**
	 * Set the object of authorisation.
	 * <p>
	 * The object of the authorisation. Usually tells the entity on whose behalf the
	 * subject entity is allowed to do something.
	 * </p>
	 *
	 * @param object object of authorisation
	 */
	public void setObject(AuthorisationObject object) {
		this.object = object;
	}

	/**
	 * Get the subject of authorisation.
	 * <p>
	 * The subject of the authorisation. Usually tells who will be authorised to do
	 * something or act on the object's behalf.
	 * </p>
	 *
	 * @return subject of authorisation
	 */
	public AuthorisationSubject getSubject() {
		if (subject == null) {
			subject = new AuthorisationSubject();
		}
		return subject;
	}

	/**
	 * Set the subject of authorisation.
	 * <p>
	 * The subject of the authorisation. Usually tells who will be authorised to do
	 * something or act on the object's behalf.
	 * </p>
	 *
	 * @param subject subject of authorisation
	 */
	public void setSubject(AuthorisationSubject subject) {
		this.subject = subject;
	}

	/**
	 * Get the valid from.
	 * <p>
	 * Valid From time in ISO 8601 format in UTC timezone.
	 * </p>
	 *
	 * @return valid from
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * Set the valid from.
	 * <p>
	 * Valid From time in ISO 8601 format in UTC timezone.
	 * </p>
	 *
	 * @param validFrom valid from
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * Get the valid to.
	 * <p>
	 * Valid To time in ISO 8601 format in UTC timezone.
	 * </p>
	 *
	 * @return valid to
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Set the valid to.
	 * <p>
	 * Valid To time in ISO 8601 format in UTC timezone.
	 * </p>
	 *
	 * @param validTo valid to
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Get the creator.
	 * <p>
	 * Information on the creator of the authorisation entry. Readonly.
	 * </p>
	 *
	 * @return creator
	 */
	public AuthorisationCreator getCreator() {
		if (creator == null) {
			creator = new AuthorisationCreator();
		}
		return creator;
	}

	/**
	 * Set the creator.
	 * <p>
	 * Information on the creator of the authorisation entry. Readonly.
	 * </p>
	 *
	 * @param creator creator
	 */
	public void setCreator(AuthorisationCreator creator) {
		this.creator = creator;
	}

	/**
	 * Get the created at.
	 * <p>
	 * Authorisation created at ISO datetime.
	 * </p>
	 *
	 * @return created at
	 */
	public String getCreatedAt() {
		return createdAt;
	}

	/**
	 * Set the created at.
	 * <p>
	 * Authorisation created at ISO datetime.
	 * </p>
	 *
	 * @param createdAt created at
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Get the revoke status.
	 * <p>
	 * True if the authorisation has been revoked. The authorisation can be revoked
	 * at the revoke endpoint.
	 * </p>
	 *
	 * @return revoke status
	 */
	public boolean isRevoked() {
		return revoked;
	}

	/**
	 * Set the revoke status.
	 * <p>
	 * True if the authorisation has been revoked. The authorisation can be revoked
	 * at the revoke endpoint.
	 * </p>
	 *
	 * @param revoked revoke status
	 */
	public void setRevoked(boolean revoked) {
		this.revoked = revoked;
	}

	/**
	 * Get the revoked at.
	 * <p>
	 * Authorisation revoked at ISO datetime.
	 * </p>
	 *
	 * @return revoked at
	 */
	public String getRevokedAt() {
		return revokedAt;
	}

	/**
	 * Set the revoked at.
	 * <p>
	 * Authorisation revoked at ISO datetime.
	 * </p>
	 *
	 * @param revokedAt revoked at
	 */
	public void setRevokedAt(String revokedAt) {
		this.revokedAt = revokedAt;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Authorisation)) {
			return false;
		}
		Authorisation o = (Authorisation) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Authorisation [id=" + id + ", author=" + author + ", nsCode=" + nsCode + ", authType=" + authType
				+ ", authSource=" + authSource + ", object=" + object + ", subject=" + subject + ", validFrom="
				+ validFrom + ", validTo=" + validTo + ", creator=" + creator + ", createdAt=" + createdAt
				+ ", revoked=" + revoked + ", revokedAt=" + revokedAt + "]";
	}

}
