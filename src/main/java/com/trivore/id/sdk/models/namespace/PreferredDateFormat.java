package com.trivore.id.sdk.models.namespace;

/**
 * An enumeration of the preferred date format.
 * <p>
 * These values are used with {@link GroupPolicy} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum PreferredDateFormat {
	INHERIT, BROWSER, YYYY_MM_DD, D_M_YYYY, D_MON_YYYY
}
