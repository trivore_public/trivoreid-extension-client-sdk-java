package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single builtin role in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID builtin role definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomRole implements Serializable {

	private String id;
	private String name;
	private List<String> permissions;
	private String nsCode;
	private List<String> memberOf;
	private Meta meta;

	/**
	 * @return role unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id role unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return english language name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name english language name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return permissions granted by having this role
	 */
	public List<String> getPermissions() {
		if (permissions == null) {
			permissions = new ArrayList<>();
		}
		return permissions;
	}

	/**
	 * @param permissions permissions granted by having this role
	 */
	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}

	/**
	 * @return code of the namespace this object belongs to
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * @param nsCode code of the namespace this object belongs to
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * @return IDs of the groups this role belongs to
	 */
	public List<String> getMemberOf() {
		if (memberOf == null) {
			memberOf = new ArrayList<>();
		}
		return memberOf;
	}

	/**
	 * @param memberOf IDs of the groups this role belongs to
	 */
	public void setMemberOf(List<String> memberOf) {
		this.memberOf = memberOf;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof CustomRole)) {
			return false;
		}
		CustomRole o = (CustomRole) object;
		if (o.getId() == null && getId() == null) {
			return super.equals(object);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

}
