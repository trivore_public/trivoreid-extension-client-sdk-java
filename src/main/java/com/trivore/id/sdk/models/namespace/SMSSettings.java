package com.trivore.id.sdk.models.namespace;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a namespace's SMS settings in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SMSSettings implements Serializable {

	private boolean enabled;
	private boolean customEnabled;
	private String defaultOriginator;
	private String defaultRegion;

	/**
	 * Get if SMS settings are enabled.
	 * <p>
	 * SMS sending enabled for the namespace.
	 * </p>
	 *
	 * @return if SMS settings are enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Set if SMS settings are enabled.
	 * <p>
	 * SMS sending enabled for the namespace.
	 * </p>
	 *
	 * @param enabled if SMS settings are enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Get if custom SMS settings are enabled.
	 * <p>
	 * Custom SMS sending enabled for the namespace. Custom SMS messages may be sent
	 * via the REST API or the Web UI.
	 * </p>
	 *
	 * @return if custom SMS settings are enabled
	 */
	public boolean isCustomEnabled() {
		return customEnabled;
	}

	/**
	 * Set if custom SMS settings are enabled.
	 * <p>
	 * Custom SMS sending enabled for the namespace. Custom SMS messages may be sent
	 * via the REST API or the Web UI.
	 * </p>
	 *
	 * @param customEnabled if custom SMS settings are enabled
	 */
	public void setCustomEnabled(boolean customEnabled) {
		this.customEnabled = customEnabled;
	}

	/**
	 * Get the default originator.
	 * <p>
	 * Alphanumeric Default originator for the SMS messages.
	 * </p>
	 *
	 * @return the default originator
	 */
	public String getDefaultOriginator() {
		return defaultOriginator;
	}

	/**
	 * Set the default originator.
	 * <p>
	 * Alphanumeric Default originator for the SMS messages.
	 * </p>
	 *
	 * @param defaultOriginator the default originator
	 */
	public void setDefaultOriginator(String defaultOriginator) {
		this.defaultOriginator = defaultOriginator;
	}

	/**
	 * Get the default region.
	 * <p>
	 * Default region for parsing and validation. Represented by a two letter
	 * country code, eg. FI.
	 * </p>
	 *
	 * @return the default region
	 */
	public String getDefaultRegion() {
		return defaultRegion;
	}

	/**
	 * Set the default region.
	 * <p>
	 * Default region for parsing and validation. Represented by a two letter
	 * country code, eg. FI.
	 * </p>
	 *
	 * @param defaultRegion the default region
	 */
	public void setDefaultRegion(String defaultRegion) {
		this.defaultRegion = defaultRegion;
	}

	@Override
	public int hashCode() {
		return Objects.hash(enabled, customEnabled, defaultOriginator, defaultRegion);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof SMSSettings)) {
			return false;
		}
		SMSSettings o = (SMSSettings) object;
		return Objects.equals(enabled, o.enabled) && Objects.equals(customEnabled, o.customEnabled)
				&& Objects.equals(defaultOriginator, o.defaultOriginator)
				&& Objects.equals(defaultRegion, o.defaultRegion);
	}

	@Override
	public String toString() {
		return "SMSSettings [enabled=" + enabled + ", customEnabled=" + customEnabled + ", defaultOriginator="
				+ defaultOriginator + ", defaultRegion=" + defaultRegion + "]";
	}
}
