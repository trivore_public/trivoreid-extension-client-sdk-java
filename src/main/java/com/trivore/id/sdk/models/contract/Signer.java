package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents signer in Trivore ID.
 * <p>
 * People who are required to sign for this contract party.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Signer implements Serializable {

	private String id;
	private String userId;
	private String externalId;
	private String name;
	private String address;
	private String mobile;
	private String email;
	private Signature signature;
	private SignerTermination termination;

	/**
	 * Construct a new signer object.
	 */
	public Signer() {
		// ...
	}

	/**
	 * Get unique identifier.
	 *
	 * @return unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set unique identifier.
	 *
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get user id.
	 *
	 * @return user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Set user id.
	 *
	 * @param userId user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Get external id.
	 *
	 * @return external id
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * Set external id.
	 *
	 * @param externalId external id
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * Get name.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set name.
	 *
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get address.
	 *
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Set address.
	 *
	 * @param address address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Get mobile.
	 *
	 * @return mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Set mobile.
	 *
	 * @param mobile mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Get email.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set email.
	 *
	 * @param email email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get signature.
	 *
	 * @return signature
	 */
	public Signature getSignature() {
		if (signature == null) {
			signature = new Signature();
		}
		return signature;
	}

	/**
	 * Set signature.
	 *
	 * @param signature signature
	 */
	public void setSignature(Signature signature) {
		this.signature = signature;
	}

	/**
	 * Get termination.
	 *
	 * @return termination
	 */
	public SignerTermination getTermination() {
		if (termination == null) {
			termination = new SignerTermination();
		}
		return termination;
	}

	/**
	 * Set termination.
	 *
	 * @param termination termination
	 */
	public void setTermination(SignerTermination termination) {
		this.termination = termination;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Signer)) {
			return false;
		}
		Signer o = (Signer) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Signer [id=" + id + ", userId=" + userId + ", externalId=" + externalId + ", name=" + name
				+ ", address=" + address + ", mobile=" + mobile + ", email=" + email + ", signature=" + signature
				+ ", termination=" + termination + "]";
	}

}
