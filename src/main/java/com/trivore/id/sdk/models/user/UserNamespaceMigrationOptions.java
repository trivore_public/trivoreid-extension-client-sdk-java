package com.trivore.id.sdk.models.user;

import java.util.Objects;

/**
 * An object which represents user namespace migration options in Trivore ID.
 * <p>
 * Used to specify user namespace migration options in Trivore ID service.
 * </p>
 */
public class UserNamespaceMigrationOptions {

	private String targetNsCode;
	private Boolean keepUserId;
	private Boolean keepProfile;
	private Boolean keepEmails;
	private Boolean keepMobiles;
	private Boolean keepConsents;
	private Boolean keepCustomFields;
	private Boolean keepPassword;
	private Boolean keepGroups;
	private String username;
	private String userPassword;

	/**
	 * Construct user namespace migration options.
	 */
	public UserNamespaceMigrationOptions() {
		// ...
	}

	/**
	 * Get target namespace's code.
	 *
	 * @return the target namespace's code.
	 */
	public String getTargetNsCode() {
		return targetNsCode;
	}

	/**
	 * Set target namespace's code.
	 *
	 * @param targetNsCode the target namespace's code.
	 */
	public void setTargetNsCode(String targetNsCode) {
		this.targetNsCode = targetNsCode;
	}

	/**
	 * If true, same user ID will be kept. If false, a new user ID is given. Some
	 * internal references to original ID may be deleted during migration even if
	 * same ID is kept.
	 *
	 * @return the keep user id
	 */
	public Boolean isKeepUserId() {
		return keepUserId;
	}

	/**
	 * If true, same user ID will be kept. If false, a new user ID is given. Some
	 * internal references to original ID may be deleted during migration even if
	 * same ID is kept.
	 *
	 * @param keepUserId the keep user id
	 */
	public void setKeepUserId(Boolean keepUserId) {
		this.keepUserId = keepUserId;
	}

	/**
	 * If true: names, street addresses, legalinfo, customergroupinfo, DoB, and
	 * other profile data will be migrated.
	 *
	 * @return the keep profile
	 */
	public Boolean isKeepProfile() {
		return keepProfile;
	}

	/**
	 * If true: names, street addresses, legalinfo, customergroupinfo, DoB, and
	 * other profile data will be migrated.
	 *
	 * @param keepProfile the keep profile
	 */
	public void setKeepProfile(Boolean keepProfile) {
		this.keepProfile = keepProfile;
	}

	/**
	 * If true, email addresses and verification status will be migrated.
	 *
	 * @return the keep emails
	 */
	public Boolean isKeepEmails() {
		return keepEmails;
	}

	/**
	 * If true, email addresses and verification status will be migrated.
	 *
	 * @param keepEmails the keep emails
	 */
	public void setKeepEmails(Boolean keepEmails) {
		this.keepEmails = keepEmails;
	}

	/**
	 * If true, mobile numbers and verification status will be migrated.
	 *
	 * @return the keep mobiles
	 */
	public Boolean isKeepMobiles() {
		return keepMobiles;
	}

	/**
	 * If true, mobile numbers and verification status will be migrated.
	 *
	 * @param keepMobiles the keep mobiles
	 */
	public void setKeepMobiles(Boolean keepMobiles) {
		this.keepMobiles = keepMobiles;
	}

	/**
	 * If true, user consents will be migrated.
	 *
	 * @return the keep consents
	 */
	public Boolean isKeepConsents() {
		return keepConsents;
	}

	/**
	 * If true, user consents will be migrated.
	 *
	 * @param keepConsents the keep consents
	 */
	public void setKeepConsents(Boolean keepConsents) {
		this.keepConsents = keepConsents;
	}

	/**
	 * If true, custom fields will be migrated.
	 *
	 * @return the keep custom fields
	 */
	public Boolean isKeepCustomFields() {
		return keepCustomFields;
	}

	/**
	 * If true, custom fields will be migrated.
	 *
	 * @param keepCustomFields the keep custom fields
	 */
	public void setKeepCustomFields(Boolean keepCustomFields) {
		this.keepCustomFields = keepCustomFields;
	}

	/**
	 * If true: password will be migrated unless new password is specified.
	 *
	 * @return the keep password
	 */
	public Boolean isKeepPassword() {
		return keepPassword;
	}

	/**
	 * If true: password will be migrated unless new password is specified.
	 *
	 * @param keepPassword the keep password
	 */
	public void setKeepPassword(Boolean keepPassword) {
		this.keepPassword = keepPassword;
	}

	/**
	 * If true: user groups will be migrated based on group names.
	 *
	 * @return the keep groups
	 */
	public Boolean isKeepGroups() {
		return keepGroups;
	}

	/**
	 * If true: user groups will be migrated based on group names.
	 *
	 * @param keepGroups the keep groups
	 */
	public void setKeepGroups(Boolean keepGroups) {
		this.keepGroups = keepGroups;
	}

	/**
	 * Username after migration. If null, username is autogenerated. Value must be
	 * unique in new namespace.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Username after migration. If null, username is autogenerated. Value must be
	 * unique in new namespace.
	 *
	 * @param username the username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Password for new migrated user.
	 *
	 * @return the user password
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Password for new migrated user.
	 *
	 * @param userPassword the user password
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	@Override
	public int hashCode() {
		return Objects.hash(targetNsCode, keepUserId, keepProfile, keepEmails, keepMobiles, keepConsents,
				keepCustomFields, keepPassword, keepGroups, username, userPassword);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof UserNamespaceMigrationOptions)) {
			return false;
		}
		UserNamespaceMigrationOptions o = (UserNamespaceMigrationOptions) obj;
		return Objects.equals(targetNsCode, o.targetNsCode) //
				&& Objects.equals(keepUserId, o.keepUserId) //
				&& Objects.equals(keepProfile, o.keepProfile) //
				&& Objects.equals(keepEmails, o.keepEmails) //
				&& Objects.equals(keepMobiles, o.keepMobiles) //
				&& Objects.equals(keepConsents, o.keepConsents) //
				&& Objects.equals(keepCustomFields, o.keepCustomFields) //
				&& Objects.equals(keepPassword, o.keepPassword) //
				&& Objects.equals(keepGroups, o.keepGroups) //
				&& Objects.equals(username, o.username) //
				&& Objects.equals(userPassword, o.userPassword);
	}

	@Override
	public String toString() {
		return "UserNamespaceMigrationOptions [targetNsCode=" + targetNsCode + ", keepUserId=" + keepUserId
				+ ", keepProfile=" + keepProfile + ", keepEmails=" + keepEmails + ", keepMobiles=" + keepMobiles
				+ ", keepConsents=" + keepConsents + ", keepCustomFields=" + keepCustomFields + ", keepPassword="
				+ keepPassword + ", keepGroups=" + keepGroups + ", username=" + username + ", userPassword="
				+ userPassword + "]";
	}

}
