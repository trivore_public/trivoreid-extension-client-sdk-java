package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.user.AccountType;
import com.trivore.id.sdk.models.user.Address;

/**
 * An object which presents a single contact in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID contact definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact implements Serializable {

	private String id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String nickName;
	private String organisation;
	private String uniqueName;
	private List<String> memberOf;
	private String nsCode;
	private String email;
	private String mobile;
	private String locationSite;
	private String dataStoreCountry;
	private String timeZone;
	private String notes;
	private String recoveryEmail;
	private String recoveryMobile;
	private List<Address> addresses;
	private String locale;
	private AccountType type;
	private Meta meta;

	/**
	 * @return contact unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id contact unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return middle name
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName middle name
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName nickName
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @return organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation organisation
	 */
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return uniqueName
	 */
	public String getUniqueName() {
		return uniqueName;
	}

	/**
	 * @param uniqueName unique name
	 */
	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}

	/**
	 * @return the list of group IDs
	 */
	public List<String> getMemberOf() {
		if (memberOf == null) {
			memberOf = new ArrayList<>();
		}
		return memberOf;
	}

	/**
	 * @param memberOf list of group IDs
	 */
	public void setMemberOf(List<String> memberOf) {
		this.memberOf = memberOf;
	}

	/**
	 * @return namespace code
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * @param nsCode namespace code
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email e-mail
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return phone number
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile phone number
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return location/site
	 */
	public String getLocationSite() {
		return locationSite;
	}

	/**
	 * @param locationSite location/site
	 */
	public void setLocationSite(String locationSite) {
		this.locationSite = locationSite;
	}

	/**
	 * @return data store country
	 */
	public String getDataStoreCountry() {
		return dataStoreCountry;
	}

	/**
	 * @param dataStoreCountry data store country
	 */
	public void setDataStoreCountry(String dataStoreCountry) {
		this.dataStoreCountry = dataStoreCountry;
	}

	/**
	 * @return time zone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone time zone
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return recovery e-mail
	 */
	public String getRecoveryEmail() {
		return recoveryEmail;
	}

	/**
	 * @param recoveryEmail recovery e-mail
	 */
	public void setRecoveryEmail(String recoveryEmail) {
		this.recoveryEmail = recoveryEmail;
	}

	/**
	 * @return recovery phone number
	 */
	public String getRecoveryMobile() {
		return recoveryMobile;
	}

	/**
	 * @param recoveryMobile recovery phone number
	 */
	public void setRecoveryMobile(String recoveryMobile) {
		this.recoveryMobile = recoveryMobile;
	}

	/**
	 * @return list of addresses
	 */
	public List<Address> getAddresses() {
		if (addresses == null) {
			addresses = new ArrayList<>();
		}
		return addresses;
	}

	/**
	 * @param addresses list of addresses
	 */
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	/**
	 * @return locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return account type
	 */
	public AccountType getType() {
		return type;
	}

	/**
	 * @param type account type
	 */
	public void setType(AccountType type) {
		this.type = type;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Contact)) {
			return false;
		}
		Contact o = (Contact) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", nickName=" + nickName + ", organisation=" + organisation + ", uniqueName=" + uniqueName
				+ ", nsCode=" + nsCode + ", email=" + email + ", mobile=" + mobile + ", locationSite=" + locationSite
				+ ", dataStoreCountry=" + dataStoreCountry + ", timeZone=" + timeZone + ", notes=" + notes
				+ ", recoveryEmail=" + recoveryEmail + ", recoveryMobile=" + recoveryMobile + ", addresses=" + addresses
				+ ", locale=" + locale + ", type=" + type + "]";
	}

}
