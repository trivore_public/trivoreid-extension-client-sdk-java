package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single group in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID group definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Group implements Serializable {

	private String id;
	private String name;
	private String nsCode;
	private String description;
	private Meta meta;

	/**
	 * Construct a new group.
	 */
	public Group() {
		// ...
	}

	/**
	 * Get the group's unique identifier.
	 *
	 * @return The identifier of the group.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set a unique identifier for the group.
	 *
	 * @param id An identifier for the group.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the display name of the group.
	 * <p>
	 * The display name must be unique within a namespace.
	 * </p>
	 *
	 * @return The group's display name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set a display name for the group.
	 * <p>
	 * The display name must be unique within a namespace.
	 * </p>
	 *
	 * @param name A display name for the group.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the identifier of the group's namespace (customer).
	 * <p>
	 * When null, this group belongs to current user's namespace.
	 * </p>
	 *
	 * @return The identifier of the owner namespace.
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set the identifier of the group's namespace (customer).
	 * <p>
	 * When null, this group belongs to current user's namespace.
	 * </p>
	 *
	 * @param nsCode The identifier of the owner namespace.
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * Get group description.
	 *
	 * @return group description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set group description.
	 *
	 * @param description group description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof Group)) {
			return false;
		}
		Group o = (Group) object;
		return Objects.equals(name, o.name) && Objects.equals(nsCode, o.nsCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, nsCode);
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", nsCode=" + nsCode + ", description=" + description + "]";
	}

}
