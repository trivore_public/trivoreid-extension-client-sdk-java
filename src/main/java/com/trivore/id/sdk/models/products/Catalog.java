package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single catalog in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID catalog definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Catalog implements Serializable {

	private String id;
	private String ownerId;
	private List<String> accessControlIds;
	private String name;
	private List<CatalogItem> products;
	private Map<String, Object> customFields;

	/**
	 * @return id unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @param ownerId ownerunique identifier
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @return list of access control IDs
	 */
	public List<String> getAccessControlIds() {
		if (accessControlIds == null) {
			accessControlIds = new ArrayList<>();
		}
		return accessControlIds;
	}

	/**
	 * @param accessControlIds list of access control IDs
	 */
	public void setAccessControlIds(List<String> accessControlIds) {
		this.accessControlIds = accessControlIds;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return list of products
	 */
	public List<CatalogItem> getProducts() {
		if (products == null) {
			products = new ArrayList<>();
		}
		return products;
	}

	/**
	 * @param products list of products
	 */
	public void setProducts(List<CatalogItem> products) {
		this.products = products;
	}

	/**
	 * @return custom fields
	 */
	public Map<String, Object> getCustomFields() {
		if (customFields == null) {
			customFields = new HashMap<>();
		}
		return customFields;
	}

	/**
	 * @param customFields custom fields
	 */
	public void setCustomFields(Map<String, Object> customFields) {
		this.customFields = customFields;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, ownerId, accessControlIds, name, products, customFields);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Catalog)) {
			return false;
		}
		Catalog o = (Catalog) obj;
		return Objects.equals(id, o.id) //
				&& Objects.equals(ownerId, o.ownerId) //
				&& Objects.equals(accessControlIds, o.accessControlIds) //
				&& Objects.equals(name, o.name) //
				&& Objects.equals(products, o.products) //
				&& Objects.equals(customFields, o.customFields);
	}

	@Override
	public String toString() {
		return "Catalog [id=" + id + ", ownerId=" + ownerId + ", accessControlIds=" + accessControlIds + ", name="
				+ name + ", products=" + products + ", customFields=" + customFields + "]";
	}

}
