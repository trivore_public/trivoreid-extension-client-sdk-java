package com.trivore.id.sdk.models.contract;

/**
 * Contract will terminate after this rule is met.
 */
@SuppressWarnings("javadoc")
public enum TerminationMode {
	AFTER_SINGLE_PARTY_LEFT, AFTER_SINGLE_PARTY_TERMINATES, AFTER_ALL_PARTIES_TERMINATE
}
