package com.trivore.id.sdk.models.contract;

/**
 * Party termination mode.
 */
@SuppressWarnings("javadoc")
public enum PartyTerminationMode {
	AFTER_ALL_SIGNERS_TERMINATE, AFTER_SINGLE_SIGNER_TERMINATES
}
