package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single location/site in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID location/site definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationSite implements Serializable {

	private String id;
	private String name;
	private String description;
	private String country;
	private String state;
	private String city;
	private String postal;
	private String street;
	private String building;
	private String room;
	private String uri;
	private String phone;
	private List<String> subnets;
	private String parent;
	private List<Coordinate> coordinates;
	private String geoLatitude;
	private String geoLongitude;
	private String inRoomPosition;
	private String contact;
	private String group;
	private String nsCode;

	/**
	 * @return unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return postal
	 */
	public String getPostal() {
		return postal;
	}

	/**
	 * @param postal postal
	 */
	public void setPostal(String postal) {
		this.postal = postal;
	}

	/**
	 * @return street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return building
	 */
	public String getBuilding() {
		return building;
	}

	/**
	 * @param building building
	 */
	public void setBuilding(String building) {
		this.building = building;
	}

	/**
	 * @return room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * @param room room
	 */
	public void setRoom(String room) {
		this.room = room;
	}

	/**
	 * @return uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param uri uri
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return phone number
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return list of subnets
	 */
	public List<String> getSubnets() {
		return subnets;
	}

	/**
	 * @param subnets list of subnets
	 */
	public void setSubnets(List<String> subnets) {
		this.subnets = subnets;
	}

	/**
	 * @return parent
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * @param parent parent
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * @return coordinates
	 */
	public List<Coordinate> getCoordinates() {
		return coordinates;
	}

	/**
	 * @param coordinates coordinates
	 */
	public void setCoordinates(List<Coordinate> coordinates) {
		this.coordinates = coordinates;
	}

	/**
	 * @return geo latitude
	 */
	public String getGeoLatitude() {
		return geoLatitude;
	}

	/**
	 * @param geoLatitude geo latitude
	 */
	public void setGeoLatitude(String geoLatitude) {
		this.geoLatitude = geoLatitude;
	}

	/**
	 * @return geo longitude
	 */
	public String getGeoLongitude() {
		return geoLongitude;
	}

	/**
	 * @param geoLongitude geo longitude
	 */
	public void setGeoLongitude(String geoLongitude) {
		this.geoLongitude = geoLongitude;
	}

	/**
	 * @return in room position
	 */
	public String getInRoomPosition() {
		return inRoomPosition;
	}

	/**
	 * @param inRoomPosition in room position
	 */
	public void setInRoomPosition(String inRoomPosition) {
		this.inRoomPosition = inRoomPosition;
	}

	/**
	 * @return contact
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact contact
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group group
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return namespace code
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * @param nsCode namespace code
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof LocationSite)) {
			return false;
		}
		LocationSite o = (LocationSite) other;
		if (o.getId() == null && getId() == null) {
			return super.equals(other);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "LocationSite [id=" + id + ", name=" + name + ", description=" + description + ", country=" + country
				+ ", state=" + state + ", city=" + city + ", postal=" + postal + ", street=" + street + ", building="
				+ building + ", room=" + room + ", uri=" + uri + ", phone=" + phone + ", subnets=" + subnets
				+ ", parent=" + parent + ", coordinates=" + coordinates + ", geoLatitude=" + geoLatitude
				+ ", geoLongitude=" + geoLongitude + ", inRoomPosition=" + inRoomPosition + ", contact=" + contact
				+ ", group=" + group + ", nsCode=" + nsCode + "]";
	}

}
