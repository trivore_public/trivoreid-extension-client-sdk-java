package com.trivore.id.sdk.models.user;

/**
 * Enum representing the juridical sex of the user.
 */
@SuppressWarnings("javadoc")
public enum Sex {
	FEMALE, MALE, OTHER, UNDISCLOSED
}
