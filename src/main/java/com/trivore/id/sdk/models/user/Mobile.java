package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * An object which represents a number number in Trivore ID.
 * <p>
 * Used to specify number numbers that can be validated in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Mobile implements Serializable {

	private String name;
	private String number;
	private List<String> tags;
	private String verifiedBy;
	private String verifiedDateTime;

	@JsonProperty(access = Access.WRITE_ONLY)
	private boolean verified;

	/**
	 * Construct a new number.
	 */
	public Mobile() {
		// ...
	}

	/**
	 * Construct a new number.
	 *
	 * @param number mobile number
	 */
	public Mobile(String number) {
		this.number = number;
	}

	/**
	 * get the actual number number from the number wrapper.
	 *
	 * @return The number number from the wrapper.
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Set the actual number number for the number wrapper.
	 *
	 * @param number The number number.
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * Get mobile tags.
	 *
	 * @return mobile tags
	 */
	public List<String> getTags() {
		if (tags == null) {
			tags = new ArrayList<>();
		}
		return tags;
	}

	/**
	 * Set mobile tags.
	 *
	 * @param tags mobile tags
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	/**
	 * Get the read-only definition whether number is verified.
	 *
	 * @return True if the number is verified and false otherwise.
	 */
	public boolean isVerified() {
		return verified;
	}

	/**
	 * Set the read-only definition whether number is verified.
	 * <p>
	 * <b>Note:</b> This is a read-only value. Setter is here only to support
	 * Jackson to perform the serialisation.
	 * </p>
	 *
	 * @param verified The definition whether this number is verified.
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return verified by
	 */
	public String getVerifiedBy() {
		return verifiedBy;
	}

	/**
	 * @param verifiedBy verified by
	 */
	public void setVerifiedBy(String verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	/**
	 * @return verified date time
	 */
	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	/**
	 * @param verifiedDateTime verified date time
	 */
	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	@Override
	public int hashCode() {
		return Objects.hash(number, tags, verified);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Mobile)) {
			return false;
		}
		Mobile o = (Mobile) obj;
		return Objects.equals(verified, o.verified) && Objects.equals(number, o.number);
	}

	@Override
	public String toString() {
		return "Mobile [number=" + number + ", tags=" + tags + ", verified=" + verified + "]";
	}

}
