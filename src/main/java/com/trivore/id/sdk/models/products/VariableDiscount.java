package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single pricing in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID pricing definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class VariableDiscount implements Serializable {

	private String title;
	private String discountPercentageEval;
	private String discountAmountEval;
	private String discountAmountPerItemEval;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the discountPercentageEval
	 */
	public String getDiscountPercentageEval() {
		return discountPercentageEval;
	}

	/**
	 * @param discountPercentageEval the discountPercentageEval to set
	 */
	public void setDiscountPercentageEval(String discountPercentageEval) {
		this.discountPercentageEval = discountPercentageEval;
	}

	/**
	 * @return the discountAmountEval
	 */
	public String getDiscountAmountEval() {
		return discountAmountEval;
	}

	/**
	 * @param discountAmountEval the discountAmountEval to set
	 */
	public void setDiscountAmountEval(String discountAmountEval) {
		this.discountAmountEval = discountAmountEval;
	}

	/**
	 * @return the discountAmountPerItemEval
	 */
	public String getDiscountAmountPerItemEval() {
		return discountAmountPerItemEval;
	}

	/**
	 * @param discountAmountPerItemEval the discountAmountPerItemEval to set
	 */
	public void setDiscountAmountPerItemEval(String discountAmountPerItemEval) {
		this.discountAmountPerItemEval = discountAmountPerItemEval;
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, discountPercentageEval, discountAmountEval, discountAmountPerItemEval);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof VariableDiscount)) {
			return false;
		}
		VariableDiscount o = (VariableDiscount) obj;
		return Objects.equals(title, o.title) //
				&& Objects.equals(discountPercentageEval, o.discountPercentageEval) //
				&& Objects.equals(discountAmountEval, o.discountAmountEval) //
				&& Objects.equals(discountAmountPerItemEval, o.discountAmountPerItemEval);
	}

	@Override
	public String toString() {
		return "VariableDiscount [title=" + title + ", discountPercentageEval=" + discountPercentageEval
				+ ", discountAmountEval=" + discountAmountEval + ", discountAmountPerItemEval="
				+ discountAmountPerItemEval + "]";
	}

}
