package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single pricing plan in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID pricing plan definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PricingPlan implements Serializable {

	private String id;
	private String ownerId;
	private List<String> accessControlIds;
	private List<Pricing> pricings;
	private Map<String, Object> customFields;
	private Boolean enabled;
	private String title;
	private String description;
	private List<Validity> availability;
	private String publishDate;
	private String unpublishDate;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @return the accessControlIds
	 */
	public List<String> getAccessControlIds() {
		if (accessControlIds == null) {
			accessControlIds = new ArrayList<>();
		}
		return accessControlIds;
	}

	/**
	 * @param accessControlIds the accessControlIds to set
	 */
	public void setAccessControlIds(List<String> accessControlIds) {
		this.accessControlIds = accessControlIds;
	}

	/**
	 * @return the pricings
	 */
	public List<Pricing> getPricings() {
		if (pricings == null) {
			pricings = new ArrayList<>();
		}
		return pricings;
	}

	/**
	 * @param pricings the pricings to set
	 */
	public void setPricings(List<Pricing> pricings) {
		this.pricings = pricings;
	}

	/**
	 * @return the customFields
	 */
	public Map<String, Object> getCustomFields() {
		if (customFields == null) {
			customFields = new HashMap<>();
		}
		return customFields;
	}

	/**
	 * @param customFields the customFields to set
	 */
	public void setCustomFields(Map<String, Object> customFields) {
		this.customFields = customFields;
	}

	/**
	 * @return the enabled
	 */
	public Boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the availability
	 */
	public List<Validity> getAvailability() {
		if (availability == null) {
			availability = new ArrayList<>();
		}
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(List<Validity> availability) {
		this.availability = availability;
	}

	/**
	 * @return the publishDate
	 */
	public String getPublishDate() {
		return publishDate;
	}

	/**
	 * @param publishDate the publishDate to set
	 */
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	/**
	 * @return the unpublishDate
	 */
	public String getUnpublishDate() {
		return unpublishDate;
	}

	/**
	 * @param unpublishDate the unpublishDate to set
	 */
	public void setUnpublishDate(String unpublishDate) {
		this.unpublishDate = unpublishDate;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PricingPlan)) {
			return false;
		}
		PricingPlan o = (PricingPlan) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "PricingPlan [id=" + id + ", ownerId=" + ownerId + ", accessControlIds=" + accessControlIds
				+ ", pricings=" + pricings + ", customFields=" + customFields + ", enabled=" + enabled + ", title="
				+ title + ", description=" + description + ", availability=" + availability + ", publishDate="
				+ publishDate + ", unpublishDate=" + unpublishDate + "]";
	}

}
