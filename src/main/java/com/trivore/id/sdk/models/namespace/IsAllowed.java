package com.trivore.id.sdk.models.namespace;

/**
 * Is allowed enumeration.
 * <p>
 * These values are used with {@link GroupPolicy} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum IsAllowed {
	ALLOWED, DISALLOWED
}
