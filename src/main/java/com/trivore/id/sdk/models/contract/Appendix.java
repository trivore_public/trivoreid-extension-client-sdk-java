package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single appendix in Trivore ID.
 * <p>
 * Used to specify the appendix Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Appendix implements Serializable {

	private String id;
	private String text;
	private String externalFileRef;
	private boolean hasInternalFile;
	private String internalFileType;
	private String title;

	/**
	 * Construct a new appendix object.
	 */
	public Appendix() {
		// ...
	}

	/**
	 * Get id.
	 *
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set id.
	 *
	 * @param id id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get text.
	 *
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set text.
	 *
	 * @param text text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Get external file ref.
	 * <p>
	 * Reference link to a file on an external service.
	 * </p>
	 *
	 * @return external file ref
	 */
	public String getExternalFileRef() {
		return externalFileRef;
	}

	/**
	 * Set external file ref.
	 * <p>
	 * Reference link to a file on an external service.
	 * </p>
	 *
	 * @param externalFileRef external file ref
	 */
	public void setExternalFileRef(String externalFileRef) {
		this.externalFileRef = externalFileRef;
	}

	/**
	 * Get if appendix has internal file.
	 *
	 * @return if appendix has internal file
	 */
	public boolean isHasInternalFile() {
		return hasInternalFile;
	}

	/**
	 * Set if appendix has internal file.
	 *
	 * @param hasInternalFile if appendix has internal file
	 */
	public void setHasInternalFile(boolean hasInternalFile) {
		this.hasInternalFile = hasInternalFile;
	}

	/**
	 * Get internal file type.
	 *
	 * @return internal file type
	 */
	public String getInternalFileType() {
		return internalFileType;
	}

	/**
	 * Set internal file type.
	 *
	 * @param internalFileType internal file type
	 */
	public void setInternalFileType(String internalFileType) {
		this.internalFileType = internalFileType;
	}

	/**
	 * Get title.
	 *
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set title.
	 *
	 * @param title title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Appendix)) {
			return false;
		}
		Appendix o = (Appendix) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Appendix [id=" + id + ", text=" + text + ", externalFileRef=" + externalFileRef + ", hasInternalFile="
				+ hasInternalFile + ", internalFileType=" + internalFileType + ", title=" + title + "]";
	}

}
