package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single creator of the authorisation in Trivore ID.
 * <p>
 * Used to specify the creator of the authorisation Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorisationCreator implements Serializable {

	private String id;
	private String type;

	/**
	 * Construct a new creator object.
	 */
	public AuthorisationCreator() {
		// ...
	}

	/**
	 * Get the creator unique identifier.
	 *
	 * @return unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the creator unique identifier.
	 *
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the type of the creator entity.
	 *
	 * @return the type of the creator entity
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set the type of the creator entity.
	 *
	 * @param type the type of the creator entity
	 */
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthorisationCreator)) {
			return false;
		}
		AuthorisationCreator o = (AuthorisationCreator) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "AuthorisationCreator [id=" + id + ", type=" + type + "]";
	}

}
