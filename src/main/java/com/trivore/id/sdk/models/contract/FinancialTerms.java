package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents finantial terms in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class FinancialTerms implements Serializable {

	private String paymentTerms;
	private String billingTerms;
	private String penaltyInterest;

	/**
	 * Construct a new finantial terms object.
	 */
	public FinancialTerms() {
		// ...
	}

	/**
	 * Get payment terms.
	 *
	 * @return payment terms
	 */
	public String getPaymentTerms() {
		return paymentTerms;
	}

	/**
	 * Set payment terms.
	 *
	 * @param paymentTerms payment terms
	 */
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	/**
	 * Get billing terms.
	 *
	 * @return billing terms
	 */
	public String getBillingTerms() {
		return billingTerms;
	}

	/**
	 * Set billing terms.
	 *
	 * @param billingTerms billing terms
	 */
	public void setBillingTerms(String billingTerms) {
		this.billingTerms = billingTerms;
	}

	/**
	 * Get penalty interest.
	 *
	 * @return penalty interest
	 */
	public String getPenaltyInterest() {
		return penaltyInterest;
	}

	/**
	 * Set penalty interest.
	 *
	 * @param penaltyInterest penalty interest
	 */
	public void setPenaltyInterest(String penaltyInterest) {
		this.penaltyInterest = penaltyInterest;
	}

	@Override
	public int hashCode() {
		return Objects.hash(paymentTerms, billingTerms, penaltyInterest);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof FinancialTerms)) {
			return false;
		}
		FinancialTerms o = (FinancialTerms) obj;
		return Objects.equals(paymentTerms, o.paymentTerms) && Objects.equals(billingTerms, o.billingTerms)
				&& Objects.equals(penaltyInterest, o.penaltyInterest);
	}

	@Override
	public String toString() {
		return "FinancialTerms [paymentTerms=" + paymentTerms + ", billingTerms=" + billingTerms + ", penaltyInterest="
				+ penaltyInterest + "]";
	}

}
