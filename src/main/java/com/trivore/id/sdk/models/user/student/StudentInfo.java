package com.trivore.id.sdk.models.user.student;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a student state information.
 * <p>
 * Used to specify a student state information in the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentInfo implements Serializable {

	private StudentStatus state;
	private String studentValidFrom;
	private String studentValidTo;
	private String informationUpdated;
	private String remoteState;
	private String notStudentReason;
	private String queryConsentEnds;

	/**
	 * Construct new student info.
	 */
	public StudentInfo() {
		// ...
	}

	/**
	 * @return the student status
	 */
	public StudentStatus getState() {
		return state;
	}

	/**
	 * @param state the student status to set
	 */
	public void setState(StudentStatus state) {
		this.state = state;
	}

	/**
	 * @return the student valid from date
	 */
	public String getStudentValidFrom() {
		return studentValidFrom;
	}

	/**
	 * @param studentValidFrom the student valid from date to set
	 */
	public void setStudentValidFrom(String studentValidFrom) {
		this.studentValidFrom = studentValidFrom;
	}

	/**
	 * @return the student valid to date
	 */
	public String getStudentValidTo() {
		return studentValidTo;
	}

	/**
	 * @param studentValidTo the student valid to date to set
	 */
	public void setStudentValidTo(String studentValidTo) {
		this.studentValidTo = studentValidTo;
	}

	/**
	 * @return the timestamp when information was last updated.
	 */
	public String getInformationUpdated() {
		return informationUpdated;
	}

	/**
	 * @param informationUpdated the timestamp when information was last updated
	 */
	public void setInformationUpdated(String informationUpdated) {
		this.informationUpdated = informationUpdated;
	}

	/**
	 * Get the remote system state.
	 * <p>
	 * May contain an error message from OPH Koski.
	 * </p>
	 *
	 * @return the remote state
	 */
	public String getRemoteState() {
		return remoteState;
	}

	/**
	 * Set the remote system state.
	 * <p>
	 * May contain an error message from OPH Koski.
	 * </p>
	 *
	 * @param remoteState the remote state to set
	 */
	public void setRemoteState(String remoteState) {
		this.remoteState = remoteState;
	}

	/**
	 * @return the notStudentReason
	 */
	public String getNotStudentReason() {
		return notStudentReason;
	}

	/**
	 * @param notStudentReason the notStudentReason to set
	 */
	public void setNotStudentReason(String notStudentReason) {
		this.notStudentReason = notStudentReason;
	}

	/**
	 * @return the queryConsentEnds
	 */
	public String getQueryConsentEnds() {
		return queryConsentEnds;
	}

	/**
	 * @param queryConsentEnds the queryConsentEnds to set
	 */
	public void setQueryConsentEnds(String queryConsentEnds) {
		this.queryConsentEnds = queryConsentEnds;
	}

	@Override
	public int hashCode() {
		return Objects.hash(state, studentValidFrom, studentValidTo, informationUpdated, remoteState, notStudentReason,
				queryConsentEnds);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof StudentInfo)) {
			return false;
		}
		StudentInfo o = (StudentInfo) obj;
		return Objects.equals(state, o.state) //
				&& Objects.equals(studentValidFrom, o.studentValidFrom) //
				&& Objects.equals(studentValidTo, o.studentValidTo) //
				&& Objects.equals(informationUpdated, o.informationUpdated) //
				&& Objects.equals(remoteState, o.remoteState) //
				&& Objects.equals(notStudentReason, o.notStudentReason) //
				&& Objects.equals(queryConsentEnds, o.queryConsentEnds);
	}

	@Override
	public String toString() {
		return "StudentInfo [state=" + state + ", studentValidFrom=" + studentValidFrom + ", studentValidTo="
				+ studentValidTo + ", informationUpdated=" + informationUpdated + ", remoteState=" + remoteState
				+ ", notStudentReason=" + notStudentReason + ", queryConsentEnds=" + queryConsentEnds + "]";
	}

}
