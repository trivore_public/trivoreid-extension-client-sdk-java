package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that presents a user role in Trivore ID.
 * <p>
 * Used to manage role names and permissions in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role implements Serializable {

	private String id;
	private String name;
	private Set<String> permissions;
	private String nsCode;

	/**
	 * Build a new role.
	 */
	public Role() {
		// ...
	}

	/**
	 * Get the identifier that uniquely identifies the role.
	 * <p>
	 * This value is automatically assigned by Trivore ID.
	 * </p>
	 *
	 * @return A identifier of the role.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the identifier that uniquely identifies the role.
	 * <p>
	 * This value is automatically assigned by Trivore ID.
	 * </p>
	 *
	 * @param id An identifier for the role.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the name of the role.
	 *
	 * @return The name of the role.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name for the role.
	 *
	 * @param name The name for the role.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the permissions attached to this role.
	 * <p>
	 * This function never returns null.
	 * </p>
	 *
	 * @return The set of permissions attached to role.
	 */
	public Set<String> getPermissions() {
		if (permissions == null) {
			permissions = new HashSet<String>();
		}
		return permissions;
	}

	/**
	 * Set the permissions attached for the role.
	 *
	 * @param permissions The set of attached permissions.
	 */
	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Add the given permission for the role.
	 *
	 * @param permission The permission to be added.
	 */
	public void addPermission(String permission) {
		getPermissions().add(permission);
	}

	/**
	 * Remove the specified permission from the role.
	 *
	 * @param permission The permission to be removed.
	 */
	public void removePermission(String permission) {
		getPermissions().remove(permission);
	}

	/**
	 * Get the code of the target namespace.
	 *
	 * @return The code of the namespace.
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set the code of the target namespace.
	 *
	 * @param nsCode The code of the namespace.
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof Role)) {
			return false;
		}
		Role o = (Role) other;
		if (o.getId() == null && getId() == null) {
			return super.equals(other);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", permissions=" + permissions + ", nsCode=" + nsCode + "]";
	}

}
