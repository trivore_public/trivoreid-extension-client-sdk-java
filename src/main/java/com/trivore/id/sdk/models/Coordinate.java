package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single coordinate in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID coordinate definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Coordinate implements Serializable {

	private String system;
	private Integer x;
	private Integer y;
	private Integer z;

	/**
	 * @return system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @param system system
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * @return x-coordinate
	 */
	public Integer getX() {
		return x;
	}

	/**
	 * @param x x-coordinate
	 */
	public void setX(Integer x) {
		this.x = x;
	}

	/**
	 * @return y-coordinate
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * @param y y-coordinate
	 */
	public void setY(Integer y) {
		this.y = y;
	}

	/**
	 * @return z-coordinate
	 */
	public Integer getZ() {
		return z;
	}

	/**
	 * @param z z-coordinate
	 */
	public void setZ(Integer z) {
		this.z = z;
	}

	@Override
	public int hashCode() {
		return Objects.hash(system, x, y, z);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Coordinate)) {
			return false;
		}
		Coordinate o = (Coordinate) obj;
		return Objects.equals(system, o.system) && Objects.equals(x, o.x) && Objects.equals(y, o.y)
				&& Objects.equals(z, o.z);
	}

	@Override
	public String toString() {
		return "Coordinate [system=" + system + ", x=" + x + ", y=" + y + ", z=" + z + "]";
	}

}
