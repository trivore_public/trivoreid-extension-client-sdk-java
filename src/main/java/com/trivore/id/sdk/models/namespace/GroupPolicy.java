package com.trivore.id.sdk.models.namespace;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a namespace's group policy in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupPolicy implements Serializable {

	private String policyName;
	private String policyDescription;
	private int inactivityTime;
	private ResetPasswordOptions resetPasswordOptions;
	private String preferredLocale;
	private PreferredTimeFormat preferredTimeFormat;
	private PreferredDateFormat preferredDateFormat;
	private DisplayNameFormat displayNameFormat;
	private String timeZone;
	private int minPwLength;
	private int minCharClasses;
	private int pwMaxAge;
	private int pwHistorySaved;
	private int incorrectLoginAttemptsThresh;
	private int incorrectLoginTimeWin;
	private int incorrectLoginLockoutTime;
	private int maxPwSequenceLength;
	private int maxConsecutiveIdenticalChars;
	private String dataStoreCountry;
	private boolean passwordUserNameRuleEnforced;
	private boolean passwordDictionaryRuleEnforced;
	private String ciscoGroupPolicy;
	private String twoFactorAuthOptions; // TODO: Enum?
	private List<LoginAllowedIpRule> loginAllowedIpRules;
	private int emailVerifyLinkExpireDays;
	private boolean mobileVerificationRequired;
	private boolean persistentLoginAllowed;
	private IsAllowed emailLoginAllowed;
	private IsAllowed mobileLoginAllowed;
	private int mobileVerificationAttemptsPerDay;
	private boolean strongIdRequiredForMyDataDownload;
	private boolean emailVerificationUnlocksUser;
	private boolean userImageAllowed;
	private DeleteMode deleteMode;
	private boolean hideOnSoftDelete;
	private List<String> loginTargetBlacklist;

	/**
	 * Get the policy name.
	 *
	 * @return policy name
	 */
	public String getPolicyName() {
		return policyName;
	}

	/**
	 * Set the policy name.
	 *
	 * @param policyName policy name
	 */
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	/**
	 * Get the policy description.
	 *
	 * @return policy description
	 */
	public String getPolicyDescription() {
		return policyDescription;
	}

	/**
	 * Set the policy description.
	 *
	 * @param policyDescription policy description
	 */
	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}

	/**
	 * Get the inactivity time.
	 *
	 * @return inactivity time
	 */
	public int getInactivityTime() {
		return inactivityTime;
	}

	/**
	 * Set the inactivity time.
	 *
	 * @param inactivityTime inactivity time
	 */
	public void setInactivityTime(int inactivityTime) {
		this.inactivityTime = inactivityTime;
	}

	/**
	 * Get the reset password options.
	 *
	 * @return reset password options
	 */
	public ResetPasswordOptions getResetPasswordOptions() {
		if (resetPasswordOptions == null) {
			resetPasswordOptions = new ResetPasswordOptions();
		}
		return resetPasswordOptions;
	}

	/**
	 * Set the reset password options.
	 *
	 * @param resetPasswordOptions reset password options
	 */
	public void setResetPasswordOptions(ResetPasswordOptions resetPasswordOptions) {
		this.resetPasswordOptions = resetPasswordOptions;
	}

	/**
	 * Get the preferred locale.
	 *
	 * @return preferred locale
	 */
	public String getPreferredLocale() {
		return preferredLocale;
	}

	/**
	 * Set the preferred locale.
	 *
	 * @param preferredLocale preferred locale
	 */
	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

	/**
	 * Get the preferred time format.
	 *
	 * @return preferred time format
	 */
	public PreferredTimeFormat getPreferredTimeFormat() {
		return preferredTimeFormat;
	}

	/**
	 * Set the preferred time format.
	 *
	 * @param preferredTimeFormat preferred time format
	 */
	public void setPreferredTimeFormat(PreferredTimeFormat preferredTimeFormat) {
		this.preferredTimeFormat = preferredTimeFormat;
	}

	/**
	 * Get the preferred date format.
	 *
	 * @return preferred date format
	 */
	public PreferredDateFormat getPreferredDateFormat() {
		return preferredDateFormat;
	}

	/**
	 * Set the preferred date format.
	 *
	 * @param preferredDateFormat preferred date format
	 */
	public void setPreferredDateFormat(PreferredDateFormat preferredDateFormat) {
		this.preferredDateFormat = preferredDateFormat;
	}

	/**
	 * Get the display name format.
	 *
	 * @return display name format
	 */
	public DisplayNameFormat getDisplayNameFormat() {
		return displayNameFormat;
	}

	/**
	 * Set the display name format.
	 *
	 * @param displayNameFormat display name format
	 */
	public void setDisplayNameFormat(DisplayNameFormat displayNameFormat) {
		this.displayNameFormat = displayNameFormat;
	}

	/**
	 * Get the time zone.
	 *
	 * @return time zone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * Set the time zone.
	 *
	 * @param timeZone time zone
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Get the minimum password length.
	 *
	 * @return minimum password length
	 */
	public int getMinPwLength() {
		return minPwLength;
	}

	/**
	 * Set the minimum password length.
	 *
	 * @param minPwLength minimum password length
	 */
	public void setMinPwLength(int minPwLength) {
		this.minPwLength = minPwLength;
	}

	/**
	 * Get the minimum charactes classes.
	 *
	 * @return minimum charactes classes
	 */
	public int getMinCharClasses() {
		return minCharClasses;
	}

	/**
	 * Set the minimum charactes classes.
	 *
	 * @param minCharClasses minimum charactes classes
	 */
	public void setMinCharClasses(int minCharClasses) {
		this.minCharClasses = minCharClasses;
	}

	/**
	 * Get the maximum password age.
	 *
	 * @return maximum password age
	 */
	public int getPwMaxAge() {
		return pwMaxAge;
	}

	/**
	 * Set the maximum password age.
	 *
	 * @param pwMaxAge maximum password age
	 */
	public void setPwMaxAge(int pwMaxAge) {
		this.pwMaxAge = pwMaxAge;
	}

	/**
	 * Get the password history saved.
	 *
	 * @return password history saved
	 */
	public int getPwHistorySaved() {
		return pwHistorySaved;
	}

	/**
	 * Set the password history saved.
	 *
	 * @param pwHistorySaved password history saved
	 */
	public void setPwHistorySaved(int pwHistorySaved) {
		this.pwHistorySaved = pwHistorySaved;
	}

	/**
	 * Get the incorrect login attempts thresh.
	 *
	 * @return incorrect login attempts thresh
	 */
	public int getIncorrectLoginAttemptsThresh() {
		return incorrectLoginAttemptsThresh;
	}

	/**
	 * Set the incorrect login attempts thresh.
	 *
	 * @param incorrectLoginAttemptsThresh incorrect login attempts thresh
	 */
	public void setIncorrectLoginAttemptsThresh(int incorrectLoginAttemptsThresh) {
		this.incorrectLoginAttemptsThresh = incorrectLoginAttemptsThresh;
	}

	/**
	 * Get the incorrect login time win.
	 *
	 * @return incorrect login time win
	 */
	public int getIncorrectLoginTimeWin() {
		return incorrectLoginTimeWin;
	}

	/**
	 * Set the incorrect login time win.
	 *
	 * @param incorrectLoginTimeWin incorrect login time win
	 */
	public void setIncorrectLoginTimeWin(int incorrectLoginTimeWin) {
		this.incorrectLoginTimeWin = incorrectLoginTimeWin;
	}

	/**
	 * Get the incorrect login lockout time.
	 *
	 * @return incorrect login lockout time
	 */
	public int getIncorrectLoginLockoutTime() {
		return incorrectLoginLockoutTime;
	}

	/**
	 * Set the incorrect login lockout time.
	 *
	 * @param incorrectLoginLockoutTime incorrect login lockout time
	 */
	public void setIncorrectLoginLockoutTime(int incorrectLoginLockoutTime) {
		this.incorrectLoginLockoutTime = incorrectLoginLockoutTime;
	}

	/**
	 * Get the maximum password sequence length.
	 *
	 * @return maximum password sequence length
	 */
	public int getMaxPwSequenceLength() {
		return maxPwSequenceLength;
	}

	/**
	 * Set the maximum password sequence length.
	 *
	 * @param maxPwSequenceLength maximum password sequence length
	 */
	public void setMaxPwSequenceLength(int maxPwSequenceLength) {
		this.maxPwSequenceLength = maxPwSequenceLength;
	}

	/**
	 * Get the maximum consecutive identical characters.
	 *
	 * @return maximum consecutive identical characters
	 */
	public int getMaxConsecutiveIdenticalChars() {
		return maxConsecutiveIdenticalChars;
	}

	/**
	 * Set the maximum consecutive identical characters.
	 *
	 * @param maxConsecutiveIdenticalChars maximum consecutive identical characters
	 */
	public void setMaxConsecutiveIdenticalChars(int maxConsecutiveIdenticalChars) {
		this.maxConsecutiveIdenticalChars = maxConsecutiveIdenticalChars;
	}

	/**
	 * Get the data store country.
	 *
	 * @return data store country
	 */
	public String getDataStoreCountry() {
		return dataStoreCountry;
	}

	/**
	 * Set the data store country.
	 *
	 * @param dataStoreCountry data store country
	 */
	public void setDataStoreCountry(String dataStoreCountry) {
		this.dataStoreCountry = dataStoreCountry;
	}

	/**
	 * Get if the password user name rule is enforced.
	 *
	 * @return if the password user name rule is enforced
	 */
	public boolean isPasswordUserNameRuleEnforced() {
		return passwordUserNameRuleEnforced;
	}

	/**
	 * Set if the password user name rule is enforced.
	 *
	 * @param passwordUserNameRuleEnforced if the password user name rule is
	 *                                     enforced
	 */
	public void setPasswordUserNameRuleEnforced(boolean passwordUserNameRuleEnforced) {
		this.passwordUserNameRuleEnforced = passwordUserNameRuleEnforced;
	}

	/**
	 * Get if the password dictionary rule is enforced.
	 *
	 * @return if the password dictionary rule is enforced
	 */
	public boolean isPasswordDictionaryRuleEnforced() {
		return passwordDictionaryRuleEnforced;
	}

	/**
	 * Set if the password dictionary rule is enforced.
	 *
	 * @param passwordDictionaryRuleEnforced if the password dictionary rule is
	 *                                       enforced
	 */
	public void setPasswordDictionaryRuleEnforced(boolean passwordDictionaryRuleEnforced) {
		this.passwordDictionaryRuleEnforced = passwordDictionaryRuleEnforced;
	}

	/**
	 * Get the cisco group policy.
	 *
	 * @return cisco group policy
	 */
	public String getCiscoGroupPolicy() {
		return ciscoGroupPolicy;
	}

	/**
	 * Set the cisco group policy.
	 *
	 * @param ciscoGroupPolicy cisco group policy
	 */
	public void setCiscoGroupPolicy(String ciscoGroupPolicy) {
		this.ciscoGroupPolicy = ciscoGroupPolicy;
	}

	/**
	 * Get the two factor authentication options.
	 *
	 * @return two factor authentication options
	 */
	public String getTwoFactorAuthOptions() {
		return twoFactorAuthOptions;
	}

	/**
	 * Set the two factor authentication options.
	 *
	 * @param twoFactorAuthOptions two factor authentication options
	 */
	public void setTwoFactorAuthOptions(String twoFactorAuthOptions) {
		this.twoFactorAuthOptions = twoFactorAuthOptions;
	}

	/**
	 * Get the login allowed IP rules.
	 *
	 * @return login allowed IP rules
	 */
	public List<LoginAllowedIpRule> getLoginAllowedIpRules() {
		if (loginAllowedIpRules == null) {
			loginAllowedIpRules = new ArrayList<>();
		}
		return loginAllowedIpRules;
	}

	/**
	 * Set the login allowed IP rules.
	 *
	 * @param loginAllowedIpRules login allowed IP rules
	 */
	public void setLoginAllowedIpRules(List<LoginAllowedIpRule> loginAllowedIpRules) {
		this.loginAllowedIpRules = loginAllowedIpRules;
	}

	/**
	 * Get the email verification link expiration days.
	 *
	 * @return email verification link expiration days
	 */
	public int getEmailVerifyLinkExpireDays() {
		return emailVerifyLinkExpireDays;
	}

	/**
	 * Set the email verification link expiration days.
	 *
	 * @param emailVerifyLinkExpireDays email verification link expiration days
	 */
	public void setEmailVerifyLinkExpireDays(int emailVerifyLinkExpireDays) {
		this.emailVerifyLinkExpireDays = emailVerifyLinkExpireDays;
	}

	/**
	 * Get if the mobile verification is required.
	 *
	 * @return if the mobile verification is required
	 */
	public boolean isMobileVerificationRequired() {
		return mobileVerificationRequired;
	}

	/**
	 * Set the if the mobile verification is required.
	 *
	 * @param mobileVerificationRequired if the mobile verification is required
	 */
	public void setMobileVerificationRequired(boolean mobileVerificationRequired) {
		this.mobileVerificationRequired = mobileVerificationRequired;
	}

	/**
	 * Get if the persistent login allowed.
	 *
	 * @return if the persistent login allowed
	 */
	public boolean isPersistentLoginAllowed() {
		return persistentLoginAllowed;
	}

	/**
	 * Set if the persistent login allowed.
	 *
	 * @param persistentLoginAllowed if the persistent login allowed
	 */
	public void setPersistentLoginAllowed(boolean persistentLoginAllowed) {
		this.persistentLoginAllowed = persistentLoginAllowed;
	}

	/**
	 * Get if the email login is allowed.
	 *
	 * @return if the email login is allowed
	 */
	public IsAllowed getEmailLoginAllowed() {
		return emailLoginAllowed;
	}

	/**
	 * Set if the email login is allowed.
	 *
	 * @param emailLoginAllowed if the email login is allowed
	 */
	public void setEmailLoginAllowed(IsAllowed emailLoginAllowed) {
		this.emailLoginAllowed = emailLoginAllowed;
	}

	/**
	 * Get if the mobile login is allowed.
	 *
	 * @return if the mobile login is allowed
	 */
	public IsAllowed getMobileLoginAllowed() {
		return mobileLoginAllowed;
	}

	/**
	 * Set if the mobile login is allowed.
	 *
	 * @param mobileLoginAllowed if the mobile login is allowed
	 */
	public void setMobileLoginAllowed(IsAllowed mobileLoginAllowed) {
		this.mobileLoginAllowed = mobileLoginAllowed;
	}

	/**
	 * Get the mobile verification attempts per day.
	 *
	 * @return mobile verification attempts per day
	 */
	public int getMobileVerificationAttemptsPerDay() {
		return mobileVerificationAttemptsPerDay;
	}

	/**
	 * Set the mobile verification attempts per day.
	 *
	 * @param mobileVerificationAttemptsPerDay mobile verification attempts per day
	 */
	public void setMobileVerificationAttemptsPerDay(int mobileVerificationAttemptsPerDay) {
		this.mobileVerificationAttemptsPerDay = mobileVerificationAttemptsPerDay;
	}

	/**
	 * Get if the strong Id required for MyData download.
	 *
	 * @return if the strong Id required for MyData download
	 */
	public boolean isStrongIdRequiredForMyDataDownload() {
		return strongIdRequiredForMyDataDownload;
	}

	/**
	 * Set if the strong Id required for MyData download.
	 *
	 * @param strongIdRequiredForMyDataDownload if the strong Id required for MyData
	 *                                          download
	 */
	public void setStrongIdRequiredForMyDataDownload(boolean strongIdRequiredForMyDataDownload) {
		this.strongIdRequiredForMyDataDownload = strongIdRequiredForMyDataDownload;
	}

	/**
	 * Get if the email verification unlocks the user.
	 *
	 * @return if the email verification unlocks the user
	 */
	public boolean isEmailVerificationUnlocksUser() {
		return emailVerificationUnlocksUser;
	}

	/**
	 * Set if the email verification unlocks the user.
	 *
	 * @param emailVerificationUnlocksUser if the email verification unlocks the
	 *                                     user
	 */
	public void setEmailVerificationUnlocksUser(boolean emailVerificationUnlocksUser) {
		this.emailVerificationUnlocksUser = emailVerificationUnlocksUser;
	}

	/**
	 * Get if the user image allowed.
	 *
	 * @return if the user image allowed
	 */
	public boolean isUserImageAllowed() {
		return userImageAllowed;
	}

	/**
	 * Set if the user image allowed.
	 *
	 * @param userImageAllowed if the user image allowed
	 */
	public void setUserImageAllowed(boolean userImageAllowed) {
		this.userImageAllowed = userImageAllowed;
	}

	/**
	 * Get the delete mode.
	 *
	 * @return delete mode
	 */
	public DeleteMode getDeleteMode() {
		return deleteMode;
	}

	/**
	 * Set the delete mode.
	 *
	 * @param deleteMode delete mode
	 */
	public void setDeleteMode(DeleteMode deleteMode) {
		this.deleteMode = deleteMode;
	}

	/**
	 * Get if the user account is hide on soft delete.
	 *
	 * @return if the user account is hide on soft delete
	 */
	public boolean isHideOnSoftDelete() {
		return hideOnSoftDelete;
	}

	/**
	 * Set if the user account is hide on soft delete.
	 *
	 * @param hideOnSoftDelete if the user account is hide on soft delete
	 */
	public void setHideOnSoftDelete(boolean hideOnSoftDelete) {
		this.hideOnSoftDelete = hideOnSoftDelete;
	}

	/**
	 * Get the login target black list.
	 *
	 * @return the login target black list
	 */
	public List<String> getLoginTargetBlacklist() {
		if (loginTargetBlacklist == null) {
			loginTargetBlacklist = new ArrayList<>();
		}
		return loginTargetBlacklist;
	}

	/**
	 * Set the the login target black list.
	 *
	 * @param loginTargetBlacklist the login target black list
	 */
	public void setLoginTargetBlacklist(List<String> loginTargetBlacklist) {
		this.loginTargetBlacklist = loginTargetBlacklist;
	}

	@Override
	public int hashCode() {
		return Objects.hash(policyName);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof GroupPolicy)) {
			return false;
		}
		GroupPolicy o = (GroupPolicy) obj;
		if (o.getPolicyName() == null && getPolicyName() == null) {
			return super.equals(obj);
		}
		return Objects.equals(policyName, o.policyName);
	}

	@Override
	public String toString() {
		return "GroupPolicy [policyName=" + policyName + ", policyDescription=" + policyDescription
				+ ", inactivityTime=" + inactivityTime + ", resetPasswordOptions=" + resetPasswordOptions
				+ ", preferredLocale=" + preferredLocale + ", preferredTimeFormat=" + preferredTimeFormat
				+ ", preferredDateFormat=" + preferredDateFormat + ", displayNameFormat=" + displayNameFormat
				+ ", timeZone=" + timeZone + ", minPwLength=" + minPwLength + ", minCharClasses=" + minCharClasses
				+ ", pwMaxAge=" + pwMaxAge + ", pwHistorySaved=" + pwHistorySaved + ", incorrectLoginAttemptsThresh="
				+ incorrectLoginAttemptsThresh + ", incorrectLoginTimeWin=" + incorrectLoginTimeWin
				+ ", incorrectLoginLockoutTime=" + incorrectLoginLockoutTime + ", maxPwSequenceLength="
				+ maxPwSequenceLength + ", maxConsecutiveIdenticalChars=" + maxConsecutiveIdenticalChars
				+ ", dataStoreCountry=" + dataStoreCountry + ", passwordUserNameRuleEnforced="
				+ passwordUserNameRuleEnforced + ", passwordDictionaryRuleEnforced=" + passwordDictionaryRuleEnforced
				+ ", ciscoGroupPolicy=" + ciscoGroupPolicy + ", twoFactorAuthOptions=" + twoFactorAuthOptions
				+ ", loginAllowedIpRules=" + loginAllowedIpRules + ", emailVerifyLinkExpireDays="
				+ emailVerifyLinkExpireDays + ", mobileVerificationRequired=" + mobileVerificationRequired
				+ ", persistentLoginAllowed=" + persistentLoginAllowed + ", emailLoginAllowed=" + emailLoginAllowed
				+ ", mobileLoginAllowed=" + mobileLoginAllowed + ", mobileVerificationAttemptsPerDay="
				+ mobileVerificationAttemptsPerDay + ", strongIdRequiredForMyDataDownload="
				+ strongIdRequiredForMyDataDownload + ", emailVerificationUnlocksUser=" + emailVerificationUnlocksUser
				+ ", userImageAllowed=" + userImageAllowed + ", deleteMode=" + deleteMode + ", hideOnSoftDelete="
				+ hideOnSoftDelete + ", loginTargetBlacklist=" + loginTargetBlacklist + "]";
	}

}
