package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents the contract's allowed actions in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllowedActions implements Serializable {

	private boolean sign;
	private boolean requestTermination;
	private boolean cancel;
	private boolean modifyDraft;
	private boolean modifyFinalised;
	private boolean finalise;

	/**
	 * Construct allowed actions object.
	 */
	public AllowedActions() {
		// ...
	}

	/**
	 * Get if the contract can be signed.
	 *
	 * @return if the contract can be signed.
	 */
	public boolean isSign() {
		return sign;
	}

	/**
	 * Set if the contract can be signed.
	 *
	 * @param sign if the contract can be signed
	 */
	public void setSign(boolean sign) {
		this.sign = sign;
	}

	/**
	 * Get if the contract can be terminated.
	 *
	 * @return if the contract can be terminated.
	 */
	public boolean isRequestTermination() {
		return requestTermination;
	}

	/**
	 * Set if the contract can be terminated.
	 *
	 * @param requestTermination if the contract can be terminated
	 */
	public void setRequestTermination(boolean requestTermination) {
		this.requestTermination = requestTermination;
	}

	/**
	 * Get if the contract can be canceled.
	 *
	 * @return if the contract can be canceled.
	 */
	public boolean isCancel() {
		return cancel;
	}

	/**
	 * Set if the contract can be canceled.
	 *
	 * @param cancel if the contract can be canceled
	 */
	public void setCancel(boolean cancel) {
		this.cancel = cancel;
	}

	/**
	 * Get if the contract can be modified as a draft.
	 *
	 * @return if the contract can be modified as a draft.
	 */
	public boolean isModifyDraft() {
		return modifyDraft;
	}

	/**
	 * Set if the contract can be modified as a draft.
	 *
	 * @param modifyDraft if the contract can be modified as a draft
	 */
	public void setModifyDraft(boolean modifyDraft) {
		this.modifyDraft = modifyDraft;
	}

	/**
	 * Get if the contract can be modified after finalising.
	 *
	 * @return if the contract can be modified after finalising.
	 */
	public boolean isModifyFinalised() {
		return modifyFinalised;
	}

	/**
	 * Set if the contract can be modified after finalising.
	 *
	 * @param modifyFinalised if the contract can be modified after finalising
	 */
	public void setModifyFinalised(boolean modifyFinalised) {
		this.modifyFinalised = modifyFinalised;
	}

	/**
	 * Get if the contract can be finalised.
	 *
	 * @return if the contract can be finalised.
	 */
	public boolean isFinalise() {
		return finalise;
	}

	/**
	 * Set if the contract can be finalised.
	 *
	 * @param finalise if the contract can be signed
	 */
	public void setFinalise(boolean finalise) {
		this.finalise = finalise;
	}

	@Override
	public int hashCode() {
		return Objects.hash(sign, requestTermination, cancel, modifyDraft, modifyFinalised, finalise);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AllowedActions)) {
			return false;
		}
		AllowedActions o = (AllowedActions) obj;
		return Objects.equals(sign, o.sign) && Objects.equals(requestTermination, o.requestTermination)
				&& Objects.equals(cancel, o.cancel) && Objects.equals(modifyDraft, o.modifyDraft)
				&& Objects.equals(modifyFinalised, o.modifyFinalised) && Objects.equals(finalise, o.finalise);
	}

	@Override
	public String toString() {
		return "AllowedActions [sign=" + sign + ", requestTermination=" + requestTermination + ", cancel=" + cancel
				+ ", modifyDraft=" + modifyDraft + ", modifyFinalised=" + modifyFinalised + ", finalise=" + finalise
				+ "]";
	}

}
