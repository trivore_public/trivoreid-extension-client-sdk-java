package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents an address.
 * <p>
 * Used to specify addresses in the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements Serializable {

	private String country;
	private String locality;
	private String postalCode;
	private String region;
	private String streetAddress;
	private AddressType type;
	private String organisation;
	private String addressName;
	private String name;
	private boolean verified;
	private String source;
	private String language;

	/**
	 * Construct a new address.
	 */
	public Address() {
		// ...
	}

	/**
	 * Get user's country.
	 *
	 * @return returns country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Set user's country.
	 *
	 * @param country country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Get user's locality or city.
	 *
	 * @return returns locality or city
	 */
	public String getLocality() {
		return locality;
	}

	/**
	 * Set user's locality or city.
	 *
	 * @param locality locality or city
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}

	/**
	 * Get user's postal code or zip code.
	 *
	 * @return returns postal code or zip code
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Set user's postal code or zip code.
	 *
	 * @param postalCode postal code or zip code
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Get user's region or state.
	 *
	 * @return returns region or state
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Set user's region or state.
	 *
	 * @param region region or state
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Get user's street address.
	 *
	 * @return returns street address
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * Set user's street address.
	 *
	 * @param streetAddress street address
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return type
	 */
	public AddressType getType() {
		return type;
	}

	/**
	 * @param type type
	 */
	public void setType(AddressType type) {
		this.type = type;
	}

	/**
	 * @return organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation organisation
	 */
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return address name
	 */
	public String getAddressName() {
		return addressName;
	}

	/**
	 * @param addressName address name
	 */
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return verification status
	 */
	public boolean isVerified() {
		return verified;
	}

	/**
	 * @param verified verified
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	/**
	 * @return source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Helper to check if all Address fields are empty/null or not.
	 *
	 * @return if all Address fields are empty/null
	 */
	public boolean areFieldsEmpty() {
		return (country == null || country.isEmpty()) && (locality == null || locality.isEmpty())
				&& (postalCode == null || postalCode.isEmpty()) && (region == null || region.isEmpty())
				&& (streetAddress == null || streetAddress.isEmpty());
	}

	@Override
	public int hashCode() {
		return Objects.hash(country, locality, postalCode, region, streetAddress, type, organisation, addressName, name,
				verified, source, language);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Address)) {
			return false;
		}
		Address o = (Address) obj;
		return Objects.equals(country, o.country) //
				&& Objects.equals(locality, o.locality) //
				&& Objects.equals(postalCode, o.postalCode) //
				&& Objects.equals(region, o.region) //
				&& Objects.equals(streetAddress, o.streetAddress) //
				&& Objects.equals(type, o.type) //
				&& Objects.equals(organisation, o.organisation) //
				&& Objects.equals(addressName, o.addressName) //
				&& Objects.equals(name, o.name) //
				&& Objects.equals(verified, o.verified) //
				&& Objects.equals(source, o.source) //
				&& Objects.equals(language, o.language);
	}

	@Override
	public String toString() {
		return "Address [country=" + country + ", locality=" + locality + ", postalCode=" + postalCode + ", region="
				+ region + ", streetAddress=" + streetAddress + ", type=" + type + ", organisation=" + organisation
				+ ", addressName=" + addressName + ", name=" + name + ", verified=" + verified + ", source=" + source
				+ ", language=" + language + "]";
	}

}
