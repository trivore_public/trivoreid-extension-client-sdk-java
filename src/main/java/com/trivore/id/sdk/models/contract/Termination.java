package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single termination in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Termination implements Serializable {

	private String noticePeriodEnds;
	private String timestamp;
	private String reason;

	/**
	 * Construct a new termination object.
	 */
	public Termination() {
		// ...
	}

	/**
	 * Get noticePeriodEnds.
	 * <p>
	 * Automatically generated after contract termination is initiated, based on
	 * current time + termOfNoticeDays.
	 * </p>
	 *
	 * @return noticePeriodEnds
	 */
	public String getNoticePeriodEnds() {
		return noticePeriodEnds;
	}

	/**
	 * Set noticePeriodEnds.
	 * <p>
	 * Automatically generated after contract termination is initiated, based on
	 * current time + termOfNoticeDays.
	 * </p>
	 *
	 * @param noticePeriodEnds noticePeriodEnds
	 */
	public void setNoticePeriodEnds(String noticePeriodEnds) {
		this.noticePeriodEnds = noticePeriodEnds;
	}

	/**
	 * Get timestamp.
	 *
	 * @return timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Set timestamp.
	 *
	 * @param timestamp timestamp
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Get reason.
	 *
	 * @return reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * Set reason
	 *
	 * @param reason reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public int hashCode() {
		return Objects.hash(noticePeriodEnds, timestamp, reason);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Termination)) {
			return false;
		}
		Termination o = (Termination) obj;
		return Objects.equals(noticePeriodEnds, o.noticePeriodEnds) && Objects.equals(timestamp, o.timestamp)
				&& Objects.equals(reason, o.reason);
	}

	@Override
	public String toString() {
		return "Termination [noticePeriodEnds=" + noticePeriodEnds + ", timestamp=" + timestamp + ", reason=" + reason
				+ "]";
	}

}
