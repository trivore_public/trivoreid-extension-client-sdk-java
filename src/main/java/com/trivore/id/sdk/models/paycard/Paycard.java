package com.trivore.id.sdk.models.paycard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.Meta;

/**
 * An object which represents a paycard.
 * <p>
 * Used to store paycard info from Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Paycard implements Serializable {

	private String id;
	private String userId;
	private String namespaceId;
	private String name;
	private PaycardState state;
	private Integer priority;
	private String method;
	private String cardBrand;
	private String cardSubBrand;
	private String cardIssuer;
	private String cardIssuerCode;
	private String cardType;
	private String nameOnCard;
	private String leadingDigits;
	private String lastDigits;
	private Integer expirationYear;
	private Integer expirationMonth;
	private String externalCode;
	private String externalRef;
	private String internalRef;
	private List<PaycardEntry> cardCapabilities;
	private List<PaycardEntry> cardData;
	private String validFrom;
	private String validTo;
	private Meta meta;
	private String cardValidUntil;
	private String cardNumber;
	private String cardValidToISODate;

	/**
	 * Construct a paycard.
	 */
	public Paycard() {
		// ...
	}

	/**
	 * Get the paycard id.
	 *
	 * @return paycard id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the paycard id.
	 *
	 * @param id paycard id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the ID of the user the paycard belongs to.
	 *
	 * @return ID of the user the paycard belongs to
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Set the ID of the user the paycard belongs to.
	 *
	 * @param userId ID of the user the paycard belongs to
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Get the ID of the namespace the paycard belongs to.
	 *
	 * @return ID of the namespace the paycard belongs to
	 */
	public String getNamespaceId() {
		return namespaceId;
	}

	/**
	 * Set ID of the namespace the paycard belongs to.
	 *
	 * @param namespaceId ID of the namespace the paycard belongs to
	 */
	public void setNamespaceId(String namespaceId) {
		this.namespaceId = namespaceId;
	}

	/**
	 * Get the name of the paycard
	 *
	 * @return name of the paycard
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the paycard.
	 *
	 * @param name name of the paycard
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the paycard state. Either active or inactive.
	 *
	 * @return paycard state
	 */
	public PaycardState getState() {
		return state;
	}

	/**
	 * Set the paycard state. Either active or inactive.
	 *
	 * @param state paycard state
	 */
	public void setState(PaycardState state) {
		this.state = state;
	}

	/**
	 * Get the optional priority when listing cards. Minimum value is 0, maximum
	 * value is 8.
	 *
	 * @return optional priority when listing cards
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * Set the optional priority when listing cards. Minimum value is 0, maximum
	 * value is 8.
	 *
	 * @param priority optional priority when listing cards
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * Get the paycard method.
	 * <p>
	 * For example cash/creditcard/debitcard/operator/blockchain/other.
	 * </p>
	 *
	 * @return paycard method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Set the paycard method.
	 * <p>
	 * For example cash/creditcard/debitcard/operator/blockchain/other.
	 * </p>
	 *
	 * @param method paycard method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Get the paycard brand.
	 * <p>
	 * For example Amex/Mastercard/Visa/ApplePay/GooglePay/MobilePay/Discover.
	 * </p>
	 *
	 * @return paycard brand
	 */
	public String getCardBrand() {
		return cardBrand;
	}

	/**
	 * Set the paycard brand.
	 * <p>
	 * For example Amex/Mastercard/Visa/ApplePay/GooglePay/MobilePay/Discover.
	 * </p>
	 *
	 * @param cardBrand paycard brand
	 */
	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	/**
	 * Get the paycard Sub brand.
	 * <p>
	 * For example Basic/Platinum/Gold/Maestro/Electron.
	 * </p>
	 *
	 * @return paycard Sub brand
	 */
	public String getCardSubBrand() {
		return cardSubBrand;
	}

	/**
	 * Set the paycard Sub brand.
	 * <p>
	 * For example Basic/Platinum/Gold/Maestro/Electron.
	 * </p>
	 *
	 * @param cardSubBrand paycard Sub brand
	 */
	public void setCardSubBrand(String cardSubBrand) {
		this.cardSubBrand = cardSubBrand;
	}

	/**
	 * Get the paycard issuer. For example the name of the bank.
	 *
	 * @return paycard issuer
	 */
	public String getCardIssuer() {
		return cardIssuer;
	}

	/**
	 * Set the paycard issuer. For example the name of the bank.
	 *
	 * @param cardIssuer paycard issuer
	 */
	public void setCardIssuer(String cardIssuer) {
		this.cardIssuer = cardIssuer;
	}

	/**
	 * Get the paycard issuer code. For example bank swift code.
	 *
	 * @return paycard issuer code
	 */
	public String getCardIssuerCode() {
		return cardIssuerCode;
	}

	/**
	 * Set the paycard issuer code. For example bank swift code.
	 *
	 * @param cardIssuerCode paycard issuer code
	 */
	public void setCardIssuerCode(String cardIssuerCode) {
		this.cardIssuerCode = cardIssuerCode;
	}

	/**
	 * Get the paycard type. For example credit/debit/virtual/other.
	 *
	 * @return paycard type
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * Set the paycard type. For example credit/debit/virtual/other.
	 *
	 * @param cardType paycard type
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * Get the name on card. For application specific usage.
	 *
	 * @return paycard id
	 */
	public String getNameOnCard() {
		return nameOnCard;
	}

	/**
	 * Set the name on card. For application specific usage.
	 *
	 * @param nameOnCard name on card
	 */
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	/**
	 * Get the paycard leading six digits.
	 *
	 * @return paycard leading six digits
	 */
	public String getLeadingDigits() {
		return leadingDigits;
	}

	/**
	 * Set the paycard leading six digits.
	 *
	 * @param leadingDigits paycard leading six digits
	 */
	public void setLeadingDigits(String leadingDigits) {
		this.leadingDigits = leadingDigits;
	}

	/**
	 * Get the paycard last four digits.
	 *
	 * @return paycard last four digits
	 */
	public String getLastDigits() {
		return lastDigits;
	}

	/**
	 * Set the paycard last four digits.
	 *
	 * @param lastDigits paycard last four digits
	 */
	public void setLastDigits(String lastDigits) {
		this.lastDigits = lastDigits;
	}

	/**
	 * Get the expiration year on the card.
	 *
	 * @return expiration year on the card
	 */
	public Integer getExpirationYear() {
		return expirationYear;
	}

	/**
	 * Set the expiration year on the card.
	 *
	 * @param expirationYear expiration year on the card
	 */
	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	/**
	 * Get the expiration month on the card.
	 *
	 * @return expiration month on the card
	 */
	public Integer getExpirationMonth() {
		return expirationMonth;
	}

	/**
	 * Set the expiration month on the card.
	 *
	 * @param expirationMonth expiration month on the card
	 */
	public void setExpirationMonth(Integer expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	/**
	 * Get the external code for application specific usage.
	 *
	 * @return external code for application specific usage
	 */
	public String getExternalCode() {
		return externalCode;
	}

	/**
	 * Set the external code for application specific usage.
	 *
	 * @param externalCode external code for application specific usage
	 */
	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	/**
	 * Get the external reference for application specific usage.
	 *
	 * @return external reference for application specific usage
	 */
	public String getExternalRef() {
		return externalRef;
	}

	/**
	 * Set the external reference for application specific usage.
	 *
	 * @param externalRef external reference for application specific usage
	 */
	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}

	/**
	 * Get the internal reference for application specific usage.
	 *
	 * @return internal reference for application specific usage
	 */
	public String getInternalRef() {
		return internalRef;
	}

	/**
	 * Set the internal reference for application specific usage.
	 *
	 * @param internalRef internal reference for application specific usage
	 */
	public void setInternalRef(String internalRef) {
		this.internalRef = internalRef;
	}

	/**
	 * Get the list of capabilities for application specific usage.
	 *
	 * @return list of capabilities for application specific usage
	 */
	public List<PaycardEntry> getCardCapabilities() {
		if (cardCapabilities == null) {
			cardCapabilities = new ArrayList<>();
		}
		return cardCapabilities;
	}

	/**
	 * Set the list of capabilities for application specific usage.
	 *
	 * @param cardCapabilities list of capabilities for application specific usage
	 */
	public void setCardCapabilities(List<PaycardEntry> cardCapabilities) {
		this.cardCapabilities = cardCapabilities;
	}

	/**
	 * Get the card data for application specific usage.
	 *
	 * @return card data for application specific usage
	 */
	public List<PaycardEntry> getCardData() {
		if (cardData == null) {
			cardData = new ArrayList<>();
		}
		return cardData;
	}

	/**
	 * Set the card data for application specific usage.
	 *
	 * @param cardData card data for application specific usage
	 */
	public void setCardData(List<PaycardEntry> cardData) {
		this.cardData = cardData;
	}

	/**
	 * Get the Valid From, refers to entity validity in user settings, not to card.
	 *
	 * @return Valid From, refers to entity validity in user settings, not to card
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * Set the Valid From, refers to entity validity in user settings, not to card.
	 *
	 * @param validFrom Valid From, refers to entity validity in user settings, not
	 *                  to card
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * Get the Valid To, refers to entity validity in user settings, not to card.
	 *
	 * @return Valid To, refers to entity validity in user settings, not to card
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Set the Valid To, refers to entity validity in user settings, not to card.
	 *
	 * @param validTo Valid To, refers to entity validity in user settings, not to
	 *                card
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	/**
	 * Card valid until. PCI DSS requirement.
	 *
	 * @return card valid until
	 */
	public String getCardValidUntil() {
		return cardValidUntil;
	}

	/**
	 * Card valid until. PCI DSS requirement.
	 *
	 * @param cardValidUntil card valid until
	 */
	public void setCardValidUntil(String cardValidUntil) {
		this.cardValidUntil = cardValidUntil;
	}

	/**
	 * The number on the card.
	 * <p>
	 * WARNING! The card number is highly secret and personal. You should probably
	 * not use it here.
	 * </p>
	 * <p>
	 * PCI DSS requirement. Reading this property requires either the
	 * PAYCARD_NUMBER_VIEW or the PAYCARD_NUMBER_MODIFY permission and writing
	 * requires the PAYCARD_NUMBER_MODIFY permission.
	 * </p>
	 *
	 * @return the number on the card.
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * The number on the card.
	 * <p>
	 * WARNING! The card number is highly secret and personal. You should probably
	 * not use it here.
	 * </p>
	 * <p>
	 * PCI DSS requirement. Reading this property requires either the
	 * PAYCARD_NUMBER_VIEW or the PAYCARD_NUMBER_MODIFY permission and writing
	 * requires the PAYCARD_NUMBER_MODIFY permission.
	 * </p>
	 *
	 * @param cardNumber the number on the card.
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * Valid until date as an ISO datetime without timezone.
	 * <p>
	 * Returns null if valid until is null, blank or in invalid format. This is a
	 * generated property and cannot be used in filters.
	 * </p>
	 *
	 * @return valid until date as an ISO datetime without timezone
	 */
	public String getCardValidToISODate() {
		return cardValidToISODate;
	}

	/**
	 * Valid until date as an ISO datetime without timezone.
	 * <p>
	 * Returns null if valid until is null, blank or in invalid format. This is a
	 * generated property and cannot be used in filters.
	 * </p>
	 *
	 * @param cardValidToISODate valid until date as an ISO datetime without
	 *                           timezone
	 */
	public void setCardValidToISODate(String cardValidToISODate) {
		this.cardValidToISODate = cardValidToISODate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Paycard)) {
			return false;
		}
		Paycard o = (Paycard) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Paycard [id=" + id + ", userId=" + userId + ", namespaceId=" + namespaceId + ", name=" + name
				+ ", state=" + state + ", priority=" + priority + ", method=" + method + ", cardBrand=" + cardBrand
				+ ", cardSubBrand=" + cardSubBrand + ", cardIssuer=" + cardIssuer + ", cardIssuerCode=" + cardIssuerCode
				+ ", cardType=" + cardType + ", nameOnCard=" + nameOnCard + ", leadingDigits=" + leadingDigits
				+ ", lastDigits=" + lastDigits + ", expirationYear=" + expirationYear + ", expirationMonth="
				+ expirationMonth + ", externalCode=" + externalCode + ", externalRef=" + externalRef + ", internalRef="
				+ internalRef + ", cardCapabilities=" + cardCapabilities + ", cardData=" + cardData + ", validFrom="
				+ validFrom + ", validTo=" + validTo + "]";
	}

}
