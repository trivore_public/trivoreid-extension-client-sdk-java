package com.trivore.id.sdk.models.namespace;

import com.trivore.id.sdk.models.namespace.Namespace;

/**
 * An enumeration of the namespace username policies.
 * <p>
 * These values are used with {@link Namespace} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum UsernamePolicy {
	EMAIL, FMLNN, CVCV, CVCVCV, EIGHT_NUMBERS, NINE_NUMBERS, TEN_NUMBERS,
	EIGHT_LOWER_CASE_CHARS, TEN_LOWER_CASE_CHARS, MANUAL
}
