package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class representing SMS response from Trivore ID server.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SMSResponse implements Serializable {

	private String status;
	private String description;
	private String action;
	private String messageId;
	private String to;
	private String toRegion;
	private String clientRef;
	private String billingRef;
	private String carrierRef;
	private int messageCount;
	private String billingState;
	private String totalPrice;
	private String remainingCredits;

	/**
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return message Id
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId message Id
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to to
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return toRegion
	 */
	public String getToRegion() {
		return toRegion;
	}

	/**
	 * @param toRegion toRegion
	 */
	public void setToRegion(String toRegion) {
		this.toRegion = toRegion;
	}

	/**
	 * @return clientRef
	 */
	public String getClientRef() {
		return clientRef;
	}

	/**
	 * @param clientRef clientRef
	 */
	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	/**
	 * @return billingRef
	 */
	public String getBillingRef() {
		return billingRef;
	}

	/**
	 * @param billingRef billingRef
	 */
	public void setBillingRef(String billingRef) {
		this.billingRef = billingRef;
	}

	/**
	 * @return carrierRef
	 */
	public String getCarrierRef() {
		return carrierRef;
	}

	/**
	 * @param carrierRef carrierRef
	 */
	public void setCarrierRef(String carrierRef) {
		this.carrierRef = carrierRef;
	}

	/**
	 * @return message count
	 */
	public int getMessageCount() {
		return messageCount;
	}

	/**
	 * @param messageCount message count
	 */
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	/**
	 * @return billing state
	 */
	public String getBillingState() {
		return billingState;
	}

	/**
	 * @param billingState billing state
	 */
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	/**
	 * @return total price
	 */
	public String getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice total price
	 */
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return remaining credits
	 */
	public String getRemainingCredits() {
		return remainingCredits;
	}

	/**
	 * @param remainingCredits remaining credits
	 */
	public void setRemainingCredits(String remainingCredits) {
		this.remainingCredits = remainingCredits;
	}

	@Override
	public int hashCode() {
		if (getMessageId() == null) {
			return super.hashCode();
		}
		return Objects.hash(messageId);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof SMSResponse)) {
			return false;
		}
		SMSResponse o = (SMSResponse) obj;
		if (o.getMessageId() == null && getMessageId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(messageId, o.messageId);
	}

	@Override
	public String toString() {
		return "SMSResponse [status=" + status + ", description=" + description + ", action=" + action + ", messageId="
				+ messageId + ", to=" + to + ", toRegion=" + toRegion + ", clientRef=" + clientRef + ", billingRef="
				+ billingRef + ", carrierRef=" + carrierRef + ", messageCount=" + messageCount + ", billingState="
				+ billingState + ", totalPrice=" + totalPrice + ", remainingCredits=" + remainingCredits
				+ ", getStatus()=" + getStatus() + ", getDescription()=" + getDescription() + ", getAction()="
				+ getAction() + ", getMessageId()=" + getMessageId() + ", getTo()=" + getTo() + ", getToRegion()="
				+ getToRegion() + ", getClientRef()=" + getClientRef() + ", getBillingRef()=" + getBillingRef()
				+ ", getCarrierRef()=" + getCarrierRef() + ", getMessageCount()=" + getMessageCount()
				+ ", getBillingState()=" + getBillingState() + ", getTotalPrice()=" + getTotalPrice()
				+ ", getRemainingCredits()=" + getRemainingCredits() + ", hashCode()=" + hashCode() + ", getClass()="
				+ getClass() + ", toString()=" + super.toString() + "]";
	}

}
