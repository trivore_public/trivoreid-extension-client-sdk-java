package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single pricing in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID pricing definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CodeDiscount implements Serializable {

	private String title;
	private Float discountPercentage;
	private Float discountAmount;
	private Float discountAmountPerItem;
	private List<String> limitedToApiClients;
	private List<String> limitedToUsers;
	private List<String> limitedToUserGroups;
	private String code;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the discountPercentage
	 */
	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	/**
	 * @param discountPercentage the discountPercentage to set
	 */
	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	/**
	 * @return the discountAmount
	 */
	public Float getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(Float discountAmount) {
		this.discountAmount = discountAmount;
	}

	/**
	 * @return the discountAmountPerItem
	 */
	public Float getDiscountAmountPerItem() {
		return discountAmountPerItem;
	}

	/**
	 * @param discountAmountPerItem the discountAmountPerItem to set
	 */
	public void setDiscountAmountPerItem(Float discountAmountPerItem) {
		this.discountAmountPerItem = discountAmountPerItem;
	}

	/**
	 * @return the limitedToApiClients
	 */
	public List<String> getLimitedToApiClients() {
		if (limitedToApiClients == null) {
			limitedToApiClients = new ArrayList<>();
		}
		return limitedToApiClients;
	}

	/**
	 * @param limitedToApiClients the limitedToApiClients to set
	 */
	public void setLimitedToApiClients(List<String> limitedToApiClients) {
		this.limitedToApiClients = limitedToApiClients;
	}

	/**
	 * @return the limitedToUsers
	 */
	public List<String> getLimitedToUsers() {
		if (limitedToUsers == null) {
			limitedToUsers = new ArrayList<>();
		}
		return limitedToUsers;
	}

	/**
	 * @param limitedToUsers the limitedToUsers to set
	 */
	public void setLimitedToUsers(List<String> limitedToUsers) {
		this.limitedToUsers = limitedToUsers;
	}

	/**
	 * @return the limitedToUserGroups
	 */
	public List<String> getLimitedToUserGroups() {
		if (limitedToUserGroups == null) {
			limitedToUserGroups = new ArrayList<>();
		}
		return limitedToUserGroups;
	}

	/**
	 * @param limitedToUserGroups the limitedToUserGroups to set
	 */
	public void setLimitedToUserGroups(List<String> limitedToUserGroups) {
		this.limitedToUserGroups = limitedToUserGroups;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, limitedToUserGroups, limitedToUsers, limitedToApiClients, discountAmountPerItem,
				discountAmount, discountPercentage, title);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof CodeDiscount)) {
			return false;
		}
		CodeDiscount o = (CodeDiscount) obj;
		return Objects.equals(title, o.title) //
				&& Objects.equals(discountPercentage, o.discountPercentage) //
				&& Objects.equals(discountAmount, o.discountAmount) //
				&& Objects.equals(discountAmountPerItem, o.discountAmountPerItem) //
				&& Objects.equals(limitedToApiClients, o.limitedToApiClients) //
				&& Objects.equals(limitedToUsers, o.limitedToUsers)//
				&& Objects.equals(limitedToUserGroups, o.limitedToUserGroups)//
				&& Objects.equals(code, o.code);
	}

	@Override
	public String toString() {
		return "CodeDiscount [title=" + title + ", discountPercentage=" + discountPercentage + ", discountAmount="
				+ discountAmount + ", discountAmountPerItem=" + discountAmountPerItem + ", limitedToApiClients="
				+ limitedToApiClients + ", limitedToUsers=" + limitedToUsers + ", limitedToUserGroups="
				+ limitedToUserGroups + ", code=" + code + "]";
	}

}
