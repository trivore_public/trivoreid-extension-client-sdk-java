package com.trivore.id.sdk.models.paycard;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.Meta;

/**
 * An object which represents a PAN token.
 * <p>
 * Used to store PAN token info from Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PanToken implements Serializable {

	private String id;
	private String paycardId;
	private String token;
	private String issuer;
	private String timestamp;
	private Meta meta;

	/**
	 * Construct a pantoken.
	 */
	public PanToken() {
		// ...
	}

	/**
	 * Get pantoken ID.
	 *
	 * @return pantoken ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set pantoken ID.
	 *
	 * @param id pantoken ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get paycard ID pantoken belongs to.
	 *
	 * @return paycard ID pantoken belongs to
	 */
	public String getPaycardId() {
		return paycardId;
	}

	/**
	 * Set paycard ID pantoken belongs to.
	 *
	 * @param paycardId paycard ID pantoken belongs to
	 */
	public void setPaycardId(String paycardId) {
		this.paycardId = paycardId;
	}

	/**
	 * Get token value.
	 *
	 * @return token value
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Set token value.
	 *
	 * @param token token value
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Get pantoken issuer.
	 *
	 * @return pantoken issuer
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * Set pantoken issuer.
	 *
	 * @param issuer pantoken issuer
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	/**
	 * Get pantoken timestamp.
	 *
	 * @return pantoken timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Set pantoken timestamp.
	 *
	 * @param timestamp pantoken timestamp
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PanToken)) {
			return false;
		}
		PanToken o = (PanToken) obj;
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "PanToken [id=" + id + ", paycardId=" + paycardId + ", token=" + token + ", issuer=" + issuer
				+ ", timestamp=" + timestamp + "]";
	}

}
