package com.trivore.id.sdk.models.namespace;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trivore.id.sdk.models.Meta;

/**
 * An object which presents a single namespace (customer) in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID namespace definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Namespace implements Serializable {

	private String id;
	private String code;
	private String name;
	private String shortName;
	private UsernamePolicy usernamePolicy;
	private Integer commMethodMaxQty;
	private boolean duplicateNicknamesAllowed;
	private String validFrom;
	private String validTo;
	private SMSSettings smsSettings;
	private boolean authorisationRestrictedMode;
	private Meta meta;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private GroupPolicy defaultGroupPolicy;

	/**
	 * Construct a new namespace.
	 */
	public Namespace() {
		// ...
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	/**
	 * Get the unique identifier of the namespace.
	 * <p>
	 * This value is automatically assigned by the Trivore ID during creation.
	 * </p>
	 *
	 * @return The unique identifier of the namespace.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the unique for the namespace.
	 * <p>
	 * This value is automatically assigned by the Trivore ID during creation.
	 * </p>
	 *
	 * @param id The unique identifier for the namespace.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the namespace code identifier.
	 * <p>
	 * This value can be also used to identify the namespace. Works also when
	 * performing REST API calls by using this code value as the target value.
	 * </p>
	 * <p>
	 * <b>Note:</b> Trivore ID requires namespace codes to be unique.
	 * </p>
	 *
	 * @return The code of the namespace.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Set the namespace code identifier.
	 * <p>
	 * This value can be also used to identify the namespace. Works also when
	 * performing REST API calls by using this code value as the target value.
	 * </p>
	 * <p>
	 * <b>Note:</b> Trivore ID requires namespace codes to be unique.
	 * </p>
	 *
	 * @param code The code for the namespace.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get the name of the namespace.
	 * <p>
	 * The name is a free-form human readable identifier for the namespace.
	 * </p>
	 *
	 * @return The name of the namespace.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name for the namespace.
	 * <p>
	 * The name is a free-form human readable identifier for the namespace.
	 * </p>
	 *
	 * @param name The name for the namespace.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the short version of the namespace's name.
	 * <p>
	 * The name is a free-form human readable identifier for the namespace.
	 * </p>
	 *
	 * @return The short version of the namespace's name.
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Set the short version of the namespace's name.
	 * <p>
	 * The name is a free-form human readable identifier for the namespace.
	 * </p>
	 *
	 * @param shortName The short version of the namespace's name.
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Get namespace username policy.
	 *
	 * @return namespace username policy
	 */
	public UsernamePolicy getUsernamePolicy() {
		return usernamePolicy;
	}

	/**
	 * Set namespace username policy.
	 *
	 * @param usernamePolicy namespace username policy
	 */
	public void setUsernamePolicy(UsernamePolicy usernamePolicy) {
		this.usernamePolicy = usernamePolicy;
	}

	/**
	 * Get maximum number of email addresses and mobile phone numbers allowed for
	 * users.
	 *
	 * @return maximum number of email addresses and mobile phone numbers allowed
	 *         for users
	 */
	public Integer getCommMethodMaxQty() {
		return commMethodMaxQty;
	}

	/**
	 * Set maximum number of email addresses and mobile phone numbers allowed for
	 * users.
	 *
	 * @param commMethodMaxQty maximum number of email addresses and mobile phone
	 *                         numbers allowed for users
	 */
	public void setCommMethodMaxQty(Integer commMethodMaxQty) {
		this.commMethodMaxQty = commMethodMaxQty;
	}

	/**
	 * Get if duplicated nicknames are allowed.
	 * <p>
	 * Sets whether duplicate nicknames are allowed within the namespace.
	 * </p>
	 *
	 * @return if duplicated nicknames are allowed
	 */
	public boolean isDuplicateNicknamesAllowed() {
		return duplicateNicknamesAllowed;
	}

	/**
	 * Set if duplicated nicknames are allowed.
	 * <p>
	 * Sets whether duplicate nicknames are allowed within the namespace.
	 * </p>
	 *
	 * @param duplicateNicknamesAllowed if duplicated nicknames are allowed
	 */
	public void setDuplicateNicknamesAllowed(boolean duplicateNicknamesAllowed) {
		this.duplicateNicknamesAllowed = duplicateNicknamesAllowed;
	}

	/**
	 * Get valid from.
	 *
	 * @return valid from
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * Set valid from.
	 *
	 * @param validFrom valid from
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * Get valid to.
	 *
	 * @return valid to
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Set valid to.
	 *
	 * @param validTo valid to
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Get SMS settings.
	 *
	 * @return SMS settings
	 */
	public SMSSettings getSmsSettings() {
		if (smsSettings == null) {
			smsSettings = new SMSSettings();
		}
		return smsSettings;
	}

	/**
	 * Set SMS settings.
	 *
	 * @param smsSettings SMS settings
	 */
	public void setSmsSettings(SMSSettings smsSettings) {
		this.smsSettings = smsSettings;
	}

	/**
	 * Get default group policy of the namespace. Sets many of the user settings.
	 *
	 * @return default group policy of the namespace
	 */
	public GroupPolicy getDefaultGroupPolicy() {
		if (defaultGroupPolicy == null) {
			defaultGroupPolicy = new GroupPolicy();
		}
		return defaultGroupPolicy;
	}

	/**
	 * Set default group policy of the namespace. Sets many of the user settings.
	 *
	 * @param defaultGroupPolicy default group policy of the namespace
	 */
	public void setDefaultGroupPolicy(GroupPolicy defaultGroupPolicy) {
		this.defaultGroupPolicy = defaultGroupPolicy;
	}

	/**
	 * Get if restricted mode for the authoristions enabled.
	 * <p>
	 * Restricted mode for the authoristions enabled. Can only be enabled in the Web
	 * UI.
	 * </p>
	 *
	 * @return if restricted mode for the authoristions enabled
	 */
	public boolean isAuthorisationRestrictedMode() {
		return authorisationRestrictedMode;
	}

	/**
	 * Set if restricted mode for the authoristions enabled.
	 * <p>
	 * Restricted mode for the authoristions enabled. Can only be enabled in the Web
	 * UI.
	 * </p>
	 *
	 * @param authorisationRestrictedMode if restricted mode for the authoristions
	 *                                    enabled
	 */
	public void setAuthorisationRestrictedMode(boolean authorisationRestrictedMode) {
		this.authorisationRestrictedMode = authorisationRestrictedMode;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof Namespace)) {
			return false;
		}
		Namespace o = (Namespace) other;
		return Objects.equals(code, o.code);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code);
	}

	@Override
	public String toString() {
		return "Namespace [id=" + id + ", code=" + code + ", name=" + name + ", shortName=" + shortName
				+ ", usernamePolicy=" + usernamePolicy + ", commMethodMaxQty=" + commMethodMaxQty
				+ ", duplicateNicknamesAllowed=" + duplicateNicknamesAllowed + ", validFrom=" + validFrom + ", validTo="
				+ validTo + ", smsSettings=" + smsSettings + ", authorisationRestrictedMode="
				+ authorisationRestrictedMode + ", defaultGroupPolicy=" + defaultGroupPolicy + "]";
	}

}
