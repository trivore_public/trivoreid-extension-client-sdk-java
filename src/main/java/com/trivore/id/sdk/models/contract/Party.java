package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents party in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Party implements Serializable {

	private String id;
	private String userId;
	private String externalId;
	private String name;
	private String address;
	private String mobile;
	private String email;
	private List<Signer> signers;
	private List<PartyContact> partyContacts;
	private String functionalReference;
	private PartyTerminationMode terminationMode;

	/**
	 * Construct a new party object.
	 */
	public Party() {
		// ...
	}

	/**
	 * Get id.
	 *
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set id.
	 *
	 * @param id id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get user id.
	 *
	 * @return user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Set user id.
	 *
	 * @param userId user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Get external id.
	 *
	 * @return external id
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * Set external id.
	 *
	 * @param externalId external id
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * Get name.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set name.
	 *
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get address.
	 *
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Set address.
	 *
	 * @param address address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Get mobile.
	 *
	 * @return mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Set mobile.
	 *
	 * @param mobile mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Get email.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set email.
	 *
	 * @param email email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get signers.
	 * <p>
	 * People who are required to sign for this contract party.
	 * </p>
	 *
	 * @return signers
	 */
	public List<Signer> getSigners() {
		if (signers == null) {
			signers = new ArrayList<>();
		}
		return signers;
	}

	/**
	 * Set signers.
	 * <p>
	 * People who are required to sign for this contract party.
	 * </p>
	 *
	 * @param signers signers
	 */
	public void setSigners(List<Signer> signers) {
		this.signers = signers;
	}

	/**
	 * Get contacts.
	 * <p>
	 * Additional people connected to this party who may be contacted regarding the
	 * contract.
	 * </p>
	 *
	 * @return contacts
	 */
	public List<PartyContact> getContacts() {
		if (partyContacts == null) {
			partyContacts = new ArrayList<>();
		}
		return partyContacts;
	}

	/**
	 * Set contacts.
	 * <p>
	 * Additional people connected to this party who may be contacted regarding the
	 * contract.
	 * </p>
	 *
	 * @param partyContacts contacts
	 */
	public void setContacts(List<PartyContact> partyContacts) {
		this.partyContacts = partyContacts;
	}

	/**
	 * Get functional reference.
	 * <p>
	 * Functional reference of the party, for example Licensee, Provider, Lender,
	 * Seller.
	 * </p>
	 *
	 * @return functional reference
	 */
	public String getFunctionalReference() {
		return functionalReference;
	}

	/**
	 * Set functional reference.
	 * <p>
	 * Functional reference of the party, for example Licensee, Provider, Lender,
	 * Seller.
	 * </p>
	 *
	 * @param functionalReference functional reference
	 */
	public void setFunctionalReference(String functionalReference) {
		this.functionalReference = functionalReference;
	}

	/**
	 * Get termination mode.
	 *
	 * @return termination mode
	 */
	public PartyTerminationMode getTerminationMode() {
		return terminationMode;
	}

	/**
	 * Set termination mode.
	 *
	 * @param terminationMode termination mode
	 */
	public void setTerminationMode(PartyTerminationMode terminationMode) {
		this.terminationMode = terminationMode;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Party)) {
			return false;
		}
		Party o = (Party) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Party [id=" + id + ", userId=" + userId + ", externalId=" + externalId + ", name=" + name + ", address="
				+ address + ", mobile=" + mobile + ", email=" + email + ", signers=" + signers + ", contacts="
				+ partyContacts + ", functionalReference=" + functionalReference + ", terminationMode=" + terminationMode
				+ "]";
	}

}
