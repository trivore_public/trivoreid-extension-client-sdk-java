package com.trivore.id.sdk.models.contract;

/**
 * Current state of the contract.
 */
@SuppressWarnings("javadoc")
public enum CurrentState {
	DRAFT, SIGNABLE, SIGNED, EXPIRED, UNDER_NOTICE, TERMINATED, ARCHIVED
}
