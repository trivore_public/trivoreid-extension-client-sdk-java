package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single builtin role in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID builtin role definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class BuiltInRole implements Serializable {

	private String id;
	private String name;
	private List<String> permissions;

	/**
	 * @return role unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id role unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return english language name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name english language name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return permissions granted by having this role
	 */
	public List<String> getPermissions() {
		if (permissions == null) {
			permissions = new ArrayList<>();
		}
		return permissions;
	}

	/**
	 * @param permissions permissions granted by having this role
	 */
	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof BuiltInRole)) {
			return false;
		}
		BuiltInRole o = (BuiltInRole) object;
		if (o.getId() == null && getId() == null) {
			return super.equals(object);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "BuiltInRole [id=" + id + ", name=" + name + ", permissions=" + permissions + "]";
	}

}
