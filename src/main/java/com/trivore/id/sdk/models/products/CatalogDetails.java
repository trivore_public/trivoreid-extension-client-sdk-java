package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents catalog details in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID catalog definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CatalogDetails implements Serializable {

	private String catalogId;
	private String name;
	private Map<Object, Object> customFields;
	private List<ProductDetails> products;

	/**
	 * @return the catalogId
	 */
	public String getCatalogId() {
		return catalogId;
	}

	/**
	 * @param catalogId the catalogId
	 */
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the customFields
	 */
	public Map<Object, Object> getCustomFields() {
		return customFields;
	}

	/**
	 * @param customFields the customFields
	 */
	public void setCustomFields(Map<Object, Object> customFields) {
		this.customFields = customFields;
	}

	/**
	 * Catalog products. Only currently sellable products included.
	 *
	 * @return the products
	 */
	public List<ProductDetails> getProducts() {
		return products;
	}

	/**
	 * Catalog products. Only currently sellable products included.
	 *
	 * @param products the products
	 */
	public void setProducts(List<ProductDetails> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		return Objects.hash(catalogId, name, customFields, products);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof CatalogDetails)) {
			return false;
		}
		CatalogDetails o = (CatalogDetails) obj;
		return Objects.equals(catalogId, o.catalogId) //
				&& Objects.equals(name, o.name) //
				&& Objects.equals(customFields, o.customFields) //
				&& Objects.equals(products, o.products);
	}

	@Override
	public String toString() {
		return "CatalogDetails [catalogId=" + catalogId + ", name=" + name + ", customFields=" + customFields
				+ ", products=" + products + "]";
	}

}
