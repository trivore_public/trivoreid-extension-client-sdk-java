package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single authorisation right object in Trivore ID.
 * <p>
 * Used to specify the right object of the authorisation in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthGrantRightObject implements Serializable {

	private String type;
	private String value;

	/**
	 * Construct a new grant right object.
	 */
	public AuthGrantRightObject() {
		// ...
	}

	/**
	 * Get the type of the authorisation right object.
	 * <p>
	 * Type of the object. Currently always "user".
	 * </p>
	 *
	 * @return the type of the authorisation right object
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set the type of the authorisation right object.
	 * <p>
	 * Type of the object. Currently always "user".
	 * </p>
	 *
	 * @param type the type of the authorisation right object
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Get the right object value.
	 * <p>
	 * The id of the authorisation object. This value will be the end-user's user id
	 * and is automatically determined from the OAuth 2.0 access token.
	 * </p>
	 *
	 * @return the right object value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the right object value.
	 * <p>
	 * The id of the authorisation object. This value will be the end-user's user id
	 * and is automatically determined from the OAuth 2.0 access token.
	 * </p>
	 *
	 * @param value the right object value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthGrantRightObject)) {
			return false;
		}
		AuthGrantRightObject o = (AuthGrantRightObject) obj;
		return Objects.equals(type, o.type) && Objects.equals(value, o.value);
	}

	@Override
	public String toString() {
		return "AuthGrantRightObject [type=" + type + ", value=" + value + "]";
	}
}
