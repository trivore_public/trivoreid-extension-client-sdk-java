package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class with the validation error.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidationError implements Serializable {

	private String message;
	private ValidationErrorCode code;

	/**
	 * @return error message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message error message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return validation error code
	 */
	public ValidationErrorCode getCode() {
		return code;
	}

	/**
	 * @param code validation error code
	 */
	public void setCode(ValidationErrorCode code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		return Objects.hash(message, code);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ValidationError)) {
			return false;
		}
		ValidationError o = (ValidationError) obj;
		return Objects.equals(message, o.message) && Objects.equals(code, o.code);
	}

	@Override
	public String toString() {
		return "ValidationError [message=" + message + ", code=" + code + "]";
	}

}
