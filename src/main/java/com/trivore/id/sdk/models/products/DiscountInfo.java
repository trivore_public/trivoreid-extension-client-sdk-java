package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents discount info in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID discount info definition.
 * </p>
 * <p>
 * Discounts applied to the final total price
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscountInfo implements Serializable {

	private String title;
	private Float amount;
	private Boolean vatIncluded;

	/**
	 * @return the discount title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the discount title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the discount amount
	 */
	public Float getAmount() {
		return amount;
	}

	/**
	 * @param amount the discount amount
	 */
	public void setAmount(Float amount) {
		this.amount = amount;
	}

	/**
	 * @return is VAT included in discount amount
	 */
	public Boolean getVatIncluded() {
		return vatIncluded;
	}

	/**
	 * @param vatIncluded is VAT included in discount amount
	 */
	public void setVatIncluded(Boolean vatIncluded) {
		this.vatIncluded = vatIncluded;
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, amount, vatIncluded);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof DiscountInfo)) {
			return false;
		}
		DiscountInfo o = (DiscountInfo) obj;
		return Objects.equals(title, o.title) //
				&& Objects.equals(amount, o.amount) //
				&& Objects.equals(vatIncluded, o.vatIncluded);
	}

	@Override
	public String toString() {
		return "DiscountInfo [title=" + title + ", amount=" + amount + ", vatIncluded=" + vatIncluded + "]";
	}

}
