package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single contract in Trivore ID.
 * <p>
 * Used to specify the contract Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contract implements Serializable {

	private String id;
	private Termination termination;
	private int termOfNoticeDays;
	private TerminationMode terminationMode;
	private String title;
	private String version;
	private ContractContent body;
	private String validFrom;
	private String validTo;
	private String code;
	private String scope;
	private FinancialTerms financialTerms;
	private String termsOfDelivery;
	private List<Party> parties;
	private List<String> links;
	private List<StateEvent> stateEvents;
	private CurrentState currentState;
	private String currentStateChanged;
	private List<Appendix> appendices;
	private String addendumFor;
	private List<String> contractRefs;
	private String frameworkAgreementRef;
	private String ownerId;
	private String notes;

	/**
	 * Construct a new contract object.
	 */
	public Contract() {
		// ...
	}

	/**
	 * Get unique identifier.
	 *
	 * @return unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set unique identifier.
	 *
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get termination.
	 *
	 * @return termination
	 */
	public Termination getTermination() {
		if (termination == null) {
			termination = new Termination();
		}
		return termination;
	}

	/**
	 * Set termination.
	 *
	 * @param termination termination
	 */
	public void setTermination(Termination termination) {
		this.termination = termination;
	}

	/**
	 * Get term of notice days.
	 * <p>
	 * When termination has been initiated, the contract will be in Under-Notice
	 * state this amount of days until moving to Terminated state. If null or 0,
	 * contract will move to Terminated state immediately.
	 * </p>
	 *
	 * @return term of notice days
	 */
	public int getTermOfNoticeDays() {
		return termOfNoticeDays;
	}

	/**
	 * Set term of notice days.
	 * <p>
	 * When termination has been initiated, the contract will be in Under-Notice
	 * state this amount of days until moving to Terminated state. If null or 0,
	 * contract will move to Terminated state immediately.
	 * </p>
	 *
	 * @param termOfNoticeDays term of notice days
	 */
	public void setTermOfNoticeDays(int termOfNoticeDays) {
		this.termOfNoticeDays = termOfNoticeDays;
	}

	/**
	 * Get termination mode.
	 * <p>
	 * Contract will terminate after this rule is met.
	 * </p>
	 *
	 * @return termination mode
	 */
	public TerminationMode getTerminationMode() {
		return terminationMode;
	}

	/**
	 * Set termination mode.
	 * <p>
	 * Contract will terminate after this rule is met.
	 * </p>
	 *
	 * @param terminationMode termination mode
	 */
	public void setTerminationMode(TerminationMode terminationMode) {
		this.terminationMode = terminationMode;
	}

	/**
	 * Get title.
	 *
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set title.
	 *
	 * @param title title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get version.
	 *
	 * @return version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Set version.
	 *
	 * @param version version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Get body.
	 * <p>
	 * Contract body or appendix content.
	 * </p>
	 *
	 * @return body
	 */
	public ContractContent getBody() {
		if (body == null) {
			body = new ContractContent();
		}
		return body;
	}

	/**
	 * Set body.
	 * <p>
	 * Contract body or appendix content.
	 * </p>
	 *
	 * @param body body
	 */
	public void setBody(ContractContent body) {
		this.body = body;
	}

	/**
	 * Get valid from.
	 *
	 * @return valid from
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * Set valid from.
	 *
	 * @param validFrom valid from
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * Get valid to.
	 *
	 * @return valid to
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Set valid to.
	 *
	 * @param validTo valid to
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Get code.
	 *
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Set code.
	 *
	 * @param code code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get scope.
	 *
	 * @return scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * Set scope.
	 *
	 * @param scope scope
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * Get financial terms.
	 *
	 * @return financial terms
	 */
	public FinancialTerms getFinancialTerms() {
		if (financialTerms == null) {
			financialTerms = new FinancialTerms();
		}
		return financialTerms;
	}

	/**
	 * Set financial terms.
	 *
	 * @param financialTerms financial terms
	 */
	public void setFinancialTerms(FinancialTerms financialTerms) {
		this.financialTerms = financialTerms;
	}

	/**
	 * Get terms of delivery.
	 *
	 * @return terms of delivery
	 */
	public String getTermsOfDelivery() {
		return termsOfDelivery;
	}

	/**
	 * Set terms of delivery.
	 *
	 * @param termsOfDelivery terms of delivery
	 */
	public void setTermsOfDelivery(String termsOfDelivery) {
		this.termsOfDelivery = termsOfDelivery;
	}

	/**
	 * Get parties.
	 *
	 * @return parties
	 */
	public List<Party> getParties() {
		if (parties == null) {
			parties = new ArrayList<>();
		}
		return parties;
	}

	/**
	 * Set parties.
	 *
	 * @param parties parties
	 */
	public void setParties(List<Party> parties) {
		this.parties = parties;
	}

	/**
	 * Get links.
	 *
	 * @return links
	 */
	public List<String> getLinks() {
		if (links == null) {
			links = new ArrayList<>();
		}
		return links;
	}

	/**
	 * Set links.
	 *
	 * @param links links
	 */
	public void setLinks(List<String> links) {
		this.links = links;
	}

	/**
	 * Get state events.
	 *
	 * @return state events
	 */
	public List<StateEvent> getStateEvents() {
		if (stateEvents == null) {
			stateEvents = new ArrayList<>();
		}
		return stateEvents;
	}

	/**
	 * Set state events.
	 *
	 * @param stateEvents state events
	 */
	public void setStateEvents(List<StateEvent> stateEvents) {
		this.stateEvents = stateEvents;
	}

	/**
	 * Get current state.
	 *
	 * @return current state
	 */
	public CurrentState getCurrentState() {
		return currentState;
	}

	/**
	 * Set current state.
	 *
	 * @param currentState current state
	 */
	public void setCurrentState(CurrentState currentState) {
		this.currentState = currentState;
	}

	/**
	 * Get current state changed.
	 *
	 * @return current state changed
	 */
	public String getCurrentStateChanged() {
		return currentStateChanged;
	}

	/**
	 * Set current state changed.
	 *
	 * @param currentStateChanged current state changed
	 */
	public void setCurrentStateChanged(String currentStateChanged) {
		this.currentStateChanged = currentStateChanged;
	}

	/**
	 * Get appendices.
	 *
	 * @return appendices
	 */
	public List<Appendix> getAppendices() {
		if (appendices == null) {
			appendices = new ArrayList<>();
		}
		return appendices;
	}

	/**
	 * Set appendices.
	 *
	 * @param appendices appendices
	 */
	public void setAppendices(List<Appendix> appendices) {
		this.appendices = appendices;
	}

	/**
	 * Get addendumFor.
	 * <p>
	 * If the ID of another contract is specified, this contract is an Addendum for
	 * that other contract.
	 * </p>
	 *
	 * @return addendumFor
	 */
	public String getAddendumFor() {
		return addendumFor;
	}

	/**
	 * Set addendumFor.
	 * <p>
	 * If the ID of another contract is specified, this contract is an Addendum for
	 * that other contract.
	 * </p>
	 *
	 * @param addendumFor addendumFor
	 */
	public void setAddendumFor(String addendumFor) {
		this.addendumFor = addendumFor;
	}

	/**
	 * Get contract references.
	 *
	 * @return contract references
	 */
	public List<String> getContractRefs() {
		if (contractRefs == null) {
			contractRefs = new ArrayList<>();
		}
		return contractRefs;
	}

	/**
	 * Set contract references.
	 *
	 * @param contractRefs contract references
	 */
	public void setContractRefs(List<String> contractRefs) {
		this.contractRefs = contractRefs;
	}

	/**
	 * Get framework agreement reference.
	 *
	 * @return framework agreement reference
	 */
	public String getFrameworkAgreementRef() {
		return frameworkAgreementRef;
	}

	/**
	 * Set framework agreement reference.
	 *
	 * @param frameworkAgreementRef framework agreement reference
	 */
	public void setFrameworkAgreementRef(String frameworkAgreementRef) {
		this.frameworkAgreementRef = frameworkAgreementRef;
	}

	/**
	 * Get ownerId.
	 * <p>
	 * User ID of contract owner. Owner has right to edit the contract while a
	 * draft, and to cancel it before it is signed.
	 * </p>
	 *
	 * @return ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * Set ownerId.
	 * <p>
	 * User ID of contract owner. Owner has right to edit the contract while a
	 * draft, and to cancel it before it is signed.
	 * </p>
	 *
	 * @param ownerId ownerId
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Get notes.
	 *
	 * @return notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Set notes.
	 *
	 * @param notes notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Contract)) {
			return false;
		}
		Contract o = (Contract) obj;
		if (o.getId() == null && getId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Contract [id=" + id + ", termination=" + termination + ", termOfNoticeDays=" + termOfNoticeDays
				+ ", terminationMode=" + terminationMode + ", title=" + title + ", version=" + version + ", body="
				+ body + ", validFrom=" + validFrom + ", validTo=" + validTo + ", code=" + code + ", scope=" + scope
				+ ", financialTerms=" + financialTerms + ", termsOfDelivery=" + termsOfDelivery + ", parties=" + parties
				+ ", links=" + links + ", stateEvents=" + stateEvents + ", currentState=" + currentState
				+ ", currentStateChanged=" + currentStateChanged + ", appendices=" + appendices + ", addendumFor="
				+ addendumFor + ", contractRefs=" + contractRefs + ", frameworkAgreementRef=" + frameworkAgreementRef
				+ ", ownerId=" + ownerId + ", notes=" + notes + "]";
	}

}
