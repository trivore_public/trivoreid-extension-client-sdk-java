package com.trivore.id.sdk.models.user.strong.identification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a user's strong identification info.
 * <p>
 * Used to get the user's strong identification info from Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StrongIdentificationInfo implements Serializable {

	private String method;
	private List<String> identificationDocuments;
	private String otherExplanation;
	private String personalId;
	private String remarks;
	private String identifier;
	private String time;
	private String firstTime;
	private int count;
	private boolean identified;

	/**
	 * Get method used to strongly identify user.
	 * <p>
	 * IN_PERSON, SUOMI_FI, SUOMI_FI_VALTUUDET or USER_API.
	 * </p>
	 *
	 * @return method used to strongly identify user
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Set method used to strongly identify user.
	 * <p>
	 * IN_PERSON, SUOMI_FI, SUOMI_FI_VALTUUDET or USER_API.
	 * </p>
	 *
	 * @param method method used to strongly identify user
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Get documents used during identification.
	 *
	 * @return documents used during identification
	 */
	public List<String> getIdentificationDocuments() {
		if (identificationDocuments == null) {
			identificationDocuments = new ArrayList<>();
		}
		return identificationDocuments;
	}

	/**
	 * Set documents used during identification.
	 *
	 * @param identificationDocuments documents used during identification
	 */
	public void setIdentificationDocuments(List<String> identificationDocuments) {
		this.identificationDocuments = identificationDocuments;
	}

	/**
	 * Get other explanation related to identification.
	 *
	 * @return other explanation related to identification
	 */
	public String getOtherExplanation() {
		return otherExplanation;
	}

	/**
	 * Set other explanation related to identification.
	 *
	 * @param otherExplanation other explanation related to identification
	 */
	public void setOtherExplanation(String otherExplanation) {
		this.otherExplanation = otherExplanation;
	}

	/**
	 * Get user's personal ID.
	 * <p>
	 * May be censored if viewer doesn't have permission ACCOUNT_VIEW_PERSONAL_ID.
	 * </p>
	 *
	 * @return user's personal ID
	 */
	public String getPersonalId() {
		return personalId;
	}

	/**
	 * Set user's personal ID.
	 * <p>
	 * May be censored if viewer doesn't have permission ACCOUNT_VIEW_PERSONAL_ID.
	 * </p>
	 *
	 * @param personalId user's personal ID
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	/**
	 * Get remarks related to last strong identification event.
	 *
	 * @return remarks related to last strong identification event
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Set remarks related to last strong identification event.
	 *
	 * @param remarks remarks related to last strong identification event
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Get user name, client ID, or other who performed last identification.
	 *
	 * @return user name, client ID, or other who performed last identification
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Set user name, client ID, or other who performed last identification.
	 *
	 * @param identifier user name, client ID, or other who performed last
	 *                   identification
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Get timestamp when user was strongly identified.
	 *
	 * @return timestamp when user was strongly identified
	 */
	public String getTime() {
		return time;
	}

	/**
	 * Set timestamp when user was strongly identified.
	 *
	 * @param time timestamp when user was strongly identified
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Get first time when user was strongly identified.
	 *
	 * @return first time when user was strongly identified
	 */
	public String getFirstTime() {
		return firstTime;
	}

	/**
	 * Set first time when user was strongly identified.
	 *
	 * @param firstTime first time when user was strongly identified
	 */
	public void setFirstTime(String firstTime) {
		this.firstTime = firstTime;
	}

	/**
	 * Get number of times user has been strongly identified.
	 *
	 * @return number of times user has been strongly identified
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Set number of times user has been strongly identified.
	 *
	 * @param count number of times user has been strongly identified
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Get identification status.
	 * <p>
	 * Has user been strongly identified.
	 * </p>
	 *
	 * @return identification status
	 */
	public boolean isIdentified() {
		return identified;
	}

	/**
	 * Set identification status.
	 * <p>
	 * Has user been strongly identified.
	 * </p>
	 *
	 * @param identified identification status
	 */
	public void setIdentified(boolean identified) {
		this.identified = identified;
	}

	@Override
	public int hashCode() {
		return Objects.hash(method, identificationDocuments, otherExplanation, personalId, remarks, identifier, time,
				firstTime, count, identified);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof StrongIdentificationInfo)) {
			return false;
		}
		StrongIdentificationInfo o = (StrongIdentificationInfo) object;
		return Objects.equals(method, o.method) && Objects.equals(identificationDocuments, o.identificationDocuments)
				&& Objects.equals(otherExplanation, o.otherExplanation) && Objects.equals(personalId, o.personalId)
				&& Objects.equals(remarks, o.remarks) && Objects.equals(identifier, o.identifier)
				&& Objects.equals(time, o.time) && Objects.equals(firstTime, o.firstTime)
				&& Objects.equals(count, o.count) && Objects.equals(identified, o.identified);
	}

	@Override
	public String toString() {
		return "StrongIdentificationInfo [method=" + method + ", identificationDocuments=" + identificationDocuments
				+ ", otherExplanation=" + otherExplanation + ", personalId=" + personalId + ", remarks=" + remarks
				+ ", identifier=" + identifier + ", time=" + time + ", firstTime=" + firstTime + ", count=" + count
				+ ", identified=" + identified + "]";
	}

}
