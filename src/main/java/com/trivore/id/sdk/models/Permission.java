package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single permission in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID permission definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Permission implements Serializable {

	private String id;
	private String name;
	private List<String> dependencies;
	private String groupId;

	/**
	 * @return permission unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id permission unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return permission's English language name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name permission's English language name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return having this permission also grants all listed dependency permissions.
	 */
	public List<String> getDependencies() {
		if (dependencies == null) {
			dependencies = new ArrayList<>();
		}
		return dependencies;
	}

	/**
	 * @param dependencies having this permission also grants all listed dependency
	 *                     permissions.
	 */
	public void setDependencies(List<String> dependencies) {
		this.dependencies = dependencies;
	}

	/**
	 * @return ID of permission group. Built in permissions may be grouped for
	 *         display purposes.
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId ID of permission group. Built in permissions may be grouped
	 *                for display purposes.
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof Permission)) {
			return false;
		}
		Permission o = (Permission) object;
		if (o.getId() == null && getId() == null) {
			return super.equals(object);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "Permission [id=" + id + ", name=" + name + ", dependencies=" + dependencies + ", groupId=" + groupId
				+ "]";
	}

}
