package com.trivore.id.sdk.models.user.strong.identification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a number number in Trivore ID.
 * <p>
 * Used to specify number numbers that can be validated in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StrongIdentification implements Serializable {

	private String id;
	private String userId;
	private StrongIdentificationMethod method;
	private List<String> identificationDocuments;
	private String otherExplanation;
	private String personalId;
	private String remarks;
	private String identifier;
	private Boolean identified;
	private String validTo;
	private Boolean expired;
	private String timestamp;

	/**
	 * @return Method used to strongly identify user
	 */
	public StrongIdentificationMethod getMethod() {
		return method;
	}

	/**
	 * @param method Method used to strongly identify user
	 */
	public void setMethod(StrongIdentificationMethod method) {
		this.method = method;
	}

	/**
	 * @return Documents used during identification
	 */
	public List<String> getIdentificationDocuments() {
		if (identificationDocuments == null) {
			identificationDocuments = new ArrayList<>();
		}
		return identificationDocuments;
	}

	/**
	 * @param identificationDocuments Documents used during identification
	 */
	public void setIdentificationDocuments(List<String> identificationDocuments) {
		this.identificationDocuments = identificationDocuments;
	}

	/**
	 * @return Other explanation related to identification
	 */
	public String getOtherExplanation() {
		return otherExplanation;
	}

	/**
	 * @param otherExplanation Other explanation related to identification
	 */
	public void setOtherExplanation(String otherExplanation) {
		this.otherExplanation = otherExplanation;
	}

	/**
	 * @return User's personal ID. May be censored if viewer doesn't have permission
	 *         ACCOUNT_VIEW_PERSONAL_ID.
	 */
	public String getPersonalId() {
		return personalId;
	}

	/**
	 * @param personalId User's personal ID. May be censored if viewer doesn't have
	 *                   permission ACCOUNT_VIEW_PERSONAL_ID.
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	/**
	 * @return Remarks related to last strong identification event.
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks Remarks related to last strong identification event.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return User name, client ID, or other who performed last identification.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier User name, client ID, or other who performed last
	 *                   identification.
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Has user been strongly identified and is the identification valid. Generated
	 * property. Not filterable.
	 *
	 * @return Has user been strongly identified
	 */
	public Boolean getIdentified() {
		return identified;
	}

	/**
	 * Has user been strongly identified and is the identification valid. Generated
	 * property. Not filterable.
	 *
	 * @param identified Has user been strongly identified
	 */
	public void setIdentified(Boolean identified) {
		this.identified = identified;
	}

	/**
	 * @return the entry id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the entry id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * The strong identification is valid until this date and time. Generated
	 * property. Not filterable.
	 *
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * The strong identification is valid until this date and time. Generated
	 * property. Not filterable.
	 *
	 * @param validTo the validTo
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Has the strong identification expired. Generated property. Not filterable.
	 *
	 * @return the expired
	 */
	public Boolean getExpired() {
		return expired;
	}

	/**
	 * Has the strong identification expired. Generated property. Not filterable.
	 *
	 * @param expired the expired
	 */
	public void setExpired(Boolean expired) {
		this.expired = expired;
	}

	/**
	 * @return the timestamp when user was strongly identified
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp when user was strongly identified
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(method, identificationDocuments, otherExplanation, personalId, remarks, identifier,
				identified, validTo, expired, timestamp, id, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof StrongIdentification)) {
			return false;
		}
		StrongIdentification o = (StrongIdentification) obj;
		return Objects.equals(method, o.method) //
				&& Objects.equals(identificationDocuments, o.identificationDocuments) //
				&& Objects.equals(otherExplanation, o.otherExplanation) //
				&& Objects.equals(personalId, o.personalId) //
				&& Objects.equals(remarks, o.remarks) //
				&& Objects.equals(identifier, o.identifier) //
				&& Objects.equals(identified, o.identified) //
				&& Objects.equals(validTo, o.validTo) //
				&& Objects.equals(expired, o.expired) //
				&& Objects.equals(timestamp, o.timestamp) //
				&& Objects.equals(id, o.id) //
				&& Objects.equals(userId, o.userId);
	}

	@Override
	public String toString() {
		return "StrongIdentification [id=" + id + ", userId=" + userId + ", method=" + method
				+ ", identificationDocuments=" + identificationDocuments + ", otherExplanation=" + otherExplanation
				+ ", personalId=" + personalId + ", remarks=" + remarks + ", identifier=" + identifier + ", identified="
				+ identified + ", validTo=" + validTo + ", expired=" + expired + ", timestamp=" + timestamp + "]";
	}

}
