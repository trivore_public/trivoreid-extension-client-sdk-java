package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single localised name in Trivore ID.
 * <p>
 * Used to specify the localised name of the {@link AuthorisationType} in
 * Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalisedName implements Serializable {

	private String locale;
	private String value;

	/**
	 * Construct a new localised name object.
	 */
	public LocalisedName() {
		// ...
	}

	/**
	 * Get locale.
	 *
	 * @return locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Set locale.
	 *
	 * @param locale locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * Get value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set value.
	 *
	 * @param value value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(locale, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof LocalisedName)) {
			return false;
		}
		LocalisedName o = (LocalisedName) obj;
		return Objects.equals(locale, o.locale) && Objects.equals(value, o.value);
	}

	@Override
	public String toString() {
		return "LocalisedName [locale=" + locale + ", value=" + value + "]";
	}

}
