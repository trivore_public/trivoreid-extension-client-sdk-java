package com.trivore.id.sdk.models.namespace;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a namespace's login allowed IP rule in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginAllowedIpRule implements Serializable {

	private String cidr;
	private boolean allowed;

	/**
	 * Get the CIDR.
	 *
	 * @return CIDR
	 */
	public String getCidr() {
		return cidr;
	}

	/**
	 * Set the CIDR.
	 *
	 * @param cidr CIDR
	 */
	public void setCidr(String cidr) {
		this.cidr = cidr;
	}

	/**
	 * Get if the rule is allowed.
	 *
	 * @return if the rule is allowed
	 */
	public boolean isAllowed() {
		return allowed;
	}

	/**
	 * Set if the rule is allowed.
	 *
	 * @param allowed if the rule is allowed
	 */
	public void setAllowed(boolean allowed) {
		this.allowed = allowed;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cidr, allowed);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof LoginAllowedIpRule)) {
			return false;
		}
		LoginAllowedIpRule o = (LoginAllowedIpRule) obj;
		return Objects.equals(cidr, o.cidr) && Objects.equals(allowed, o.allowed);
	}

	@Override
	public String toString() {
		return "LoginAllowedIpRule [cidr=" + cidr + ", allowed=" + allowed + "]";
	}

}
