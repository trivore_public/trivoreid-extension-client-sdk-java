package com.trivore.id.sdk.models.contract;

/**
 * Contact role.
 */
@SuppressWarnings("javadoc")
public enum ContactRole {
	CONTACT, BILLING, SUPPORT, ESCALATION, MANAGER, BUSINESS, CONTRACT, TERMINATOR
}
