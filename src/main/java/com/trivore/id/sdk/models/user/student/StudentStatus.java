package com.trivore.id.sdk.models.user.student;

/**
 * An object which represents a student status.
 */
@SuppressWarnings("javadoc")
public enum StudentStatus {

	FULL_TIME, PART_TIME, NOT_STUDENT, FORBIDDEN, UNKNOWN;

}
