package com.trivore.id.sdk.models.wallet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that represents a wallet.
 * <p>
 * Used to manage wallets in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Wallet implements Serializable {

	private String id;
	private String balance;
	private String currency;
	private List<WalletEvent> events;
	private String ownerId;
	private List<String> holderIds;
	private List<String> accessControlIds;
	private String name;
	private String identifiableNamespaceId;
	private String identifiableNamespace;
	private String identifiableName;

	/**
	 * @return the entity ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the entity ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the wallet balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the wallet balance
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * Wallet currency, ISO 4217 code.
	 * <p>
	 * NB! Unmodifiable after wallet creation.
	 * </p>
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Wallet currency, ISO 4217 code.
	 * <p>
	 * NB! Unmodifiable after wallet creation.
	 * </p>
	 *
	 * @param currency the currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the wallet event history
	 */
	public List<WalletEvent> getEvents() {
		if (events == null) {
			events = new ArrayList<>();
		}
		return events;
	}

	/**
	 * @param events the wallet event history
	 */
	public void setEvents(List<WalletEvent> events) {
		this.events = events;
	}

	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @param ownerId the ownerId
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @return the holderIds
	 */
	public List<String> getHolderIds() {
		if (holderIds == null) {
			holderIds = new ArrayList<>();
		}
		return holderIds;
	}

	/**
	 * @param holderIds the holderIds
	 */
	public void setHolderIds(List<String> holderIds) {
		this.holderIds = holderIds;
	}

	/**
	 * @return the accessControlIds
	 */
	public List<String> getAccessControlIds() {
		if (accessControlIds == null) {
			accessControlIds = new ArrayList<>();
		}
		return accessControlIds;
	}

	/**
	 * @param accessControlIds the accessControlIds
	 */
	public void setAccessControlIds(List<String> accessControlIds) {
		this.accessControlIds = accessControlIds;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the identifiableNamespaceId
	 */
	public String getIdentifiableNamespaceId() {
		return identifiableNamespaceId;
	}

	/**
	 * @param identifiableNamespaceId the identifiableNamespaceId
	 */
	public void setIdentifiableNamespaceId(String identifiableNamespaceId) {
		this.identifiableNamespaceId = identifiableNamespaceId;
	}

	/**
	 * @return the identifiableNamespace
	 */
	public String getIdentifiableNamespace() {
		return identifiableNamespace;
	}

	/**
	 * @param identifiableNamespace the identifiableNamespace
	 */
	public void setIdentifiableNamespace(String identifiableNamespace) {
		this.identifiableNamespace = identifiableNamespace;
	}

	/**
	 * @return the identifiableName
	 */
	public String getIdentifiableName() {
		return identifiableName;
	}

	/**
	 * @param identifiableName the identifiableName
	 */
	public void setIdentifiableName(String identifiableName) {
		this.identifiableName = identifiableName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, balance, currency, events, ownerId, holderIds, accessControlIds, name,
				identifiableNamespaceId, identifiableNamespace, identifiableName);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Wallet)) {
			return false;
		}
		Wallet o = (Wallet) obj;
		return Objects.equals(id, o.id) //
				&& Objects.equals(balance, o.balance) //
				&& Objects.equals(currency, o.currency) //
				&& Objects.equals(events, o.events) //
				&& Objects.equals(ownerId, o.ownerId) //
				&& Objects.equals(holderIds, o.holderIds) //
				&& Objects.equals(accessControlIds, o.accessControlIds) && Objects.equals(name, o.name) //
				&& Objects.equals(identifiableNamespaceId, o.identifiableNamespaceId) //
				&& Objects.equals(identifiableNamespace, o.identifiableNamespace) //
				&& Objects.equals(identifiableName, o.identifiableName);

	}

	@Override
	public String toString() {
		return "Wallet [id=" + id + ", balance=" + balance + ", currency=" + currency + ", events=" + events
				+ ", ownerId=" + ownerId + ", holderIds=" + holderIds + ", accessControlIds=" + accessControlIds
				+ ", name=" + name + ", identifiableNamespaceId=" + identifiableNamespaceId + ", identifiableNamespace="
				+ identifiableNamespace + ", identifiableName=" + identifiableName + "]";
	}

}
