package com.trivore.id.sdk.models.products;

/**
 * An enumeration of the days of week.
 * <p>
 * These values are used with {@link Validity} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum DaysOfWeek {
	MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
