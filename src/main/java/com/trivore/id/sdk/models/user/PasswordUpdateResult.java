package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class with password update result.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordUpdateResult implements Serializable {

	private boolean success;
	private List<ValidationError> validationErrors;

	/**
	 * @return if password was successfully updated
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success if password was successfully updated
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return validation errors
	 */
	public List<ValidationError> getValidationErrors() {
		if (validationErrors == null) {
			validationErrors = new ArrayList<>();
		}
		return validationErrors;
	}

	/**
	 * @param validationErrors validation errors
	 */
	public void setValidationErrors(List<ValidationError> validationErrors) {
		this.validationErrors = validationErrors;
	}

	@Override
	public int hashCode() {
		return Objects.hash(success, validationErrors);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PasswordUpdateResult)) {
			return false;
		}
		PasswordUpdateResult o = (PasswordUpdateResult) obj;
		return Objects.equals(success, o.success) && Objects.equals(validationErrors, o.validationErrors);
	}

	@Override
	public String toString() {
		return "PasswordUpdateResult [success=" + success + ", validationErrors=" + validationErrors + "]";
	}

}
