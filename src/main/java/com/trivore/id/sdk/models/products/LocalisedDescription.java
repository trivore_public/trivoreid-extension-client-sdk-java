package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single localosed description in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID product definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalisedDescription implements Serializable {

	private String locale;
	private String name;
	private String shortName;
	private String description;

	/**
	 * Locale. Value is Java-Locale.toString() compatible locale identifier (ex.
	 * 'fi' or 'fi_FI')
	 *
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Locale. Value is Java-Locale.toString() compatible locale identifier (ex.
	 * 'fi' or 'fi_FI')
	 *
	 * @param locale the locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		return Objects.hash(locale, name, shortName, description);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof LocalisedDescription)) {
			return false;
		}
		LocalisedDescription o = (LocalisedDescription) obj;
		return Objects.equals(locale, o.locale) //
				&& Objects.equals(name, o.name) //
				&& Objects.equals(shortName, o.shortName) //
				&& Objects.equals(description, o.description);
	}

	@Override
	public String toString() {
		return "LocalisedDescription [locale=" + locale + ", name=" + name + ", shortName=" + shortName
				+ ", description=" + description + "]";
	}

}
