package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents all catalog details in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID catalog definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllCatalogs implements Serializable {

	private List<CatalogDetails> catalogs;

	/**
	 * @return the catalog details
	 */
	public List<CatalogDetails> getCatalogs() {
		if (catalogs == null) {
			catalogs = new ArrayList<>();
		}
		return catalogs;
	}

	/**
	 * @param catalogs the catalog details
	 */
	public void setCatalogs(List<CatalogDetails> catalogs) {
		this.catalogs = catalogs;
	}

	@Override
	public int hashCode() {
		return Objects.hash(catalogs);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AllCatalogs)) {
			return false;
		}
		AllCatalogs o = (AllCatalogs) obj;
		return Objects.equals(catalogs, o.catalogs);
	}

	@Override
	public String toString() {
		return "AllCatalogs [catalogs=" + catalogs + "]";
	}

}
