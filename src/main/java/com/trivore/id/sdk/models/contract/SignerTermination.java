package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents signer termination in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignerTermination implements Serializable {

	private String timestamp;
	private String reason;

	/**
	 * Construct a new signer termination object.
	 */
	public SignerTermination() {
		// ...
	}

	/**
	 * Get timestamp.
	 *
	 * @return timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Set timestamp.
	 *
	 * @param timestamp timestamp
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Get reason.
	 *
	 * @return reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * Set reason.
	 *
	 * @param reason reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public int hashCode() {
		return Objects.hash(timestamp, reason);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof SignerTermination)) {
			return false;
		}
		SignerTermination o = (SignerTermination) obj;
		return Objects.equals(timestamp, o.timestamp) && Objects.equals(reason, o.reason);
	}

	@Override
	public String toString() {
		return "SignerTermination [timestamp=" + timestamp + ", reason=" + reason + "]";
	}

}
