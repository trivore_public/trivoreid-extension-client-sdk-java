package com.trivore.id.sdk.models.authorisation;

/**
 * An enumeration of the Authorisation object types.
 * <p>
 * These values are used with {@link AuthorisationSubject} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum AuthSubjectType {
	User, Group, String
}
