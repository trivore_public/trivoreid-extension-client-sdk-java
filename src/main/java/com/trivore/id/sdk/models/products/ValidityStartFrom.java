package com.trivore.id.sdk.models.products;

/**
 * An enumeration of the product validity start from option.
 * <p>
 * These values are used with {@link CatalogItem} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum ValidityStartFrom {
	PURCHASE, FIRST_USE
}
