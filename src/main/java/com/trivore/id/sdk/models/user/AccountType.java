package com.trivore.id.sdk.models.user;

/**
 * An enumeration of the user account types.
 * <p>
 * These values are used with {@link User} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum AccountType {
	PERSON, LEGAL_ENTITY
}
