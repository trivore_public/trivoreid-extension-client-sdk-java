package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single target in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID target definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Target implements Serializable {

	private String id;
	private String nsCode;
	private String name;
	private String locationSite;
	private String targetClass;
	private String contact;
	private String group;
	private String parent;
	private List<String> alertOnMaintenanceGroups;
	private List<String> alertOnIncidentGroups;
	private List<String> alertOnMajorIncidentGroups;
	private List<String> alertOnDangerNoticeGroups;
	private List<String> alertOnHaltNoticeGroups;
	private Meta meta;

	/**
	 * @return unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return Code of the namespace owning the target
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * @param nsCode Code of the namespace owning the target
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * @return A free form name for the target
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name A free form name for the target
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return The location of the target
	 */
	public String getLocationSite() {
		return locationSite;
	}

	/**
	 * @param locationSite The location of the target
	 */
	public void setLocationSite(String locationSite) {
		this.locationSite = locationSite;
	}

	/**
	 * @return Free form class of the target
	 */
	public String getTargetClass() {
		return targetClass;
	}

	/**
	 * @param targetClass Free form class of the target
	 */
	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	/**
	 * @return The contact responsible for the target
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact The contact responsible for the target
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return A group responsible for this target
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group A group responsible for this target
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return Parent target of this target in hierarchy
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * @param parent Parent target of this target in hierarchy
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * @return Send alert to pre-selected group(s) on maintenance of this target
	 */
	public List<String> getAlertOnMaintenanceGroups() {
		if (alertOnMaintenanceGroups == null) {
			alertOnMaintenanceGroups = new ArrayList<>();
		}
		return alertOnMaintenanceGroups;
	}

	/**
	 * @param alertOnMaintenanceGroups Send alert to pre-selected group(s) on
	 *                                 maintenance of this target
	 */
	public void setAlertOnMaintenanceGroups(List<String> alertOnMaintenanceGroups) {
		this.alertOnMaintenanceGroups = alertOnMaintenanceGroups;
	}

	/**
	 * @return Send alert to pre-selected group(s) on incident of this target
	 */
	public List<String> getAlertOnIncidentGroups() {
		if (alertOnIncidentGroups == null) {
			alertOnIncidentGroups = new ArrayList<>();
		}
		return alertOnIncidentGroups;
	}

	/**
	 * @param alertOnIncidentGroups Send alert to pre-selected group(s) on incident
	 *                              of this target
	 */
	public void setAlertOnIncidentGroups(List<String> alertOnIncidentGroups) {
		this.alertOnIncidentGroups = alertOnIncidentGroups;
	}

	/**
	 * @return Send alert to pre-selected group(s) on major incident of this target
	 */
	public List<String> getAlertOnMajorIncidentGroups() {
		if (alertOnMajorIncidentGroups == null) {
			alertOnMajorIncidentGroups = new ArrayList<>();
		}
		return alertOnMajorIncidentGroups;
	}

	/**
	 * @param alertOnMajorIncidentGroups Send alert to pre-selected group(s) on
	 *                                   major incident of this target
	 */
	public void setAlertOnMajorIncidentGroups(List<String> alertOnMajorIncidentGroups) {
		this.alertOnMajorIncidentGroups = alertOnMajorIncidentGroups;
	}

	/**
	 * @return Send alert to pre-selected group(s) on danger notice of this target
	 */
	public List<String> getAlertOnDangerNoticeGroups() {
		if (alertOnDangerNoticeGroups == null) {
			alertOnDangerNoticeGroups = new ArrayList<>();
		}
		return alertOnDangerNoticeGroups;
	}

	/**
	 * @param alertOnDangerNoticeGroups Send alert to pre-selected group(s) on
	 *                                  danger notice of this target
	 */
	public void setAlertOnDangerNoticeGroups(List<String> alertOnDangerNoticeGroups) {
		this.alertOnDangerNoticeGroups = alertOnDangerNoticeGroups;
	}

	/**
	 * @return Send alert automatically to pre-selected group(s) on halt notice of
	 *         this target
	 */
	public List<String> getAlertOnHaltNoticeGroups() {
		if (alertOnHaltNoticeGroups == null) {
			alertOnHaltNoticeGroups = new ArrayList<>();
		}
		return alertOnHaltNoticeGroups;
	}

	/**
	 * @param alertOnHaltNoticeGroups Send alert automatically to pre-selected
	 *                                group(s) on halt notice of this target
	 */
	public void setAlertOnHaltNoticeGroups(List<String> alertOnHaltNoticeGroups) {
		this.alertOnHaltNoticeGroups = alertOnHaltNoticeGroups;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof Target)) {
			return false;
		}
		Target o = (Target) other;
		if (o.getId() == null && getId() == null) {
			return super.equals(other);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "Target [id=" + id + ", nsCode=" + nsCode + ", name=" + name + ", locationSite=" + locationSite
				+ ", targetClass=" + targetClass + ", contact=" + contact + ", group=" + group + ", parent=" + parent
				+ ", alertOnMaintenanceGroups=" + alertOnMaintenanceGroups + ", alertOnIncidentGroups="
				+ alertOnIncidentGroups + ", alertOnMajorIncidentGroups=" + alertOnMajorIncidentGroups
				+ ", alertOnDangerNoticeGroups=" + alertOnDangerNoticeGroups + ", alertOnHaltNoticeGroups="
				+ alertOnHaltNoticeGroups + "]";
	}

}
