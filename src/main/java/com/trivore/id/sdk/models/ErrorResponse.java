package com.trivore.id.sdk.models;

/**
 * Error response from Trivore ID.
 */
public class ErrorResponse {

	private Integer statusCode;
	private String errorMessage;
	private String errorCode;

	/**
	 * Get status code.
	 *
	 * @return status code
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * Set status code.
	 *
	 * @param statusCode status code
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Get error message.
	 *
	 * @return error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Set error message.
	 *
	 * @param errorMessage error message
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Get error code.
	 *
	 * @return error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Set error code.
	 *
	 * @param errorCode error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return String.format("ErrorResponse [statusCode=%s, errorMessage=%s, errorCode=%s]", statusCode, errorMessage,
				errorCode);
	}

}
