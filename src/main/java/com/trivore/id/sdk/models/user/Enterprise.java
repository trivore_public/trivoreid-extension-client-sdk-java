package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.Meta;

/**
 * An object which presents a single enterprise user extension in Trivore ID.
 * <p>
 * Enterprises are stored as normal users in Trivore ID but they are extended
 * with some additional fields that are managed via separate REST end point.
 * This object is used to wrap and map that enterprise information extent.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Enterprise implements Serializable {

	private String businessId;
	private String tradeName;
	private String vatId;
	private String domicile;
	private String leiCode;
	private String duns;
	private List<String> parallelTradeNames;
	private List<String> auxiliaryTradeNames;
	private String tradeRegisterNumber;
	private String taxIdentificationNumber;
	private EnterpriseContactInfo general;
	private EnterpriseContactInfo sales;
	private EnterpriseContactInfo service;
	private EnterpriseContactInfo customerSupport;
	private Meta meta;

	/**
	 * Construct a new enterprise info.
	 */
	public Enterprise() {
		// ...
	}

	/**
	 * Get the business identifier of the enterprise.
	 *
	 * @return The enterprise's business identifier.
	 */
	public String getBusinessId() {
		return businessId;
	}

	/**
	 * Set the business identifier for the enterprise.
	 *
	 * @param businessId The business identifier for the enterprise.
	 */
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	/**
	 * Get the trade name of the enterprise.
	 *
	 * @return The trade name of the enterprise.
	 */
	public String getTradeName() {
		return tradeName;
	}

	/**
	 * Set the trade for the enterprise.
	 *
	 * @param tradeName The trade name for the enterprise.
	 */
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	/**
	 * Get VAT ID.
	 *
	 * @return VAT id
	 */
	public String getVatId() {
		return vatId;
	}

	/**
	 * Set VAT ID.
	 *
	 * @param vatId VAT id
	 */
	public void setVatId(String vatId) {
		this.vatId = vatId;
	}

	/**
	 * Get the domicicle.
	 *
	 * @return id
	 */
	public String getDomicile() {
		return domicile;
	}

	/**
	 * Set the domicicle.
	 *
	 * @param domicile the domicicle
	 */
	public void setDomicile(String domicile) {
		this.domicile = domicile;
	}

	/**
	 * Get LEI code (Global Legal Entity Identifier).
	 *
	 * @return LEI code (Global Legal Entity Identifier)
	 */
	public String getLeiCode() {
		return leiCode;
	}

	/**
	 * Set LEI code (Global Legal Entity Identifier).
	 *
	 * @param leiCode LEI code (Global Legal Entity Identifier)
	 */
	public void setLeiCode(String leiCode) {
		this.leiCode = leiCode;
	}

	/**
	 * Get Data Universal Numbering System.
	 *
	 * @return Data Universal Numbering System
	 */
	public String getDuns() {
		return duns;
	}

	/**
	 * Set Data Universal Numbering System.
	 *
	 * @param duns Data Universal Numbering System
	 */
	public void setDuns(String duns) {
		this.duns = duns;
	}

	/**
	 * Get parallel trade names.
	 *
	 * @return parallel trade names
	 */
	public List<String> getParallelTradeNames() {
		if (parallelTradeNames == null) {
			parallelTradeNames = new ArrayList<>();
		}
		return parallelTradeNames;
	}

	/**
	 * Set parallel trade names.
	 *
	 * @param parallelTradeNames parallel trade names
	 */
	public void setParallelTradeNames(List<String> parallelTradeNames) {
		this.parallelTradeNames = parallelTradeNames;
	}

	/**
	 * Get auxiliary trade names.
	 *
	 * @return auxiliary trade names
	 */
	public List<String> getAuxiliaryTradeNames() {
		if (auxiliaryTradeNames == null) {
			auxiliaryTradeNames = new ArrayList<>();
		}
		return auxiliaryTradeNames;
	}

	/**
	 * Set auxiliary trade names.
	 *
	 * @param auxiliaryTradeNames auxiliary trade names
	 */
	public void setAuxiliaryTradeNames(List<String> auxiliaryTradeNames) {
		this.auxiliaryTradeNames = auxiliaryTradeNames;
	}

	/**
	 * Get trade register number.
	 *
	 * @return trade register number
	 */
	public String getTradeRegisterNumber() {
		return tradeRegisterNumber;
	}

	/**
	 * Set trade register number.
	 *
	 * @param tradeRegisterNumber trade register number
	 */
	public void setTradeRegisterNumber(String tradeRegisterNumber) {
		this.tradeRegisterNumber = tradeRegisterNumber;
	}

	/**
	 * Get tax identification number.
	 *
	 * @return tax identification number
	 */
	public String getTaxIdentificationNumber() {
		return taxIdentificationNumber;
	}

	/**
	 * Set tax identification number.
	 *
	 * @param taxIdentificationNumber tax identification number
	 */
	public void setTaxIdentificationNumber(String taxIdentificationNumber) {
		this.taxIdentificationNumber = taxIdentificationNumber;
	}

	/**
	 * Get general customer support contact info.
	 *
	 * @return general customer support contact info
	 */
	public EnterpriseContactInfo getGeneral() {
		if (general == null) {
			general = new EnterpriseContactInfo();
		}
		return general;
	}

	/**
	 * Set general customer support contact info.
	 *
	 * @param general customer support contact info
	 */
	public void setGeneral(EnterpriseContactInfo general) {
		this.general = general;
	}

	/**
	 * Get sales customer support contact info.
	 *
	 * @return sales customer support contact info
	 */
	public EnterpriseContactInfo getSales() {
		if (sales == null) {
			sales = new EnterpriseContactInfo();
		}
		return sales;
	}

	/**
	 * Set sales customer support contact info.
	 *
	 * @param sales sales customer support contact info
	 */
	public void setSales(EnterpriseContactInfo sales) {
		this.sales = sales;
	}

	/**
	 * Get service customer support contact info.
	 *
	 * @return service customer support contact info
	 */
	public EnterpriseContactInfo getService() {
		if (service == null) {
			service = new EnterpriseContactInfo();
		}
		return service;
	}

	/**
	 * Set service customer support contact info.
	 *
	 * @param service service customer support contact info
	 */
	public void setService(EnterpriseContactInfo service) {
		this.service = service;
	}

	/**
	 * Get customer support contact info.
	 *
	 * @return customer support contact info
	 */
	public EnterpriseContactInfo getCustomerSupport() {
		if (customerSupport == null) {
			customerSupport = new EnterpriseContactInfo();
		}
		return customerSupport;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	/**
	 * Set customer support contact info.
	 *
	 * @param customerSupport customer support contact info
	 */
	public void setCustomerSupport(EnterpriseContactInfo customerSupport) {
		this.customerSupport = customerSupport;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof Enterprise)) {
			return false;
		}
		Enterprise o = (Enterprise) object;
		if (o.getBusinessId() == null && getBusinessId() == null) {
			return super.equals(object);
		}
		return Objects.equals(businessId, o.businessId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(businessId);
	}

	@Override
	public String toString() {
		return "Enterprise [businessId=" + businessId + ", tradeName=" + tradeName + ", vatId=" + vatId + ", domicile="
				+ domicile + ", leiCode=" + leiCode + ", duns=" + duns + ", parallelTradeNames=" + parallelTradeNames
				+ ", auxiliaryTradeNames=" + auxiliaryTradeNames + ", tradeRegisterNumber=" + tradeRegisterNumber
				+ ", taxIdentificationNumber=" + taxIdentificationNumber + ", general=" + general + ", sales=" + sales
				+ ", service=" + service + ", customerSupport=" + customerSupport + "]";
	}

}
