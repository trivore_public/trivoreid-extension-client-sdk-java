package com.trivore.id.sdk.models.namespace;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a namespace's reset password options in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResetPasswordOptions implements Serializable {

	private boolean namespaceInfoHidden;
	private boolean namespaceInfoHiddenEnabled;
	private boolean allFieldsEnabled;

	/**
	 * Get if the namespace info is hidden.
	 *
	 * @return if the namespace info is hidden
	 */
	public boolean isNamespaceInfoHidden() {
		return namespaceInfoHidden;
	}

	/**
	 * Set if the namespace info is hidden.
	 *
	 * @param namespaceInfoHidden if the namespace info is hidden
	 */
	public void setNamespaceInfoHidden(boolean namespaceInfoHidden) {
		this.namespaceInfoHidden = namespaceInfoHidden;
	}

	/**
	 * Get if the namespace info hidden is enabled.
	 *
	 * @return if the namespace info hidden is enabled
	 */
	public boolean isNamespaceInfoHiddenEnabled() {
		return namespaceInfoHiddenEnabled;
	}

	/**
	 * Set if the namespace info hidden is enabled.
	 *
	 * @param namespaceInfoHiddenEnabled if the namespace info hidden is enabled
	 */
	public void setNamespaceInfoHiddenEnabled(boolean namespaceInfoHiddenEnabled) {
		this.namespaceInfoHiddenEnabled = namespaceInfoHiddenEnabled;
	}

	/**
	 * Get if all namespace fields are enabled.
	 *
	 * @return if all namespace fields are enabled
	 */
	public boolean isAllFieldsEnabled() {
		return allFieldsEnabled;
	}

	/**
	 * Set if all namespace fields are enabled.
	 *
	 * @param allFieldsEnabled if all namespace fields are enabled
	 */
	public void setAllFieldsEnabled(boolean allFieldsEnabled) {
		this.allFieldsEnabled = allFieldsEnabled;
	}

	@Override
	public int hashCode() {
		return Objects.hash(namespaceInfoHidden, namespaceInfoHiddenEnabled, allFieldsEnabled);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ResetPasswordOptions)) {
			return false;
		}
		ResetPasswordOptions o = (ResetPasswordOptions) obj;
		return Objects.equals(namespaceInfoHidden, o.namespaceInfoHidden)
				&& Objects.equals(namespaceInfoHiddenEnabled, o.namespaceInfoHiddenEnabled)
				&& Objects.equals(allFieldsEnabled, o.allFieldsEnabled);
	}

	@Override
	public String toString() {
		return "ResetPasswordOptions [namespaceInfoHidden=" + namespaceInfoHidden + ", namespaceInfoHiddenEnabled="
				+ namespaceInfoHiddenEnabled + ", allFieldsEnabled=" + allFieldsEnabled + "]";
	}

}
