package com.trivore.id.sdk.models.contract;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a contract body in Trivore ID.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractContent implements Serializable {

	private String text;
	private String externalFileRef;
	private boolean hasInternalFile;
	private String internalFileType;

	/**
	 * Construct a new contract body object.
	 */
	public ContractContent() {
		// ...
	}

	/**
	 * Get text.
	 *
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set text.
	 *
	 * @param text text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Get external file reference.
	 * <p>
	 * Reference link to a file on an external service.
	 * </p>
	 *
	 * @return external file reference
	 */
	public String getExternalFileRef() {
		return externalFileRef;
	}

	/**
	 * Set external file reference.
	 * <p>
	 * Reference link to a file on an external service.
	 * </p>
	 *
	 * @param externalFileRef external file reference
	 */
	public void setExternalFileRef(String externalFileRef) {
		this.externalFileRef = externalFileRef;
	}

	/**
	 * Get hasInternalFile.
	 *
	 * @return hasInternalFile
	 */
	public boolean isHasInternalFile() {
		return hasInternalFile;
	}

	/**
	 * Set hasInternalFile.
	 *
	 * @param hasInternalFile hasInternalFile
	 */
	public void setHasInternalFile(boolean hasInternalFile) {
		this.hasInternalFile = hasInternalFile;
	}

	/**
	 * Get internalFileType.
	 *
	 * @return internalFileType
	 */
	public String getInternalFileType() {
		return internalFileType;
	}

	/**
	 * Set internalFileType.
	 *
	 * @param internalFileType internalFileType
	 */
	public void setInternalFileType(String internalFileType) {
		this.internalFileType = internalFileType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(text, externalFileRef, hasInternalFile, internalFileType);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ContractContent)) {
			return false;
		}
		ContractContent o = (ContractContent) obj;
		return Objects.equals(text, o.text) && Objects.equals(externalFileRef, o.externalFileRef)
				&& Objects.equals(hasInternalFile, o.hasInternalFile)
				&& Objects.equals(internalFileType, o.internalFileType);
	}

	@Override
	public String toString() {
		return "ContractContent [text=" + text + ", externalFileRef=" + externalFileRef + ", hasInternalFile="
				+ hasInternalFile + ", internalFileType=" + internalFileType + "]";
	}

}
