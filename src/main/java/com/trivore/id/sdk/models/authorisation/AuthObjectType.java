package com.trivore.id.sdk.models.authorisation;

/**
 * An enumeration of the Authorisation object types.
 * <p>
 * These values are used with {@link AuthorisationObject} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum AuthObjectType {
	User, Group, String
	
	// These types are not supported and the moment
	// Target, Contact
}
