package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single authorisation object in Trivore ID.
 * <p>
 * Used to specify the authorisation object Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorisationObject implements Serializable {

	private AuthObjectType type;
	private String value;

	/**
	 * Construct a new authorisation object.
	 */
	public AuthorisationObject() {
		// ...
	}

	/**
	 * Get the type of the authorisation object.
	 *
	 * @return the type of the authorisation object
	 */
	public AuthObjectType getType() {
		return type;
	}

	/**
	 * Set the type of the authorisation object.
	 *
	 * @param type the type of the authorisation object
	 */
	public void setType(AuthObjectType type) {
		this.type = type;
	}

	/**
	 * Get the object value.
	 * <p>
	 * Subject value. Must be either User or Group ID or if type is 'String', then
	 * may be any value. The entity, whose ID is specifed, must belong to the same
	 * namespace as the authorisation.
	 * </p>
	 *
	 * @return the object value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the object value.
	 * <p>
	 * Subject value. Must be either User or Group ID or if type is 'String', then
	 * may be any value. The entity, whose ID is specifed, must belong to the same
	 * namespace as the authorisation.
	 * </p>
	 *
	 * @param value the object value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthorisationObject)) {
			return false;
		}
		AuthorisationObject o = (AuthorisationObject) obj;
		return Objects.equals(type, o.type) && Objects.equals(value, o.value);
	}

	@Override
	public String toString() {
		return "AuthorisationObject [type=" + type + ", value=" + value + "]";
	}

}
