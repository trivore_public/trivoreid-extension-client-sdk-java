package com.trivore.id.sdk.models.authorisation;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single authorisation principal in Trivore ID.
 * <p>
 * Used to specify the principal of the authorisation in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthGrantRightPrincipal implements Serializable {

	private String type;
	private String value;

	/**
	 * Construct a new principal object.
	 */
	public AuthGrantRightPrincipal() {
		// ...
	}

	/**
	 * Get the type of the authorisation principal.
	 * <p>
	 * Type of the principal. Can be any of the valid authorisation principal types.
	 * </p>
	 *
	 * @return the type of the authorisation principal
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set the type of the authorisation principal.
	 * <p>
	 * Type of the principal. Can be any of the valid authorisation principal types.
	 * </p>
	 *
	 * @param type the type of the authorisation principal
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Get the principal value.
	 * <p>
	 * ID of the principal.
	 * </p>
	 *
	 * @return the principal value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set the principal value.
	 * <p>
	 * ID of the principal.
	 * </p>
	 *
	 * @param value the principal value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof AuthGrantRightPrincipal)) {
			return false;
		}
		AuthGrantRightPrincipal o = (AuthGrantRightPrincipal) obj;
		return Objects.equals(type, o.type) && Objects.equals(value, o.value);
	}

	@Override
	public String toString() {
		return "AuthGrantRightPrincipal [type=" + type + ", value=" + value + "]";
	}
}
