package com.trivore.id.sdk.models.products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single validity in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID validity definition.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Validity implements Serializable {

	private String title;
	private String validFrom;
	private String validUntil;
	private List<DaysOfWeek> daysOfWeek;
	private String timeOfDayFrom;
	private String timeOfDayUntil;
	private String zoneId;
	private List<Map<String, Object>> exceptions;
	private Boolean validNow;

	/**
	 * @return short title for this validity configuration.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title short title for this validity configuration.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return valid starting from this datetime
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom valid starting from this datetime
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return valid until this datetime
	 */
	public String getValidUntil() {
		return validUntil;
	}

	/**
	 * @param validUntil valid until this datetime
	 */
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}

	/**
	 * @return if set, is valid only on these days
	 */
	public List<DaysOfWeek> getDaysOfWeek() {
		if (daysOfWeek == null) {
			daysOfWeek = new ArrayList<>();
		}
		return daysOfWeek;
	}

	/**
	 * @param daysOfWeek if set, is valid only on these days
	 */
	public void setDaysOfWeek(List<DaysOfWeek> daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	/**
	 * If set, is valid only if time of day is at least this.
	 *
	 * @return the time of day from
	 */
	public String getTimeOfDayFrom() {
		return timeOfDayFrom;
	}

	/**
	 * If set, is valid only if time of day is at least this.
	 *
	 * @param timeOfDayFrom the time of day from
	 */
	public void setTimeOfDayFrom(String timeOfDayFrom) {
		this.timeOfDayFrom = timeOfDayFrom;
	}

	/**
	 * If set, is valid only if time of day is before this.
	 *
	 * @return the time of day until
	 */
	public String getTimeOfDayUntil() {
		return timeOfDayUntil;
	}

	/**
	 * If set, is valid only if time of day is before this.
	 *
	 * @param timeOfDayUntil the time of day until
	 */
	public void setTimeOfDayUntil(String timeOfDayUntil) {
		this.timeOfDayUntil = timeOfDayUntil;
	}

	/**
	 * @return the zoneId
	 */
	public String getZoneId() {
		return zoneId;
	}

	/**
	 * @param zoneId the zoneId to set
	 */
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	/**
	 * @return the exceptions
	 */
	public List<Map<String, Object>> getExceptions() {
		if (exceptions == null) {
			exceptions = new ArrayList<>();
		}
		return exceptions;
	}

	/**
	 * @param exceptions the exceptions to set
	 */
	public void setExceptions(List<Map<String, Object>> exceptions) {
		this.exceptions = exceptions;
	}

	/**
	 * @return is valid
	 */
	public Boolean getValidNow() {
		return validNow;
	}

	/**
	 * @param validNow is valid
	 */
	public void setValidNow(Boolean validNow) {
		this.validNow = validNow;
	}

	@Override
	public int hashCode() {
		return Objects.hash(validNow, exceptions, zoneId, daysOfWeek, validUntil, validFrom, title);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Validity)) {
			return false;
		}
		Validity o = (Validity) obj;
		return Objects.equals(title, o.title) //
				&& Objects.equals(validFrom, o.validFrom) //
				&& Objects.equals(validUntil, o.validUntil) //
				&& Objects.equals(daysOfWeek, o.daysOfWeek) //
				&& Objects.equals(zoneId, o.zoneId) //
				&& Objects.equals(exceptions, o.exceptions)//
				&& Objects.equals(validNow, o.validNow);
	}

	@Override
	public String toString() {
		return "Validity [title=" + title + ", validFrom=" + validFrom + ", validUntil=" + validUntil + ", daysOfWeek="
				+ daysOfWeek + ", zoneId=" + zoneId + ", exceptions=" + exceptions + ", validNow=" + validNow + "]";
	}

}
