package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a user's consents.
 * <p>
 * Used to specify user's consents in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Consents implements Serializable {

	private boolean marketingPost;
	private boolean marketingEmail;
	private boolean marketingPhone;
	private boolean marketingMobileMessage;
	private boolean marketingPushNotification;
	private boolean marketingOther;
	private boolean locationing;
	private boolean profiling;

	/**
	 * Get if there is consent for the marketing post.
	 *
	 * @return if there is consent for the marketing post.
	 */
	public boolean isMarketingPost() {
		return marketingPost;
	}

	/**
	 * Set if there is consent for the marketing post.
	 *
	 * @param marketingPost if there is consent for the marketing post.
	 */
	public void setMarketingPost(boolean marketingPost) {
		this.marketingPost = marketingPost;
	}

	/**
	 * Get if there is consent for the marketing email.
	 *
	 * @return if there is consent for the marketing email.
	 */
	public boolean isMarketingEmail() {
		return marketingEmail;
	}

	/**
	 * Set if there is consent for the marketing email.
	 *
	 * @param marketingEmail if there is consent for the marketing email.
	 */
	public void setMarketingEmail(boolean marketingEmail) {
		this.marketingEmail = marketingEmail;
	}

	/**
	 * Get if there is consent for the marketing phone.
	 *
	 * @return if there is consent for the marketing phone.
	 */
	public boolean isMarketingPhone() {
		return marketingPhone;
	}

	/**
	 * Set if there is consent for the marketing phone.
	 *
	 * @param marketingPhone if there is consent for the marketing phone.
	 */
	public void setMarketingPhone(boolean marketingPhone) {
		this.marketingPhone = marketingPhone;
	}

	/**
	 * Get if there is consent for the marketing mobile message.
	 *
	 * @return if there is consent for the marketing mobile message.
	 */
	public boolean isMarketingMobileMessage() {
		return marketingMobileMessage;
	}

	/**
	 * Set if there is consent for the marketing mobile message.
	 *
	 * @param marketingMobileMessage if there is consent for the marketing mobile
	 *                               message.
	 */
	public void setMarketingMobileMessage(boolean marketingMobileMessage) {
		this.marketingMobileMessage = marketingMobileMessage;
	}

	/**
	 * Get if there is consent for the marketing push notification.
	 *
	 * @return if there is consent for the marketing push notification.
	 */
	public boolean isMarketingPushNotification() {
		return marketingPushNotification;
	}

	/**
	 * Set if there is consent for the marketing push notification.
	 *
	 * @param marketingPushNotification if there is consent for the marketing push
	 *                                  notification.
	 */
	public void setMarketingPushNotification(boolean marketingPushNotification) {
		this.marketingPushNotification = marketingPushNotification;
	}

	/**
	 * Get if there is consent for the marketing other.
	 *
	 * @return if there is consent for the marketing other.
	 */
	public boolean isMarketingOther() {
		return marketingOther;
	}

	/**
	 * Set if there is consent for the marketing other.
	 *
	 * @param marketingOther if there is consent for the marketing other.
	 */
	public void setMarketingOther(boolean marketingOther) {
		this.marketingOther = marketingOther;
	}

	/**
	 * Get if there is consent for the locationing.
	 *
	 * @return if there is consent for the locationing.
	 */
	public boolean isLocationing() {
		return locationing;
	}

	/**
	 * Set if there is consent for the locationing.
	 *
	 * @param locationing if there is consent for the locationing.
	 */
	public void setLocationing(boolean locationing) {
		this.locationing = locationing;
	}

	/**
	 * Get if there is consent for the profiling.
	 *
	 * @return if there is consent for the profiling.
	 */
	public boolean isProfiling() {
		return profiling;
	}

	/**
	 * Set if there is consent for the profiling.
	 *
	 * @param profiling if there is consent for the profiling.
	 */
	public void setProfiling(boolean profiling) {
		this.profiling = profiling;
	}

	@Override
	public int hashCode() {
		return Objects.hash(marketingPost, marketingEmail, marketingPhone, marketingMobileMessage, marketingOther,
				locationing, profiling, marketingPushNotification);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Consents)) {
			return false;
		}
		Consents o = (Consents) obj;
		return Objects.equals(marketingPost, o.marketingPost) 
				&& Objects.equals(marketingEmail, o.marketingEmail)
				&& Objects.equals(marketingPhone, o.marketingPhone)
				&& Objects.equals(marketingMobileMessage, o.marketingMobileMessage)
				&& Objects.equals(marketingOther, o.marketingOther) 
				&& Objects.equals(locationing, o.locationing)
				&& Objects.equals(profiling, o.profiling)
				&& Objects.equals(marketingPushNotification, o.marketingPushNotification);
	}

	@Override
	public String toString() {
		return "Consents [marketingPost=" + marketingPost + ", marketingEmail=" + marketingEmail + ", marketingPhone="
				+ marketingPhone + ", marketingMobileMessage=" + marketingMobileMessage + ", marketingPushNotification="
				+ marketingPushNotification + ", marketingOther=" + marketingOther + ", locationing=" + locationing
				+ ", profiling=" + profiling + "]";
	}

}
