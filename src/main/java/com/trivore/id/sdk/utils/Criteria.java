package com.trivore.id.sdk.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Criteria is used to limit and filter the results from the data source.
 * <p>
 * This criteria implementation follows the principles of the Trivore ID service.
 * </p>
 * <p>
 * An example how to use the criteria to filter results.
 * </p>
 *
 * <pre>
 * Criteria criteria = new Criteria();
 * criteria.setStartIndex(2);
 * criteria.setCount(10);
 * criteria.setFilter(Filter.and(
 *   Filter.equal("foo", "bar"),
 *   Filter.present("foobar")
 * ));
 * </pre>
 */
public class Criteria {

	/** The URI attribute used to specify the page index for the pagination. */
	public static final String ATTR_START_INDEX = "startIndex";
	/** The URI attribute used to specify the amount of items within a page. */
	public static final String ATTR_COUNT = "count";
	/** The URI attribute used to specify the filter for the query. */
	public static final String ATTR_FILTER = "filter";

	private Integer startIndex;
	private Integer count;
	private Filter filter;

	/**
	 * Construct a new blank Trivore ID specific query criteria.
	 */
	public Criteria() {
		// ...
	}

	/**
	 * Construct a new blank Trivore ID specific query criteria.
	 *
	 * @param filter The filter to be assigned for the criteria.
	 */
	public Criteria(Filter filter) {
		this.filter = filter;
	}

	/**
	 * Construct a new blank Trivore ID specific query criteria.
	 *
	 * @param filter     The filter to be assigned for the criteria.
	 * @param startIndex The pagination start index.
	 * @param count      The maximum amount of items in the result set.
	 */
	public Criteria(Filter filter, Integer startIndex, Integer count) {
		this.filter = filter;
		this.startIndex = startIndex;
		this.count = count;
	}

	/**
	 * Get the pagination start index.
	 * <p>
	 * Note that this index is zero-based.
	 * </p>
	 * <p>
	 * Defaults to null (i.e. zero).
	 * <p>
	 *
	 * @return The pagination start index.
	 */
	public Integer getStartIndex() {
		return startIndex;
	}

	/**
	 * Set the pagination start index.
	 * <p>
	 * Note that this index is zero-based.
	 * </p>
	 *
	 * @param startIndex The pagination start index.
	 */
	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	/**
	 * Get the size of a single result page.
	 * <p>
	 * Note that different end points specify different maximums for this value.
	 * </p>
	 *
	 * @return The maximum amount of items in the result set.
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * Set the size of a single result page.
	 * <p>
	 * Note that different end points specify different maximums for this value.
	 * </p>
	 *
	 * @param count The maximum amount of items in the result set.
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * Get the {@link Filter} for the query.
	 * <p>
	 * Filters are used to reduce the amount of results gathered from the service.
	 * </p>
	 *
	 * @return The filter assigned for the criteria.
	 */
	public Filter getFilter() {
		return filter;
	}

	/**
	 * Set the {@link Filter} for the query.
	 * <p>
	 * Filters are used to reduce the amount of results gathered from the service.
	 * </p>
	 *
	 * @param filter The filter to be assigned for the criteria.
	 */
	public void setFilter(Filter filter) {
		this.filter = filter;
	}

	/**
	 * Get the criteria in a query parameter map format.
	 * <p>
	 * This map can be directly given to WebClient to add request parameters.
	 * </p>
	 *
	 * @return The criteria in a query parameters map format.
	 */
	public Map<String, String> toQueryParams() {
		Map<String, String> map = new HashMap<>();
		if (startIndex != null) {
			map.put(ATTR_START_INDEX, startIndex.toString());
		}
		if (count != null) {
			map.put(ATTR_COUNT, count.toString());
		}
		if (filter != null) {
			map.put(ATTR_FILTER, filter.toFilterString());
		}
		return map;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof Criteria)) {
			return false;
		}
		Criteria o = (Criteria) other;
		return Objects.equals(startIndex, o.startIndex) && Objects.equals(count, o.count)
				&& Objects.equals(filter, o.filter);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startIndex, count, filter);
	}

	@Override
	public String toString() {
		return "Criteria [startIndex=" + startIndex + ", count=" + count + ", filter=" + filter + "]";
	}

}
