package com.trivore.id.sdk.utils;

import java.util.Collection;

/**
 * Helper to check if the assertion is wrong.
 */
public class Assert {

	/**
	 * Only static methods.
	 */
	private Assert() {
		// ...
	}

	/**
	 * Assert that the object is not Null.
	 *
	 * @param obj     target object
	 * @param message exception message
	 * @throws IllegalArgumentException if assertion is wrong
	 */
	public static void notNull(Object obj, String message) {
		if (obj == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that the text is not null or empty.
	 *
	 * @param obj     target object
	 * @param message exception message
	 * @throws IllegalArgumentException if assertion is wrong
	 */
	public static void hasText(String obj, String message) {
		if (obj == null || obj.isEmpty()) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that the statement is true.
	 *
	 * @param statement target statement
	 * @param message   exception message
	 * @throws IllegalArgumentException if assertion is wrong
	 */
	public static void isTrue(boolean statement, String message) {
		if (!statement) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that the collection is not null or empty.
	 *
	 * @param collection collection to check
	 * @param message    exception message
	 * @throws IllegalArgumentException if assertion is wrong
	 */
	public static void notEmpty(Collection<?> collection, String message) {
		if (collection == null || collection.isEmpty()) {
			throw new IllegalArgumentException(message);
		}
	}

}
