package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.paycard.PanToken;
import com.trivore.id.sdk.models.paycard.Paycard;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A paycard service for all kinds of paycard management operations.
 * <p>
 * Handles all kinds of operations with Trivore ID paycard objects.
 * </p>
 */
public interface PaycardService {

	/**
	 * Get all paycards from accessible namespaces.
	 *
	 * @param userId user unique ID
	 * @return page with paycard objects
	 */
	@GET("user/{userId}/paycard")
	Call<Page<Paycard>> getAll(@Path("userId") String userId);

	/**
	 * Get all paycards satisfying the provided filter.
	 *
	 * @param userId     user unique ID
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query paycards.
	 * @return page with paycard objects
	 */
	@GET("user/{userId}/paycard")
	Call<Page<Paycard>> getAll(
			@Path("userId") String userId,
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Add a new paycard into the service.
	 *
	 * @param userId  user unique ID
	 * @param paycard The paycard to be added.
	 * @return A paycard saved in Trivore ID.
	 */
	@POST("user/{userId}/paycard")
	Call<Paycard> create(@Path("userId") String userId, @Body Paycard paycard);

	/**
	 * Get the paycard with the given identifier.
	 *
	 * @param userId    user unique ID
	 * @param paycardId The id of the target paycard.
	 * @return The paycard with the given identifier.
	 */
	@GET("user/{userId}/paycard/{paycardId}")
	Call<Paycard> get(@Path("userId") String userId, @Path("paycardId") String paycardId);

	/**
	 * Save the changes of the provided paycard.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided paycard.
	 * </p>
	 *
	 * @param userId    user unique ID
	 * @param paycardId the ID of the target paycard
	 * @param paycard   The target paycard.
	 * @return A paycard saved in Trivore ID.
	 */
	@PUT("user/{userId}/paycard/{paycardId}")
	Call<Paycard> update(
			@Path("userId") String userId,
			@Path("paycardId") String paycardId,
			@Body Paycard paycard);

	/**
	 * Delete a paycard from Trivore ID.
	 *
	 * @param userId    user unique ID
	 * @param paycardId The id of the target paycard.
	 * @return void
	 */
	@DELETE("user/{userId}/paycard/{paycardId}")
	Call<Void> delete(@Path("userId") String userId, @Path("paycardId") String paycardId);

	/**
	 * Get all paycard's pantokens.
	 *
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @return A set of paycard items from the Trivore ID.
	 */
	@GET("user/{userId}/paycard/{paycardId}/pantoken")
	Call<Page<PanToken>> getAllPantokens(@Path("userId") String userId, @Path("paycardId") String paycardId);

	/**
	 * Get pantokens by criteria.
	 *
	 * @param userId     user unique ID
	 * @param paycardId  paycard unique ID
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query paycards.
	 * @return A set of paycard items from the Trivore ID.
	 */
	@GET("user/{userId}/paycard/{paycardId}/pantoken")
	Call<Page<PanToken>> getAllPantokens(
			@Path("userId") String userId,
			@Path("paycardId") String paycardId,
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Create a new pantoken in the Trivore ID service.
	 *
	 * @param pantoken  The pantoken to be added into Trivore ID.
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @return The paycard that was added (id is auto-assigned by Trivore ID).
	 */
	@POST("user/{userId}/paycard/{paycardId}/pantoken")
	Call<PanToken> createPantoken(
			@Path("userId") String userId,
			@Path("paycardId") String paycardId,
			@Body PanToken pantoken);

	/**
	 * Get a single paycard's pantoken by its ID.
	 *
	 * @param userId    user unique ID
	 * @param paycardId paycard unique identifier.
	 * @param tokenId   pantoken unique identifier.
	 * @return PanToken object
	 */
	@GET("user/{userId}/paycard/{paycardId}/pantoken/{tokenId}")
	Call<PanToken> getPantoken(
			@Path("userId") String userId,
			@Path("paycardId") String paycardId,
			@Path("tokenId") String tokenId);

	/**
	 * Update an existing pantoken in Trivore ID service.
	 * <p>
	 * The provided pantoken must contain id.
	 * </p>
	 *
	 * @param pantoken  The pantoken to be upodated in Trivore ID.
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @param tokenId   pantoken unique identifier.
	 * @return The same paycard returned from the Trivore ID.
	 */
	@PUT("user/{userId}/paycard/{paycardId}/pantoken/{tokenId}")
	Call<PanToken> updatePantoken(
			@Path("userId") String userId,
			@Path("paycardId") String paycardId,
			@Path("tokenId") String tokenId,
			@Body PanToken pantoken);

	/**
	 * Delete a pantoken from Trivore ID.
	 *
	 * @param userId    user unique ID
	 * @param paycardId paycard unique identifier.
	 * @param tokenId   pantoken unique identifier.
	 * @return void
	 */
	@DELETE("user/{userId}/paycard/{paycardId}/pantoken/{tokenId}")
	Call<Void> deletePantoken(
			@Path("userId") String userId,
			@Path("paycardId") String paycardId,
			@Path("tokenId") String tokenId);
}
