package com.trivore.id.sdk.service;

import java.util.List;

import com.trivore.id.sdk.models.BuiltInRole;
import com.trivore.id.sdk.models.CustomRole;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.Permission;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * An access right service to process different kinds of access right specific
 * operations.
 * <p>
 * Handles access right specific tasks and communicates with the Trivore ID
 * service.
 * </p>
 */
public interface AccessRightService {

	/**
	 * Get all builtin permissions.
	 *
	 * @return list with the builtin permissions
	 */
	@GET("permission")
	Call<List<Permission>> getAllPermissions();

	/**
	 * Get a single builtin permission.
	 *
	 * @param permissionId permission ID
	 * @return builtin permission
	 */
	@GET("permission/{permissionId}")
	Call<Permission> getPermission(@Path("permissionId") String permissionId);

	/**
	 * Get all builtin role objects.
	 *
	 * @return list with the builtin roles
	 */
	@GET("role/builtin")
	Call<List<BuiltInRole>> getAllBuiltinRoles();

	/**
	 * Query custom role objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query groups.
	 * @return page with custom role objects
	 */
	@GET("role/custom")
	Call<Page<CustomRole>> getAllCustomRoles(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Create new custom role object.
	 *
	 * @param customRole custom role object to create
	 * @return created custom role object
	 */
	@POST("role/custom")
	Call<CustomRole> createCustomRole(@Body CustomRole customRole);

	/**
	 * Get custom role object.
	 *
	 * @param roleId target custom role unique identifier
	 * @return custom role object
	 */
	@GET("role/custom/{roleId}")
	Call<CustomRole> getCustomRole(@Path("roleId") String roleId);

	/**
	 * Update custom role object.
	 *
	 * @param roleId     custom role unique identifier
	 * @param customRole target custom role object
	 * @return updated custom role object
	 */
	@PUT("role/custom/{roleId}")
	Call<CustomRole> updateCustomRole(@Path("roleId") String roleId, @Body CustomRole customRole);

	/**
	 * Delete custom role.
	 *
	 * @param roleId role id to delete
	 * @return void
	 */
	@Headers("Accept: application/json")
	@DELETE("role/custom/{roleId}")
	Call<Void> deleteCustomRole(@Path("roleId") String roleId);

}
