package com.trivore.id.sdk.service;

import java.util.Map;

import com.trivore.id.sdk.models.user.mydata.MyDataPackage;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * A MyData service for all kinds of MyData operations.
 * <p>
 * Handles all kinds of operations with Trivore ID MyData objects.
 * </p>
 */
public interface MyDataService {

	/**
	 * Request a creation of a MyData package for the user with target id.
	 * <p>
	 * Function will send a MyData package creation request to Trivore ID.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return A package containing all MyData entries for the user.
	 */
	@POST("user/{userId}/mydata")
	Call<MyDataPackage> request(@Path("userId") String userId);

	/**
	 * Get all MyData entries for the target user.
	 * <p>
	 * Function queries all MyData entries for the user from the Trivore ID.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return A package containing all MyData entries for the user.
	 */
	@GET("user/{userId}/mydata")
	Call<MyDataPackage> getAll(@Path("userId") String userId);

	/**
	 * Get a single MyData file for the target user.
	 * <p>
	 * Returns a single file from the MyData package. Requires access to user's
	 * Namespace.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @param dataId The id of the target MyData entry.
	 * @return A byte array containing the target MyData entry.
	 */
	@GET("user/{userId}/mydata/{dataId}")
	Call<Map<String, Object>> getMyData(@Path("userId") String userId, @Path("dataId") String dataId);

	/**
	 * Get a single MyData entry for the target user.
	 * <p>
	 * Returns user information in unstable JSON format. Format may change. Contents
	 * may be censored. If specific user information is required, other APIs should
	 * be used.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return A byte array containing the target MyData entry.
	 */
	@GET("user/{userId}/mydata/user")
	Call<Map<String, Object>> getUserInfo(@Path("userId") String userId);

}
