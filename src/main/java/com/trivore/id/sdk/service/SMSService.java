package com.trivore.id.sdk.service;

import java.util.List;

import com.trivore.id.sdk.models.Region;
import com.trivore.id.sdk.models.SMSMessage;
import com.trivore.id.sdk.models.SMSResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * A group service for all kinds of sms messaging operations.
 * <p>
 * Handles all kinds of operations with Trivore ID sms messages.
 * </p>
 */
public interface SMSService {

	/**
	 * Send a custom SMS message.
	 *
	 * @param message SMS message
	 * @return void
	 */
	@POST("sms/send")
	Call<SMSResponse> send(@Body SMSMessage message);

	/**
	 * Send a custom SMS message to a user.
	 *
	 * @param message SMS message
	 * @param userId  target user id
	 * @return void
	 */
	@POST("user/{userId}/sms/send")
	Call<SMSResponse> sendToUser(@Body SMSMessage message, @Path("userId") String userId);

	/**
	 * Get the list of all supported regions.
	 *
	 * @return list of all supported regions
	 */
	@GET("sms/regions")
	Call<List<Region>> getSupportedRegions();

}
