package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.LocationSite;
import com.trivore.id.sdk.models.Page;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * An location/site service to process different kinds of location/site specific
 * operations.
 * <p>
 * Handles location/site specific tasks and communicates with the Trivore ID
 * service.
 * </p>
 */
public interface LocationSiteService {

	/**
	 * Query location/site objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query groups.
	 * @param sortBy     Sort by attribute name
	 * @param sortOrder  Sort direction ('ascending' or 'descending')
	 * @param flat       Set to true to fetch a flattened hierarchy of the
	 *                   location/sites.
	 * @return page with location/site objects
	 */
	@GET("locationsite")
	Call<Page<LocationSite>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter,
			@Query("sortBy") String sortBy,
			@Query("sortOrder") String sortOrder,
			@Query("flat") Boolean flat);

	/**
	 * Create new location/site object.
	 *
	 * @param locationsite location/site object to create
	 * @return created location/site object
	 */
	@POST("locationsite")
	Call<LocationSite> create(@Body LocationSite locationsite);

	/**
	 * Get location/site object.
	 *
	 * @param locationsiteId target location/site unique identifier
	 * @return location/site object
	 */
	@GET("locationsite/{locationsiteId}")
	Call<LocationSite> get(@Path("locationsiteId") String locationsiteId);

	/**
	 * Update location/site object.
	 *
	 * @param locationsiteId location/site unique identifier
	 * @param locationsite   target location/site object
	 * @return updated location/site object
	 */
	@PUT("locationsite/{locationsiteId}")
	Call<LocationSite> update(@Path("locationsiteId") String locationsiteId, @Body LocationSite locationsite);

	/**
	 * Delete update location/site object.
	 *
	 * @param locationsiteId location/site unique identifier
	 * @return void
	 */
	@DELETE("locationsite/{locationsiteId}")
	Call<Void> delete(@Path("locationsiteId") String locationsiteId);

}
