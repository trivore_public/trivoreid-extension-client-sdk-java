package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.products.AllCatalogs;
import com.trivore.id.sdk.models.products.Catalog;
import com.trivore.id.sdk.models.products.CatalogDetails;
import com.trivore.id.sdk.models.products.PricingPlan;
import com.trivore.id.sdk.models.products.Product;
import com.trivore.id.sdk.models.products.ProductDetails;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A product service for all kinds of products and sales management operations.
 * <p>
 * Handles all kinds of operations with Trivore ID products and sales objects.
 * </p>
 */
public interface ProductService {

	/**
	 * Get all catalogs satisfying the provided criteria.
	 *
	 * @param startIndex          pagination start index
	 * @param count               pagination count
	 * @param filter              The filter query catalogs.
	 * @param sortBy              sort by attribute name
	 * @param sortOrder           sort direction ('ascending' or 'descending')
	 * @param mergeProductDetails copy missing values from original products to
	 *                            catalog items
	 * @return page with catalog objects
	 */
	@GET("products/catalog")
	Call<Page<Catalog>> getAllCatalogs( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter, //
			@Query("sortBy") String sortBy, //
			@Query("sortOrder") String sortOrder, //
			@Query("mergeProductDetails") boolean mergeProductDetails);

	/**
	 * Add a new catalog into the service.
	 *
	 * @param catalog The catalog to be added.
	 * @return A catalog saved in Trivore ID.
	 */
	@POST("products/catalog")
	Call<Catalog> createCatalog(@Body Catalog catalog);

	/**
	 * Get the catalog with the given identifier.
	 *
	 * @param catalogId The id of the target catalog.
	 * @return The catalog with the given identifier.
	 */
	@GET("products/catalog/{catalogId}")
	Call<Catalog> getCatalog(@Path("catalogId") String catalogId);

	/**
	 * Save the changes of the provided catalog.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided catalog.
	 * </p>
	 *
	 * @param catalogId the ID of the target catalog
	 * @param catalog   The target catalog.
	 * @return A catalog saved in Trivore ID.
	 */
	@PUT("products/catalog/{catalogId}")
	Call<Catalog> updateCatalog(@Path("catalogId") String catalogId, @Body Catalog catalog);

	/**
	 * Delete a catalog from Trivore ID.
	 *
	 * @param catalogId The id of the target catalog.
	 * @return void
	 */
	@DELETE("products/catalog/{catalogId}")
	Call<Void> deleteCatalog(@Path("catalogId") String catalogId);

	/**
	 * Get all pricing plans satisfying the provided criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query pricing plans.
	 * @param sortBy     sort by attribute name
	 * @param sortOrder  sort direction ('ascending' or 'descending')
	 * @return page with pricing plan objects
	 */
	@GET("products/pricingplan")
	Call<Page<PricingPlan>> getAllPricingPlans( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter, //
			@Query("sortBy") String sortBy, //
			@Query("sortOrder") String sortOrder);

	/**
	 * Add a new pricing plan into the service.
	 *
	 * @param pricingPlan The pricing plan to be added.
	 * @return A pricing plan saved in Trivore ID.
	 */
	@POST("products/pricingplan")
	Call<PricingPlan> createPricingPlan(@Body PricingPlan pricingPlan);

	/**
	 * Get the pricing plan with the given identifier.
	 *
	 * @param pricingPlanId The id of the target pricing plan.
	 * @return The pricing plan with the given identifier.
	 */
	@GET("products/pricingplan/{pricingPlanId}")
	Call<PricingPlan> getPricingPlan(@Path("pricingPlanId") String pricingPlanId);

	/**
	 * Save the changes of the provided pricing plan.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided pricing plan.
	 * </p>
	 *
	 * @param pricingPlanId the ID of the target pricing plan
	 * @param pricingPlan   The target pricing plan.
	 * @return A pricing plan saved in Trivore ID.
	 */
	@PUT("products/pricingplan/{pricingPlanId}")
	Call<PricingPlan> updatePricingPlan( //
			@Path("pricingPlanId") String pricingPlanId, //
			@Body PricingPlan pricingPlan);

	/**
	 * Delete a pricing plan from Trivore ID.
	 *
	 * @param pricingPlanId The id of the target pricing plan.
	 * @return void
	 */
	@DELETE("products/pricingplan/{pricingPlanId}")
	Call<Void> deletePricingPlan(@Path("pricingPlanId") String pricingPlanId);

	/**
	 * Get all products satisfying the provided criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query products.
	 * @param sortBy     sort by attribute name
	 * @param sortOrder  sort direction ('ascending' or 'descending')
	 * @return page with product objects
	 */
	@GET("products/product")
	Call<Page<Product>> getAllProducts( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter, //
			@Query("sortBy") String sortBy, //
			@Query("sortOrder") String sortOrder);

	/**
	 * Add a new product into the service.
	 *
	 * @param product The product to be added.
	 * @return A product saved in Trivore ID.
	 */
	@POST("products/product")
	Call<Product> createProduct(@Body Product product);

	/**
	 * Get the product with the given identifier.
	 *
	 * @param productId The id of the target product.
	 * @return The product with the given identifier.
	 */
	@GET("products/product/{productId}")
	Call<Product> getProduct(@Path("productId") String productId);

	/**
	 * Save the changes of the provided product.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided product.
	 * </p>
	 *
	 * @param productId the ID of the target product
	 * @param product   The target product.
	 * @return A product saved in Trivore ID.
	 */
	@PUT("products/product/{productId}")
	Call<Product> updateProduct(@Path("productId") String productId, @Body Product product);

	/**
	 * Delete a product from Trivore ID.
	 *
	 * @param productId The id of the target product.
	 * @return void
	 */
	@DELETE("products/product/{productId}")
	Call<Void> deleteProduct(@Path("productId") String productId);

	/**
	 * Get all accessible catalogs and their product item details.
	 *
	 * @param locale          locale code for names. Accept-Language header can also
	 *                        be used.
	 * @param currency        currency code. Only prices in this currency are used.
	 *                        If not specified, default or configured currency is
	 *                        used.
	 * @param paymentMethod   payment method for possible discount effect.
	 * @param customerSegment customer segment IDs used in price discounts
	 * @param atTime          dateTime for which prices are provided. Leave empty to
	 *                        use current time. Example: '2007-12-03T10:15:30.00Z'
	 * @return all accessible catalogs and their product item details.
	 */
	@GET("sales/products")
	Call<AllCatalogs> getAllCatalogsAndItems( //
			@Query("locale") String locale, //
			@Query("currency") String currency, //
			@Query("paymentMethod") String paymentMethod, //
			@Query("customerSegment") String[] customerSegment, //
			@Query("atTime") String atTime);

	/**
	 * Get catalog and its product item details.
	 *
	 * @param catalogId       target catalog unique identifier
	 * @param locale          locale code for names. Accept-Language header can also
	 *                        be used.
	 * @param currency        currency code. Only prices in this currency are used.
	 *                        If not specified, default or configured currency is
	 *                        used.
	 * @param paymentMethod   payment method for possible discount effect.
	 * @param customerSegment customer segment IDs used in price discounts
	 * @param code            Price discount codes applied.
	 * @param volume          Purchase volume (how many items), for volume discount.
	 * @param atTime          dateTime for which prices are provided. Leave empty to
	 *                        use current time. Example: '2007-12-03T10:15:30.00Z'
	 * @return catalog details.
	 */
	@GET("sales/products/catalog/{catalogId}")
	Call<CatalogDetails> getCatalogDetails( //
			@Path("catalogId") String catalogId, //
			@Query("locale") String locale, //
			@Query("currency") String currency, //
			@Query("customerSegment") String[] customerSegment, //
			@Query("atTime") String[] code, //
			@Query("atTime") String paymentMethod, //
			@Query("atTime") Integer volume, //
			@Query("atTime") String atTime);

	/**
	 * Get product item details.
	 *
	 * @param catalogId       target catalog unique identifier
	 * @param itemId          Item's product ID, SKU, or own ID
	 * @param locale          locale code for names. Accept-Language header can also
	 *                        be used.
	 * @param currency        currency code. Only prices in this currency are used.
	 *                        If not specified, default or configured currency is
	 *                        used.
	 * @param paymentMethod   payment method for possible discount effect.
	 * @param customerSegment customer segment IDs used in price discounts
	 * @param code            Price discount codes applied.
	 * @param volume          Purchase volume (how many items), for volume discount.
	 * @param atTime          dateTime for which prices are provided. Leave empty to
	 *                        use current time. Example: '2007-12-03T10:15:30.00Z'
	 * @return catalog's product item details.
	 */
	@GET("sales/products/catalog/{catalogId}/item/{itemId}")
	Call<ProductDetails> getProductDetails( //
			@Path("catalogId") String catalogId, //
			@Path("itemId") String itemId, //
			@Query("locale") String locale, //
			@Query("currency") String currency, //
			@Query("customerSegment") String[] customerSegment, //
			@Query("code") String[] code, //
			@Query("paymentMethod") String paymentMethod, //
			@Query("volume") Integer volume, //
			@Query("atTime") String atTime);

}
