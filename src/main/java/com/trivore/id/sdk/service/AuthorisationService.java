package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.authorisation.Authorisation;
import com.trivore.id.sdk.models.authorisation.AuthorisationGrantRight;
import com.trivore.id.sdk.models.authorisation.AuthorisationType;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * An authorisation service to process different kinds of authorisation specific
 * operations.
 * <p>
 * Handles authorisation specific tasks and communicates with the Trivore ID
 * service.
 * </p>
 */
public interface AuthorisationService {

	/**
	 * Get all authorisations from accessible namespaces.
	 *
	 * @return page with authorisation objects
	 */
	@GET("authorisation")
	Call<Page<Authorisation>> getAll();

	/**
	 * Get all authorisations satisfying the provided filter.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query authorisations.
	 * @return page with authorisation objects
	 */
	@GET("authorisation")
	Call<Page<Authorisation>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Add a new authorisation into the service.
	 * <p>
	 * If the namespace code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param authorisation The authorisation to be added.
	 * @return A authorisation saved in Trivore ID.
	 */
	@POST("authorisation")
	Call<Authorisation> create(@Body Authorisation authorisation);

	/**
	 * Get the authorisation with the given identifier.
	 *
	 * @param authorisationId The id of the target authorisation.
	 * @return The authorisation with the given identifier.
	 */
	@GET("authorisation/{authorisationId}")
	Call<Authorisation> get(@Path("authorisationId") String authorisationId);

	/**
	 * Save the changes of the provided authorisation.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided authorisation.
	 * </p>
	 *
	 * 
	 * @param authorisationId the ID of the target authorisation
	 * @param authorisation   The target authorisation.
	 * @return A authorisation saved in Trivore ID.
	 */
	@PUT("authorisation/{authorisationId}")
	Call<Authorisation> update(
			@Path("authorisationId") String authorisationId,
			@Body Authorisation authorisation);

	/**
	 * Revoke a new authorisation into the service.
	 *
	 * @param authorisationId The id of the target authorisation.
	 * @return A authorisation saved in Trivore ID.
	 */
	@POST("authorisation/{authorisationId}/revoke")
	Call<Void> revoke(@Path("authorisationId") String authorisationId);

	/**
	 * Delete a authorisation from Trivore ID.
	 *
	 * @param authorisationId The id of the target authorisation.
	 * @return void
	 */
	@DELETE("authorisation/{authorisationId}")
	Call<Void> delete(@Path("authorisationId") String authorisationId);

	/**
	 * Get authorisation types.
	 *
	 * @return A set of authorisation type items from the Trivore ID.
	 */
	@GET("authorisation_type")
	Call<Page<AuthorisationType>> getAllAuthorisationTypes();

	/**
	 * Get types by criteria.
	 *
	 * @param startIndex      pagination start index
	 * @param count           pagination count
	 * @param filter          The filter query authorisations.
	 * @return A set of authorisation type items from the Trivore ID.
	 */
	@GET("authorisation_type")
	Call<Page<AuthorisationType>> getAllAuthorisationTypes(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Create a new type in the Trivore ID service.
	 *
	 * @param type            The type to be added into Trivore ID.
	 * @return The authorisation type that was added (id is auto-assigned by
	 *         Trivore ID).
	 */
	@POST("authorisation_type")
	Call<AuthorisationType> createAuthorisationType(@Body AuthorisationType type);

	/**
	 * Get a single authorisation's type by its ID.
	 *
	 * @param typeId          type unique identifier.
	 * @return AuthorisationType object
	 */
	@GET("authorisation_type/{typeId}")
	Call<AuthorisationType> getAuthorisationType(@Path("typeId") String typeId);

	/**
	 * Update an existing type in Trivore ID service.
	 * <p>
	 * The provided type must contain id.
	 * </p>
	 *
	 * @param type            The type to be upodated in Trivore ID.
	 * @param typeId          type unique identifier.
	 * @return The same authorisation type returned from the Trivore ID.
	 */
	@PUT("authorisation_type/{typeId}")
	Call<AuthorisationType> updateAuthorisationType(
			@Path("typeId") String typeId,
			@Body AuthorisationType type);

	/**
	 * Delete a type from Trivore ID.
	 *
	 * @param typeId          type unique identifier.
	 * @return void
	 */
	@DELETE("authorisation_type/{typeId}")
	Call<Void> deleteAuthorisationType(@Path("typeId") String typeId);

	/**
	 * Get all authorisation's sources.
	 *
	 * @return A set of authorisation source items from the Trivore ID.
	 */
	@GET("authorisation_source")
	Call<Page<AuthorisationType>> getAllAuthorisationSources();

	/**
	 * Get sources by criteria.
	 *
	 * @param startIndex      pagination start index
	 * @param count           pagination count
	 * @param filter          The filter query authorisations.
	 * @return A set of authorisation source items from the Trivore ID.
	 */
	@GET("authorisation_source")
	Call<Page<AuthorisationType>> getAllAuthorisationSources(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Create a new source in the Trivore ID service.
	 *
	 * @param source          The source to be added into Trivore ID.
	 * @return The authorisation source that was added (id is auto-assigned by
	 *         Trivore ID).
	 */
	@POST("authorisation_source")
	Call<AuthorisationType> createAuthorisationSource(@Body AuthorisationType source);

	/**
	 * Get a single authorisation's source by its ID.
	 *
	 * @param sourceId        source unique identifier.
	 * @return AuthorisationType object
	 */
	@GET("authorisation_source/{sourceId}")
	Call<AuthorisationType> getAuthorisationSource(@Path("sourceId") String sourceId);

	/**
	 * Update an existing source in Trivore ID service.
	 * <p>
	 * The provided source must contain id.
	 * </p>
	 *
	 * @param source          The source to be upodated in Trivore ID.
	 * @param sourceId        source unique identifier.
	 * @return The same authorisation source returned from the Trivore ID.
	 */
	@PUT("authorisation_source/{sourceId}")
	Call<AuthorisationType> updateAuthorisationSource(
			@Path("sourceId") String sourceId,
			@Body AuthorisationType source);

	/**
	 * Delete a source from Trivore ID.
	 *
	 * @param sourceId        source unique identifier.
	 * @return void
	 */
	@DELETE("authorisation_source/{sourceId}")
	Call<Void> deleteAuthorisationSource(@Path("sourceId") String sourceId);

	/**
	 * Get all grant rights.
	 *
	 * @return A set of grant right items from the Trivore ID.
	 */
	@GET("authorisation_grant_right")
	Call<Page<AuthorisationGrantRight>> getAllAuthorisationGrantRights();

	/**
	 * Get grant rights by criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query.
	 * @return A set of grant right items from the Trivore ID.
	 */
	@GET("authorisation_grant_right")
	Call<Page<AuthorisationGrantRight>> getAllAuthorisationGrantRights(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Create a new grant right in the Trivore ID service.
	 *
	 * @param right The right to be added into Trivore ID.
	 * @return The grant right that was added (id is auto-assigned by Trivore ID).
	 */
	@POST("authorisation_grant_right")
	Call<AuthorisationGrantRight> createAuthorisationGrantRight(@Body AuthorisationGrantRight right);

	/**
	 * Get a single grant right by its ID.
	 *
	 * @param rightId grant right unique identifier.
	 * @return AuthorisationGrantRight object
	 */
	@GET("authorisation_grant_right/{rightId}")
	Call<AuthorisationGrantRight> getAuthorisationGrantRight(@Path("rightId") String rightId);

	/**
	 * Update an existing grant right in Trivore ID service.
	 * <p>
	 * The provided right must contain id.
	 * </p>
	 *
	 * @param right   The right to be upodated in Trivore ID.
	 * @param rightId grant right unique ID
	 * @return The same grant right returned from the Trivore ID.
	 */
	@PUT("authorisation_grant_right/{rightId}")
	Call<AuthorisationGrantRight> updateAuthorisationGrantRight(
			@Path("rightId") String rightId,
			@Body AuthorisationGrantRight right);

	/**
	 * Revoke a new authorisation grant right in the service.
	 *
	 * @param rightId The id of the target authorisation.
	 * @return void
	 */
	@POST("authorisation_grant_right/{rightId}/revoke")
	Call<Void> revokeGrantRight(@Path("rightId") String rightId);

	/**
	 * Delete a grant right from Trivore ID.
	 *
	 * @param rightId grant right unique identifier.
	 * @return void
	 */
	@DELETE("authorisation_grant_right/{rightId}")
	Call<Void> deleteAuthorisationGrantRight(@Path("rightId") String rightId);
}
