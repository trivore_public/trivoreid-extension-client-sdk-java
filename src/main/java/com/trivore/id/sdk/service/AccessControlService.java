package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.AccessControl;
import com.trivore.id.sdk.models.Page;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * An access control service to process different kinds of access control
 * specific operations.
 * <p>
 * Handles access control specific tasks and communicates with the Trivore ID
 * service.
 * </p>
 */
public interface AccessControlService {

	/**
	 * Query access control objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query groups.
	 * @param sortBy     Sort by attribute name
	 * @param sortOrder  Sort direction ('ascending' or 'descending')
	 * @return page with access control objects
	 */
	@GET("accesscontrol")
	Call<Page<AccessControl>> getAll( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter, //
			@Query("sortBy") String sortBy, //
			@Query("sortOrder") String sortOrder);

	/**
	 * Create new access control object.
	 *
	 * @param accessControl access control object to create
	 * @return created access control object
	 */
	@POST("accesscontrol")
	Call<AccessControl> create(@Body AccessControl accessControl);

	/**
	 * Get access control object.
	 *
	 * @param accessControlId target access control unique identifier
	 * @return access control object
	 */
	@GET("accesscontrol/{accessControlId}")
	Call<AccessControl> get(@Path("accessControlId") String accessControlId);

	/**
	 * Update access control object.
	 *
	 * @param accessControlId access control unique identifier
	 * @param accessControl   target access control object
	 * @return updated access control object
	 */
	@PUT("accesscontrol/{accessControlId}")
	Call<AccessControl> update( //
			@Path("accessControlId") String accessControlId, //
			@Body AccessControl accessControl);

	/**
	 * 
	 * @param accessControlId access control unique identifier
	 * @return void
	 */
	@DELETE("accesscontrol/{accessControlId}")
	Call<Void> delete(@Path("accessControlId") String accessControlId);

}
