package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.wallet.Transaction;
import com.trivore.id.sdk.models.wallet.Wallet;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A wallet service for all kinds of wallet management operations.
 * <p>
 * Handles all kinds of operations with Trivore ID wallet objects.
 * </p>
 */
public interface WalletService {

	/**
	 * Get all wallets satisfying the provided criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query catalogs.
	 * @param sortBy     sort by attribute name
	 * @param sortOrder  sort direction ('ascending' or 'descending')
	 * @return page with wallet objects
	 */
	@GET("wallet")
	Call<Page<Wallet>> getAll( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter, //
			@Query("sortBy") String sortBy, //
			@Query("sortOrder") String sortOrder);

	/**
	 * Create new wallet.
	 *
	 * @param wallet wallet to create
	 * @return new wallet object
	 */
	@POST("wallet")
	Call<Wallet> create(@Body Wallet wallet);

	/**
	 * Get a wallet with the given identifier.
	 *
	 * @param walletId target wallet ID
	 * @return new target wallet object
	 */
	@GET("wallet/{walletId}")
	Call<Wallet> get(@Path("walletId") String walletId);

	/**
	 * Update a wallet.
	 *
	 * @param walletId target wallet ID
	 * @param wallet   wallet to update
	 * @return new wallet object
	 */
	@PUT("wallet/{walletId}")
	Call<Wallet> update(@Path("walletId") String walletId, @Body Wallet wallet);

	/**
	 * Delete a wallet.
	 *
	 * @param walletId target wallet ID
	 * @return void
	 */
	@DELETE("wallet/{walletId}")
	Call<Void> delete(@Path("walletId") String walletId);

	/**
	 * Deposit funds to an account.
	 * <p>
	 * Funds are not transferred from another wallet, they are simply added to the
	 * wallet.
	 * </p>
	 *
	 * @param walletId    target wallet ID
	 * @param transaction transaction info
	 * @return new wallet object
	 */
	@POST("wallet/{walletId}/deposit")
	Call<Void> deposit(@Path("walletId") String walletId, @Body Transaction transaction);

	/**
	 * Transfer funds to another wallet
	 * <p>
	 * Only wallet owner, wallet holders, or those with read access to wallet and
	 * DEPOSIT and WITHDRAW permissions can transfer funds between wallets.
	 * </p>
	 *
	 * @param walletId    target wallet ID
	 * @param transaction transaction info
	 * @return new wallet object
	 */
	@POST("wallet/{walletId}/transfer")
	Call<Void> transfer(@Path("walletId") String walletId, @Body Transaction transaction);

	/**
	 * Withdraw funds from a wallet.
	 * <p>
	 * Funds are not transferred to another wallet, they are simply removed from
	 * source wallet.
	 * </p>
	 *
	 * @param walletId    target wallet ID
	 * @param transaction transaction info
	 * @return new wallet object
	 */
	@POST("wallet/{walletId}/withdraw")
	Call<Void> withdraw(@Path("walletId") String walletId, @Body Transaction transaction);

}
