package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.EmailMessage;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * A group service for all kinds of email messaging operations.
 * <p>
 * Handles all kinds of operations with Trivore ID email messages.
 * </p>
 */
public interface EmailService {

	/**
	 * Send a custom email message.
	 *
	 * @param message email message
	 * @return void
	 */
	@POST("email/send")
	public Call<Void> send(@Body EmailMessage message);

	/**
	 * Send a custom email message to a user.
	 *
	 * @param message email message
	 * @param userId  target user unique identifier
	 * @return void
	 */
	@POST("user/{userId}/email/send")
	public Call<Void> sendToUser(@Path("userId") String userId, @Body EmailMessage message);

	/**
	 * Send email message to all group members.
	 *
	 * @param message email message to send
	 * @param groupId target group unique identifier
	 * @return void
	 */
	@POST("group/{groupId}/email/send")
	public Call<Void> sendToGroupMembers(@Path("groupId") String groupId, @Body EmailMessage message);

}
