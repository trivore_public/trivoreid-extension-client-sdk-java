package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.Period;
import com.trivore.id.sdk.models.Subscription;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A subscription service to process different kinds of subscription specific
 * operations.
 * <p>
 * Handles user specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface SubscriptionService {

	/**
	 * Get all subscriptions from accessible periods.
	 *
	 * @return A set of subscriptions from the Trivore ID.
	 */
	@GET("subscription")
	Call<Page<Subscription>> getAll();

	/**
	 * Get all subscriptions from accessible periods.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query subscriptions.
	 * @return A set of subscriptions from the Trivore ID.
	 */
	@GET("subscription")
	Call<Page<Subscription>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Create the provided subscription into Trivore ID service.
	 *
	 * @param subscription The subscription to add into Trivore ID.
	 * @return The subscription that was added (id is auto-assigned by Trivore ID).
	 */
	@POST("subscription")
	Call<Subscription> create(@Body Subscription subscription);

	/**
	 * Get the subscription with the given identifier.
	 *
	 * @param subscriptionId The target identifier.
	 * @return The subscription with the given identifier.
	 */
	@GET("subscription/{subscriptionId}")
	Call<Subscription> get(@Path("subscriptionId") String subscriptionId);

	/**
	 * Update the provided subscription in Trivore ID service.
	 * <p>
	 * The subscription's id should be present in the subscription object.
	 * </p>
	 *
	 * @param subscriptionId The target identifier.
	 * @param subscription   The subscription to add into Trivore ID.
	 * @return The subscription that was added (id is auto-assigned by Trivore ID).
	 */
	@PUT("subscription/{subscriptionId}")
	Call<Subscription> update(@Path("subscriptionId") String subscriptionId, @Body Subscription subscription);

	/**
	 * Terminate the subsctiption.
	 *
	 * @param subscriptionId The target identifier.
	 * @return terminated Subscription object
	 */
	@POST("subscription/{subscriptionId}/terminate")
	Call<Subscription> terminate(@Path("subscriptionId") String subscriptionId);

	/**
	 * Add period to the subscription in Trivore ID service.
	 *
	 * @param subscriptionId The target identifier.
	 * @param period         The period to add into Trivore ID.
	 * @return The period that was added (id is auto-assigned by Trivore ID).
	 */
	@POST("subscription/{subscriptionId}/period")
	Call<Period> createPeriod(@Path("subscriptionId") String subscriptionId, @Body Period period);

	/**
	 * Get the subscription's period with the given identifier.
	 *
	 * @param subscriptionId The target subscription identifier.
	 * @param periodId       The target period identifier.
	 * @return The period with the given identifier.
	 */
	@GET("subscription/{subscriptionId}/period/{periodId}")
	Call<Period> getPeriod(@Path("subscriptionId") String subscriptionId, @Path("periodId") String periodId);

	/**
	 * Update the provided period in Trivore ID service.
	 * <p>
	 * The period id should be present in the subscription object.
	 * </p>
	 *
	 * @param subscriptionId The target subscription identifier.
	 * @param periodId       The target period identifier.
	 * @param period         The period to modify.
	 * @return The period that was updated.
	 */
	@PUT("subscription/{subscriptionId}/period/{periodId}")
	Call<Period> updatePeriod(
			@Path("subscriptionId") String subscriptionId,
			@Path("periodId") String periodId,
			@Body Period period);

}
