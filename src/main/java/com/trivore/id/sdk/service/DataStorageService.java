package com.trivore.id.sdk.service;

import java.util.Map;

import com.trivore.id.sdk.models.DataStorage;
import com.trivore.id.sdk.models.Page;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A storage service for all kinds of data management operations.
 * <p>
 * Handles data CRUD operations with generic data operations with the Trivore ID.
 * </p>
 */
public interface DataStorageService {

	/**
	 * Get a set of all accessible data storages within the Trivore ID.
	 * <p>
	 * This function will fetch all accessible data storages for the current user
	 * from the Trivore ID management REST API.
	 * </p>
	 *
	 * @return A set containing all accessible data storages.
	 */
	@GET("datastorage")
	Call<Page<DataStorage>> getAll();

	/**
	 * Get a set of all accessible data storages within the Trivore ID.
	 * <p>
	 * This function will fetch all accessible data storages for the current user
	 * from the Trivore ID management REST API.
	 * </p>
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query storages.
	 * @param dataFilter filter for quering data.
	 * @return A set containing all accessible data storages.
	 */
	@GET("datastorage")
	Call<Page<DataStorage>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("dsfilter") String filter,
			@Query("datafilter") String dataFilter);

	/**
	 * Add a new storage into the service.
	 * <p>
	 * If the storage code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param storage The storage to be added.
	 * @return A storage saved in Trivore ID.
	 */
	@POST("datastorage")
	Call<DataStorage> create(@Body DataStorage storage);

	/**
	 * Get the storage with the given identifier.
	 *
	 * @param dsId The id of the target storage.
	 * @return The storage with the given identifier.
	 */
	@GET("datastorage/{dsId}")
	Call<DataStorage> get(@Path("dsId") String dsId);

	/**
	 * Save the changes of the provided storage.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided storage.
	 * </p>
	 *
	 * @param dsId    the ID of the target storage
	 * @param storage The target storage.
	 * @return A storage saved in Trivore ID.
	 */
	@PUT("datastorage/{dsId}")
	Call<DataStorage> update(@Path("dsId") String dsId, @Body DataStorage storage);

	/**
	 * Delete a storage from Trivore ID.
	 *
	 * @param dsId The id of the target storage.
	 * @return void
	 */
	@DELETE("datastorage/{dsId}")
	Call<Void> delete(@Path("dsId") String dsId);

	/**
	 * Get data from the datastorage.
	 *
	 * @param dsId The id of the target storage.
	 * @return data from the datastorage
	 */
	@GET("datastorage/{dsId}/data")
	Call<Map<String, Object>> getData(@Path("dsId") String dsId);

	/**
	 * Replace all contained data in the datastorage.
	 *
	 * @param data data to update
	 * @param dsId The id of the target storage.
	 * @return void
	 */
	@PUT("datastorage/{dsId}/data")
	Call<Void> updateData(@Path("dsId") String dsId, @Body Map<String, Object> data);

	/**
	 * Get value from the datastorage.
	 * <p>
	 * Returns Map opject with one value entry.
	 * </p>
	 *
	 * @param key  key to get the value.
	 * @param dsId The id of the target storage.
	 * @return value from the datastorage
	 */
	@GET("datastorage/{dsId}/data/{dataKey}")
	Call<Map<String, Object>> getValue(
			@Path("dsId") String dsId,
			@Path("dataKey") String key);

	/**
	 * Replace value in the datastorage by its key.
	 *
	 * @param key   key to get the value.
	 * @param dsId  The id of the target storage.
	 * @param value map with value entry.
	 * @return void
	 */
	@PUT("datastorage/{dsId}/data/{dataKey}")
	Call<Void> updateValue(
			@Path("dsId") String dsId,
			@Path("dataKey") String key,
			@Body Map<String, Object> value);

	/**
	 * Delete value in the datastorage by its key.
	 *
	 * @param key  key to get the value.
	 * @param dsId The id of the target storage.
	 * @return void
	 */
	@DELETE("datastorage/{dsId}/data/{dataKey}")
	Call<Void> deleteValue(@Path("dsId") String dsId, @Path("dataKey") String key);

}
