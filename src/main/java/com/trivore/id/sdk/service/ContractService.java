package com.trivore.id.sdk.service;

import java.util.List;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.contract.AllowedActions;
import com.trivore.id.sdk.models.contract.Appendix;
import com.trivore.id.sdk.models.contract.Contract;
import com.trivore.id.sdk.models.contract.ContractContent;
import com.trivore.id.sdk.models.contract.Party;
import com.trivore.id.sdk.models.contract.PartyContact;
import com.trivore.id.sdk.models.contract.Signer;
import com.trivore.id.sdk.utils.Criteria;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A contract service to process different kinds of contract specific
 * operations.
 * <p>
 * Handles contract specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface ContractService {

	/**
	 * Get all contracts from accessible namespaces.
	 *
	 * @return page with contract objects
	 */
	@GET("contract")
	Call<Page<Contract>> getAll();

	/**
	 * Get all contracts satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query namespaces.
	 * @return page with contract objects
	 */
	@GET("contract")
	Call<Page<Contract>> getAll( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter);

	/**
	 * Add a new contract into the service.
	 *
	 * @param contract The contract to be added.
	 * @return A contract saved in Trivore ID.
	 */
	@POST("contract")
	Call<Contract> create(@Body Contract contract);

	/**
	 * Get the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @return The contract with the given identifier.
	 */
	@GET("contract/{contractId}")
	Call<Contract> get(@Path("contractId") String contractId);

	/**
	 * Save the changes of the provided contract.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided contract.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param contract   The target contract.
	 * @return A contract saved in Trivore ID.
	 */
	@PUT("contract/{contractId}")
	Call<Contract> update(@Path("contractId") String contractId, @Body Contract contract);

	/**
	 * Delete a contract from Trivore ID.
	 *
	 * @param contractId The id of the target contract.
	 * @return void
	 */
	@DELETE("contract/{contractId}")
	Call<Void> delete(@Path("contractId") String contractId);

	/**
	 * Get the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @return The contract with the given identifier.
	 */
	@GET("contract/{contractId}/actions")
	Call<AllowedActions> getAllowedActions(@Path("contractId") String contractId);

	/**
	 * Get the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @return The contract with the given identifier.
	 */
	@GET("contract/{contractId}/body")
	Call<ContractContent> getContractContent(@Path("contractId") String contractId);

	/**
	 * Save the changes of the provided contract content.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided contract content.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param content    The content of the target contract.
	 * @return A contract saved in Trivore ID.
	 */
	@PUT("contract/{contractId}/body")
	Call<ContractContent> updateContractContent(@Path("contractId") String contractId, @Body ContractContent content);

	/**
	 * Terminate the contract.
	 *
	 * @param contractId target contract ID
	 * @param reason     reason for termination
	 * @return the target contract object
	 */
	@POST("contract/{contractId}/actions/terminate")
	Call<Contract> terminate(@Path("contractId") String contractId, @Query("reason") String reason);

	/**
	 * Finalise the contract.
	 *
	 * @param contractId target contract ID
	 * @return the target contract object
	 */
	@POST("contract/{contractId}/actions/finalise")
	Call<Contract> finalise(@Path("contractId") String contractId);

	/**
	 * Sign the contract.
	 *
	 * @param contractId target contract ID
	 * @return the target contract object
	 */
	@POST("contract/{contractId}/actions/sign")
	Call<Contract> sign(@Path("contractId") String contractId);

	/**
	 * Get all parties from the target contract.
	 *
	 * @param contractId target contract ID
	 * @return list with contract's parties
	 */
	@GET("contract/{contractId}/party")
	Call<List<Party>> getAllParties(@Path("contractId") String contractId);

	/**
	 * Add a new party to a contract.
	 *
	 * @param contractId target contract ID
	 * @param party      The party to be created.
	 * @return A party saved in Trivore ID.
	 */
	@POST("contract/{contractId}/party")
	Call<Party> createParty(@Path("contractId") String contractId, @Body Party party);

	/**
	 * Get the party from the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @return The party with the given identifier.
	 */
	@GET("contract/{contractId}/party/{partyId}")
	Call<Party> getParty(@Path("contractId") String contractId, @Path("partyId") String partyId);

	/**
	 * Save the changes of the provided party.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided party.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param party      The party to update.
	 * @return A party saved in Trivore ID.
	 */
	@PUT("contract/{contractId}/party/{partyId}")
	Call<Party> updateParty( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Body Party party);

	/**
	 * Delete a party from a contract in Trivore ID.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the party to delete.
	 * @return void
	 */
	@DELETE("contract/{contractId}/party/{partyId}")
	Call<Void> deleteParty( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId);

	/**
	 * Get all party signers from the target contract.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @return list with contract's party signers
	 */
	@GET("contract/{contractId}/party/{partyId}/signer")
	Call<List<Signer>> getAllPartySigners( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId);

	/**
	 * Add a new signer to a party.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signer     The signer to be created.
	 * @return A signer saved in Trivore ID.
	 */
	@POST("contract/{contractId}/party/{partyId}/signer")
	Call<Signer> createPartySigner( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Body Signer signer);

	/**
	 * Get the party signer with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signerId   The id of the target signer.
	 * @return The party signer with the given identifier.
	 */
	@GET("contract/{contractId}/party/{partyId}/signer/{signerId}")
	Call<Signer> getPartySigner( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Path("signerId") String signerId);

	/**
	 * Save the changes of the provided signer.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided signer.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signerId   The id of the target signer.
	 * @param signer     The signer to be updated.
	 * @return A signer saved in Trivore ID.
	 */
	@PUT("contract/{contractId}/party/{partyId}/signer/{signerId}")
	Call<Signer> updatePartySigner(@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Path("signerId") String signerId, //
			@Body Signer signer);

	/**
	 * Delete a party signer from a contract in Trivore ID.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signerId   The id of the target signer.
	 * @return void
	 */
	@DELETE("contract/{contractId}/party/{partyId}/signer/{signerId}")
	Call<Void> deletePartySigner( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Path("signerId") String signerId);

	/**
	 * Get all parties from the target contract.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @return list with party contacts
	 */
	@GET("contract/{contractId}/party/{partyId}/contact")
	Call<List<PartyContact>> getAllPartyContacts( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId);

	/**
	 * Add a new contact to a party.
	 *
	 * @param contractId   The id of the target contract.
	 * @param partyId      The id of the target party.
	 * @param partyContact The contact to be created.
	 * @return A contact saved in Trivore ID.
	 */
	@POST("contract/{contractId}/party/{partyId}/contact")
	Call<PartyContact> createPartyContact( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Body PartyContact partyContact);

	/**
	 * Get the party contact with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param contactId  The id of the target signer.
	 * @return The party contact with the given identifier.
	 */
	@GET("contract/{contractId}/party/{partyId}/contact/{contactId}")
	Call<PartyContact> getPartyContact( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Path("contactId") String contactId);

	/**
	 * Save the changes of the provided contact.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided contact.
	 * </p>
	 *
	 * @param contractId   The id of the target contract.
	 * @param partyId      The id of the target party.
	 * @param contactId    The id of the target signer.
	 * @param partyContact The contact to be updated.
	 * @return A contact saved in Trivore ID.
	 */
	@PUT("contract/{contractId}/party/{partyId}/contact/{contactId}")
	Call<PartyContact> updatePartyContact( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Path("contactId") String contactId, //
			@Body PartyContact partyContact);

	/**
	 * Delete a contact from a party in Trivore ID.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param contactId  The id of the target contact.
	 * @return void
	 */
	@DELETE("contract/{contractId}/party/{partyId}/contact/{contactId}")
	Call<Void> deletePartyContact( //
			@Path("contractId") String contractId, //
			@Path("partyId") String partyId, //
			@Path("contactId") String contactId);

	/**
	 * Get all appendices from the target contract.
	 *
	 * @param contractId target contract ID
	 * @return list with contract's parties
	 */
	@GET("contract/{contractId}/appendix")
	Call<List<Appendix>> getAllAppendices(@Path("contractId") String contractId);

	/**
	 * Add a new appendix to a contract.
	 *
	 * @param contractId target contract ID
	 * @param appendix   appendix to be created.
	 * @return An appendix saved in Trivore ID.
	 */
	@POST("contract/{contractId}/appendix")
	Call<Appendix> createAppendix(@Path("contractId") String contractId, @Body Appendix appendix);

	/**
	 * Get the appendix from the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @return The appendix with the given identifier.
	 */
	@GET("contract/{contractId}/appendix/{appendixId}")
	Call<Appendix> getAppendix(@Path("contractId") String contractId, @Path("appendixId") String appendixId);

	/**
	 * Save the changes of the provided appendix.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided appendix.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @param appendix   The appendix to update.
	 * @return An appendix saved in Trivore ID.
	 */
	@PUT("contract/{contractId}/appendix/{appendixId}")
	Call<Appendix> updateAppendix( //
			@Path("contractId") String contractId, //
			@Path("appendixId") String appendixId, //
			@Body Appendix appendix);

	/**
	 * Delete appendix from a contract in Trivore ID.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the appendix to delete.
	 * @return void
	 */
	@DELETE("contract/{contractId}/appendix/{appendixId}")
	Call<Void> deleteAppendix(@Path("contractId") String contractId, @Path("appendixId") String appendixId);

	/**
	 * Change the order of the contract appendices.
	 *
	 * @param contractId The id of the target contract.
	 * @param order      the list with ordered appendix identifiers.
	 * @return List of ordered appendices
	 */
	@POST("contract/{contractId}/appendix/order")
	Call<List<Appendix>> changeAppendixOrder(@Path("contractId") String contractId, @Body List<String> order);

	/**
	 * Get the file from the contract's body.
	 *
	 * @param contractId The id of the target contract.
	 * @return a contract's body file
	 */
	@GET("contract/{contractId}/body/file")
	Call<byte[]> getContractBodyFile(@Path("contractId") String contractId);

	/**
	 * Upload a file to the contract body.
	 *
	 * @param contractId The id of the target contract.
	 * @param file       file to upload
	 * @return void
	 */
	@PUT("contract/{contractId}/body/file")
	Call<Void> uploadContractBodyFile(@Path("contractId") String contractId, @Body byte[] file);

	/**
	 * Upload a file to the contract body.
	 *
	 * @param contractId The id of the target contract.
	 * @return void
	 */
	@DELETE("contract/{contractId}/body/file")
	Call<Void> deleteContractBodyFile(@Path("contractId") String contractId);

	/**
	 * Get the file from the contract's body.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @return an appendix file
	 */
	@GET("contract/{contractId}/appendix/{appendixId}/file")
	Call<byte[]> getAppendixFile(@Path("contractId") String contractId, @Path("appendixId") String appendixId);

	/**
	 * Upload a file to a contract's appendix.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @param file       file to upload
	 * @return void
	 */
	@PUT("contract/{contractId}/appendix/{appendixId}/file")
	Call<Void> uploadAppendixFile( //
			@Path("contractId") String contractId, //
			@Path("appendixId") String appendixId, //
			@Body byte[] file);

	/**
	 * Delete file from a contract's appendix.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @return void
	 */
	@DELETE("contract/{contractId}/appendix/{appendixId}/file")
	Call<Void> deleteAppendixFile(@Path("contractId") String contractId, @Path("appendixId") String appendixId);
}
