package com.trivore.id.sdk.service;

import java.util.List;
import java.util.Map;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.user.Enterprise;
import com.trivore.id.sdk.models.user.LegalInfo;
import com.trivore.id.sdk.models.user.PasswordRequirements;
import com.trivore.id.sdk.models.user.PasswordUpdateResult;
import com.trivore.id.sdk.models.user.User;
import com.trivore.id.sdk.models.user.UserNamespaceMigrationOptions;
import com.trivore.id.sdk.models.user.strong.identification.StrongIdentification;
import com.trivore.id.sdk.models.user.student.StudentStateInfo;
import com.trivore.id.sdk.requests.EmailVerificationRequest;
import com.trivore.id.sdk.requests.InviteRequest;
import com.trivore.id.sdk.requests.PhoneNumberVerificationRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A user service to process different kinds of user specific operations.
 * <p>
 * Handles user specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface UserService {

	/**
	 * Get all users from accessible namespaces.
	 *
	 * @return page with user objects
	 */
	@GET("user")
	Call<Page<User>> getAll();

	/**
	 * Get all users satisfying the provided filter.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query users.
	 * @return page with user objects
	 */
	@GET("user")
	Call<Page<User>> getAll( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter);

	/**
	 * Add a new user into the service.
	 * <p>
	 * If the namespace code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param user The user to be added.
	 * @return A user saved in Trivore ID.
	 */
	@POST("user")
	Call<User> create(@Body User user);

	/**
	 * Get the user with the given identifier.
	 *
	 * @param userId The id of the target user
	 * @return The user with the given identifier.
	 */
	@GET("user/{userId}")
	Call<User> get(@Path("userId") String userId);

	/**
	 * Save the changes of the provided user.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided user.
	 * </p>
	 *
	 * @param userId the ID of the target user
	 * @param user   The target user.
	 * @return A user saved in Trivore ID.
	 */
	@PUT("user/{userId}")
	Call<User> update(@Path("userId") String userId, @Body User user);

	/**
	 * Delete a user from Trivore ID.
	 *
	 * @param userId The id of the target user
	 * @return void
	 */
	@DELETE("user/{userId}")
	Call<Void> delete(@Path("userId") String userId);

	/**
	 * Get user's legal info.
	 *
	 * @param userId The id of the target user
	 * @return user's legal info
	 */
	@GET("user/{userId}/legal")
	Call<LegalInfo> getLegalInfo(@Path("userId") String userId);

	/**
	 * Send an email verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to send an email
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login by using the email address.
	 * </p>
	 *
	 * @param userId  The id of target user.
	 * @param request email verification request.
	 * @return void
	 */
	@POST("user/{userId}/email/verify")
	Call<Void> sendEmailVerification( //
			@Path("userId") String userId, //
			@Body EmailVerificationRequest request);

	/**
	 * Send an SMS code verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of the target user
	 * @param mobile Mobile number to send verification to. If not provided, user's
	 *               primary number will be used.
	 * @return void
	 */
	@POST("user/{userId}/sms/verify/send")
	Call<Void> sendPhoneNumberVerification(@Path("userId") String userId, @Query("mobile") String mobile);

	/**
	 * Check if the code that was sent to the target user is correct.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to check the code
	 * sent to the mobile phone number. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @param code   code which was entered by user.
	 * @return void. If the response status code is 404, then the code is not
	 *         correct.
	 */
	@POST("user/{userId}/sms/verify/check/{code}")
	Call<Void> checkPhoneNumberVerificationCode(@Path("userId") String userId, @Path("code") String code);

	/**
	 * Send an SMS verification message with link to the user with the given user
	 * id.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId  The id of target user.
	 * @param request phone number verification request
	 * @return void
	 */
	@POST("user/{userId}/sms/verify/sendlink")
	Call<Void> sendPhoneNumberVerificationLink( //
			@Path("userId") String userId, //
			@Body PhoneNumberVerificationRequest request);

	/**
	 * Invite new users to use the service.
	 * <p>
	 * Requests Trivore ID to send an invitation e-mail to user(s).
	 * </p>
	 *
	 * @param request The request class with list of emails, subject and text body.
	 * @return void
	 */
	@POST("invite/user")
	Call<Void> invite(@Body InviteRequest request);

	/**
	 * Read user's custom field values.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to get user's
	 * custom field values by the user id.
	 * </p>
	 *
	 * @param userId The id of the target user
	 * @return the object representing custom user fields.
	 */
	@GET("user/{userId}/customfields")
	Call<Map<String, Object>> getCustomFields(@Path("userId") String userId);

	/**
	 * Add or modify user's custom field values.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to add or modify
	 * user's custom field values by the user id.
	 * </p>
	 *
	 * @param userId       The id of the target user
	 * @param customFields User's custom fields.
	 * @return the object with updated custom user fields.
	 */
	@POST("user/{userId}/customfields")
	Call<Map<String, Object>> updateCustomFields( //
			@Path("userId") String userId, //
			@Body Map<String, Object> customFields);

	/**
	 * Delete all user's custom fields.
	 * <p>
	 * When this function is invoked, it will request Trivore ID to delete user's
	 * custom fields by the user id.
	 * </p>
	 *
	 * @param userId The id of the target user
	 * @return void
	 */
	@DELETE("user/{userId}/customfields")
	Call<Void> deleteCustomFields(@Path("userId") String userId);

	/**
	 * Get password validation / complexity requirement information.
	 *
	 * @param userId The id of the target user
	 * @return the password complexity requirements.
	 */
	@GET("user/{userId}/password/requirements")
	Call<PasswordRequirements> getPasswordComplexity(@Path("userId") String userId);

	/**
	 * Get the enterprise info from the given user identifier.
	 *
	 * @param userId The identifier of the underlying user instance.
	 * @return The enterprise with the given identifier.
	 */
	@GET("user/{userId}/enterprise")
	Call<Enterprise> getEnterprise(@Path("userId") String userId);

	/**
	 * Save (add/update) the enterprise with the given identifier.
	 *
	 * @param userId     The identifier of the underlying user instance.
	 * @param enterprise The enterprise info to be saved.
	 * @return The same enterprise returned from the Trivore ID.
	 */
	@PUT("user/{userId}/enterprise")
	Call<Enterprise> saveEnterprise(@Path("userId") String userId, @Body Enterprise enterprise);

	/**
	 * Get user's strong identification info.
	 *
	 * @param userId The id of the target user
	 * @return the object representing strong identification.
	 */
	@GET("user/{userId}/strongidentification")
	Call<StrongIdentification> getStrongIdentification(@Path("userId") String userId);

	/**
	 * Report user's strong identification.
	 *
	 * @param userId               The id of the target user
	 * @param strongIdentification user's report for strong identification
	 * @return the object representing strong identification.
	 */
	@POST("user/{userId}/strongidentification")
	Call<StrongIdentification> reportStrongIdentification( //
			@Path("userId") String userId, //
			@Body Map<String, String> strongIdentification);

	/**
	 * Get user's strong identification history.
	 *
	 * @param userId     The id of the target user
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query groups.
	 * @param sortBy     Sort by attribute name
	 * @param sortOrder  Sort direction ('ascending' or 'descending')
	 * @return page with strongidentification resources
	 */
	@GET("user/{userId}/strongidentification/history")
	Call<Page<StrongIdentification>> getStrongIdentificationHistory( //
			@Path("userId") String userId, //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter, //
			@Query("sortBy") String sortBy, //
			@Query("sortOrder") String sortOrder);

	/**
	 * Get user's strong identification history entry.
	 *
	 * @param userId    The id of the target user
	 * @param historyId single strong identification history entry id
	 * @return the object representing strong identification.
	 */
	@GET("user/{userId}/strongidentification/history/{id}")
	Call<StrongIdentification> getStrongIdentificationHistoryEntry( //
			@Path("userId") String userId, //
			@Path("id") String historyId);

	/**
	 * Change user's password. New password must be valid for current password
	 * requirements, invalid new password request is ignored.
	 *
	 * @param userId      The id of the target user
	 * @param newPassword Map with new password ({"newPassword": "string"})
	 * @return the object with the password update result.
	 */
	@PUT("user/{userId}/password")
	Call<PasswordUpdateResult> changePassword( //
			@Path("userId") String userId, //
			@Body Map<String, String> newPassword);

	/**
	 * Get user's custom permissions.
	 *
	 * @param userId The id of the target user
	 * @return list with user's custom permissions
	 */
	@GET("user/{userId}/permissions/custom")
	Call<List<String>> getCustomPermissions(@Path("userId") String userId);

	/**
	 * Update user's custom permissions.
	 *
	 * @param userId            The id of the target user
	 * @param permissionChanges map with the permission changes {"add" : [string],
	 *                          "remove" : [string]}
	 * @return list with updated user's custom permissions
	 */
	@PUT("user/{userId}/permissions/custom")
	Call<List<String>> updateCustomPermissions( //
			@Path("userId") String userId, //
			@Body Map<String, List<String>> permissionChanges);

	/**
	 * Get user's effective permissions.
	 *
	 * @param userId The id of the target user
	 * @return list with user's effective permissions
	 */
	@GET("user/{userId}/permissions/effective")
	Call<List<String>> getEffectivePermissions(@Path("userId") String userId);

	/**
	 * Get user's built-in roles.
	 *
	 * @param userId The id of the target user
	 * @return list with user's built-in roles
	 */
	@GET("user/{userId}/roles/builtin")
	Call<List<String>> getBuiltinRoles(@Path("userId") String userId);

	/**
	 * Update user's built-in roles.
	 *
	 * @param userId      The id of the target user
	 * @param roleChanges map with the roles changes {"add" : [string], "remove" :
	 *                    [string]}
	 * @return list with updated user's built-in roles
	 */
	@PUT("user/{userId}/roles/builtin")
	Call<List<String>> updateBuiltinRoles( //
			@Path("userId") String userId, //
			@Body Map<String, List<String>> roleChanges);

	/**
	 * Get user's custom roles.
	 *
	 * @param userId The id of the target user
	 * @return list with user's custom roles
	 */
	@GET("user/{userId}/roles/custom")
	Call<List<String>> getCustomRoles(@Path("userId") String userId);

	/**
	 * Update user's custom roles.
	 *
	 * @param userId      The id of the target user
	 * @param roleChanges map with the roles changes {"add" : [string], "remove" :
	 *                    [string]}
	 * @return list with updated user's custom roles
	 */
	@PUT("user/{userId}/roles/custom")
	Call<List<String>> updateCustomRoles( //
			@Path("userId") String userId, //
			@Body Map<String, List<String>> roleChanges);

	/**
	 * Read last student state info.
	 *
	 * @param userId The id of the target user
	 * @return list with updated user's custom roles
	 */
	@GET("user/{userId}/student/state")
	Call<StudentStateInfo> getStudentInfo(@Path("userId") String userId);

	/**
	 * Update user's custom roles.
	 *
	 * @param userId    The id of the target user
	 * @param stateInfo student state info to update
	 * @return list with updated user's custom roles
	 */
	@POST("user/{userId}/student/state")
	Call<StudentStateInfo> updateStudentInfo(@Path("userId") String userId, @Body StudentStateInfo stateInfo);

	/**
	 * Partially (!) migrate user to another namespace.
	 * <p>
	 * Warning: Original user will be deleted and another will be created in the
	 * target namespace.
	 * </p>
	 * <p>
	 * NB! Not all information will be migrated!
	 * </p>
	 *
	 * @param userId  The id of the target user
	 * @param options user namestace migration options
	 * @return list with updated user's custom roles
	 */
	@POST("user/{userId}/migratenamespace")
	Call<User> migrateNamespace(@Path("userId") String userId, @Body UserNamespaceMigrationOptions options);

}