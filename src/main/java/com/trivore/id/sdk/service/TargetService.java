package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.Target;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * An target service to process different kinds of target specific operations.
 * <p>
 * Handles target specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface TargetService {

	/**
	 * Get all targets satisfying the provided criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query targets.
	 * @return page with target objects
	 */
	@GET("target")
	Call<Page<Target>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Add a new target into the service.
	 *
	 * @param target The target to be added.
	 * @return A target saved in Trivore ID.
	 */
	@POST("target")
	Call<Target> create(@Body Target target);

	/**
	 * Get the target with the given identifier.
	 *
	 * @param targetId The id of the target target.
	 * @return The target with the given identifier.
	 */
	@GET("target/{targetId}")
	Call<Target> get(@Path("targetId") String targetId);

	/**
	 * Save the changes of the provided target.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided target.
	 * </p>
	 *
	 * @param targetId the ID of the target target
	 * @param target   The target target.
	 * @return A target saved in Trivore ID.
	 */
	@PUT("target/{targetId}")
	Call<Target> update(@Path("targetId") String targetId, @Body Target target);

	/**
	 * Delete a target from Trivore ID.
	 *
	 * @param targetId The id of the target target.
	 * @return void
	 */
	@DELETE("target/{targetId}")
	Call<Void> delete(@Path("targetId") String targetId);

}
