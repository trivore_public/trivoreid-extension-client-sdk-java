package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Contact;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.user.Enterprise;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A contact service to process different kinds of contact specific operations.
 * <p>
 * Handles contact specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface ContactService {

	/**
	 * Get all contacts satisfying the provided criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query contacts.
	 * @return page with contact objects
	 */
	@GET("contact")
	Call<Page<Contact>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Add a new contact into the service.
	 *
	 * @param contact The contact to be added.
	 * @return A contact saved in Trivore ID.
	 */
	@POST("contact")
	Call<Contact> create(@Body Contact contact);

	/**
	 * Get the contact with the given identifier.
	 *
	 * @param contactId The id of the target contact.
	 * @return The contact with the given identifier.
	 */
	@GET("contact/{contactId}")
	Call<Contact> get(@Path("contactId") String contactId);

	/**
	 * Save the changes of the provided contact.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided contact.
	 * </p>
	 *
	 * @param contactId the ID of the target contact
	 * @param contact   The target contact.
	 * @return A contact saved in Trivore ID.
	 */
	@PUT("contact/{contactId}")
	Call<Contact> update(@Path("contactId") String contactId, @Body Contact contact);

	/**
	 * Delete a contact from Trivore ID.
	 *
	 * @param contactId The id of the target contact.
	 * @return void
	 */
	@DELETE("contact/{contactId}")
	Call<Void> delete(@Path("contactId") String contactId);

	/**
	 * Get the contact enterprise with the given identifier.
	 *
	 * @param contactId The id of the target contact.
	 * @return The contact enterprise with the given identifier.
	 */
	@GET("contact/{contactId}/enterpriseinfo")
	Call<Enterprise> getEnterprise(@Path("contactId") String contactId);

	/**
	 * Save the changes of the provided contact.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided contact enterprise.
	 * </p>
	 *
	 * @param contactId  the ID of the target contact
	 * @param enterprise The target enterprise.
	 * @return A contact enterprise saved in Trivore ID.
	 */
	@PUT("contact/{contactId}/enterpriseinfo")
	Call<Enterprise> updateEnterprise(@Path("contactId") String contactId, @Body Enterprise enterprise);

}
