package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.BuiltInRole;
import com.trivore.id.sdk.models.CustomRole;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.Permission;
import com.trivore.id.sdk.service.AccessRightService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * An access right service to process different kinds of access right specific
 * operations.
 * <p>
 * Handles access right specific tasks and communicates with the TrivoreID
 * service.
 * </p>
 */
public class AccessRightServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private AccessRightService service;

	/**
	 * Construct Access Right Service helper.
	 *
	 * @param service access right service
	 */
	public AccessRightServiceImpl(AccessRightService service) {
		this.service = service;
	}

	/**
	 * Get all builtin permissions.
	 *
	 * @return list with the builtin permissions
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<Permission> getAllPermissions() throws IOException, TrivoreIDException {

		Response<List<Permission>> response = service.getAllPermissions().execute();

		if (response.isSuccessful()) {
			log.info("Successfully found all builtin permissions.");
			return response.body();
		} else {
			log.error("Failed find all builtin permissions.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get a single builtin permission.
	 *
	 * @param permissionId permission ID
	 * @return builtin permission
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Permission getPermission(String permissionId) throws IOException, TrivoreIDException {
		Assert.notNull(permissionId, "The id cannot be null");
		Assert.hasText(permissionId, "The id cannot be empty");

		Response<Permission> response = service.getPermission(permissionId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully found a builtin permission qith id {}.", permissionId);
			return response.body();
		} else {
			log.error("Failed find a builtin permission with id {}.", permissionId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all builtin roles.
	 *
	 * @return list with the builtin roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<BuiltInRole> getAllBuiltinRoles() throws IOException, TrivoreIDException {

		Response<List<BuiltInRole>> response = service.getAllBuiltinRoles().execute();

		if (response.isSuccessful()) {
			log.info("Successfully found all builtin roles.");
			return response.body();
		} else {
			log.error("Failed find all builtin roles.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all custom roles.
	 *
	 * @return page with the custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Page<CustomRole> getAllCustomRoles() throws IOException, TrivoreIDException {
		return getAllCustomRoles(new Criteria());
	}

	/**
	 * Get all custom roles.
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with the custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Page<CustomRole> getAllCustomRoles(Criteria criteria) throws IOException, TrivoreIDException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<CustomRole>> response = service
				.getAllCustomRoles(criteria.getStartIndex(), criteria.getCount(), filter).execute();

		if (response.isSuccessful()) {
			Page<CustomRole> page = response.body();
			log.info("Successfully found {} custom roles with criteria {}", page.getTotalResults(), criteria);
			return response.body();
		} else {
			log.error("Failed find all builtin roles.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new role into the service.
	 *
	 * @param role the role to be added.
	 * @return A custom role saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public CustomRole createCustomRole(CustomRole role) throws IOException, TrivoreIDException {
		Assert.notNull(role, "The role cannot be null");

		Response<CustomRole> response = service.createCustomRole(role).execute();

		if (response.isSuccessful()) {
			CustomRole createdRole = response.body();
			log.info("Successfully created custom role with id {}", createdRole.getId());
			return createdRole;
		} else {
			log.error("Failed to create a custom role.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the custom role with the given identifier.
	 *
	 * @param roleId The id of the target custom role.
	 * @return The custom role with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public CustomRole getCustomRole(String roleId) throws IOException, TrivoreIDException {
		Assert.notNull(roleId, "The id cannot be null");
		Assert.hasText(roleId, "The id cannot be empty");

		Response<CustomRole> response = service.getCustomRole(roleId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the custom role with id {}", roleId);
			return response.body();
		} else {
			log.error("Failed to get the custom role with id {}", roleId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided custom role.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided custom role.
	 * </p>
	 *
	 * @param role The target custom role.
	 * @return A custom role saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public CustomRole updateCustomRole(CustomRole role) throws IOException, TrivoreIDException {
		Assert.notNull(role, "The role cannot be null");
		Assert.notNull(role.getId(), "The ID cannot be null");
		Assert.hasText(role.getId(), "The ID cannot be empty");

		Response<CustomRole> response = service.updateCustomRole(role.getId(), role).execute();

		if (response.isSuccessful()) {
			log.info("Successfully modified the custom role with ID {}.", role.getId());
			return response.body();
		} else {
			log.error("Failed to get the custom role profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a custom role from TrivoreID.
	 *
	 * @param roleId The id of the target custom role.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteCustomRole(String roleId) throws IOException, TrivoreIDException {
		Assert.notNull(roleId, "The roleId cannot be null");
		Assert.hasText(roleId, "The roleId cannot be empty");

		Response<Void> response = service.deleteCustomRole(roleId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted custom role with id {}.", roleId);
		} else {
			log.error("Failed to delete custom role with id {}.", roleId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
