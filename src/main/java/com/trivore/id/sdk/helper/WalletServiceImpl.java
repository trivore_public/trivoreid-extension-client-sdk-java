package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.wallet.Transaction;
import com.trivore.id.sdk.models.wallet.Wallet;
import com.trivore.id.sdk.service.WalletService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;
import retrofit2.http.Body;

/**
 * Wallet Service helper to perform wallet related requests to TrivoreID.
 */
public class WalletServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final WalletService service;

	/**
	 * Construct Sales Service helper.
	 *
	 * @param service sales service
	 */
	public WalletServiceImpl(WalletService service) {
		this.service = service;
	}

	/**
	 * Get all accessible wallets.
	 *
	 * @return page with wallet objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Wallet> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria(), null, null);
	}

	/**
	 * Get all accessible wallets satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with wallet objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Wallet> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		return getAll(criteria, null, null);
	}

	/**
	 * Get all accessible wallets satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    sort by attribute name
	 * @param ascending sort direction (ascending or descending)
	 * @return page with wallet objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Wallet> getAll(Criteria criteria, String sortBy, Boolean ascending)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null && ascending) {
			sortOrder = "ascending";
		} else {
			sortOrder = "descending";
		}

		Response<Page<Wallet>> response = service.getAll( //
				criteria.getStartIndex(), //
				criteria.getCount(), //
				filter, //
				sortBy, //
				sortOrder).execute();
		if (response.isSuccessful()) {
			Page<Wallet> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} wallets with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find wallets with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new wallet into the service.
	 *
	 * @param wallet The wallet to be added.
	 * @return A wallet saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Wallet create(Wallet wallet) throws IOException, TrivoreIDException {
		Assert.notNull(wallet, "The wallet cannot be null");

		Response<Wallet> response = service.create(wallet).execute();

		if (response.isSuccessful()) {
			Wallet newWallet = response.body();
			log.info("Successfully created wallet with id {}", newWallet.getId());
			return newWallet;
		} else {
			log.error("Failed to create a wallet.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the wallet with the given identifier.
	 *
	 * @param walletId The id of the target wallet.
	 * @return The wallet with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Wallet get(String walletId) throws IOException, TrivoreIDException {
		Assert.notNull(walletId, "The id cannot be null");
		Assert.hasText(walletId, "The id cannot be empty");

		Response<Wallet> response = service.get(walletId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the wallet with id {}", walletId);
			return response.body();
		} else {
			log.error("Failed to get the wallet with id {}", walletId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided wallet.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided wallet.
	 * </p>
	 *
	 * @param wallet The target wallet.
	 * @return A wallet saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Wallet update(Wallet wallet) throws IOException, TrivoreIDException {
		Assert.notNull(wallet, "The wallet cannot be null");
		Assert.notNull(wallet.getId(), "The ID cannot be null");
		Assert.hasText(wallet.getId(), "The ID cannot be empty");

		Response<Wallet> response = service.update(wallet.getId(), wallet).execute();

		if (response.isSuccessful()) {
			log.info("Successfully modified the wallet with ID {}.", wallet.getId());
			return response.body();
		} else {
			log.error("Failed to update the wallet with id {}.", wallet.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a wallet from TrivoreID.
	 *
	 * @param walletId The id of the target wallet.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String walletId) throws IOException, TrivoreIDException {
		Assert.notNull(walletId, "The walletId cannot be null");
		Assert.hasText(walletId, "The walletId cannot be empty");

		Response<Void> response = service.delete(walletId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted wallet with id {}.", walletId);
		} else {
			log.error("Failed to delete wallet with id {}.", walletId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Deposit funds to an account.
	 * <p>
	 * Funds are not transferred from another wallet, they are simply added to the
	 * wallet.
	 * </p>
	 *
	 * @param walletId    The id of the target wallet.
	 * @param transaction transaction info
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deposit(String walletId, @Body Transaction transaction) throws IOException, TrivoreIDException {
		Assert.notNull(walletId, "The walletId cannot be null");
		Assert.hasText(walletId, "The walletId cannot be empty");
		Assert.notNull(transaction, "The transaction cannot be null");
		Assert.notNull(transaction.getAmount(), "The amount cannot be null");
		Assert.hasText(transaction.getAmount(), "The amount cannot be empty");
		Assert.notNull(transaction.getCurrency(), "The currency cannot be null");
		Assert.hasText(transaction.getCurrency(), "The currency cannot be empty");

		Response<Void> response = service.deposit(walletId, transaction).execute();

		if (response.isSuccessful()) {
			log.info("Successfully made a transaction for a wallet with id {}.", walletId);
		} else {
			log.error("Failed to make a transaction for a wallet with id {}.", walletId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Transfer funds to another wallet
	 * <p>
	 * Only wallet owner, wallet holders, or those with read access to wallet and
	 * DEPOSIT and WITHDRAW permissions can transfer funds between wallets.
	 * </p>
	 *
	 * @param walletId    target wallet ID
	 * @param transaction transaction info
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void transfer(String walletId, @Body Transaction transaction) throws IOException, TrivoreIDException {
		Assert.notNull(walletId, "The walletId cannot be null");
		Assert.hasText(walletId, "The walletId cannot be empty");
		Assert.notNull(transaction, "The transaction cannot be null");
		Assert.notNull(transaction.getAmount(), "The amount cannot be null");
		Assert.hasText(transaction.getAmount(), "The amount cannot be empty");
		Assert.notNull(transaction.getCurrency(), "The currency cannot be null");
		Assert.hasText(transaction.getCurrency(), "The currency cannot be empty");

		Response<Void> response = service.transfer(walletId, transaction).execute();

		if (response.isSuccessful()) {
			log.info("Successfully made a transfer for a wallet with id {}.", walletId);
		} else {
			log.error("Failed to make a transfer for a wallet with id {}.", walletId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Withdraw funds from a wallet.
	 * <p>
	 * Funds are not transferred to another wallet, they are simply removed from
	 * source wallet.
	 * </p>
	 *
	 * @param walletId    target wallet ID
	 * @param transaction transaction info
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void withdraw(String walletId, @Body Transaction transaction) throws IOException, TrivoreIDException {
		Assert.notNull(walletId, "The walletId cannot be null");
		Assert.hasText(walletId, "The walletId cannot be empty");
		Assert.notNull(transaction, "The transaction cannot be null");
		Assert.notNull(transaction.getAmount(), "The amount cannot be null");
		Assert.hasText(transaction.getAmount(), "The amount cannot be empty");
		Assert.notNull(transaction.getCurrency(), "The currency cannot be null");
		Assert.hasText(transaction.getCurrency(), "The currency cannot be empty");

		Response<Void> response = service.withdraw(walletId, transaction).execute();

		if (response.isSuccessful()) {
			log.info("Successfully made a transfer for a wallet with id {}.", walletId);
		} else {
			log.error("Failed to make a transfer for a wallet with id {}.", walletId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
