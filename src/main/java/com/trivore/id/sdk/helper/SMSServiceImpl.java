package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Region;
import com.trivore.id.sdk.models.SMSMessage;
import com.trivore.id.sdk.models.SMSResponse;
import com.trivore.id.sdk.service.SMSService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A group service for all kinds of sms messaging operations.
 * <p>
 * Handles all kinds of operations with TrivoreID sms messages.
 * </p>
 */
public class SMSServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private SMSService service;

	/**
	 * Construct SMS Service helper.
	 *
	 * @param service SMS service
	 */
	public SMSServiceImpl(SMSService service) {
		this.service = service;
	}

	/**
	 * Send a custom SMS message.
	 *
	 * @param message SMS message
	 * @return SMS response
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public SMSResponse send(SMSMessage message) throws IOException, TrivoreIDException {
		Assert.notNull(message, "The message cannot be null");
		Assert.notNull(message.getTo(), "The message.to cannot be null");
		Assert.hasText(message.getTo(), "The message.to cannot be empty");

		Response<SMSResponse> response = service.send(message).execute();

		if (response.isSuccessful()) {
			log.info("Successfully sent SMS message to {}", message.getTo());
			return response.body();
		} else {
			log.error("Failed to send SMS message to {}.", message.getTo());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Send a custom SMS message to a user.
	 *
	 * @param message SMS message
	 * @param userId  target user
	 * @return SMS response
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public SMSResponse sendToUser(SMSMessage message, String userId) throws IOException, TrivoreIDException {
		Assert.notNull(message, "The message cannot be null");
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<SMSResponse> response = service.sendToUser(message, userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully sended SMS message to the user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to send SMS message to the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the list of all supported regions.
	 *
	 * @return list of all supported regions
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<Region> getSupportedRegions() throws IOException, TrivoreIDException {

		Response<List<Region>> response = service.getSupportedRegions().execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the list of regions");
			return response.body();
		} else {
			log.error("Failed to get the list of the reions.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
