package com.trivore.id.sdk.helper;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Group;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.service.GroupService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;
import com.trivore.id.sdk.utils.Filter;

import retrofit2.Response;

/**
 * A group service helper for all kinds of group management operations.
 * <p>
 * Handles all kinds of group CRUD and membership operations.
 * </p>
 */
public class GroupServiceImpl {
	private static final Logger log = getLogger(GroupServiceImpl.class);

	private final GroupService service;

	/**
	 * Construct Group Service helper.
	 *
	 * @param service group service
	 */
	public GroupServiceImpl(GroupService service) {
		this.service = service;
	}

	/**
	 * Get all groups satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with group objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Group> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all groups satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with group objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Group> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Group>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Group> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} groups with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find groups with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new group into the service.
	 *
	 * @param group The group to be added.
	 * @return A group saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Group create(Group group) throws IOException, TrivoreIDException {
		Assert.notNull(group, "The group cannot be null");

		Response<Group> response = service.create(group).execute();

		if (response.isSuccessful()) {
			Group newGroup = response.body();
			log.info("Successfully created group with id {}", newGroup.getId());
			return newGroup;
		} else {
			log.error("Failed to create a group.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the group with the given identifier.
	 *
	 * @param groupId The id of the target group.
	 * @return The group with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Group get(String groupId) throws IOException, TrivoreIDException {
		Assert.notNull(groupId, "The id cannot be null");
		Assert.hasText(groupId, "The id cannot be empty");

		Response<Group> response = service.get(groupId).execute();

		if (response.isSuccessful()) {
			Group group = response.body();
			log.info("Successfully got the group with id {}", groupId);
			return group;
		} else {
			log.error("Failed to get the group with id {}", groupId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided group.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided group.
	 * </p>
	 *
	 * @param group   The target group.
	 * @return A group saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Group update(Group group) throws IOException, TrivoreIDException {
		Assert.notNull(group, "The group cannot be null");
		Assert.notNull(group.getId(), "The ID cannot be null");
		Assert.hasText(group.getId(), "The ID cannot be empty");

		Response<Group> response = service.update(group.getId(), group).execute();

		if (response.isSuccessful()) {
			Group updatedGroup = response.body();
			log.info("Successfully modified the group with ID {}.", updatedGroup.getId());
			return updatedGroup;
		} else {
			log.error("Failed to get the group profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a group from TrivoreID.
	 *
	 * @param groupId The id of the target group.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String groupId) throws IOException, TrivoreIDException {
		Assert.notNull(groupId, "The groupId cannot be null");
		Assert.hasText(groupId, "The groupId cannot be empty");

		Response<Void> response = service.delete(groupId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted group with id {}.", groupId);
		} else {
			log.error("Failed to delete group with id {}.", groupId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all subgroups of the target group.
	 *
	 * @param groupId The id of the target group.
	 * @return List with the subgroups of the target group.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<Group> getNested(String groupId) throws IOException, TrivoreIDException {
		Assert.notNull(groupId, "The id cannot be null");
		Assert.hasText(groupId, "The id cannot be empty");

		Response<List<Group>> response = service.getNested(groupId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully found subgroups of the group with id {}", groupId);
			return response.body();
		} else {
			log.error("Failed to find subgroups of the group with id {}", groupId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Check the defined array of group ID or names.
	 * <p>
	 * <b>NB!</b> The length of the returned list may differ from the list with the
	 * group names if the group with default name and nsCode was not found. Then the
	 * warning message will be logged.
	 * </p>
	 *
	 * @param nsCode          namespace code of the groups
	 * @param groupIdsOrNames group ID or names
	 * @return list with group IDs.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<String> checkGroupIds(String nsCode, List<String> groupIdsOrNames)
			throws TrivoreIDException, IOException {
		return checkGroupIds(nsCode, false, groupIdsOrNames.toArray(new String[0]));
	}

	/**
	 * Check the defined array of group ID or names.
	 * <p>
	 * <b>NB!</b> The length of the returned list may differ from the list with the
	 * group names if the group with default name and nsCode was not found. Then the
	 * warning message will be logged.
	 * </p>
	 *
	 * @param nsCode          namespace code of the groups
	 * @param groupIdsOrNames group ID or names
	 * @return list with group IDs.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<String> checkGroupIds(String nsCode, String... groupIdsOrNames) throws TrivoreIDException, IOException {
		return checkGroupIds(nsCode, false, groupIdsOrNames);
	}

	/**
	 * Check the defined array of group ID or names.
	 * <p>
	 * <b>NB!</b> The length of the returned list may differ from the list with the
	 * group names if the group with default name and nsCode was not found. Then the
	 * warning message will be logged.
	 * </p>
	 *
	 * @param nsCode          namespace code of the groups
	 * @param create          if new groups should be created
	 * @param groupIdsOrNames group ID or names
	 * @return list with group IDs.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<String> checkGroupIds(String nsCode, boolean create, List<String> groupIdsOrNames)
			throws TrivoreIDException, IOException {
		return checkGroupIds(nsCode, create, groupIdsOrNames.toArray(new String[0]));
	}

	/**
	 * Check the defined array of group ID or names.
	 * <p>
	 * <b>NB!</b> The length of the returned list may differ from the list with the
	 * group names if the group with default name and nsCode was not found. Then the
	 * warning message will be logged.
	 * </p>
	 *
	 * @param nsCode          namespace code of the groups
	 * @param create          if new groups should be created
	 * @param groupIdsOrNames group ID or names
	 * @return list with group IDs.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<String> checkGroupIds(String nsCode, boolean create, String... groupIdsOrNames)
			throws TrivoreIDException, IOException {
		// check if ids are on the list
		List<String> groupIds = new ArrayList<>(); // list with group ids
		List<String> groupNames = new ArrayList<>(); // list with group names
		List<String> notIdsNorNames = new ArrayList<>(); // if groups doesn't contain group ID or name
		for (String group : groupIdsOrNames) {
			Filter filter = Filter.equal("id", group);
			Page<Group> page = getAll(new Criteria(filter));
			if (page.getResources().isEmpty()) {
				groupNames.add(group);
			} else {
				groupIds.add(group);
			}
		}

		// check if names are on the list
		for (String group : groupNames) {
			Filter filter = Filter.and(Filter.equal("name", group), Filter.equal("nsCode", nsCode));
			Page<Group> page = getAll(new Criteria(filter));
			if (page.getResources().isEmpty()) {
				notIdsNorNames.add(group);
			} else {
				groupIds.add(page.getResources().get(0).getId());
			}
		}

		if (!notIdsNorNames.isEmpty()) {
			if (create) {
				Group group = new Group();
				group.setNsCode(nsCode);
				for (String name : notIdsNorNames) {
					group.setName(name);
					groupIds.add(create(group).getId());
				}
			} else {
				log.warn("WARNING! Following group ids/names don't exist : {}", notIdsNorNames);
			}
		}

		return groupIds;
	}
}
