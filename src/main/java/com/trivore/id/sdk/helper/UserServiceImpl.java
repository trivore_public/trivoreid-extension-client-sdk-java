package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.user.Enterprise;
import com.trivore.id.sdk.models.user.LegalInfo;
import com.trivore.id.sdk.models.user.PasswordRequirements;
import com.trivore.id.sdk.models.user.PasswordUpdateResult;
import com.trivore.id.sdk.models.user.User;
import com.trivore.id.sdk.models.user.UserNamespaceMigrationOptions;
import com.trivore.id.sdk.models.user.strong.identification.StrongIdentification;
import com.trivore.id.sdk.models.user.student.StudentStateInfo;
import com.trivore.id.sdk.requests.EmailVerificationRequest;
import com.trivore.id.sdk.requests.InviteRequest;
import com.trivore.id.sdk.requests.PhoneNumberVerificationRequest;
import com.trivore.id.sdk.service.UserService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Path;

/**
 * User Service helper to perform user related requests to TrivoreID.
 */
public class UserServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private UserService service;

	/**
	 * Construct User Service helper.
	 */
	public UserServiceImpl() {
		// ..
	}

	/**
	 * Construct User Service helper.
	 *
	 * @param service user service
	 */
	public UserServiceImpl(UserService service) {
		this.service = service;
	}

	/**
	 * Get all users satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with user objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<User> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all users satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with user objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<User> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<User>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter).execute();
		if (response.isSuccessful()) {
			Page<User> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} users with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find users with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new user into the service.
	 *
	 * @param user The user to be added.
	 * @return A user saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public User create(User user) throws IOException, TrivoreIDException {
		return create(user, false, false);
	}

	/**
	 * Add a new user into the service.
	 *
	 * @param user               The user to be added.
	 * @param emailVerification  should the email verification be sent
	 * @param mobileVerification should the mobile verification be sent
	 * @return A user saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public User create(User user, boolean emailVerification, boolean mobileVerification)
			throws IOException, TrivoreIDException {
		Assert.notNull(user, "The user cannot be null");

		Response<User> response = service.create(user).execute();

		if (response.isSuccessful()) {
			User newUser = response.body();
			log.info("Successfully created user with id {}", newUser.getId());

			if (emailVerification) {
				try {
					sendEmailVerification(newUser.getId());
				} catch (Exception e) {
					log.warn("Failed to send email verification : {}", e.getMessage());
				}
			}
			if (mobileVerification) {
				try {
					sendPhoneNumberVerification(newUser.getId());
				} catch (Exception e) {
					log.warn("Failed to send mobile verification : {}", e.getMessage());
				}
			}

			return newUser;
		} else {
			log.error("Failed to create a user.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the user with the given identifier.
	 *
	 * @param userId The id of the target user.
	 * @return The user with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public User get(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The id cannot be null");
		Assert.hasText(userId, "The id cannot be empty");

		Response<User> response = service.get(userId).execute();

		if (response.isSuccessful()) {
			User user = response.body();
			log.info("Successfully got the user with id {}", userId);
			return user;
		} else {
			log.error("Failed to get the user with id {}", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided user.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided user.
	 * </p>
	 *
	 * @param user The target user.
	 * @return A user saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public User update(User user) throws IOException, TrivoreIDException {
		Assert.notNull(user, "The user cannot be null");
		Assert.notNull(user.getId(), "The ID cannot be null");
		Assert.hasText(user.getId(), "The ID cannot be empty");

		Response<User> response = service.update(user.getId(), user).execute();

		if (response.isSuccessful()) {
			User updatedUser = response.body();
			log.info("Successfully modified the user with ID {}.", updatedUser.getId());
			return updatedUser;
		} else {
			log.error("Failed to get the user profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a user from TrivoreID.
	 *
	 * @param userId The id of the target user.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Void> response = service.delete(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted user with id {}.", userId);
		} else {
			log.error("Failed to delete user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's legal info.
	 *
	 * @param userId The id of the target user.
	 * @return user's legal info
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public LegalInfo getLegalInfo(@Path("userId") String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<LegalInfo> response = service.getLegalInfo(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got legal info of the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to get legal info of the  user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Send an email verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an email
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login by using the email address.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendEmailVerification(String userId) throws IOException, TrivoreIDException {
		sendEmailVerification(userId, null, null);
	}

	/**
	 * Send an email verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an email
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login by using the email address.
	 * </p>
	 *
	 * @param userId  The id of target user.
	 * @param address The email address that shoudld be verified. The address must
	 *                be one of the email addresses in the user emails list.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendEmailVerification(String userId, String address) throws IOException, TrivoreIDException {
		sendEmailVerification(userId, null, null, address);
	}

	/**
	 * Send an email verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an email
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login by using the email address.
	 * </p>
	 *
	 * @param userId         The id of target user.
	 * @param returnUrl      The return URL for the email verification.
	 * @param expirationDays The amount of days before the email expires.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendEmailVerification(String userId, String returnUrl, Integer expirationDays)
			throws IOException, TrivoreIDException {
		sendEmailVerification(userId, returnUrl, expirationDays, null);
	}

	/**
	 * Send an email verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an email
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login by using the email address.
	 * </p>
	 *
	 * @param userId         The id of target user.
	 * @param returnUrl      The return URL for the email verification.
	 * @param expirationDays The amount of days before the email expires.
	 * @param address        The email address that shoudld be verified. The address
	 *                       must be one of the email addresses in the user emails
	 *                       list.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendEmailVerification(String userId, String returnUrl, Integer expirationDays, String address)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		EmailVerificationRequest request = new EmailVerificationRequest(returnUrl, expirationDays, address);
		Response<Void> response = service.sendEmailVerification(userId, request).execute();

		if (response.isSuccessful()) {
			log.info("Successfully sent email verification to {}", userId);
		} else {
			log.error("Failed to send email verification to {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Send an SMS code verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendPhoneNumberVerification(String userId) throws IOException, TrivoreIDException {
		sendPhoneNumberVerification(userId, null);
	}

	/**
	 * Send an SMS code verification message to the user with the given user id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @param number Mobile number to send verification to. If not provided, user's
	 *               primary number will be used.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendPhoneNumberVerification(String userId, String number) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Void> response = service.sendPhoneNumberVerification(userId, number).execute();

		if (response.isSuccessful()) {
			log.info("Successfully sent phone code-verification to {}", userId);
		} else {
			log.error("Failed to send phone number verification to {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Check if the code that was sent to the target user is correct.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to check the code
	 * sent to the mobile phone number. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @param code   code which was entered by user.
	 * @return if the verification code is correct.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public boolean checkPhoneNumberVerificationCode(String userId, String code) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(code, "The code cannot be null.");
		Assert.hasText(code, "The code cannot be empty.");

		Response<Void> response = service.checkPhoneNumberVerificationCode(userId, code).execute();

		if (response.isSuccessful()) {
			log.info("Mobile phone number of the user {} was verified", userId);
			return true;
		} else {
			if (response.code() != 404) {
				log.error("Failed to check phone number verification code for user with id {}.", userId);
				throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
			}
			log.info("Wrong verification code.");
			return false;
		}
	}

	/**
	 * Send an SMS verification message with link to the user with the given user
	 * id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendPhoneNumberVerificationLink(String userId) throws IOException, TrivoreIDException {
		sendPhoneNumberVerificationLink(userId, null, null);
	}

	/**
	 * Send an SMS verification message with link to the user with the given user
	 * id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId The id of target user.
	 * @param number The phone number that shoudld be verified. The number must be
	 *               one of the phone numberes in the user mobiles list
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendPhoneNumberVerificationLink(String userId, String number) throws IOException, TrivoreIDException {
		sendPhoneNumberVerificationLink(userId, null, null, number);
	}

	/**
	 * Send an SMS verification message with link to the user with the given user
	 * id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId         The id of target user.
	 * @param returnUrl      The return URL for the phone number verification.
	 * @param expirationDays The amount of days before the verification phone number
	 *                       expires.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendPhoneNumberVerificationLink(String userId, String returnUrl, Integer expirationDays)
			throws IOException, TrivoreIDException {
		sendPhoneNumberVerificationLink(userId, returnUrl, expirationDays, null);
	}

	/**
	 * Send an SMS verification message with link to the user with the given user
	 * id.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to send an SMS
	 * verification message to user. After the user has verified the message, it
	 * will become possible to user to login.
	 * </p>
	 *
	 * @param userId         The id of target user.
	 * @param returnUrl      The return URL for the phone number verification.
	 * @param expirationDays The amount of days before the verification phone number
	 *                       expires.
	 * @param number         The phone number that shoudld be verified. The number
	 *                       must be one of the phone numberes in the user mobiles
	 *                       list
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void sendPhoneNumberVerificationLink(String userId, String returnUrl, Integer expirationDays, String number)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		PhoneNumberVerificationRequest request = new PhoneNumberVerificationRequest(returnUrl, expirationDays, number);
		Response<Void> response = service.sendPhoneNumberVerificationLink(userId, request).execute();

		if (response.isSuccessful()) {
			log.info("Successfully sent phone code-verification link to {}", userId);
		} else {
			log.error("Failed to send phone number verification link to {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Invite new users to use the service.
	 * <p>
	 * Requests TrivoreID to send an invitation e-mail to user(s).
	 * </p>
	 *
	 * @param request The request class with list of emails, subject and text body.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void invite(InviteRequest request) throws IOException, TrivoreIDException {
		Assert.notNull(request, "The request cannot be null");
		Assert.notEmpty(request.getEmails(), "The email list cannot be empty");

		Response<Void> response = service.invite(request).execute();

		if (response.isSuccessful()) {
			log.info("Successfully sent the invitation request");
		} else {
			log.error("Failed to send the invitation request.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Read user's custom field values.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to get user's custom
	 * field values by the user id.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return the object representing custom user fields.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Map<String, Object> getCustomFields(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Map<String, Object>> response = service.getCustomFields(userId).execute();

		if (response.isSuccessful()) {
			Map<String, Object> fields = response.body();
			log.info("Successfully got the custom fields for user with id {}", userId);
			return fields;
		} else {
			log.error("Failed to send the invitation request.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add or modify user's custom field values.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to add or modify
	 * user's custom field values by the user id.
	 * </p>
	 *
	 * @param userId       The id of the target user.
	 * @param customFields User's custom fields.
	 * @return the object with updated custom user fields.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Map<String, Object> updateCustomFields(String userId, Map<String, Object> customFields)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(customFields, "The customFields cannot be null");

		Response<Map<String, Object>> response = service.updateCustomFields(userId, customFields).execute();

		if (response.isSuccessful()) {
			Map<String, Object> fields = response.body();
			log.info("Successfully updated custom fields for the user with id {}.", userId);
			return fields;
		} else {
			log.error("Failed to update custom fields of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete all user's custom fields.
	 * <p>
	 * When this function is invoked, it will request TrivoreID to delete user's
	 * custom fields by the user id.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteCustomFields(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Void> response = service.deleteCustomFields(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted custom fields for the user with id {}.", userId);
		} else {
			log.error("Failed to delete custom fields of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get password validation / complexity requirement information.
	 *
	 * @param userId The id of the target user.
	 * @return the password complexity requirements.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PasswordRequirements getPasswordComplexity(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<PasswordRequirements> response = service.getPasswordComplexity(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the password complexity requirements for user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get custom fields of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get password validation / complexity requirement information.
	 *
	 * @param userId The id of the target user.
	 * @return the password complexity requirements.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Enterprise getEnterprise(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Enterprise> response = service.getEnterprise(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the the enterprice of user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get the enterprice of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save (add/update) the enterprise with the given identifier.
	 *
	 * @param userId     The identifier of the underlying user instance.
	 * @param enterprise The enterprise info to be saved.
	 * @return The same enterprise returned from the TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Enterprise updateEnterprise(String userId, Enterprise enterprise) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The id cannot be null");
		Assert.hasText(userId, "The id cannot be empty");
		Assert.notNull(enterprise, "The enterprise cannot be null");

		Response<Enterprise> response = service.saveEnterprise(userId, enterprise).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updates enterprice of the user with ID {}.", userId);
			return response.body();
		} else {
			log.error("Failed to update the enterprice.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's strong identification info.
	 *
	 * @param userId The id of the target user.
	 * @return the object representing strong identification.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public StrongIdentification getStrongIdentification(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<StrongIdentification> response = service.getStrongIdentification(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the the strong identification of user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get the strong identification of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's strong identification info.
	 *
	 * @param userId The id of the target user.
	 * @return page with strongidentification resources
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Page<StrongIdentification> getStrongIdentificationHistory(String userId)
			throws IOException, TrivoreIDException {
		return getStrongIdentificationHistory(userId, new Criteria(), null, null);
	}

	/**
	 * Get user's strong identification info.
	 *
	 * @param userId    The id of the target user.
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    Sort by attribute name
	 * @param ascending Sort direction ('ascending' or 'descending')
	 * @return page with strongidentification resources
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Page<StrongIdentification> getStrongIdentificationHistory(String userId, Criteria criteria, String sortBy,
			Boolean ascending) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null) {
			sortOrder = ascending ? "ascending" : "descending";
		}

		Response<Page<StrongIdentification>> response = service.getStrongIdentificationHistory(userId,
				criteria.getStartIndex(), criteria.getCount(), filter, sortBy, sortOrder).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the the strong identification of user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get the strong identification of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's strong identification history single entry.
	 *
	 * @param userId    The id of the target user.
	 * @param historyId single strong identification history entry id
	 * @return the object representing strong identification.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public StrongIdentification getStrongIdentificationHistoryEntry(String userId, String historyId)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(historyId, "The historyId cannot be null");
		Assert.hasText(historyId, "The historyId cannot be empty");

		Response<StrongIdentification> response = service.getStrongIdentificationHistoryEntry(userId, historyId)
				.execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the the strong identification of user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get the strong identification of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Report user's strong identification.
	 *
	 * @param userId     The id of the target user.
	 * @param personalId Personal ID code
	 * @param remarks    Additional remarks about identification.
	 * @return the object representing strong identification.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public StrongIdentification reportStrongIdentification(String userId, String personalId, String remarks)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(personalId, "The personalId cannot be null");
		Assert.hasText(personalId, "The personalId cannot be empty");

		Map<String, String> map = new HashMap<>();
		map.put("personalId", personalId);
		map.put("remarks", remarks);

		Response<StrongIdentification> response = service.reportStrongIdentification(userId, map).execute();

		if (response.isSuccessful()) {
			log.info("Successfully Report strong identification for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to Report strong identification for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Change user's password. New password must be valid for current password
	 * requirements, invalid new password request is ignored.
	 *
	 * @param userId      The id of the target user.
	 * @param newPassword new password
	 * @return the object with the password update result.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PasswordUpdateResult changePassword(String userId, String newPassword)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(newPassword, "The newPassword cannot be null");
		Assert.hasText(newPassword, "The newPassword cannot be empty");

		Map<String, String> map = new HashMap<>();
		map.put("newPassword", newPassword);

		Response<PasswordUpdateResult> response = service.changePassword(userId, map).execute();

		if (response.isSuccessful()) {
			PasswordUpdateResult result = response.body();
			if (result.isSuccess()) {
				log.info("Successfully changed the password for the user with id {}.", userId);
			} else {
				log.info("Failed to change the password for the user with id {}.", userId);
			}
			return response.body();
		} else {
			log.error("Failed to change the password for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's custom permissions.
	 *
	 * @param userId The id of the target user.
	 * @return list with user's custom permissions
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> getCustomPermissions(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<List<String>> response = service.getCustomPermissions(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully found custom permissions for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to find custom permissions for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's custom permissions.
	 *
	 * @param userId The id of the target user.
	 * @param add    list of permissions to add
	 * @param remove list of permissions to remove
	 * @return list with user's custom permissions
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> updateCustomPermissions(String userId, List<String> add, List<String> remove)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Map<String, List<String>> map = new HashMap<>();
		map.put("add", add);
		map.put("remove", remove);

		Response<List<String>> response = service.updateCustomPermissions(userId, map).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated custom permissions for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to update custom permissions for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's effective permissions.
	 *
	 * @param userId The id of the target user.
	 * @return list with user's effective permissions
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> getEffectivePermissions(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<List<String>> response = service.getEffectivePermissions(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully found effective permissions for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to find effective permissions for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's built-in roles.
	 *
	 * @param userId The id of the target user.
	 * @return list with user's built-in roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> getBuiltinRoles(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<List<String>> response = service.getBuiltinRoles(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully found builtin roles for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to find builtin roles for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update user's built-in roles.
	 *
	 * @param userId The id of the target user.
	 * @param add    list of roles to add
	 * @param remove list of roles to remove
	 * @return list with updated user's built-in roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> updateBuiltinRoles(String userId, List<String> add, List<String> remove)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Map<String, List<String>> map = new HashMap<>();
		map.put("add", add);
		map.put("remove", remove);

		Response<List<String>> response = service.updateBuiltinRoles(userId, map).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated builtin roles for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to update builtin roles for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get user's custom roles.
	 *
	 * @param userId The id of the target user.
	 * @return list with user's custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> getCustomRoles(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<List<String>> response = service.getCustomRoles(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully found custom roles for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to find custom roles for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update user's custom roles.
	 *
	 * @param userId The id of the target user.
	 * @param add    list of roles to add
	 * @param remove list of roles to remove
	 * @return list with updated user's custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public List<String> updateCustomRoles(String userId, List<String> add, List<String> remove)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Map<String, List<String>> map = new HashMap<>();
		map.put("add", add);
		map.put("remove", remove);

		Response<List<String>> response = service.updateCustomRoles(userId, map).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated custom roles for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to update custom roles for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Read last student state info.
	 *
	 * @param userId The id of the target user
	 * @return list with updated user's custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public StudentStateInfo getStudentInfo(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<StudentStateInfo> response = service.getStudentInfo(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got user's student info for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to get user's student info for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update user's custom roles.
	 *
	 * @param userId    The id of the target user
	 * @param stateInfo student state info to update
	 * @return list with updated user's custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public StudentStateInfo updateStudentInfo(String userId, StudentStateInfo stateInfo)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(stateInfo, "The stateInfo cannot be null");

		Response<StudentStateInfo> response = service.updateStudentInfo(userId, stateInfo).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated user's student info for the user with id {}.", userId);
			return response.body();
		} else {
			log.error("Failed to update user's student info for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Partially (!) migrate user to another namespace.
	 * <p>
	 * Warning: Original user will be deleted and another will be created in the
	 * target namespace.
	 * </p>
	 * <p>
	 * NB! Not all information will be migrated!
	 * </p>
	 *
	 * @param userId  The id of the target user
	 * @param options user namestace migration options
	 * @return list with updated user's custom roles
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public User migrateNamespace(@Path("userId") String userId, @Body UserNamespaceMigrationOptions options)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(options, "The options cannot be null");

		Response<User> response = service.migrateNamespace(userId, options).execute();

		if (response.isSuccessful()) {
			User user = response.body();
			log.info("Successfully migrated user {} to namespace {}.", user.getId(), options.getTargetNsCode());
			return user;
		} else {
			log.error("Failed to update custom roles for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
