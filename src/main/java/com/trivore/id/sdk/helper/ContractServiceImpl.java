package com.trivore.id.sdk.helper;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.contract.AllowedActions;
import com.trivore.id.sdk.models.contract.Appendix;
import com.trivore.id.sdk.models.contract.Contract;
import com.trivore.id.sdk.models.contract.ContractContent;
import com.trivore.id.sdk.models.contract.Party;
import com.trivore.id.sdk.models.contract.PartyContact;
import com.trivore.id.sdk.models.contract.Signer;
import com.trivore.id.sdk.service.ContractService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A contract service to process different kinds of contract specific
 * operations.
 * <p>
 * Handles contract specific tasks and communicates with the TrivoreID service.
 * </p>
 */
public class ContractServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final ContractService service;

	/**
	 * Construct Contract Service helper.
	 *
	 * @param service contract service
	 */
	public ContractServiceImpl(ContractService service) {
		this.service = service;
	}

	/**
	 * Get all contracts from accessible namespaces.
	 *
	 * @return page with contract objects
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Contract> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all contracts satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with contract objects
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Contract> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Contract>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Contract> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} contracts with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find contracts with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new contract into the service.
	 *
	 * @param contract The contract to be added.
	 * @return A contract saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract create(Contract contract) throws TrivoreIDException, IOException {
		Assert.notNull(contract, "The contract cannot be null");

		Response<Contract> response = service.create(contract).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created contract.");
			return response.body();
		} else {
			log.error("Failed to create contract.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @return The contract with the given identifier.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract get(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The id cannot be null");
		Assert.hasText(contractId, "The id cannot be empty");

		Response<Contract> response = service.get(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to get the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided contract.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided contract.
	 * </p>
	 *
	 * @param contract The target contract.
	 * @return A contract saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract update(Contract contract) throws TrivoreIDException, IOException {
		Assert.notNull(contract, "The contract cannot be null");
		Assert.notNull(contract.getId(), "The ID cannot be null");
		Assert.hasText(contract.getId(), "The ID cannot be empty");

		Response<Contract> response = service.update(contract.getId(), contract).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated the contract with id {}.", contract.getId());
			return response.body();
		} else {
			log.error("Failed to update the contract with id {}.", contract.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a contract from TrivoreID.
	 *
	 * @param contractId The id of the target contract.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void delete(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<Void> response = service.delete(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the contract with id {}.", contractId);
		} else {
			log.error("Failed to delete the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}

	}

	/**
	 * Get allowed actions for the target contract.
	 *
	 * @param contractId target contract ID
	 * @return allowed actions
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public AllowedActions getAllowedActions(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<AllowedActions> response = service.getAllowedActions(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got allowed actions from the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to get allowed actions from the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the body content from the target contract.
	 *
	 * @param contractId target contract ID
	 * @return body content
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public ContractContent getContractContent(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<ContractContent> response = service.getContractContent(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got body content from the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to get body content from the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided contract content.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided contract content.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param content    The content of the target contract.
	 * @return A contract saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public ContractContent updateContractContent(String contractId, ContractContent content)
			throws TrivoreIDException, IOException {
		Assert.notNull(content, "The content cannot be null");
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<ContractContent> response = service.updateContractContent(contractId, content).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got body content from the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to get body content from the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Terminate the contract.
	 *
	 * @param contractId target contract ID
	 * @return the target contract object
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract terminate(String contractId) throws TrivoreIDException, IOException {
		return terminate(contractId, null);
	}

	/**
	 * Terminate the contract.
	 *
	 * @param contractId target contract ID
	 * @param reason     reason for termination
	 * @return the target contract object
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract terminate(String contractId, String reason) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<Contract> response = service.terminate(contractId, reason).execute();

		if (response.isSuccessful()) {
			log.info("Successfully terminated contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to terminate contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Finalise the contract.
	 *
	 * @param contractId target contract ID
	 * @return the target contract object
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract finalise(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<Contract> response = service.finalise(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully finalised contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to finalise contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Sign the contract.
	 *
	 * @param contractId target contract ID
	 * @return the target contract object
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Contract sign(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<Contract> response = service.sign(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully signed contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to sign contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all parties from the target contract.
	 *
	 * @param contractId target contract ID
	 * @return list with contract's parties
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<Party> getAllParties(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<List<Party>> response = service.getAllParties(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got all parties from the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to get all parties from the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new party to a contract.
	 *
	 * @param contractId target contract ID
	 * @param party      The party to be created.
	 * @return A party saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Party createParty(String contractId, Party party) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(party, "The party cannot be null");

		Response<Party> response = service.createParty(contractId, party).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created party for the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to created party for the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the party from the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @return The party with the given identifier.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Party getParty(String contractId, String partyId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");

		Response<Party> response = service.getParty(contractId, partyId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the party {} for the contract with id {}.", partyId, contractId);
			return response.body();
		} else {
			log.error("Failed to got the  party {} for the contract with id {}.", partyId, contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided party.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided party.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param party      The party to update.
	 * @return A party saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Party updateParty(String contractId, Party party) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(party, "The party cannot be null");
		Assert.notNull(party.getId(), "The party.id cannot be null");
		Assert.hasText(party.getId(), "The party.id cannot be empty");

		Response<Party> response = service.updateParty(contractId, party.getId(), party).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated the party {} for the contract with id {}.", party.getId(), contractId);
			return response.body();
		} else {
			log.error("Failed to update the  party {} for the contract with id {}.", party.getId(), contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a party from a contract in TrivoreID.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the party to delete.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void deleteParty(String contractId, String partyId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");

		Response<Void> response = service.deleteParty(contractId, partyId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the party {} for the contract with id {}.", partyId, contractId);
		} else {
			log.error("Failed to delete the  party {} for the contract with id {}.", partyId, contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all party signers from the target contract.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @return list with contract's party signers
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<Signer> getAllPartySigners(String contractId, String partyId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");

		Response<List<Signer>> response = service.getAllPartySigners(contractId, partyId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the signers from the party {} of the contract with id {}.", partyId, contractId);
			return response.body();
		} else {
			log.error("Failed to get the signers from the  party {} for the contract with id {}.", partyId, contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new signer to a party.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signer     The signer to be created.
	 * @return A signer saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Signer createPartySigner(String contractId, String partyId, Signer signer)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(signer, "The signer cannot be null");

		Response<Signer> response = service.createPartySigner(contractId, partyId, signer).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created signer for the party {}.", partyId);
			return response.body();
		} else {
			log.error("Failed to create signer for the  party {}.", partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the party from the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signerId   The id of the target signer.
	 * @return The party with the given identifier.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Signer getPartySigner(String contractId, String partyId, String signerId)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(signerId, "The signerId cannot be null");
		Assert.hasText(signerId, "The signerId cannot be empty");

		Response<Signer> response = service.getPartySigner(contractId, partyId, signerId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the signer {} for the party {}.", signerId, partyId);
			return response.body();
		} else {
			log.error("Failed to get the signer {} for the  party {}.", signerId, partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided signer.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided signer.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signer     The signer to be updated.
	 * @return A signer saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Signer updatePartySigner(String contractId, String partyId, Signer signer)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(signer.getId(), "The signer.id cannot be null");
		Assert.hasText(signer.getId(), "The signer.id cannot be empty");
		Assert.notNull(signer, "The signer cannot be null");

		Response<Signer> response = service.updatePartySigner(contractId, partyId, signer.getId(), signer).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated the signer {} for the party {}.", signer.getId(), partyId);
			return response.body();
		} else {
			log.error("Failed to update the signer {} for the  party {}.", signer.getId(), partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a party from a contract in TrivoreID.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param signerId   The id of the target signer.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void deletePartySigner(String contractId, String partyId, String signerId)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(signerId, "The signerId cannot be null");
		Assert.hasText(signerId, "The signerId cannot be empty");

		Response<Void> response = service.deletePartySigner(contractId, partyId, signerId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the signer {} for the party {}.", signerId, partyId);
		} else {
			log.error("Failed to delete the signer {} for the  party {}.", signerId, partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all parties from the target contract.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @return list with contract's parties
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<PartyContact> getAllPartyContacts(String contractId, String partyId)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");

		Response<List<PartyContact>> response = service.getAllPartyContacts(contractId, partyId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the contacts from the party {} of the contract with id {}.", partyId,
					contractId);
			return response.body();
		} else {
			log.error("Failed to get the contacts from the  party {} for the contract with id {}.", partyId,
					contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new contact to a party.
	 *
	 * @param contractId   The id of the target contract.
	 * @param partyId      The id of the target party.
	 * @param partyContact The contact to be created.
	 * @return A contact saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public PartyContact createPartyContact(String contractId, String partyId, PartyContact partyContact)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(partyContact, "The contact cannot be null");

		Response<PartyContact> response = service.createPartyContact(contractId, partyId, partyContact).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created contact for the party {}.", partyId);
			return response.body();
		} else {
			log.error("Failed to create contact for the  party {}.", partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the party from the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param contactId  The id of the target signer.
	 * @return The party with the given identifier.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public PartyContact getPartyContact(String contractId, String partyId, String contactId)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(contactId, "The contactId cannot be null");
		Assert.hasText(contactId, "The contactId cannot be empty");

		Response<PartyContact> response = service.getPartyContact(contractId, partyId, contactId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the contact {} for the party {}.", contactId, partyId);
			return response.body();
		} else {
			log.error("Failed to get the contact {} for the  party {}.", contactId, partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided contact.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided contact.
	 * </p>
	 *
	 * @param contractId   The id of the target contract.
	 * @param partyId      The id of the target party.
	 * @param partyContact The contact to be updated.
	 * @return A signer saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public PartyContact updatePartyContact(String contractId, String partyId, PartyContact partyContact)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(partyContact.getId(), "The contact.id cannot be null");
		Assert.hasText(partyContact.getId(), "The contact.id cannot be empty");
		Assert.notNull(partyContact, "The contact cannot be null");

		Response<PartyContact> response = service
				.updatePartyContact(contractId, partyId, partyContact.getId(), partyContact).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated the contact {} for the party {}.", partyContact.getId(), partyId);
			return response.body();
		} else {
			log.error("Failed to update the contact {} for the  party {}.", partyContact.getId(), partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a contact from a party in TrivoreID.
	 *
	 * @param contractId The id of the target contract.
	 * @param partyId    The id of the target party.
	 * @param contactId  The id of the target contact.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void deletePartyContact(String contractId, String partyId, String contactId)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(partyId, "The partyId cannot be null");
		Assert.hasText(partyId, "The partyId cannot be empty");
		Assert.notNull(contactId, "The contactId cannot be null");
		Assert.hasText(contactId, "The contactId cannot be empty");

		Response<Void> response = service.deletePartyContact(contractId, partyId, contactId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the contact {} for the party {}.", contactId, partyId);
		} else {
			log.error("Failed to delete the contact {} for the  party {}.", contactId, partyId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all appendices from the target contract.
	 *
	 * @param contractId target contract ID
	 * @return list with contract's parties
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<Appendix> getAllAppendices(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<List<Appendix>> response = service.getAllAppendices(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got all appendices from the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to get all appendices from the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new appendix to a contract.
	 *
	 * @param contractId target contract ID
	 * @param appendix   appendix to be created.
	 * @return An appendix saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Appendix createAppendix(String contractId, Appendix appendix) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendix, "The appendix cannot be null");

		Response<Appendix> response = service.createAppendix(contractId, appendix).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created appendix for the contract with id {}.", contractId);
			return response.body();
		} else {
			log.error("Failed to created appendix for the contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the appendix from the contract with the given identifier.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @return The appendix with the given identifier.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Appendix getAppendix(String contractId, String appendixId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendixId, "The appendixId cannot be null");
		Assert.hasText(appendixId, "The appendixId cannot be empty");

		Response<Appendix> response = service.getAppendix(contractId, appendixId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the appendix {} for the contract with id {}.", appendixId, contractId);
			return response.body();
		} else {
			log.error("Failed to got the appendix {} for the contract with id {}.", appendixId, contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided appendix.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided appendix.
	 * </p>
	 *
	 * @param contractId The id of the target contract.
	 * @param appendix   The appendix to update.
	 * @return An appendix saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Appendix updateAppendix(String contractId, Appendix appendix) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendix, "The appendix cannot be null");
		Assert.notNull(appendix.getId(), "The appendix.id cannot be null");
		Assert.hasText(appendix.getId(), "The appendix.id cannot be empty");

		Response<Appendix> response = service.updateAppendix(contractId, appendix.getId(), appendix).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated the appendix {} for the contract with id {}.", appendix.getId(), contractId);
			return response.body();
		} else {
			log.error("Failed to update the appendix {} for the contract with id {}.", appendix.getId(), contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete appendix from a contract in TrivoreID.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the appendix to delete.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void deleteAppendix(String contractId, String appendixId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendixId, "The appendixId cannot be null");
		Assert.hasText(appendixId, "The appendixId cannot be empty");

		Response<Void> response = service.deleteAppendix(contractId, appendixId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the appendix {} for the contract with id {}.", appendixId, contractId);
		} else {
			log.error("Failed to delete the appendix {} for the contract with id {}.", appendixId, contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Change the order of the contract appendices.
	 *
	 * @param contractId The id of the target contract.
	 * @param order      the list with ordered appendix identifiers.
	 * @return List of ordered appendices
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public List<Appendix> changeAppendixOrder(String contractId, List<String> order)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(order, "The order cannot be null");

		Response<List<Appendix>> response = service.changeAppendixOrder(contractId, order).execute();

		if (response.isSuccessful()) {
			log.info("Successfully modified the order of appendices for the contract with id {}", contractId);
			return response.body();
		} else {
			log.error("Failed to modify the order of appendices for contract with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the file from the contract's body.
	 *
	 * @param contractId The id of the target contract.
	 * @return a contract's body file
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public byte[] getContractBodyFile(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<byte[]> response = service.getContractBodyFile(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the file from the contact with id {}", contractId);
			return response.body();
		} else {
			log.error("Failed to get a file from the contact with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Upload a file to the contract body.
	 *
	 * @param contractId The id of the target contract.
	 * @param file       file to upload
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void uploadContractBodyFile(String contractId, File file) throws TrivoreIDException, IOException {
		Assert.notNull(file, "The contract cannot be null");

		byte[] content = Files.readAllBytes(file.toPath());
		uploadContractBodyFile(contractId, content);
	}

	/**
	 * Upload a file to the contract body.
	 *
	 * @param contractId The id of the target contract.
	 * @param file       file to upload
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void uploadContractBodyFile(String contractId, byte[] file) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(file, "The contract cannot be null");

		Response<Void> response = service.uploadContractBodyFile(contractId, file).execute();

		if (response.isSuccessful()) {
			log.info("Successfully uploaded the file for the contact with id {}", contractId);
		} else {
			log.error("Failed to upload the file for the contact with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete file from a contract body.
	 *
	 * @param contractId The id of the target contract.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void deleteContractBodyFile(String contractId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");

		Response<Void> response = service.deleteContractBodyFile(contractId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the file for the contact with id {}", contractId);
		} else {
			log.error("Failed to delete a file for the contact with id {}.", contractId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the file from the contract's body.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @return an appendix file
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public byte[] getAppendixFile(String contractId, String appendixId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendixId, "The appendixId cannot be null");
		Assert.hasText(appendixId, "The appendixId cannot be empty");

		Response<byte[]> response = service.getAppendixFile(contractId, appendixId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the file for the appendix with id {}", appendixId);
			return response.body();
		} else {
			log.error("Failed to get a file for the appendix with id {}.", appendixId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Upload a file to a contract's appendix.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @param file       file to upload
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void uploadAppendixFile(String contractId, String appendixId, File file)
			throws TrivoreIDException, IOException {
		Assert.notNull(file, "The contract cannot be null");

		byte[] content = Files.readAllBytes(file.toPath());
		uploadAppendixFile(contractId, appendixId, content);
	}

	/**
	 * Upload a file to a contract's appendix.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @param file       file to upload
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void uploadAppendixFile(String contractId, String appendixId, byte[] file)
			throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendixId, "The appendixId cannot be null");
		Assert.hasText(appendixId, "The appendixId cannot be empty");
		Assert.notNull(file, "The contract cannot be null");

		Response<Void> response = service.uploadAppendixFile(contractId, appendixId, file).execute();

		if (response.isSuccessful()) {
			log.info("Successfully uploaded the file for the appendix with id {}", appendixId);
		} else {
			log.error("Failed to upload a file for the appendix with id {}.", appendixId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete file from a contract's appendix.
	 *
	 * @param contractId The id of the target contract.
	 * @param appendixId The id of the target appendix.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void deleteAppendixFile(String contractId, String appendixId) throws TrivoreIDException, IOException {
		Assert.notNull(contractId, "The contractId cannot be null");
		Assert.hasText(contractId, "The contractId cannot be empty");
		Assert.notNull(appendixId, "The appendixId cannot be null");
		Assert.hasText(appendixId, "The appendixId cannot be empty");

		Response<Void> response = service.deleteAppendixFile(contractId, appendixId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully uploaded the file for the appendix with id {}", appendixId);
		} else {
			log.error("Failed to upload a file for the appendix with id {}.", appendixId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
