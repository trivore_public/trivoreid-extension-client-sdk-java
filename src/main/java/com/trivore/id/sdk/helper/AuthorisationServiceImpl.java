package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.authorisation.Authorisation;
import com.trivore.id.sdk.models.authorisation.AuthorisationGrantRight;
import com.trivore.id.sdk.models.authorisation.AuthorisationType;
import com.trivore.id.sdk.service.AuthorisationService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * An authorisation service to process different kinds of authorisation specific
 * operations.
 * <p>
 * Handles authorisation specific tasks and communicates with the TrivoreID
 * service.
 * </p>
 */
public class AuthorisationServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final AuthorisationService service;

	/**
	 * Construct Authorisation Service helper.
	 *
	 * @param service authorisation service
	 */
	public AuthorisationServiceImpl(AuthorisationService service) {
		this.service = service;
	}

	/**
	 * Get all authorisations satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with authorisation objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Authorisation> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all authorisations satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with authorisation objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Authorisation> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Authorisation>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Authorisation> page = response.body();
			log.info("Successfully found {} authorisations with criteria {}", page.getTotalResults(), criteria);
			return page;
		} else {
			log.error("Failed find authorisations with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new authorisation into the service.
	 *
	 * @param authorisation The authorisation to be added.
	 * @return A authorisation saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Authorisation create(Authorisation authorisation) throws IOException, TrivoreIDException {
		Assert.notNull(authorisation, "The authorisation cannot be null");

		Response<Authorisation> response = service.create(authorisation).execute();

		if (response.isSuccessful()) {
			Authorisation newAuthorisation = response.body();
			log.info("Successfully created authorisation with id {}", newAuthorisation.getId());
			return newAuthorisation;
		} else {
			log.error("Failed to create a authorisation.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the authorisation with the given identifier.
	 *
	 * @param authId The id of the target authorisation.
	 * @return The authorisation with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Authorisation get(String authId) throws IOException, TrivoreIDException {
		Assert.notNull(authId, "The id cannot be null");
		Assert.hasText(authId, "The id cannot be empty");

		Response<Authorisation> response = service.get(authId).execute();

		if (response.isSuccessful()) {
			Authorisation authorisation = response.body();
			log.info("Successfully got the authorisation with id {}", authId);
			return authorisation;
		} else {
			log.error("Failed to get the authorisation with id {}", authId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided authorisation.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided authorisation.
	 * </p>
	 *
	 * @param authorisation The target authorisation.
	 * @return A authorisation saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Authorisation update(Authorisation authorisation) throws IOException, TrivoreIDException {
		Assert.notNull(authorisation, "The authorisation cannot be null");
		Assert.notNull(authorisation.getId(), "The ID cannot be null");
		Assert.hasText(authorisation.getId(), "The ID cannot be empty");

		Response<Authorisation> response = service.update(authorisation.getId(), authorisation).execute();

		if (response.isSuccessful()) {
			Authorisation updatedAuthorisation = response.body();
			log.info("Successfully modified the authorisation with ID {}.", updatedAuthorisation.getId());
			return updatedAuthorisation;
		} else {
			log.error("Failed to get the authorisation profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Revoke an authorisation in TrivoreID.
	 *
	 * @param authId The id of the target authorisation.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void revoke(String authId) throws IOException, TrivoreIDException {
		Assert.notNull(authId, "The authId cannot be null");
		Assert.hasText(authId, "The authId cannot be empty");

		Response<Void> response = service.revoke(authId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully revokes authorisation with id {}.", authId);
		} else {
			log.error("Failed to revoke authorisation with id {}.", authId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a authorisation from TrivoreID.
	 *
	 * @param authId The id of the target authorisation.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String authId) throws IOException, TrivoreIDException {
		Assert.notNull(authId, "The authId cannot be null");
		Assert.hasText(authId, "The authId cannot be empty");

		Response<Void> response = service.delete(authId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted authorisation with id {}.", authId);
		} else {
			log.error("Failed to delete authorisation with id {}.", authId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all authorisation types satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with authorisation type objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AuthorisationType> getAllAuthorisationTypes() throws TrivoreIDException, IOException {
		return getAllAuthorisationTypes(new Criteria());
	}

	/**
	 * Get all authorisation types satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with authorisation type objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AuthorisationType> getAllAuthorisationTypes(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<AuthorisationType>> response = service
				.getAllAuthorisationTypes(criteria.getStartIndex(), criteria.getCount(), filter).execute();
		if (response.isSuccessful()) {
			Page<AuthorisationType> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} authorisation types with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find authorisation types with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new authorisation type into the service.
	 *
	 * @param type The authorisation type to be added.
	 * @return A authorisation type saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationType createAuthorisationType(AuthorisationType type) throws IOException, TrivoreIDException {
		Assert.notNull(type, "The authorisation type cannot be null");

		Response<AuthorisationType> response = service.createAuthorisationType(type).execute();

		if (response.isSuccessful()) {
			AuthorisationType newAuthorisationType = response.body();
			log.info("Successfully created authorisation type with id {}", newAuthorisationType.getId());
			return newAuthorisationType;
		} else {
			log.error("Failed to create a authorisation type.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the authorisation type with the given identifier.
	 *
	 * @param typeId The id of the target authorisation type.
	 * @return The authorisation type with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationType getAuthorisationType(String typeId) throws IOException, TrivoreIDException {
		Assert.notNull(typeId, "The id cannot be null");
		Assert.hasText(typeId, "The id cannot be empty");

		Response<AuthorisationType> response = service.getAuthorisationType(typeId).execute();

		if (response.isSuccessful()) {
			AuthorisationType type = response.body();
			log.info("Successfully got the authorisation type with id {}", typeId);
			return type;
		} else {
			log.error("Failed to get the authorisation type with id {}", typeId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided authorisation type.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided authorisation type.
	 * </p>
	 *
	 * @param type The target authorisation type.
	 * @return A authorisation type saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationType updateAuthorisationType(AuthorisationType type) throws IOException, TrivoreIDException {
		Assert.notNull(type, "The authorisation type cannot be null");
		Assert.notNull(type.getId(), "The ID cannot be null");
		Assert.hasText(type.getId(), "The ID cannot be empty");

		Response<AuthorisationType> response = service.updateAuthorisationType(type.getId(), type).execute();

		if (response.isSuccessful()) {
			AuthorisationType updatedAuthorisationType = response.body();
			log.info("Successfully modified the authorisation type with ID {}.", updatedAuthorisationType.getId());
			return updatedAuthorisationType;
		} else {
			log.error("Failed to get the authorisation type profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a authorisation type from TrivoreID.
	 *
	 * @param typeId The id of the target authorisation type.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteAuthorisationType(String typeId) throws IOException, TrivoreIDException {
		Assert.notNull(typeId, "The typeId cannot be null");
		Assert.hasText(typeId, "The typeId cannot be empty");

		Response<Void> response = service.deleteAuthorisationType(typeId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted authorisation type with id {}.", typeId);
		} else {
			log.error("Failed to delete authorisation type with id {}.", typeId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all sources satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with source objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AuthorisationType> getAllAuthorisationSources() throws TrivoreIDException, IOException {
		return getAllAuthorisationSources(new Criteria());
	}

	/**
	 * Get all sources satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with source objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AuthorisationType> getAllAuthorisationSources(Criteria criteria)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<AuthorisationType>> response = service
				.getAllAuthorisationSources(criteria.getStartIndex(), criteria.getCount(), filter).execute();
		if (response.isSuccessful()) {
			Page<AuthorisationType> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} sources with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find sources with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new source into the service.
	 * <p>
	 * If the source code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param source The source to be added.
	 * @return A source saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationType createAuthorisationSource(AuthorisationType source)
			throws IOException, TrivoreIDException {
		Assert.notNull(source, "The source cannot be null");

		Response<AuthorisationType> response = service.createAuthorisationSource(source).execute();

		if (response.isSuccessful()) {
			AuthorisationType newAuthorisationType = response.body();
			log.info("Successfully created source with id {}", newAuthorisationType.getId());
			return newAuthorisationType;
		} else {
			log.error("Failed to create a source.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the source with the given identifier.
	 *
	 * @param sourceId The id of the target source.
	 * @return The source with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationType getAuthorisationSource(String sourceId) throws IOException, TrivoreIDException {
		Assert.notNull(sourceId, "The id cannot be null");
		Assert.hasText(sourceId, "The id cannot be empty");

		Response<AuthorisationType> response = service.getAuthorisationSource(sourceId).execute();

		if (response.isSuccessful()) {
			AuthorisationType source = response.body();
			log.info("Successfully got the source with id {}", sourceId);
			return source;
		} else {
			log.error("Failed to get the source with id {}", sourceId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided source.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided source.
	 * </p>
	 *
	 * @param source   The target source.
	 * @return A source saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationType updateAuthorisationSource(AuthorisationType source)
			throws IOException, TrivoreIDException {
		Assert.notNull(source, "The source cannot be null");
		Assert.notNull(source.getId(), "The ID cannot be null");
		Assert.hasText(source.getId(), "The ID cannot be empty");

		Response<AuthorisationType> response = service.updateAuthorisationSource(source.getId(), source).execute();

		if (response.isSuccessful()) {
			AuthorisationType updatedAuthorisationType = response.body();
			log.info("Successfully modified the source with ID {}.", updatedAuthorisationType.getId());
			return updatedAuthorisationType;
		} else {
			log.error("Failed to get the source profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a source from TrivoreID.
	 *
	 * @param sourceId The id of the target source.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteAuthorisationSource(String sourceId) throws IOException, TrivoreIDException {
		Assert.notNull(sourceId, "The sourceId cannot be null");
		Assert.hasText(sourceId, "The sourceId cannot be empty");

		Response<Void> response = service.deleteAuthorisationSource(sourceId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted source with id {}.", sourceId);
		} else {
			log.error("Failed to delete source with id {}.", sourceId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all rights satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with right objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AuthorisationGrantRight> getAllAuthorisationGrantRights() throws TrivoreIDException, IOException {
		return getAllAuthorisationGrantRights(new Criteria());
	}

	/**
	 * Get all rights satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with right objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AuthorisationGrantRight> getAllAuthorisationGrantRights(Criteria criteria)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<AuthorisationGrantRight>> response = service
				.getAllAuthorisationGrantRights(criteria.getStartIndex(), criteria.getCount(), filter).execute();
		if (response.isSuccessful()) {
			Page<AuthorisationGrantRight> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} rights with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find rights with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new right into the service.
	 * <p>
	 * If the right code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param right The right to be added.
	 * @return A right saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationGrantRight createAuthorisationGrantRight(AuthorisationGrantRight right)
			throws IOException, TrivoreIDException {
		Assert.notNull(right, "The right cannot be null");

		Response<AuthorisationGrantRight> response = service.createAuthorisationGrantRight(right).execute();

		if (response.isSuccessful()) {
			AuthorisationGrantRight newAuthorisationGrantRight = response.body();
			log.info("Successfully created right with id {}", newAuthorisationGrantRight.getId());
			return newAuthorisationGrantRight;
		} else {
			log.error("Failed to create a right.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the right with the given identifier.
	 *
	 * @param rightId The id of the target right.
	 * @return The right with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationGrantRight getAuthorisationGrantRight(String rightId) throws IOException, TrivoreIDException {
		Assert.notNull(rightId, "The id cannot be null");
		Assert.hasText(rightId, "The id cannot be empty");

		Response<AuthorisationGrantRight> response = service.getAuthorisationGrantRight(rightId).execute();

		if (response.isSuccessful()) {
			AuthorisationGrantRight right = response.body();
			log.info("Successfully got the right with id {}", rightId);
			return right;
		} else {
			log.error("Failed to get the right with id {}", rightId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided right.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided right.
	 * </p>
	 *
	 * @param right   The target right.
	 * @return A right saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AuthorisationGrantRight updateAuthorisationGrantRight(AuthorisationGrantRight right)
			throws IOException, TrivoreIDException {
		Assert.notNull(right, "The right cannot be null");
		Assert.notNull(right.getId(), "The ID cannot be null");
		Assert.hasText(right.getId(), "The ID cannot be empty");

		Response<AuthorisationGrantRight> response = service.updateAuthorisationGrantRight(right.getId(), right)
				.execute();

		if (response.isSuccessful()) {
			AuthorisationGrantRight updatedAuthorisationGrantRight = response.body();
			log.info("Successfully modified the right with ID {}.", updatedAuthorisationGrantRight.getId());
			return updatedAuthorisationGrantRight;
		} else {
			log.error("Failed to get the right profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Revoke an authorisation grant right in TrivoreID.
	 *
	 * @param rightId The id of the target authorisation grant right.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void revokeGrantRight(String rightId) throws IOException, TrivoreIDException {
		Assert.notNull(rightId, "The authId cannot be null");
		Assert.hasText(rightId, "The authId cannot be empty");

		Response<Void> response = service.revokeGrantRight(rightId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully revoked authorisation grant right with id {}.", rightId);
		} else {
			log.error("Failed to revoke authorisation grant right with id {}.", rightId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a right from TrivoreID.
	 *
	 * @param rightId The id of the target right.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteAuthorisationGrantRight(String rightId) throws IOException, TrivoreIDException {
		Assert.notNull(rightId, "The rightId cannot be null");
		Assert.hasText(rightId, "The rightId cannot be empty");

		Response<Void> response = service.deleteAuthorisationGrantRight(rightId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted right with id {}.", rightId);
		} else {
			log.error("Failed to delete right with id {}.", rightId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
