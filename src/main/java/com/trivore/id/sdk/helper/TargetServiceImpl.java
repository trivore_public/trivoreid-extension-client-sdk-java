package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.Target;
import com.trivore.id.sdk.service.TargetService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * An target service to process different kinds of target specific operations.
 * <p>
 * Handles target specific tasks and communicates with the TrivoreID service.
 * </p>
 */
public class TargetServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final TargetService service;

	/**
	 * Construct Target Service helper.
	 *
	 * @param service target service
	 */
	public TargetServiceImpl(TargetService service) {
		this.service = service;
	}

	/**
	 * Get all targets satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with target objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Target> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all targets satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with target objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Target> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Target>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Target> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} targets with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find targets with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new target into the service.
	 *
	 * @param target The target to be added.
	 * @return A target saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Target create(Target target) throws IOException, TrivoreIDException {
		Assert.notNull(target, "The target cannot be null");

		Response<Target> response = service.create(target).execute();

		if (response.isSuccessful()) {
			Target newTarget = response.body();
			log.info("Successfully created target with id {}", newTarget.getId());
			return newTarget;
		} else {
			log.error("Failed to create a target.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the target with the given identifier.
	 *
	 * @param targetId The id of the target target.
	 * @return The target with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Target get(String targetId) throws IOException, TrivoreIDException {
		Assert.notNull(targetId, "The id cannot be null");
		Assert.hasText(targetId, "The id cannot be empty");

		Response<Target> response = service.get(targetId).execute();

		if (response.isSuccessful()) {
			Target target = response.body();
			log.info("Successfully got the target with id {}", targetId);
			return target;
		} else {
			log.error("Failed to get the target with id {}", targetId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided target.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided target.
	 * </p>
	 *
	 * @param target   The target target.
	 * @return A target saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Target update(Target target) throws IOException, TrivoreIDException {
		Assert.notNull(target, "The target cannot be null");
		Assert.notNull(target.getId(), "The ID cannot be null");
		Assert.hasText(target.getId(), "The ID cannot be empty");

		Response<Target> response = service.update(target.getId(), target).execute();

		if (response.isSuccessful()) {
			Target updatedTarget = response.body();
			log.info("Successfully modified the target with ID {}.", updatedTarget.getId());
			return updatedTarget;
		} else {
			log.error("Failed to get the target profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a target from TrivoreID.
	 *
	 * @param targetId The id of the target target.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String targetId) throws IOException, TrivoreIDException {
		Assert.notNull(targetId, "The targetId cannot be null");
		Assert.hasText(targetId, "The targetId cannot be empty");

		Response<Void> response = service.delete(targetId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted target with id {}.", targetId);
		} else {
			log.error("Failed to delete target with id {}.", targetId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
