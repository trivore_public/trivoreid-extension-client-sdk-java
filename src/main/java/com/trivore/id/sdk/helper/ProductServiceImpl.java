package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.products.AllCatalogs;
import com.trivore.id.sdk.models.products.Catalog;
import com.trivore.id.sdk.models.products.CatalogDetails;
import com.trivore.id.sdk.models.products.PricingPlan;
import com.trivore.id.sdk.models.products.Product;
import com.trivore.id.sdk.models.products.ProductDetails;
import com.trivore.id.sdk.service.ProductService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * Product Service helper to perform product and sales related requests to
 * TrivoreID.
 */
public class ProductServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final ProductService service;

	/**
	 * Construct Product Service helper.
	 *
	 * @param service product service
	 */
	public ProductServiceImpl(ProductService service) {
		this.service = service;
	}

	/**
	 * Get all catalogs satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with catalog objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Catalog> getAllCatalogs() throws TrivoreIDException, IOException {
		return getAllCatalogs(new Criteria());
	}

	/**
	 * Get all catalogs satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with catalog objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Catalog> getAllCatalogs(Criteria criteria) throws TrivoreIDException, IOException {
		return getAllCatalogs(criteria, null, null);
	}

	/**
	 * Get all catalogs satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    sort by attribute name
	 * @param ascending sort direction (ascending or descending)
	 * @return page with catalog objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Catalog> getAllCatalogs(Criteria criteria, String sortBy, Boolean ascending)
			throws TrivoreIDException, IOException {
		return getAllCatalogs(criteria, sortBy, ascending, false);
	}

	/**
	 * Get all catalogs satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria            the query criteria for the operation.
	 * @param sortBy              sort by attribute name
	 * @param ascending           sort direction (ascending or descending)
	 * @param mergeProductDetails copy missing values from original products to
	 *                            catalog items
	 * @return page with catalog objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Catalog> getAllCatalogs(Criteria criteria, String sortBy, Boolean ascending,
			boolean mergeProductDetails) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null && ascending) {
			sortOrder = "ascending";
		} else {
			sortOrder = "descending";
		}

		Response<Page<Catalog>> response = service.getAllCatalogs( //
				criteria.getStartIndex(), //
				criteria.getCount(), //
				filter, //
				sortBy, //
				sortOrder, //
				mergeProductDetails).execute();
		if (response.isSuccessful()) {
			Page<Catalog> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} catalogs with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find catalogs with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new catalog into the service.
	 *
	 * @param catalog The catalog to be added.
	 * @return A catalog saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Catalog createCatalog(Catalog catalog) throws IOException, TrivoreIDException {
		Assert.notNull(catalog, "The catalog cannot be null");

		Response<Catalog> response = service.createCatalog(catalog).execute();

		if (response.isSuccessful()) {
			Catalog newCatalog = response.body();
			log.info("Successfully created catalog with id {}", newCatalog.getId());
			return newCatalog;
		} else {
			log.error("Failed to create a catalog.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the catalog with the given identifier.
	 *
	 * @param catalogId The id of the target catalog.
	 * @return The catalog with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Catalog getCatalog(String catalogId) throws IOException, TrivoreIDException {
		Assert.notNull(catalogId, "The id cannot be null");
		Assert.hasText(catalogId, "The id cannot be empty");

		Response<Catalog> response = service.getCatalog(catalogId).execute();

		if (response.isSuccessful()) {
			Catalog catalog = response.body();
			log.info("Successfully got the catalog with id {}", catalogId);
			return catalog;
		} else {
			log.error("Failed to get the catalog with id {}", catalogId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided catalog.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided catalog.
	 * </p>
	 *
	 * @param catalog The target catalog.
	 * @return A catalog saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Catalog updateCatalog(Catalog catalog) throws IOException, TrivoreIDException {
		Assert.notNull(catalog, "The catalog cannot be null");
		Assert.notNull(catalog.getId(), "The ID cannot be null");
		Assert.hasText(catalog.getId(), "The ID cannot be empty");

		Response<Catalog> response = service.updateCatalog(catalog.getId(), catalog).execute();

		if (response.isSuccessful()) {
			Catalog updatedCatalog = response.body();
			log.info("Successfully modified the catalog with ID {}.", updatedCatalog.getId());
			return updatedCatalog;
		} else {
			log.error("Failed to update the catalog with id {}.", catalog.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a catalog from TrivoreID.
	 *
	 * @param catalogId The id of the target catalog.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteCatalog(String catalogId) throws IOException, TrivoreIDException {
		Assert.notNull(catalogId, "The catalogId cannot be null");
		Assert.hasText(catalogId, "The catalogId cannot be empty");

		Response<Void> response = service.deleteCatalog(catalogId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted catalog with id {}.", catalogId);
		} else {
			log.error("Failed to delete catalog with id {}.", catalogId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all pricing plan satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with pricing plan objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<PricingPlan> getAllPricingPlans() throws TrivoreIDException, IOException {
		return getAllPricingPlans(new Criteria());
	}

	/**
	 * Get all pricing plan satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with pricing plan objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<PricingPlan> getAllPricingPlans(Criteria criteria) throws TrivoreIDException, IOException {
		return getAllPricingPlans(criteria, null, null);
	}

	/**
	 * Get all pricing plan satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    sort by attribute name
	 * @param ascending sort direction (ascending or descending)
	 * @return page with pricing plan objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<PricingPlan> getAllPricingPlans(Criteria criteria, String sortBy, Boolean ascending)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null && ascending) {
			sortOrder = "ascending";
		} else {
			sortOrder = "descending";
		}

		Response<Page<PricingPlan>> response = service.getAllPricingPlans( //
				criteria.getStartIndex(), //
				criteria.getCount(), //
				filter, //
				sortBy, //
				sortOrder).execute();
		if (response.isSuccessful()) {
			Page<PricingPlan> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} pricingPlans with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find pricingPlans with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new pricing plan into the service.
	 *
	 * @param pricingPlan The pricing plan to be added.
	 * @return A pricing plan saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PricingPlan createPricingPlan(PricingPlan pricingPlan) throws IOException, TrivoreIDException {
		Assert.notNull(pricingPlan, "The pricingPlan cannot be null");

		Response<PricingPlan> response = service.createPricingPlan(pricingPlan).execute();

		if (response.isSuccessful()) {
			PricingPlan newPricingPlan = response.body();
			log.info("Successfully created pricingPlan with id {}", newPricingPlan.getId());
			return newPricingPlan;
		} else {
			log.error("Failed to create a pricingPlan.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the pricing plan with the given identifier.
	 *
	 * @param pricingPlanId The id of the target pricing plan.
	 * @return The pricing plan with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PricingPlan getPricingPlan(String pricingPlanId) throws IOException, TrivoreIDException {
		Assert.notNull(pricingPlanId, "The id cannot be null");
		Assert.hasText(pricingPlanId, "The id cannot be empty");

		Response<PricingPlan> response = service.getPricingPlan(pricingPlanId).execute();

		if (response.isSuccessful()) {
			PricingPlan pricingPlan = response.body();
			log.info("Successfully got the pricingPlan with id {}", pricingPlanId);
			return pricingPlan;
		} else {
			log.error("Failed to get the pricingPlan with id {}", pricingPlanId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided pricing plan.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided pricing plan.
	 * </p>
	 *
	 * @param pricingPlan The target pricing plan.
	 * @return A pricingPlan saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PricingPlan updatePricingPlan(PricingPlan pricingPlan) throws IOException, TrivoreIDException {
		Assert.notNull(pricingPlan, "The pricingPlan cannot be null");
		Assert.notNull(pricingPlan.getId(), "The ID cannot be null");
		Assert.hasText(pricingPlan.getId(), "The ID cannot be empty");

		Response<PricingPlan> response = service.updatePricingPlan(pricingPlan.getId(), pricingPlan).execute();

		if (response.isSuccessful()) {
			PricingPlan updatedPricingPlan = response.body();
			log.info("Successfully modified the pricingPlan with ID {}.", updatedPricingPlan.getId());
			return updatedPricingPlan;
		} else {
			log.error("Failed to update the pricingPlan with id {}.", pricingPlan.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a pricing plan from TrivoreID.
	 *
	 * @param pricingPlanId The id of the target pricing plan.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deletePricingPlan(String pricingPlanId) throws IOException, TrivoreIDException {
		Assert.notNull(pricingPlanId, "The pricingPlanId cannot be null");
		Assert.hasText(pricingPlanId, "The pricingPlanId cannot be empty");

		Response<Void> response = service.deletePricingPlan(pricingPlanId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted pricingPlan with id {}.", pricingPlanId);
		} else {
			log.error("Failed to delete pricingPlan with id {}.", pricingPlanId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all products satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with product objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Product> getAllProducts() throws TrivoreIDException, IOException {
		return getAllProducts(new Criteria());
	}

	/**
	 * Get all products satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with product objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Product> getAllProducts(Criteria criteria) throws TrivoreIDException, IOException {
		return getAllProducts(criteria, null, null);
	}

	/**
	 * Get all pricing plan satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    sort by attribute name
	 * @param ascending sort direction (ascending or descending)
	 * @return page with pricing plan objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Product> getAllProducts(Criteria criteria, String sortBy, Boolean ascending)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null && ascending) {
			sortOrder = "ascending";
		} else {
			sortOrder = "descending";
		}

		Response<Page<Product>> response = service.getAllProducts( //
				criteria.getStartIndex(), //
				criteria.getCount(), //
				filter, //
				sortBy, //
				sortOrder).execute();
		if (response.isSuccessful()) {
			Page<Product> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} products with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find products with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new product into the service.
	 *
	 * @param product The product to be added.
	 * @return A product saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Product createProduct(Product product) throws IOException, TrivoreIDException {
		Assert.notNull(product, "The product cannot be null");

		Response<Product> response = service.createProduct(product).execute();

		if (response.isSuccessful()) {
			Product newProduct = response.body();
			log.info("Successfully created product with id {}", newProduct.getId());
			return newProduct;
		} else {
			log.error("Failed to create a product.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the product with the given identifier.
	 *
	 * @param productId The id of the target product.
	 * @return The product with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Product getProduct(String productId) throws IOException, TrivoreIDException {
		Assert.notNull(productId, "The id cannot be null");
		Assert.hasText(productId, "The id cannot be empty");

		Response<Product> response = service.getProduct(productId).execute();

		if (response.isSuccessful()) {
			Product product = response.body();
			log.info("Successfully got the product with id {}", productId);
			return product;
		} else {
			log.error("Failed to get the product with id {}", productId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided product.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided product.
	 * </p>
	 *
	 * @param product The target product.
	 * @return A product saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Product updateProduct(Product product) throws IOException, TrivoreIDException {
		Assert.notNull(product, "The product cannot be null");
		Assert.notNull(product.getId(), "The ID cannot be null");
		Assert.hasText(product.getId(), "The ID cannot be empty");

		Response<Product> response = service.updateProduct(product.getId(), product).execute();

		if (response.isSuccessful()) {
			Product updatedProduct = response.body();
			log.info("Successfully modified the product with ID {}.", updatedProduct.getId());
			return updatedProduct;
		} else {
			log.error("Failed to update the product with id {}.", product.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a product from TrivoreID.
	 *
	 * @param productId The id of the target product.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteProduct(String productId) throws IOException, TrivoreIDException {
		Assert.notNull(productId, "The productId cannot be null");
		Assert.hasText(productId, "The productId cannot be empty");

		Response<Void> response = service.deleteProduct(productId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted product with id {}.", productId);
		} else {
			log.error("Failed to delete product with id {}.", productId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all accessible catalogs and their product item details.
	 * <p>
	 * Requests TrivoreID to get all accessible catalogs and their product item
	 * details.
	 * </p>
	 *
	 * @return all accessible catalogs and their product item details
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AllCatalogs getAllCatalogsAndItems() throws IOException, TrivoreIDException {
		return getAllCatalogsAndItems(null, null, null, null, null);
	}

	/**
	 * Get all accessible catalogs and their product item details.
	 * <p>
	 * Requests TrivoreID to get all accessible catalogs and their product item
	 * details.
	 * </p>
	 *
	 * @param locale          locale code for names. Accept-Language header can also
	 *                        be used.
	 * @param currency        currency code. Only prices in this currency are used.
	 *                        If not specified, default or configured currency is
	 *                        used.
	 * @param paymentMethod   payment method for possible discount effect.
	 * @param customerSegment customer segment IDs used in price discounts
	 * @param atTime          dateTime for which prices are provided. Leave empty to
	 *                        use current time. Example: '2007-12-03T10:15:30.00Z'
	 * @return all accessible catalogs and their product item details
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public AllCatalogs getAllCatalogsAndItems(String locale, String currency, String paymentMethod,
			String[] customerSegment, String atTime) throws IOException, TrivoreIDException {

		Response<AllCatalogs> response = service
				.getAllCatalogsAndItems(locale, currency, paymentMethod, customerSegment, atTime).execute();

		if (response.isSuccessful()) {
			AllCatalogs catalogs = response.body();
			log.info("Successfully found {} catalogs and items.", catalogs.getCatalogs().size());
			return catalogs;
		} else {
			log.error("Failed to get all accessible catalogs and items.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get catalogs and its product item details.
	 * <p>
	 * Requests TrivoreID to get catalogs and its product item details.
	 * </p>
	 *
	 * @param catalogId target catalog unique identifier
	 * @return catalog details.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public CatalogDetails getCatalogDetails(String catalogId) throws IOException, TrivoreIDException {
		return getCatalogDetails(catalogId, null, null, null, null, null, null, null);
	}

	/**
	 * Get catalogs and its product item details.
	 * <p>
	 * Requests TrivoreID to get catalogs and its product item details.
	 * </p>
	 *
	 * @param catalogId       target catalog unique identifier
	 * @param locale          locale code for names. Accept-Language header can also
	 *                        be used.
	 * @param currency        currency code. Only prices in this currency are used.
	 *                        If not specified, default or configured currency is
	 *                        used.
	 * @param paymentMethod   payment method for possible discount effect.
	 * @param customerSegment customer segment IDs used in price discounts
	 * @param code            Price discount codes applied.
	 * @param volume          Purchase volume (how many items), for volume discount.
	 * @param atTime          dateTime for which prices are provided. Leave empty to
	 *                        use current time. Example: '2007-12-03T10:15:30.00Z'
	 * @return catalog details.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public CatalogDetails getCatalogDetails(String catalogId, String locale, String currency, String[] customerSegment,
			String[] code, String paymentMethod, Integer volume, String atTime) throws IOException, TrivoreIDException {
		Assert.notNull(catalogId, "The catalogId cannot be null");
		Assert.hasText(catalogId, "The catalogId cannot be empty");

		Response<CatalogDetails> response = service
				.getCatalogDetails(catalogId, locale, currency, customerSegment, code, paymentMethod, volume, atTime)
				.execute();

		if (response.isSuccessful()) {
			log.info("Successfully got details for the catalog {}.", catalogId);
			return response.body();
		} else {
			log.error("Failed to get details for the catalog {}.", catalogId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get catalog's product item details.
	 * <p>
	 * Requests TrivoreID to get catalog's product item details.
	 * </p>
	 *
	 * @param catalogId target catalog unique identifier
	 * @param itemId    Item's product ID, SKU, or own ID
	 * @return catalog's product item details.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public ProductDetails getProductDetails(String catalogId, String itemId) throws IOException, TrivoreIDException {
		return getProductDetails(catalogId, itemId, null, null, null, null, null, null, null);
	}

	/**
	 * Get catalog's product item details.
	 * <p>
	 * Requests TrivoreID to get catalog's product item details.
	 * </p>
	 *
	 * @param catalogId       target catalog unique identifier
	 * @param itemId          Item's product ID, SKU, or own ID
	 * @param locale          locale code for names. Accept-Language header can also
	 *                        be used.
	 * @param currency        currency code. Only prices in this currency are used.
	 *                        If not specified, default or configured currency is
	 *                        used.
	 * @param paymentMethod   payment method for possible discount effect.
	 * @param customerSegment customer segment IDs used in price discounts
	 * @param code            Price discount codes applied.
	 * @param volume          Purchase volume (how many items), for volume discount.
	 * @param atTime          dateTime for which prices are provided. Leave empty to
	 *                        use current time. Example: '2007-12-03T10:15:30.00Z'
	 * @return catalog's product item details.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public ProductDetails getProductDetails(String catalogId, String itemId, String locale, String currency,
			String[] customerSegment, String[] code, String paymentMethod, Integer volume, String atTime)
			throws IOException, TrivoreIDException {
		Assert.notNull(catalogId, "The catalogId cannot be null");
		Assert.hasText(catalogId, "The catalogId cannot be empty");
		Assert.notNull(itemId, "The itemId cannot be null");
		Assert.hasText(itemId, "The itemId cannot be empty");

		Response<ProductDetails> response = service.getProductDetails(catalogId, itemId, locale, currency,
				customerSegment, code, paymentMethod, volume, atTime).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got details for the catalog {} and product {}.", catalogId, itemId);
			return response.body();
		} else {
			log.error("Failed to get details for the catalog {} and product {}.", catalogId, itemId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
