package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.AccessControl;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.service.AccessControlService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * An access control service to process different kinds of access control
 * specific operations.
 * <p>
 * Handles access control specific tasks and communicates with the Trivore ID
 * service.
 * </p>
 */
public class AccessControlServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private AccessControlService service;

	/**
	 * Construct Access Control Service helper.
	 *
	 * @param service Access Control service
	 */
	public AccessControlServiceImpl(AccessControlService service) {
		this.service = service;
	}

	/**
	 * Query access control objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with access control objects
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AccessControl> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria(), null, null);
	}

	/**
	 * Query access control objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with access control objects
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AccessControl> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		return getAll(criteria, null, null);
	}

	/**
	 * Query access control objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    Sort by attribute name
	 * @param ascending Sort direction ('ascending' or 'descending')
	 * @return page with access control objects
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<AccessControl> getAll(Criteria criteria, String sortBy, Boolean ascending)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null) {
			sortOrder = ascending ? "ascending" : "descending";
		}

		Response<Page<AccessControl>> response = service
				.getAll(criteria.getStartIndex(), criteria.getCount(), filter, sortBy, sortOrder).execute();
		if (response.isSuccessful()) {
			Page<AccessControl> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} accessControls with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find accessControls with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new accessControl into the service.
	 * <p>
	 * If the accessControl code is defined, then the list of groups (memberOf) will
	 * be checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param accessControl The accessControl to be added.
	 * @return An access control saved in Trivore ID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 */
	public AccessControl create(AccessControl accessControl) throws IOException, TrivoreIDException {
		Assert.notNull(accessControl, "The accessControl cannot be null");

		Response<AccessControl> response = service.create(accessControl).execute();

		if (response.isSuccessful()) {
			AccessControl newAccessControl = response.body();
			log.info("Successfully created accessControl with id {}", newAccessControl.getId());
			return newAccessControl;
		} else {
			log.error("Failed to create a accessControl.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the access control with the given identifier.
	 *
	 * @param accessControlId The id of the target access control.
	 * @return The access control with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 */
	public AccessControl get(String accessControlId) throws IOException, TrivoreIDException {
		Assert.notNull(accessControlId, "The id cannot be null");
		Assert.hasText(accessControlId, "The id cannot be empty");

		Response<AccessControl> response = service.get(accessControlId).execute();

		if (response.isSuccessful()) {
			AccessControl accessControl = response.body();
			log.info("Successfully got the accessControl with id {}", accessControlId);
			return accessControl;
		} else {
			log.error("Failed to get the accessControl with id {}", accessControlId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided access control.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided access control.
	 * </p>
	 *
	 * @param accessControl The target accessControl.
	 * @return An access control saved in Trivore ID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 */
	public AccessControl update(AccessControl accessControl) throws IOException, TrivoreIDException {
		Assert.notNull(accessControl, "The accessControl cannot be null");
		Assert.notNull(accessControl.getId(), "The ID cannot be null");
		Assert.hasText(accessControl.getId(), "The ID cannot be empty");

		Response<AccessControl> response = service.update(accessControl.getId(), accessControl).execute();

		if (response.isSuccessful()) {
			AccessControl updatedAccessControl = response.body();
			log.info("Successfully modified the accessControl with ID {}.", updatedAccessControl.getId());
			return updatedAccessControl;
		} else {
			log.error("Failed to update the accessControl with id {}.", accessControl.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete an access control from Trivore ID.
	 *
	 * @param accessControlId The id of the target access control.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or Trivore ID error.
	 */
	public void delete(String accessControlId) throws IOException, TrivoreIDException {
		Assert.notNull(accessControlId, "The accessControlId cannot be null");
		Assert.hasText(accessControlId, "The accessControlId cannot be empty");

		Response<Void> response = service.delete(accessControlId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted accessControl with id {}.", accessControlId);
		} else {
			log.error("Failed to delete accessControl with id {}.", accessControlId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
