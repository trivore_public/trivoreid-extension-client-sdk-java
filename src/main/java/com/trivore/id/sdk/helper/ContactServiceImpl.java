package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Contact;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.user.Enterprise;
import com.trivore.id.sdk.service.ContactService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A contact service to process different kinds of contact specific operations.
 * <p>
 * Handles contact specific tasks and communicates with the TrivoreID service.
 * </p>
 */
public class ContactServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final ContactService service;

	/**
	 * Construct Contact Service helper.
	 *
	 * @param service contact service
	 */
	public ContactServiceImpl(ContactService service) {
		this.service = service;
	}

	/**
	 * Get all contacts satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with contact objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Contact> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all contacts satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with contact objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Contact> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Contact>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Contact> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} contacts with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find contacts with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new contact into the service.
	 *
	 * @param contact The contact to be added.
	 * @return A contact saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Contact create(Contact contact) throws IOException, TrivoreIDException {
		Assert.notNull(contact, "The contact cannot be null");

		Response<Contact> response = service.create(contact).execute();

		if (response.isSuccessful()) {
			Contact newContact = response.body();
			log.info("Successfully created contact with id {}", newContact.getId());
			return newContact;
		} else {
			log.error("Failed to create a contact.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the contact with the given identifier.
	 *
	 * @param contactId The id of the target contact.
	 * @return The contact with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Contact get(String contactId) throws IOException, TrivoreIDException {
		Assert.notNull(contactId, "The id cannot be null");
		Assert.hasText(contactId, "The id cannot be empty");

		Response<Contact> response = service.get(contactId).execute();

		if (response.isSuccessful()) {
			Contact contact = response.body();
			log.info("Successfully got the contact with id {}", contactId);
			return contact;
		} else {
			log.error("Failed to get the contact with id {}", contactId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided contact.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided contact.
	 * </p>
	 *
	 * @param contact   The target contact.
	 * @return A contact saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Contact update(Contact contact) throws IOException, TrivoreIDException {
		Assert.notNull(contact, "The contact cannot be null");
		Assert.notNull(contact.getId(), "The ID cannot be null");
		Assert.hasText(contact.getId(), "The ID cannot be empty");

		Response<Contact> response = service.update(contact.getId(), contact).execute();

		if (response.isSuccessful()) {
			Contact updatedContact = response.body();
			log.info("Successfully modified the contact with ID {}.", updatedContact.getId());
			return updatedContact;
		} else {
			log.error("Failed to get the contact profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a contact from TrivoreID.
	 *
	 * @param contactId The id of the target contact.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String contactId) throws IOException, TrivoreIDException {
		Assert.notNull(contactId, "The contactId cannot be null");
		Assert.hasText(contactId, "The contactId cannot be empty");

		Response<Void> response = service.delete(contactId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted contact with id {}.", contactId);
		} else {
			log.error("Failed to delete contact with id {}.", contactId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the contact enterprise with the given identifier.
	 *
	 * @param contactId The id of the target contact.
	 * @return The contact enterprise with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Enterprise getEnterprise(String contactId) throws IOException, TrivoreIDException {
		Assert.notNull(contactId, "The id cannot be null");
		Assert.hasText(contactId, "The id cannot be empty");

		Response<Enterprise> response = service.getEnterprise(contactId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the contact enterprise with id {}", contactId);
			return response.body();
		} else {
			log.error("Failed to get the contact enterprise with id {}", contactId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided contact.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided contact enterprise.
	 * </p>
	 *
	 * @param contactId  the ID of the target contact
	 * @param enterprise The target enterprise.
	 * @return A contact enterprise saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Enterprise updateEnterprise(String contactId, Enterprise enterprise) throws IOException, TrivoreIDException {
		Assert.notNull(enterprise, "The contact cannot be null");
		Assert.notNull(contactId, "The ID cannot be null");
		Assert.hasText(contactId, "The ID cannot be empty");

		Response<Enterprise> response = service.updateEnterprise(contactId, enterprise).execute();

		if (response.isSuccessful()) {
			log.info("Successfully modified the contact enterprise with ID {}.", contactId);
			return response.body();
		} else {
			log.error("Failed to get the contact enterprise.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
