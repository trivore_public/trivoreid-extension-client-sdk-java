package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.paycard.PanToken;
import com.trivore.id.sdk.models.paycard.Paycard;
import com.trivore.id.sdk.service.PaycardService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A paycard service for all kinds of paycard management operations.
 * <p>
 * Handles all kinds of operations with TrivoreID paycard objects.
 * </p>
 */
public class PaycardServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaycardService service;

	/**
	 * Construct Paycard Service helper.
	 *
	 * @param service paycard service
	 */
	public PaycardServiceImpl(PaycardService service) {
		this.service = service;
	}

	/**
	 * Get all paycards satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param userId   The id of the target user.
	 * @return page with paycard objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Paycard> getAll(String userId) throws TrivoreIDException, IOException {
		return getAll(userId, new Criteria());
	}

	/**
	 * Get all paycards satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param userId   The id of the target user.
	 * @param criteria the query criteria for the operation.
	 * @return page with paycard objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Paycard> getAll(String userId, Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Paycard>> response = service.getAll(userId, criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Paycard> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} paycards with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find paycards with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new paycard into the service.
	 *
	 * @param userId  The id of the target user.
	 * @param paycard The paycard to be added.
	 * @return A paycard saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Paycard create(String userId, Paycard paycard) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycard, "The paycard cannot be null");

		Response<Paycard> response = service.create(userId, paycard).execute();

		if (response.isSuccessful()) {
			Paycard newPaycard = response.body();
			log.info("Successfully created paycard with id {}", newPaycard.getId());
			return newPaycard;
		} else {
			log.error("Failed to create a paycard.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the paycard with the given identifier.
	 *
	 * @param userId    The id of the target user.
	 * @param paycardId The id of the target paycard.
	 * @return The paycard with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Paycard get(String userId, String paycardId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The id cannot be null");
		Assert.hasText(paycardId, "The id cannot be empty");

		Response<Paycard> response = service.get(userId, paycardId).execute();

		if (response.isSuccessful()) {
			Paycard paycard = response.body();
			log.info("Successfully got the paycard with id {}", paycardId);
			return paycard;
		} else {
			log.error("Failed to get the paycard with id {}", paycardId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided paycard.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided paycard.
	 * </p>
	 *
	 * @param userId    The id of the target user.
	 * @param paycard   The target paycard.
	 * @return A paycard saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Paycard update(String userId, Paycard paycard) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycard, "The paycard cannot be null");
		Assert.notNull(paycard.getId(), "The ID cannot be null");
		Assert.hasText(paycard.getId(), "The ID cannot be empty");

		Response<Paycard> response = service.update(userId, paycard.getId(), paycard).execute();

		if (response.isSuccessful()) {
			Paycard updatedPaycard = response.body();
			log.info("Successfully modified the paycard with ID {}.", updatedPaycard.getId());
			return updatedPaycard;
		} else {
			log.error("Failed to get the paycard profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a paycard from TrivoreID.
	 *
	 * @param userId    The id of the target user.
	 * @param paycardId The id of the target paycard.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String userId, String paycardId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The paycardId cannot be null");
		Assert.hasText(paycardId, "The paycardId cannot be empty");

		Response<Void> response = service.delete(userId, paycardId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted paycard with id {}.", paycardId);
		} else {
			log.error("Failed to delete paycard with id {}.", paycardId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all paycard's pantokens.
	 *
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @return A set of paycard items from the TrivoreID.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<PanToken> getAllPantokens(String userId, String paycardId) throws TrivoreIDException, IOException {
		return getAllPantokens(userId, paycardId, new Criteria());
	}

	/**
	 * Get pantokens by criteria.
	 *
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @param criteria  criteria
	 * @return A set of paycard items from the TrivoreID.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<PanToken> getAllPantokens(String userId, String paycardId, Criteria criteria)
			throws TrivoreIDException, IOException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The paycardId cannot be null");
		Assert.hasText(paycardId, "The paycardId cannot be empty");
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<PanToken>> response = service
				.getAllPantokens(userId, paycardId, criteria.getStartIndex(), criteria.getCount(), filter).execute();
		if (response.isSuccessful()) {
			Page<PanToken> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} pantokens with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed find paycards with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Create a new pantoken in the TrivoreID service.
	 *
	 * @param pantoken  The panToken to be added into TrivoreID.
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @return The paycard that was added (id is auto-assigned by TrivoreID).
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PanToken createPantoken(String userId, String paycardId, PanToken pantoken)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The paycardId cannot be null");
		Assert.hasText(paycardId, "The paycardId cannot be empty");
		Assert.notNull(pantoken, "The pantoken cannot be null");

		Response<PanToken> response = service.createPantoken(userId, paycardId, pantoken).execute();

		if (response.isSuccessful()) {
			PanToken newPanToken = response.body();
			log.info("Successfully created pantoken with id {}", newPanToken.getId());
			return newPanToken;
		} else {
			log.error("Failed to create a pantoken.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get a single paycard's pantoken by its ID.
	 *
	 * @param userId    user unique ID
	 * @param paycardId paycard unique identifier.
	 * @param tokenId   pantoken unique identifier.
	 * @return PanToken object
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PanToken getPantoken(String userId, String paycardId, String tokenId)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The paycardId cannot be null");
		Assert.hasText(paycardId, "The paycardId cannot be empty");
		Assert.notNull(tokenId, "The tokenId cannot be null");
		Assert.hasText(tokenId, "The tokenId cannot be empty");

		Response<PanToken> response = service.getPantoken(userId, paycardId, tokenId).execute();

		if (response.isSuccessful()) {
			PanToken token = response.body();
			log.info("Successfully got the pantoken with id {}", tokenId);
			return token;
		} else {
			log.error("Failed to get the pantoken with id {}", paycardId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update an existing paycard in TrivoreID service.
	 * <p>
	 * The provided panToken must contain id.
	 * </p>
	 *
	 * @param pantoken  The panToken to be upodated in TrivoreID.
	 * @param userId    user unique ID
	 * @param paycardId paycard unique ID
	 * @return The same paycard returned from the TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public PanToken updatePantoken(String userId, String paycardId, PanToken pantoken)
			throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The paycardId cannot be null");
		Assert.hasText(paycardId, "The paycardId cannot be empty");
		Assert.notNull(pantoken, "The paycard cannot be null");
		Assert.notNull(pantoken.getId(), "The ID cannot be null");
		Assert.hasText(pantoken.getId(), "The ID cannot be empty");

		Response<PanToken> response = service.updatePantoken(userId, paycardId, pantoken.getId(), pantoken).execute();

		if (response.isSuccessful()) {
			PanToken updatedPanToken = response.body();
			log.info("Successfully modified the pantoken with ID {}.", updatedPanToken.getId());
			return updatedPanToken;
		} else {
			log.error("Failed to get the pantoken profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a panToken from TrivoreID.
	 *
	 * @param userId    card unique ID
	 * @param paycardId paycard unique identifier.
	 * @param tokenId   pantoken unique identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deletePantoken(String userId, String paycardId, String tokenId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");
		Assert.notNull(paycardId, "The paycardId cannot be null");
		Assert.hasText(paycardId, "The paycardId cannot be empty");
		Assert.notNull(tokenId, "The tokenId cannot be null");
		Assert.hasText(tokenId, "The tokenId cannot be empty");

		Response<Void> response = service.deletePantoken(userId, paycardId, tokenId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted paycard with id {}.", paycardId);
		} else {
			log.error("Failed to delete paycard with id {}.", paycardId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
