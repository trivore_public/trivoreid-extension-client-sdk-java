package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.Period;
import com.trivore.id.sdk.models.Subscription;
import com.trivore.id.sdk.service.SubscriptionService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A subscription service to process different kinds of subscription specific
 * operations.
 * <p>
 * Handles user specific tasks and communicates with the TrivoreID service.
 * </p>
 */
public class SubscriptionServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private SubscriptionService service;

	/**
	 * Construct Subscription Service helper.
	 *
	 * @param service subscription service
	 */
	public SubscriptionServiceImpl(SubscriptionService service) {
		this.service = service;
	}

	/**
	 * Get all subscriptions from accessible namespaces.
	 *
	 * @return A set of subscriptions from the TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Page<Subscription> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all subscriptions satisfying the provided {@link Criteria}.
	 *
	 * @param criteria The query criteria for the operation.
	 * @return A set of subscriptions from the TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Page<Subscription> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Subscription>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();

		if (response.isSuccessful()) {
			Page<Subscription> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} subscriptions with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find subscriptions with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Create the provided subscription into TrivoreID service.
	 *
	 * @param subscription The subscription to add into TrivoreID.
	 * @return The subscription that was added (id is auto-assigned by TrivoreID).
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Subscription create(Subscription subscription) throws TrivoreIDException, IOException {
		Assert.notNull(subscription, "The subscription cannot be null");

		Response<Subscription> response = service.create(subscription).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created subscription.");
			return response.body();
		} else {
			log.error("Failed to create subscription.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update the provided subscription in TrivoreID service.
	 * <p>
	 * The subscription's id should be present in the subscription object.
	 * </p>
	 *
	 * @param subscription The subscription to add into TrivoreID.
	 * @return The subscription that was added (id is auto-assigned by TrivoreID).
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Subscription update(Subscription subscription) throws TrivoreIDException, IOException {
		Assert.notNull(subscription, "The subscription cannot be null");
		Assert.notNull(subscription.getId(), "The subscription.id cannot be null");
		Assert.hasText(subscription.getId(), "The subscription.id cannot be empty");

		Response<Subscription> response = service.update(subscription.getId(), subscription).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated subscription with id {}.", subscription.getId());
			return response.body();
		} else {
			log.error("Failed to update subscription with id {}.", subscription.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the subscription with the given identifier.
	 *
	 * @param subscriptionId The target identifier.
	 * @return The subscription with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Subscription get(String subscriptionId) throws TrivoreIDException, IOException {
		Assert.notNull(subscriptionId, "The subscriptionId cannot be null");
		Assert.hasText(subscriptionId, "The subscriptionId cannot be empty");

		Response<Subscription> response = service.get(subscriptionId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got subscription with id {}.", subscriptionId);
			return response.body();
		} else {
			log.error("Failed to get the subscription with id {}.", subscriptionId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Terminate the subsctiption.
	 *
	 * @param subscriptionId The target identifier.
	 * @return terminated Subscription object
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Subscription terminate(String subscriptionId) throws TrivoreIDException, IOException {
		Assert.notNull(subscriptionId, "The subscriptionId cannot be null");
		Assert.hasText(subscriptionId, "The subscriptionId cannot be empty");

		Response<Subscription> response = service.terminate(subscriptionId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully terminated subscription with id {}.", subscriptionId);
			return response.body();
		} else {
			log.error("Failed to terminate the subscription with id {}.", subscriptionId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add period to the subscription in TrivoreID service.
	 *
	 * @param subscriptionId The target identifier.
	 * @param period         The period to add into TrivoreID.
	 * @return The period that was added (id is auto-assigned by TrivoreID).
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Period createPeriod(String subscriptionId, Period period) throws TrivoreIDException, IOException {
		Assert.notNull(period, "The subscription cannot be null");
		Assert.notNull(subscriptionId, "The subscriptionId cannot be null");
		Assert.hasText(subscriptionId, "The subscriptionId cannot be empty");

		Response<Period> response = service.createPeriod(subscriptionId, period).execute();

		if (response.isSuccessful()) {
			log.info("Successfully created period for subscription with id {}.", subscriptionId);
			return response.body();
		} else {
			log.error("Failed to terminate the subscription with id {}.", subscriptionId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the subscription's period with the given identifier.
	 *
	 * @param subscriptionId The target subscription identifier.
	 * @param periodId       The target period identifier.
	 * @return The period with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Period getPeriod(String subscriptionId, String periodId) throws TrivoreIDException, IOException {
		Assert.notNull(subscriptionId, "The subscriptionId cannot be null");
		Assert.hasText(subscriptionId, "The subscriptionId cannot be empty");
		Assert.notNull(periodId, "The periodId cannot be null");
		Assert.hasText(periodId, "The periodId cannot be empty");

		Response<Period> response = service.getPeriod(subscriptionId, periodId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got period with id {} for subscription {}.", periodId, subscriptionId);
			return response.body();
		} else {
			log.error("Failed to get period with id {} for subscription id {}.", periodId, subscriptionId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update the provided period in TrivoreID service.
	 * <p>
	 * The period id should be present in the subscription object.
	 * </p>
	 *
	 * @param subscriptionId The target subscription identifier.
	 * @param period         The period to modify.
	 * @return The period that was updated.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException On a network or TrivoreID error.
	 */
	public Period updatePeriod(String subscriptionId, Period period) throws TrivoreIDException, IOException {
		Assert.notNull(subscriptionId, "The subscriptionId cannot be null");
		Assert.hasText(subscriptionId, "The subscriptionId cannot be empty");
		Assert.notNull(period, "The subscription cannot be null");
		Assert.notNull(period.getId(), "The subscription.id cannot be null");
		Assert.hasText(period.getId(), "The subscription.id cannot be empty");

		Response<Period> response = service.updatePeriod(subscriptionId, period.getId(), period).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got period with id {} for subscription {}.", period.getId(), subscriptionId);
			return response.body();
		} else {
			log.error("Failed to get period with id {} for subscription id {}.", period.getId(), subscriptionId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
