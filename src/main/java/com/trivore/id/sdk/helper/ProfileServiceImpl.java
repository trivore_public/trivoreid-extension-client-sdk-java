package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.user.profile.Profile;
import com.trivore.id.sdk.service.ProfileService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A profile service to process different kinds of profile specific operations.
 * <p>
 * Handles profile specific tasks and communicates with the TrivoreID service.
 * </p>
 */
public class ProfileServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private ProfileService service;

	/**
	 * Construct Profile Service helper.
	 *
	 * @param service profile service
	 */
	public ProfileServiceImpl(ProfileService service) {
		this.service = service;
	}

	/**
	 * Get the user profile with the given identifier.
	 *
	 * @param userId The id of the target user.
	 * @return The user with the given identifier.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Profile get(String userId) throws TrivoreIDException, IOException {
		Assert.notNull(userId, "The id cannot be null");
		Assert.hasText(userId, "The id cannot be empty");

		Response<Profile> response = service.get(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got the user profile for id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get the user profile for id {}", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Update user profile.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) user profile.
	 * </p>
	 *
	 * @param userId  The id of the target user.
	 * @param profile The user profile.
	 * @return A user saved in TrivoreID.
	 * @throws TrivoreIDException In a case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Profile update(String userId, Profile profile) throws TrivoreIDException, IOException {
		Assert.notNull(profile, "The profile cannot be null");
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Profile> response = service.update(userId, profile).execute();

		if (response.isSuccessful()) {
			log.info("Successfully modified the profile for the user with ID {}.", userId);
			return response.body();
		} else {
			log.error("Failed to get the profile for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
