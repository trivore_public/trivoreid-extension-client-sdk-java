package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.LocationSite;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.service.LocationSiteService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * An location/site service to process different kinds of location/site specific
 * operations.
 * <p>
 * Handles location/site specific tasks and communicates with the TrivoreID
 * service.
 * </p>
 */
public class LocationSiteServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final LocationSiteService service;

	/**
	 * Construct LocationSite Service helper.
	 *
	 * @param service LocationSite service
	 */
	public LocationSiteServiceImpl(LocationSiteService service) {
		this.service = service;
	}

	/**
	 * Query location/site objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with location/site objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<LocationSite> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria(), null, null, null);
	}

	/**
	 * Query location/site objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @return page with location/site objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<LocationSite> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		return getAll(criteria, null, null, null);
	}

	/**
	 * Query location/site objects.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria  the query criteria for the operation.
	 * @param sortBy    Sort by attribute name
	 * @param ascending Sort direction ('ascending' or 'descending')
	 * @param flat      Set to true to fetch a flattened hierarchy of the
	 *                  location/sites.
	 * @return page with location/site objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<LocationSite> getAll(Criteria criteria, String sortBy, Boolean ascending, Boolean flat)
			throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		String sortOrder = null;
		if (ascending != null) {
			sortOrder = ascending ? "ascending" : "descending";
		}

		Response<Page<LocationSite>> response = service
				.getAll(criteria.getStartIndex(), criteria.getCount(), filter, sortBy, sortOrder, flat).execute();
		if (response.isSuccessful()) {
			Page<LocationSite> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} locationsites with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find locationsites with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new locationsite into the service.
	 * <p>
	 * If the locationsite code is defined, then the list of groups (memberOf) will
	 * be checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param locationsite The locationsite to be added.
	 * @return An location/site saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public LocationSite create(LocationSite locationsite) throws IOException, TrivoreIDException {
		Assert.notNull(locationsite, "The locationsite cannot be null");

		Response<LocationSite> response = service.create(locationsite).execute();

		if (response.isSuccessful()) {
			LocationSite newLocationSite = response.body();
			log.info("Successfully created locationsite with id {}", newLocationSite.getId());
			return newLocationSite;
		} else {
			log.error("Failed to create a locationsite.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the location/site with the given identifier.
	 *
	 * @param locationsiteId The id of the target location/site.
	 * @return The location/site with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public LocationSite get(String locationsiteId) throws IOException, TrivoreIDException {
		Assert.notNull(locationsiteId, "The id cannot be null");
		Assert.hasText(locationsiteId, "The id cannot be empty");

		Response<LocationSite> response = service.get(locationsiteId).execute();

		if (response.isSuccessful()) {
			LocationSite locationsite = response.body();
			log.info("Successfully got the locationsite with id {}", locationsiteId);
			return locationsite;
		} else {
			log.error("Failed to get the locationsite with id {}", locationsiteId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided location/site.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided location/site.
	 * </p>
	 *
	 * @param locationsite   The target locationsite.
	 * @return An location/site saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public LocationSite update(LocationSite locationsite) throws IOException, TrivoreIDException {
		Assert.notNull(locationsite, "The locationsite cannot be null");
		Assert.notNull(locationsite.getId(), "The ID cannot be null");
		Assert.hasText(locationsite.getId(), "The ID cannot be empty");

		Response<LocationSite> response = service.update(locationsite.getId(), locationsite).execute();

		if (response.isSuccessful()) {
			LocationSite updatedLocationSite = response.body();
			log.info("Successfully modified the locationsite with ID {}.", updatedLocationSite.getId());
			return updatedLocationSite;
		} else {
			log.error("Failed to update the locationsite with id {}.", locationsite.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete an location/site from TrivoreID.
	 *
	 * @param locationsiteId The id of the target location/site.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String locationsiteId) throws IOException, TrivoreIDException {
		Assert.notNull(locationsiteId, "The locationsiteId cannot be null");
		Assert.hasText(locationsiteId, "The locationsiteId cannot be empty");

		Response<Void> response = service.delete(locationsiteId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted locationsite with id {}.", locationsiteId);
		} else {
			log.error("Failed to delete locationsite with id {}.", locationsiteId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}
}
