package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.DataStorage;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.service.DataStorageService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;
import com.trivore.id.sdk.utils.Filter;

import retrofit2.Response;

/**
 * A storage service for all kinds of data management operations.
 * <p>
 * Handles data CRUD operations with generic data operations with the TrivoreID.
 * </p>
 */
public class DataStorageServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final DataStorageService service;

	/**
	 * Construct DataStorage Service helper.
	 *
	 * @param service storage service
	 */
	public DataStorageServiceImpl(DataStorageService service) {
		this.service = service;
	}

	/**
	 * Get all storages satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with storage objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<DataStorage> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria(), "");
	}

	/**
	 * Get all storages satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria   the query criteria for the operation.
	 * @param dataFilter filter for quering data.
	 * @return page with storage objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<DataStorage> getAll(Criteria criteria, Filter dataFilter) throws TrivoreIDException, IOException {
		Assert.notNull(dataFilter, "The dataFilter cannot be null");
		return getAll(criteria, dataFilter.toFilterString());
	}

	/**
	 * Get all storages satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria   the query criteria for the operation.
	 * @param dataFilter filter for quering data.
	 * @return page with storage objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<DataStorage> getAll(Criteria criteria, String dataFilter) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<DataStorage>> response = service
				.getAll(criteria.getStartIndex(), criteria.getCount(), filter, dataFilter).execute();
		if (response.isSuccessful()) {
			Page<DataStorage> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} storages with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find storages with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new storage into the service.
	 * <p>
	 * If the storage code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param storage The storage to be added.
	 * @return A storage saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public DataStorage create(DataStorage storage) throws IOException, TrivoreIDException {
		Assert.notNull(storage, "The storage cannot be null");

		Response<DataStorage> response = service.create(storage).execute();

		if (response.isSuccessful()) {
			DataStorage newDataStorage = response.body();
			log.info("Successfully created storage with id {}", newDataStorage.getId());
			return newDataStorage;
		} else {
			log.error("Failed to create a storage.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the storage with the given identifier.
	 *
	 * @param storageId The id of the target storage.
	 * @return The storage with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public DataStorage get(String storageId) throws IOException, TrivoreIDException {
		Assert.notNull(storageId, "The id cannot be null");
		Assert.hasText(storageId, "The id cannot be empty");

		Response<DataStorage> response = service.get(storageId).execute();

		if (response.isSuccessful()) {
			DataStorage storage = response.body();
			log.info("Successfully got the storage with id {}", storageId);
			return storage;
		} else {
			log.error("Failed to get the storage with id {}", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided storage.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided storage.
	 * </p>
	 *
	 * @param storage   The target storage.
	 * @return A storage saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public DataStorage update(DataStorage storage) throws IOException, TrivoreIDException {
		Assert.notNull(storage, "The storage cannot be null");
		Assert.notNull(storage.getId(), "The ID cannot be null");
		Assert.hasText(storage.getId(), "The ID cannot be empty");

		Response<DataStorage> response = service.update(storage.getId(), storage).execute();

		if (response.isSuccessful()) {
			DataStorage updatedDataStorage = response.body();
			log.info("Successfully modified the storage with ID {}.", updatedDataStorage.getId());
			return updatedDataStorage;
		} else {
			log.error("Failed to get the storage profile.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a storage from TrivoreID.
	 *
	 * @param storageId The id of the target storage.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String storageId) throws IOException, TrivoreIDException {
		Assert.notNull(storageId, "The storageId cannot be null");
		Assert.hasText(storageId, "The storageId cannot be empty");

		Response<Void> response = service.delete(storageId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted storage with id {}.", storageId);
		} else {
			log.error("Failed to delete storage with id {}.", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the storage data with the given identifier.
	 *
	 * @param storageId The id of the target storage.
	 * @return The storage data with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Map<String, Object> getData(String storageId) throws IOException, TrivoreIDException {
		Assert.notNull(storageId, "The id cannot be null");
		Assert.hasText(storageId, "The id cannot be empty");

		Response<Map<String, Object>> response = service.getData(storageId).execute();

		if (response.isSuccessful()) {
			Map<String, Object> storage = response.body();
			log.info("Successfully got the storage data with id {}", storageId);
			return storage;
		} else {
			log.error("Failed to get the storage data with id {}", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Replace all contained data in the datastorage.
	 *
	 * @param data      data to update
	 * @param storageId The id of the target storage.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void updateData(String storageId, Map<String, Object> data) throws IOException, TrivoreIDException {
		Assert.notNull(data, "The storage cannot be null");
		Assert.notNull(storageId, "The ID cannot be null");
		Assert.hasText(storageId, "The ID cannot be empty");

		Response<Void> response = service.updateData(storageId, data).execute();

		if (response.isSuccessful()) {
			log.info("Successfully modified the data in the storage with ID {}.", storageId);
		} else {
			log.error("Failed to get the data from the storage with id {}.", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get value from the datastorage.
	 * <p>
	 * Returns Map opject with one value entry.
	 * </p>
	 *
	 * @param key       key to get the value.
	 * @param storageId The id of the target storage.
	 * @return value from the datastorage
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Map<String, Object> getValue(String storageId, String key) throws IOException, TrivoreIDException {
		Assert.notNull(storageId, "The id cannot be null");
		Assert.hasText(storageId, "The id cannot be empty");
		Assert.notNull(key, "The key cannot be null");
		Assert.hasText(key, "The key cannot be empty");

		Response<Map<String, Object>> response = service.getValue(storageId, key).execute();

		if (response.isSuccessful()) {
			Map<String, Object> value = response.body();
			log.info("Successfully got the storage data value with id {}", storageId);
			return value;
		} else {
			log.error("Failed to get the storage data value with id {}", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Replace value in the datastorage by its key.
	 *
	 * @param key       key to get the value.
	 * @param storageId The id of the target storage.
	 * @param value     value to update.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void updateValue(String storageId, String key, Object value) throws IOException, TrivoreIDException {
		Assert.notNull(storageId, "The id cannot be null");
		Assert.hasText(storageId, "The id cannot be empty");
		Assert.notNull(key, "The key cannot be null");
		Assert.hasText(key, "The key cannot be empty");

		Map<String, Object> map = new HashMap<>();
		map.put("value", value);
		Response<Void> response = service.updateValue(storageId, key, map).execute();

		if (response.isSuccessful()) {
			log.info("Successfully updated the storage data value with id {}", storageId);
		} else {
			log.error("Failed to update the storage data value with id {}", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete value in the datastorage by its key.
	 *
	 * @param key       key to get the value.
	 * @param storageId The id of the target storage.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void deleteValue(String storageId, String key) throws IOException, TrivoreIDException {
		Assert.notNull(storageId, "The id cannot be null");
		Assert.hasText(storageId, "The id cannot be empty");
		Assert.notNull(key, "The key cannot be null");
		Assert.hasText(key, "The key cannot be empty");

		Response<Void> response = service.deleteValue(storageId, key).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted the storage data value with id {}", storageId);
		} else {
			log.error("Failed to delete the storage data value with id {}", storageId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
