package com.trivore.id.sdk.requests;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A data transfer object (DTO) used to execute invitation request.
 * <p>
 * Used to execute the invitation procedure at Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
public class InviteRequest implements Serializable {

	private Set<String> groups;
	private Set<String> emails;

	private String locale;
	private String nsCode;
	private String bodyText;
	private String subject;

	/**
	 * Construct a new empty invite request.
	 */
	public InviteRequest() {
		// ...
	}

	/**
	 * Get list of emails where the invitation request will be sent.
	 *
	 * @return returns list of emails.
	 */
	public Set<String> getEmails() {
		if (emails == null) {
			emails = new HashSet<>();
		}
		return emails;
	}

	/**
	 * Set list of emails where the invitation request will be sent.
	 *
	 * @param emails list of emails.
	 */
	public void setEmails(Set<String> emails) {
		this.emails = emails;
	}

	/**
	 * Get list of user group IDs for the invitation request.
	 *
	 * @return returns list of user group IDs.
	 */
	public Set<String> getGroups() {
		if (groups == null) {
			groups = new HashSet<>();
		}
		return groups;
	}

	/**
	 * Set list of user group IDs for the invitation request.
	 *
	 * @param groups list of user group IDs.
	 */
	public void setGroups(Set<String> groups) {
		this.groups = groups;
	}

	/**
	 * Get the subject for the invitation email.
	 *
	 * @return the subject for the invitation email.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Set the subject for the invitation email.
	 *
	 * @param subject the subject for the invitation email.
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Get the text of the invitation email.
	 *
	 * @return the text body of the invitation email.
	 */
	public String getBodyText() {
		return bodyText;
	}

	/**
	 * Set the text of the invitation email.
	 *
	 * @param bodyText the text body of the invitation email.
	 */
	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	/**
	 * Get the locale of the user.
	 *
	 * @return returns locale.
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Set the locale for the user.
	 *
	 * @param locale locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * Get the Namespace code of the user.
	 * <p>
	 * Should be used only when creating user, must be among allowed Namespaces.
	 * </p>
	 *
	 * @return returns namespace code of the user.
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set the Namespace code of the user.
	 * <p>
	 * Should be used only when creating user, must be among allowed Namespaces.
	 * </p>
	 *
	 * @param nsCode namespace code of the user
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getEmails(), subject, bodyText, locale, nsCode, this.getGroups());
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof InviteRequest)) {
			return false;
		}
		InviteRequest o = (InviteRequest) object;
		return Objects.equals(subject, o.subject) && Objects.equals(bodyText, o.bodyText)
				&& new HashSet<>(emails).equals(new HashSet<>(o.emails))
				&& new HashSet<>(groups).equals(new HashSet<>(o.groups));
	}

	@Override
	public String toString() {
		return "InviteRequest [groups=" + groups + ", emails=" + emails + ", locale=" + locale + ", nsCode=" + nsCode
				+ ", bodyText=" + bodyText + ", subject=" + subject + "]";
	}

}
